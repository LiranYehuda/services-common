﻿using System;
using System.Linq;

using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

using IDoBet.CoreLibrary.Logging;
using IDoBet.CoreLibrary.Logging.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Request;
using IDoBet.CoreLibrary.Logic.Model.Response;

namespace IDoBet.CoreLibrary.Api
{
    public class LogObjectFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            try
            {
                var descriptor = context.ActionDescriptor;
                var actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ActionName;
                var ControllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ControllerName;

                if (context.Result is Microsoft.AspNetCore.Mvc.ObjectResult res)
                {
                    Logger.WriteLog(eLogLevel.Debug, string.Format("Response (IP : {3}) -> {0} -> {1} -> {2}", ControllerName, actionName, ControllerName == "Event" ? "" : JsonConvert.SerializeObject(res), context.HttpContext.Connection.RemoteIpAddress));
                }                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(eLogLevel.Error, "", ex);
            }
            finally
            {
                base.OnActionExecuted(context);
            }

        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                var req = context.HttpContext.Request.QueryString.ToString();
                if (!string.IsNullOrEmpty(req))
                {
                    var descriptor = context.ActionDescriptor;
                    var actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ActionName;
                    var ControllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ControllerName;
                    Logger.WriteLog(eLogLevel.Debug, string.Format("Request (IP : {3}) -> {0} -> {1} -> {2}", ControllerName, actionName, $"Query Is {req}", context.HttpContext.Connection.RemoteIpAddress));
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(eLogLevel.Error, "", ex);
            }
            finally
            {
                base.OnActionExecuting(context);
            }
        }
    }
}
