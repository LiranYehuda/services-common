﻿using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Configuration;
using IDoBet.CoreLibrary.SqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace IDoBet.CoreLibrary.Api
{
    public static class EnvironmentLoaderOld
    {
        private static InitApiConfiguration InitApiConfiguration { get; set; }
        public static string RootFolder { get; set; }
        public static void Configure(string instance, int? brandId = null)
        {
            Configuration.Instance = instance;
            InitApiConfiguration = JsonConvert.DeserializeObject<InitApiConfiguration>(File.ReadAllText(Path.Combine(RootFolder, "configuration.json")));
            using (var sh = new SqlHandler(InitApiConfiguration.EndPoint, InitApiConfiguration.Value))
            {
                sh.AddParameter("BrandId", SqlDbType.Int, brandId);
                var response = sh.GetDataSet().Item1;

                if (response != null && response.Tables?.Count >= 5)
                {
                    int tableIndex = 0;
                    #region response validation                    
                    if (response.Tables[tableIndex].Rows.Count == 0 ||
                        !response.Tables[tableIndex].Columns.Contains("CodeNumber") ||
                        int.Parse(response.Tables[tableIndex].Rows[0]["CodeNumber"].ToString()) != 0)
                        return;
                    tableIndex++;
                    #endregion

                    #region SetIsAutoMap

                    if (response.Tables[tableIndex].Rows.Count > 0 && response.Tables[tableIndex].Columns.Contains("isAutoMapped"))
                        Configuration.SetAutoMapped(bool.Parse((response.Tables[tableIndex].Rows[0]["isAutoMapped"] ?? 0).ToString()));

                    tableIndex++;
                    #endregion

                    #region Connections
                    if (response.Tables[tableIndex].Rows.Count > 0)
                    {
                        Configuration.LoadConnections(from rowData in response.Tables[tableIndex].AsEnumerable()
                                                      select new ApiEndpointConfiguration()
                                                      {
                                                          BrandId = rowData.Field<long>("BrandId"),
                                                          Key = rowData.Field<string>("Key")?.ToLower(),
                                                          Value = rowData.Field<string>("Value")                                                          
                                                      });
                    }
                    tableIndex++;
                    #endregion

                    #region Routes
                    if (response.Tables[tableIndex].Rows.Count > 0)
                    {
                        Configuration.LoadRoute(from rowData in response.Tables[tableIndex].AsEnumerable()
                                                select new ApiRouteConfiguration()
                                                {
                                                    Id  = rowData.Field<long>("Id"),
                                                    BrandId = rowData.Field<long>("BrandId"),
                                                    MethodName = rowData.Field<string>("MethodName")?.ToLower(),
                                                    SpName = rowData.Field<string>("SpName").ToLower(),
                                                    CacheTimeout = rowData.Field<long>("CacheTimeout"),                                                    
                                                    RewData = rowData
                                                });
                    }
                    tableIndex++;
                    #endregion

                    #region Varibles
                    if (response.Tables[tableIndex].Rows.Count > 0)
                    {
                        Configuration.LoadVariables(from rowData in response.Tables[tableIndex].AsEnumerable()
                                                    select new ApiVariableConfiguration()
                                                    {
                                                        Id = rowData.Field<long>("Id"),
                                                        BrandId = rowData.Field<long>("BrandId"),
                                                        Key = rowData.Field<string>("Key").ToLower(),
                                                        Value = rowData.Field<string>("Value"),
                                                        RewData = rowData
                                                    });
                    }
                    tableIndex++;
                    #endregion
                }
            }
        }
    }
}
