﻿using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Configuration;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.SqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace IDoBet.CoreLibrary.Api
{
    public static class EnvironmentLoader
    {
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(EnvironmentLoader));

        private static InitApiConfiguration InitApiConfiguration { get; set; }
        public static string RootFolder { get; set; }
        public static void Configure(string instance,string apiId = null, int? brandId = null)
        {
            Configuration.Instance = instance;
            Logger.Info($"Config Instance: {instance}");
            Configuration.Log = Logger.Info;
            InitApiConfiguration = JsonConvert.DeserializeObject<InitApiConfiguration>(File.ReadAllText(Path.Combine(RootFolder, "configuration.json")));
            
            using (var sh = new SqlHandler(InitApiConfiguration.EndPoint, InitApiConfiguration.Value))
            {
                sh.AddParameter("BrandId", SqlDbType.Int, brandId);
                sh.AddParameter("Instance", SqlDbType.NVarChar, instance);
                sh.AddParameter("ApiId", SqlDbType.NVarChar, apiId);
                if (sh.Read())
                    if (sh.Get<int>("CodeNumber") != (int)eErrorCode.Success)
                        return;
                sh.NextResult();
                if (sh.Read())
                    Configuration.SetAutoMapped(sh.Get<int>("IsAutoMapped") == 1);
                sh.NextResult();
                /*
Id,
BrandId,
Instance,
ModuleId,
[Key],
[Value],
[Authentication],
[AdditionalData],
[Description]                 
                 */
                while (sh.Read())
                    Configuration.AddEnpoint(new ApiEndpointConfiguration
                    {
                        Id = sh.Get<long>("Id"),
                        BrandId = sh.Get<int>("BrandId"),
                        Instance = sh.Get<string>("Instance"),
                        ModuleId = sh.Get<int>("ModuleId"),
                        Key = sh.Get<string>("Key")?.ToLower(),
                        Value = sh.Get<string>("Value"),
                        Authentication = sh.Get<string>("Authentication"),
                        AdditionalData = sh.Get<string>("AdditionalData"),
                        Description = sh.Get<string>("Description")
                    });
                sh.NextResult();
                /*
                Id, 
                BrandId, 
                MethodName, 
                SpName,
                CacheTimeout 
                             */
                while (sh.Read())
                    Configuration.AddRoute(new ApiRouteConfiguration
                    {
                        Id = sh.Get<long>("Id"),
                        BrandId = sh.Get<int>("BrandId"),
                        MethodName = sh.Get<string>("MethodName")?.ToLower(),
                        SpName = sh.Get<string>("SpName")?.ToLower(),
                        CacheTimeout = sh.Get<long>("CacheTimeout"),
                        IsReadOnly = sh.Get<bool>("IsReadOnly")
                    });

                sh.NextResult();
                /*
                Id, 
                BrandId,
                Instance,	
                ModuleId,
                [Key],
                [Value],
                [Description] 
                */
                while (sh.Read())
                    Configuration.AddVariable(new ApiVariableConfiguration
                    {
                        Id = sh.Get<long>("Id"),
                        BrandId = sh.Get<int>("BrandId"),
                        Instance = sh.Get<string>("Instance"),
                        ModuleId = sh.Get<int>("ModuleId"),
                        Key = sh.Get<string>("Key")?.ToLower(),
                        Value = sh.Get<string>("Value"),
                        Description = sh.Get<string>("Description"),
                        ChannelId = sh.Get<int?>("ChannelId"),
                        BranchId = sh.Get<int?>("BranchId")

                    });

            }
        }
    }
}
