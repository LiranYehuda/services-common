﻿using IDoBet.CoreLibrary.Api.BL;
using IDoBet.CoreLibrary.Api.Helper;
using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using IDoBet.CoreLibrary.Logic.Model.Request.Bet;
using IDoBet.CoreLibrary.Logic.Model.Response;
using IDoBet.CoreLibrary.Logic.Model.Response.Bet;
using System;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace IDoBet.CoreLibrary.Api.Handlers
{
    public static class PlaceBetHandler
    {
        public static PlaceBetBasicResponse PlaceBetBasic(PlaceBetBasicRequest request) => Order.PlaceBetBasic(request);

        public static ExecuteBetBasicResponse ExecuteBetBasic(ExecuteBetBasicRequest request) => Order.ExecuteBetBasic(request);

        public static PlaceBetLottoResponse PlaceBetLotto(PlaceBetLottoRequest request)
        {
            var response = new PlaceBetLottoResponse();
            var placebetResponse = PlaceBetBasic(new PlaceBetBasicRequest()
            {
                BrandId = request.BrandId,
                ChannelId = request.ChannelId,
                ExternalData = new { lotteryTypeId = request.LotteryTypeId, betTypeId = request.BetTypeId }.ToString(),
                Terminal = request.Terminal,
                Token = request.Token,
                ProdType = (byte)eProductType.Lotto,
                Amount = request.Payment,
                IsExecute = request.IsExecute,
                ExternalId = request.ExternalId,
                UserInfoRequired = request.UserInfoRequired,
                IsBasedOnBookingCode = !string.IsNullOrEmpty(request.BookingCodeOrigin)
            });

            if (placebetResponse.IsSuccessfull)
            {
                var lottoUri = Configuration.GetEndpoint(request.BrandId.Value, eModule.Lotto, "LottoApi").Value;

                request.CurrencyId = placebetResponse.Order.CurrencyId;
                request.UserId = placebetResponse.Order.EmployeeId.Value;
                if (request.IsExecute)
                    request.TransactionId = placebetResponse.Order.Id.ToString();

                var lottoBetresponse = HttpHelper.PostJson<BaseResponse, PlaceBetLottoRequest>(lottoUri + "/api/Dynamic/Execute/", request, null, request.GetHeaders());
                if (lottoBetresponse.Item2 != null)
                {
                    response.SetErrorResult(eErrorCode.ApiError, lottoBetresponse.Item2.ToString());
                }
                else if (!lottoBetresponse.Item1.IsSuccessfull)
                {
                    response.SetErrorResult(eErrorCode.ApiError, lottoBetresponse.Item1.Result.ErrorCodeDescription);
                }
                else
                {
                    string lottoUserBetId = ResolveExternalBetIdForLottoResponse(lottoBetresponse.Item1.Data?.ToString());
                    Order.UpdateLottoUserBetId(placebetResponse.Order.Id, int.Parse(lottoUserBetId), Configuration.GetEndpoint(request.BrandId.Value, eModule.Lotto, "Sport").Value);
                    response.LottoUserBetId = lottoUserBetId;
                    if (request.IsExecute)
                    {
                        response.Number = placebetResponse.Order.OrderNumber;
                        response.UserInfo = placebetResponse.UserInfo;
                        response.Barcode = placebetResponse.Order.Barcode;
                        response.Payout = placebetResponse.Order.Payout;

                        response.CancelUntil = placebetResponse.Order.CancelUntil;
                        response.OrderProd = placebetResponse.Order.OrderProd;
                        response.TotalCommission = placebetResponse.Order.TotalCommission;
                        response.CommissionDesc = placebetResponse.Order.CommissionDesc;
                        response.TotalBonus = placebetResponse.Order.TotalBonus;
                        response.BonusDesc = placebetResponse.Order.BonusDesc;
                        response.TotalTax = placebetResponse.Order.TotalTax;
                        response.MaxPayout = placebetResponse.Order.MaxPayout;
                        response.TransactionId = placebetResponse.Order.TransactionId;




                        if (request.ChannelId.HasValue && request.ChannelId.Value == (int)eChannel.CashBox)
                        {
                            var encryptionKey = DateTime.Now.Month.ToString() + "GS_ORDER_KEY";
                            response.UrlKey = CoreLibrary.Api.SymmetricEncryptor.Encrypt(JsonSerializer.Serialize(new CoreLibrary.Logic.Model.Request.Legacy.PrintService.OrderHtmlRequest
                            {
                                UserToken = request.Token,
                                RequestGuid = placebetResponse.Order.RequestGuid,
                                ForPrint = true,
                                Language = request.Language
                            }), encryptionKey);
                        }


                    }
                    else
                    {
                        response.RequestGuid = placebetResponse.Order.RequestGuid;
                        response.EmployeeId = placebetResponse.Order.EmployeeId;
                    }
                }

                if (!response.IsSuccessfull && request.IsExecute)
                {
                    Order.CancelOrder(new CancelOrderRequest()
                    {
                        Token = request.Token,
                        Language = request.Language,
                        BrandId = request.BrandId,
                        ExtraData = request.ExtraData,
                        ChannelId = request.ChannelId,
                        Terminal = request.Terminal,
                        IP = request.IP,
                        OrderId = placebetResponse.Order.Id,
                        CanceledByToken = true,
                        CancellationReason = JsonSerializer.Serialize(response.Result),
                        ExpectedProductIds = ((int)eProductType.Lotto).ToString(),
                        CancelReasonTypeId = 6 //Third party reason
                    });
                }
            }
            else
            {
                response.Result = placebetResponse.Result;
            }

            return response;
        }

        public static ExecuteBetLottoResponse ExecuteBetLotto(ExecuteBetLottoRequest request)
        {
            var response = new ExecuteBetLottoResponse();
            var executeBetResponse = ExecuteBetBasic(new ExecuteBetBasicRequest()
            {
                BrandId = request.BrandId,
                ChannelId = request.ChannelId,
                Terminal = request.Terminal,
                Token = request.Token,
                RequestGuid = request.RequestGuid,
                ExternalId = request.ExternalId,
                LottoUserBetId = request.UserBetId,
                ForUserId = request.ForUserId,
                ExternalTransactionId = request.ExternalTransactionId,
                TransactionExtraData = request.TransactionExtraData,
                UserInfoRequired = request.UserInfoRequired
            });

            if (executeBetResponse.IsSuccessfull)
            {
                var lottoUri = Configuration.GetEndpoint(request.BrandId.Value, eModule.Lotto, "LottoApi").Value;

                request.UserId = executeBetResponse.Order.EmployeeId.Value;
                request.TransactionId = executeBetResponse.Order.Id.ToString();

                var lottoBetresponse = HttpHelper.PostJson<BaseResponse, ExecuteBetLottoRequest>(lottoUri + "/api/Dynamic/Execute/", request, null, request.GetHeaders());
                if (lottoBetresponse.Item2 != null)
                {
                    response.SetErrorResult(eErrorCode.ApiError, lottoBetresponse.Item2.ToString());
                }
                else if (!lottoBetresponse.Item1.IsSuccessfull)
                {
                    response.SetErrorResult(eErrorCode.ApiError, lottoBetresponse.Item1.Result.ErrorCodeDescription);
                }
                else
                {
                    response.UserInfo = executeBetResponse.UserInfo;
                    response.UserBetId = request.UserBetId;
                    response.ExternalId = request.ExternalId;
                    response.Number = executeBetResponse.Order.OrderNumber;
                    response.Barcode = executeBetResponse.Order.Barcode;
                    if (request.ChannelId.HasValue && request.ChannelId.Value == (int)eChannel.CashBox)
                    {
                        var encryptionKey = DateTime.Now.Month.ToString() + "GS_ORDER_KEY";
                        response.UrlKey = CoreLibrary.Api.SymmetricEncryptor.Encrypt(JsonSerializer.Serialize(new CoreLibrary.Logic.Model.Request.Legacy.PrintService.OrderHtmlRequest
                        {
                            UserToken = request.Token,
                            RequestGuid = executeBetResponse.Order.RequestGuid,
                            ForPrint = true,
                            Language = request.Language
                        }), encryptionKey);
                    }
                    response.Payout = executeBetResponse.Order.Payout;
                    response.CancelUntil = executeBetResponse.Order.CancelUntil;
                    response.OrderProd = executeBetResponse.Order.OrderProd;
                    response.TotalCommission = executeBetResponse.Order.TotalCommission;
                    response.CommissionDesc = executeBetResponse.Order.CommissionDesc;
                    response.TotalBonus = executeBetResponse.Order.TotalBonus;
                    response.BonusDesc = executeBetResponse.Order.BonusDesc;
                    response.TotalTax = executeBetResponse.Order.TotalTax;
                    response.TaxDesc = executeBetResponse.Order.TaxDesc;
                    response.MaxPayout = executeBetResponse.Order.MaxPayout;
                    response.TransactionId = executeBetResponse.Order.TransactionId;

                }
            }
            else
            {
                response.Result = executeBetResponse.Result;
            }

            return response;
        }



        private static string ResolveExternalBetIdForLottoResponse(string jsonResponse)
        {
            using JsonDocument document = JsonDocument.Parse(jsonResponse, new JsonDocumentOptions
            {
                AllowTrailingCommas = true
            });
            return document.RootElement.EnumerateArray().FirstOrDefault().GetProperty("userBetId").ToString();
        }

        public static CancelOrderResponse CancelOrder(CancelOrderRequest request) =>
            Order.CancelOrder(request);

    }
}
