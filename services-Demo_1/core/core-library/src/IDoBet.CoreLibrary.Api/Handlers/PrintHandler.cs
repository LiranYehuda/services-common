﻿using IDoBet.CoreLibrary.Api.Helper;
using IDoBet.CoreLibrary.Api.Model.PrintHelper;
using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Response.Bet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Api.Handlers
{
    public static class PrintHandler
    {
        private static string GetOrderKey(string userToken, string requestGuid, bool forPrint, int brandId, int? width = null, string language = null, string barcode = null, bool isReprint = false)
        {
            OrderHTMLRequest r = new OrderHTMLRequest(userToken, requestGuid, forPrint, width, language, null, barcode, isReprint);
            string plainText = JsonConvert.SerializeObject(r);
            string key = DateTime.Now.Month.ToString() + Encryptions.ORDER_KEY;
            string encryptedCode = SymmetricEncryptor.Encrypt(plainText, key);
            return encryptedCode;
        }

        public static GetOrderKeyResponse GetOrderKey(GetOrderKeyRequest request)
        {
            var orderKey = GetOrderKey(request.Token, null, false, request.BrandId.Value, 300, request.Language, request.Barcode);
            var fullURL = $"{Configuration.GetValue(request.BrandId.Value, CoreLibrary.Logic.Model.Enums.eModule.Game, "PrintServiceGetOrderURL").Value}?q={orderKey}";
            return new GetOrderKeyResponse { Data = fullURL };
        }        
    }
}
