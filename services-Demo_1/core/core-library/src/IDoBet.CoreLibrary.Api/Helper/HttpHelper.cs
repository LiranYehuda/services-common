﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Net;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace IDoBet.CoreLibrary.Api.Helper
{
    public static class HttpHelper
    {
        public static string Get(string uri, Encoding encoding = null, NameValueCollection headers = null)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (headers != null)
                request.Headers.Add(headers);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream, encoding))
                return reader.ReadToEnd();
        }

        public static Tuple<T, Exception> GetJson<T>(string uri, Encoding encoding = null)
        {
            T result = default;
            if (encoding == null)
                encoding = Encoding.UTF8;
            var content = Get(uri, encoding);
            try
            {
                result = JsonConvert.DeserializeObject<T>(content, new JsonSerializerSettings()
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                });

                return new Tuple<T, Exception>(result, null);
            }
            catch // (Exception ex)
            {
                return new Tuple<T, Exception>(result, new Exception(content));
            }
        }

        public static Tuple<T, string> GetXml<T>(string uri, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;
            var serializer = new XmlSerializer(typeof(T));
            var content = Get(uri, encoding);
            try
            {
                return new Tuple<T, string>((T)serializer.Deserialize(new StringReader(content)), content);
            }
            catch // (Exception ex)
            {
                return new Tuple<T, string>(default(T), content);
            }
        }

        public static Tuple<T, Exception> PostJson<T, U>(string uri, U request, Encoding encoding = null, NameValueCollection headers = null)
        {
            T result = default;
            if (encoding == null)
                encoding = Encoding.UTF8;
            try
            {
                var payLoad = JsonConvert.SerializeObject(request, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Include,
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                });

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                if (headers != null)
                    httpWebRequest.Headers.Add(headers);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(payLoad);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var response = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream(), encoding))
                {
                    response = streamReader.ReadToEnd();
                }

                result = JsonConvert.DeserializeObject<T>(response, new JsonSerializerSettings()
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                });

            }
            catch (Exception ex)
            {
                return new Tuple<T, Exception>(result, ex);
            }
            return new Tuple<T, Exception>(result, null);
        }

        public static Tuple<T, Exception> PostJsonString<T>(string uri, string json, Encoding encoding = null, NameValueCollection headers = null)
        {
            var response = Post<T>(uri, json, encoding, headers);
            return new Tuple<T, Exception>(response.Item1, response.Item2);
        }


        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 20 * 60 * 1000;
                return w;
            }
        }

        public static string PostForm(string uri, Dictionary<string, string> parameters, Dictionary<string, string> headers, out Dictionary<string, string> responseHeaders, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;
            var post = new NameValueCollection();
            foreach (var key in parameters.Keys)
                if (parameters[key] != null)
                    post.Add(key, parameters[key]);
            using (var client = new MyWebClient())
            {
                if (headers != null)
                    foreach (var header in headers)
                        client.Headers.Add(header.Key, header.Value);
                var res = client.UploadValues(uri, post);
                var str = new string(encoding.GetChars(res));
                responseHeaders = new Dictionary<string, string>();
                foreach (var key in client.ResponseHeaders.Keys)
                    responseHeaders.Add(key.ToString(), client.ResponseHeaders[key.ToString()]);
                return str;
            }
        }

        public static void InitiateSSLTrust()
        {
            //Change SSL checks so that all checks pass
            ServicePointManager.ServerCertificateValidationCallback =
                new System.Net.Security.RemoteCertificateValidationCallback(
                    delegate
                    { return true; }
                );
        }

        public static Tuple<T, Exception, int?> Post<T>(string uri, string json, Encoding encoding = null, NameValueCollection headers = null)
        {
            int? httpStatusCode = null;
            T result = default;
            if (encoding == null)
                encoding = Encoding.UTF8;
            try
            {

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                if (headers != null)
                    httpWebRequest.Headers.Add(headers);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var response = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream(), encoding))
                {
                    response = streamReader.ReadToEnd();
                }
                httpStatusCode = (int)httpResponse.StatusCode;
                result = JsonConvert.DeserializeObject<T>(response, new JsonSerializerSettings()
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                });

            }
            catch (Exception ex)
            {
                return new Tuple<T, Exception, int?>(result, ex, httpStatusCode);
            }
            return new Tuple<T, Exception, int?>(result, null, httpStatusCode);
        }

        public static Tuple<string, Exception, int?> Post(string uri, string json, Encoding encoding = null, NameValueCollection headers = null)
        {
            int? httpStatusCode = null;
            string result = null;
            if (encoding == null)
                encoding = Encoding.UTF8;
            try
            {

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                if (headers != null)
                    httpWebRequest.Headers.Add(headers);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var response = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream(), encoding))
                {
                    response = streamReader.ReadToEnd();
                }
                httpStatusCode = (int)httpResponse.StatusCode;
                result = response;
            }
            catch (Exception ex)
            {
                return new Tuple<string, Exception, int?>(result, ex, httpStatusCode);
            }
            return new Tuple<string, Exception, int?>(result, null, httpStatusCode);
        }

        public static Tuple<T, Exception, int?> Get<T>(string uri,  Encoding encoding = null, NameValueCollection headers = null)
        {
            int? httpStatusCode = null;
            T result = default;
            if (encoding == null)
                encoding = Encoding.UTF8;


            try
            {

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                if (headers != null)
                    httpWebRequest.Headers.Add(headers);

              

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var response = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream(), encoding))
                {
                    response = streamReader.ReadToEnd();
                }
                httpStatusCode = (int)httpResponse.StatusCode;
                result = JsonConvert.DeserializeObject<T>(response, new JsonSerializerSettings()
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                });

            }
            catch (Exception ex)
            {
                return new Tuple<T, Exception, int?>(result, ex, httpStatusCode);
            }
            return new Tuple<T, Exception, int?>(result, null, httpStatusCode);
        }

    }
}
