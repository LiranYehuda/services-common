// using iTextSharp.text;
//using iTextSharp.text.pdf;
using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
// using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;

namespace IDoBet.CoreLibrary.Api.Helper
{
    public static class ExtensionsMethods
    {
        public static object ToCsv(this DataSet data, int? brandId, bool isResultNamed = false, bool isInMemory = false, int startIndex = 1)
        {
            if (data == null || data.Tables == null)
                return null;

            if (data.Tables.Count - startIndex > 1)
                return data.ToXlsx(brandId, isResultNamed, isInMemory, startIndex);

            if (!brandId.HasValue)
                return null;

            var filePrefix = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFilePrefix")?.Value ?? string.Empty;
            var loacalPath = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFileLocalPrefix")?.Value;
            var serverPath = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFileServerPrefix")?.Value;

            if (string.IsNullOrEmpty(loacalPath) || string.IsNullOrEmpty(serverPath))
                return null;
            var name = $"{filePrefix}{DateTime.Now.ToFileTimeUtc().ToString()}.csv";
            var fileName = $"{loacalPath}\\{name}";
            var outputFile = $"{serverPath}/{name}";

            MemoryStream memoryStream = null;
            if (isInMemory)
            {
                memoryStream = new MemoryStream();
                using (StreamWriter writer = new StreamWriter(memoryStream))
                {
                    Write(writer, data.Tables[startIndex]);
                }

                return memoryStream != null ? memoryStream.ToArray() : null;
            }
            else
            {
                using (StreamWriter writer = new StreamWriter(fileName))
                {
                    Write(writer, data.Tables[startIndex]);
                }

                return new List<object>() { new { Url = outputFile } };
            }

            void Write(StreamWriter writer, DataTable sourceTable)
            {
                IEnumerable<String> headerValues = sourceTable.Columns
                .OfType<DataColumn>()
                .Select(column => QuoteValue(column.ColumnName));

                writer.WriteLine(String.Join(",", headerValues));

                IEnumerable<String> items = null;

                foreach (DataRow row in sourceTable.Rows)
                {
                    items = row.ItemArray.Select(o => QuoteValue(o?.ToString() ?? String.Empty));
                    writer.WriteLine(String.Join(",", items));
                }

                writer.Flush();
            }


            string QuoteValue(string value)
            {
                return String.Concat("\"",
                value.Replace("\"", "\"\""), "\"");
            }
        }

        public static object ToPdf(this DataSet data, int? brandId, bool isResultNamed = false, bool isInMemory = false, int startIndex = 1)
        {
            if (!brandId.HasValue)
                return null;

            var filePrefix = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFilePrefix")?.Value ?? string.Empty;
            var loacalPath = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFileLocalPrefix")?.Value;
            var serverPath = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFileServerPrefix")?.Value;

            if (string.IsNullOrEmpty(loacalPath) || string.IsNullOrEmpty(serverPath))
                return null;
            var name = $"{filePrefix}{DateTime.Now.ToFileTimeUtc().ToString()}.pdf";
            var fileName = $"{loacalPath}\\{name}";
            var outputFile = $"{serverPath}/{name}";

            int index = 0;
            PdfWriter writer = null;
            MemoryStream memoryStream = null;
            if (isInMemory)
            {
                memoryStream = new MemoryStream();
                writer = new PdfWriter(memoryStream);
            }
            else
            {
                FileInfo file = new FileInfo(fileName);
                file.Directory.Create();
                writer = new PdfWriter(fileName);
            }

            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf, PageSize.A4.Rotate());

            foreach (DataTable dt in data.Tables)
            {
                if (index >= startIndex)
                {
                    AddTable(dt);
                }
                index++;
            }

            document.Close();

            if (isInMemory)
            {
                return memoryStream.ToArray();
            }
            else
            {
                return new List<object>() { new { Url = outputFile } };
            }

            void AddTable(DataTable dataTable)
            {
                var maximumLengthForColumns =
                        Enumerable.Range(0, dataTable.Columns.Count)
                                  .Select(col => (float)100).ToArray();

                var table = new Table(maximumLengthForColumns);
                table.SetWidth(UnitValue.CreatePercentValue(100));
                table.SetAutoLayout();


                //Set columns names in the pdf file
                for (int k = 0; k < dataTable.Columns.Count; k++)
                {
                    var cell = new Cell().Add(new Paragraph(dataTable.Columns[k].ColumnName));
                    cell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                    cell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                    table.AddCell(cell);
                }

                //Add values of DataTable in pdf file
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        var cell = new Cell().Add(new Paragraph(dataTable.Rows[i][j].ToString()));
                        cell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                        cell.SetVerticalAlignment(VerticalAlignment.MIDDLE);

                        table.AddCell(cell);
                    }
                }

                document.Add(table);

                pdf.AddNewPage();
            }
        }

        public static object ToXlsx(this DataSet data, int? brandId, bool isResultNamed = false, bool isInMemory = false, int startIndex = 1)
        {
            if (!brandId.HasValue)
                return null;

            var filePrefix = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFilePrefix")?.Value ?? string.Empty;
            var loacalPath = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFileLocalPrefix")?.Value;
            var serverPath = Configuration.GetValue(brandId.Value, eModule.Core, "ImportFileServerPrefix")?.Value;

            if (!isInMemory && (string.IsNullOrEmpty(loacalPath) || string.IsNullOrEmpty(serverPath)))
                return null;
            var name = $"{filePrefix}{DateTime.Now.ToFileTimeUtc().ToString()}.xlsx";
            var fileName = $"{loacalPath}\\{name}";
            var outputFile = $"{serverPath}/{name}";
            byte[] outputArray = null;

            int sheetIndex = 0;
            string sheetName = null;
            ExcelWorksheet worksheet = null;
            using (ExcelPackage excel = new ExcelPackage())
            {
                foreach (DataTable dt in data.Tables)
                {
                    if (sheetIndex >= startIndex)
                    {
                        if (isResultNamed)
                        {
                            if (sheetName == null)
                            {
                                sheetName = dt.Rows[0][0].ToString();
                            }
                            else
                            {
                                proccessWorksheet();
                            }
                        }
                        else
                        {
                            sheetName = sheetIndex.ToString();
                            proccessWorksheet();
                        }

                        void proccessWorksheet()
                        {
                            worksheet = excel.Workbook.Worksheets.Add(sheetName);
                            var headerRow = new List<string[]>() { dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray() };
                            // Determine the header range (e.g. A1:D1)
                            string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                            worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                            var cellData = dt.AsEnumerable().Select(i => i.ItemArray.Cast<object>().ToArray()).ToList();
                            worksheet.Cells[2, 1].LoadFromArrays(cellData);
                        }

                    }

                    sheetIndex++;
                }

                if (isInMemory)
                {
                    outputArray = excel.GetAsByteArray();
                }
                else
                {
                    FileInfo excelFile = new FileInfo(fileName);
                    excel.SaveAs(excelFile);
                }
            }

            if (isInMemory)
            {
                return outputArray;
            }
            else
            {
                return new List<object>() { new { Url = outputFile } };
            }
        }

        public static object ToJson(this DataSet dataSet, bool isResultNamed = false, bool isJsonObject = false)
        {
            bool isFirst = true;
            string currentTableName = null;
            object convertResult = isResultNamed || isJsonObject ? (object)new JObject() : (object)new List<object>();
            bool isJsonList = false;

            foreach (DataTable table in dataSet.Tables)
            {
                if (isFirst)
                {
                    isFirst = false;
                }
                else
                {
                    if (isResultNamed)
                    {
                        if (currentTableName == null)
                        {
                            currentTableName = table.Rows[0][0].ToString();
                        }
                        else
                        {
                            if ( currentTableName != null && currentTableName.ToLower().StartsWith("object") && table.Rows.Count == 1 && table.Columns.Count == 1)
                            {
                                currentTableName = currentTableName.Substring("object".Length);
                               
                                if (table.Columns[0].ColumnName.ToLower().StartsWith("json")) // value is json format
                                {                                    
                                    ((JObject)convertResult).Add(currentTableName, JObject.Parse(table.Rows[0][0].ToString()));                                   
                                }
                                else // value is premitive  (string,int,bool...)
                                {
                                    ((JObject)convertResult).Add(currentTableName, table.Rows[0][0].ToString());
                                }
                            }
                            else
                            {
                                ((JObject)convertResult).Add(currentTableName, ConvertDataRowCollectionToJarray(table.Rows, table.Columns));
                            }

                            currentTableName = null;
                        }
                    }
                    else if (isJsonObject)
                    {
                         isJsonList = table.Rows[0][0].ToString().StartsWith("[");
                        if(isJsonList)
                            convertResult = JArray.Parse(table.Rows[0][0].ToString());
                        else
                            convertResult = JObject.Parse(table.Rows[0][0].ToString());                                             
                    }
                    else
                    {
                        ((List<object>)convertResult).Add(ConvertDataRowCollectionToJarray(table.Rows, table.Columns));
                    }
                }
            }

         


            return !isResultNamed  && !isJsonObject && !isJsonList ?
                (
                    ((List<object>)convertResult).Count < 2 ? ((List<object>)convertResult).FirstOrDefault() : ((List<object>)convertResult)) :
                convertResult;
        }

        private static JArray ConvertDataRowCollectionToJarray(DataRowCollection rows, DataColumnCollection columns)
        {

            var currentArray = new JArray();
            var serializer = new JsonSerializer()
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            };

            foreach (DataRow row in rows)
            {
                currentArray.Add(new JObject(
                columns.Cast<DataColumn>()
                  .Select(c => c.ColumnName.ToLower().StartsWith("json") ?
                   new JProperty(c.ColumnName.Remove(0, "json".Length), row[c] != DBNull.Value ? JToken.Parse(row[c].ToString()) : null) :
                   new JProperty(c.ColumnName, JToken.FromObject(row[c], serializer)))));
            }

            return currentArray;
        }
    }
}
