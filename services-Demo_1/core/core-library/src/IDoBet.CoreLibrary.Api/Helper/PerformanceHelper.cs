﻿using System.Diagnostics;
using System.Text;

namespace IDoBet.CoreLibrary.Api.Helper
{
    public class PerformanceHelper
    {
        Stopwatch stopWatch;
        StringBuilder performanceLog;
        string methodName;
        long totalMemory;
        public PerformanceHelper(string methodName)
        {
            stopWatch = new Stopwatch();
            performanceLog = new StringBuilder();
            this.methodName = methodName;
        }
        public void Start(string operation)
        {
            totalMemory = System.GC.GetTotalMemory(false);
            performanceLog.Append($"{methodName}: {operation} - TotalMemory: {totalMemory}");
            stopWatch.Restart();
        }
        public void Next(string operation)
        {
            Stop();            
            Start(operation);
        }
        public void Stop() =>
            performanceLog.AppendLine($" took {stopWatch.ElapsedMilliseconds} ms. Memory usage change: {System.GC.GetTotalMemory(false) - totalMemory}");
        public string Log =>
            performanceLog.ToString();
        public void Clear() =>
            performanceLog.Clear();
        public long MilliSeconds => stopWatch.ElapsedMilliseconds;
        public void Reset() => stopWatch.Reset();
    }
}
