﻿using IDoBet.CoreLibrary.Logic.Helper;
using IDoBet.CoreLibrary.Logic.Model.Request;
using IDoBet.CoreLibrary.Logic.Model.Response;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace IDoBet.CoreLibrary.Api.Helper
{
    public static class CacheHelper
    {
        private static readonly MemoryCache Cache = new MemoryCache(new MemoryCacheOptions());

        public static string GenerateRequestKey(Dictionary<string, string> parameter, ApiRequestDynamic request)
        {
            dynamic keyJson = JObject.Parse(JsonConvert.SerializeObject(parameter));
            keyJson.Language = request.Language;
            keyJson.BrandId = request.BrandId;
            keyJson.ChannelId = request.ChannelId;

            return HashGenerator.GetInt64HashCode(JsonConvert.SerializeObject(keyJson)).ToString();
        }

        public static T Get<T>(string key) where T : BaseResponse   => (Get(key) as T);
        public static object Get(string key)
        {
            if (Cache.TryGetValue(key, out object result))
                return result;
            return null;
        }

        public static void Set<T>(string key, T Data, long cacheTimeout)
        {
            if (cacheTimeout > 0)
            {
                var cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMilliseconds(cacheTimeout));
                Cache.Set<T>(key, Data, cacheEntryOptions);
            }
        }
    }
}
