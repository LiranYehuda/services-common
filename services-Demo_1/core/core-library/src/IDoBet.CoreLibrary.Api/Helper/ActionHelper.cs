﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Request;
using IDoBet.CoreLibrary.Logic.Model.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json.Linq;
using IDoBet.CoreLibrary.SqlClient;
using Newtonsoft.Json;
using Microsoft.Extensions.Caching.Memory;
using IDoBet.CoreLibrary.Logic.Helper;
using System.Collections.Concurrent;
using System.Data;

namespace IDoBet.CoreLibrary.Api.Helper
{
    public static class ActionHelper
    {
        #region consts
        public static readonly string DefaultResultFormat = "Data";
        public static readonly string DefaultGroupName = "sport";

        public static readonly string OriginGroupNameField = "originGroupName";
        public static readonly string MethodGroupField = "methodGroup";
        public static readonly string MethodGroupFieldFallback = "GroupName";
        public static readonly string MethodNameField = "methodName";
        public static readonly string FormatFileField = "FormatFile";
        public static readonly string IsInMemoryField = "IsInMemory";
        public static readonly string IsDataNamedField = "IsDataNamed";
        public static readonly string IsJsonObjectField = "IsJsonObject";
        public static readonly string IsUserInfoIncluded = "IsUserInfoIncluded";
        #endregion
        private static string ServiceName = Configuration.GetValue(Configuration.SystemBrandId, eModule.Core, "ServiceName")?.Value;
        private static bool ShowExceptionDetails = bool.Parse(Configuration.GetValue(Configuration.SystemBrandId, eModule.Core, "ShowExceptionDetails")?.Value ?? "true");

        private static void SetHeaders<T>(T parameter, HttpRequest request) where T : BaseRequest
        {
            var headers = new Dictionary<string, List<string>>();
            foreach (var key in request.Headers.Keys)
            {
                var val = request.Headers[key];
                if (val != StringValues.Empty)
                    headers[key.ToLower()] = new List<string>(request.Headers[key]);
            }
            parameter.SetHeaders(headers);
        }

        public static Dictionary<string, string> ProcessParamers(Dictionary<string, string> dict, HttpRequest request, bool isFile = false)
        {

            if (dict.ContainsKey("Parameter"))
            {
                var insideParameters = JsonConvert.DeserializeObject<Dictionary<string, string>>(dict["Parameter"]);
                foreach (var key in insideParameters.Keys)
                    dict.Add(key, insideParameters[key]);
                dict.Remove("Parameter");
            }

            if (isFile)
            {
                if (!((Dictionary<string, string>)(dict)).ContainsKey("FormatFile"))
                {
                    ((Dictionary<string, string>)(dict)).Add("FormatFile", "XLSX");
                }
            }
            else
            {
                dict.Remove("FormatFile");
            }

            var headerParams = new List<string> { "Token", "BrandId", "Language", "Terminal", "ExtraData", "ChannelId" };
            foreach (var name in headerParams)
                if (request.Headers.ContainsKey(name))
                    if (!string.IsNullOrEmpty(request.Headers[name]))
                        if (!dict.ContainsKey(name))
                            dict.Add(name, request.Headers[name]);
                        else dict[name] = request.Headers[name];

            return dict;
        }

        public static void UpdateHeaderFromBody(BaseRequest request, HttpRequest Request)
        {
            if (!Request.Headers.ContainsKey("Token"))
            {
                Request.Headers.Add("Token", request.Token?.ToString());
            }

            if (!Request.Headers.ContainsKey("BrandId"))
            {
                Request.Headers.Add("BrandId", request.BrandId?.ToString());
            }

            if (!Request.Headers.ContainsKey("Language"))
            {
                Request.Headers.Add("Language", request.Language?.ToString());
            }

            if (!Request.Headers.ContainsKey("Terminal"))
            {
                Request.Headers.Add("Terminal", request.Terminal?.ToString());
            }

            if (!Request.Headers.ContainsKey("ExtraData"))
            {
                Request.Headers.Add("ExtraData", request.ExtraData?.ToString());
            }
        }

        private static T HandleException<T>(Exception ex, string controllerName, string methodName, int? brandId) where T : BaseResponse, new()
        {
            Logging.Logger.WriteLog(Logging.Model.Enums.eLogLevel.Error, ServiceName, controllerName, methodName, null, ex, brandId);
            return new T()
            {
                Result = new ResponseResult
                {
                    ErrorCode = eErrorCode.GerneralApi,
                    ErrorCodeDescription = "Error in processing the request.",
                    ResultCode = eResultCode.Failure,
                    ErrorDescription = ShowExceptionDetails ? ex.ToString() : ""
                }
            };
        }

        private static BaseResponse HandleException(Exception ex, BaseRequest parameter) =>
            HandleException<BaseResponse>(ex, parameter.GroupName, parameter.MethodName, parameter.BrandId);

        private static T HandleException<T>(Exception ex, BaseRequest parameter) where T : BaseResponse, new() =>
            HandleException<T>(ex, parameter.GroupName, parameter.MethodName, parameter.BrandId);

        public static BaseResponse Execute<T>(Func<T, BaseResponse> command, T parameter, HttpRequest request) where T : BaseRequest =>
            Execute<T, BaseResponse>(command, parameter, request);

        public static U Execute<T, U>(Func<T, U> command, T parameter, HttpRequest request) where T : BaseRequest where U : BaseResponse, new()
        {
            var onlineChannelIds = new List<int> { (int)eChannel.Online, (int)eChannel.Mobile, (int)eChannel.Kiosk };
            try
            {
                if (parameter != null)
                {
                    if (parameter.ChannelId != null && onlineChannelIds.Contains(parameter.ChannelId.Value) && (string.IsNullOrEmpty(parameter.Terminal) || parameter.Terminal.ToLower() == "online"))
                        parameter.Terminal = request.Host.Host.ToLower();
                    SetHeaders(parameter, request);
                }

                return command(parameter);
            }
            catch (Exception ex)
            {
                return HandleException<U>(ex, parameter);
            }
        }

        private static object Convert(System.Data.DataSet dataSet, int brandId, string formatFile, bool isResultNamed, bool isInMemoryFile, bool isJsonObject)
        {
            switch (formatFile)
            {
                case "csv":
                    return dataSet.ToCsv(brandId, isResultNamed, isInMemoryFile);
                case "xlsx":
                    return dataSet.ToXlsx(brandId, isResultNamed, isInMemoryFile);
                case "pdf":
                    return dataSet.ToPdf(brandId, isResultNamed, isInMemoryFile);
                default:
                    return dataSet.ToJson(isResultNamed, isJsonObject);
            }
        }

        private static void ProccessUserInfo<T>(System.Data.DataSet dataSet, T result) where T : BaseResponse
        {
            result.UserInfo = JObject.Parse(dataSet.Tables[1].Rows[0][0].ToString());
            dataSet.Tables.RemoveAt(1);
        }

        public static T ExecuteDynamic<T>(Dictionary<string, string> parameter, HttpRequest request, eModule module) where T : BaseResponse, new()
        {
            parameter = new Dictionary<string, string>(parameter, StringComparer.OrdinalIgnoreCase);
            var originGroupName = parameter.ContainsKey(OriginGroupNameField) ? parameter[OriginGroupNameField] : DynamicSql.DefaultSchemaName;
            var groupName = parameter.ContainsKey(MethodGroupField)  ? parameter[MethodGroupField] : parameter.ContainsKey(MethodGroupFieldFallback) ? parameter[MethodGroupFieldFallback] : DefaultGroupName;
            var methodName = parameter[MethodNameField];
            var dynamicRequest = new ApiRequestDynamic { MethodName = methodName, GroupName = groupName };
            SetHeaders(dynamicRequest, request);
            try
            {
                var cachKey = CacheHelper.GenerateRequestKey(parameter, dynamicRequest);
                T responseCache = CacheHelper.Get<T>(cachKey);
                if (responseCache != null)
                    return responseCache;
                var configuration = Configuration.GetAction(dynamicRequest.BrandId ?? 0, module, groupName, methodName, originGroupName);
                if (configuration == null)
                    throw new Exception("Not routed.");
                var dynamicResult = DynamicSql.Execute<T>(configuration, parameter, dynamicRequest);
                var formatFile = parameter.ContainsKey(FormatFileField) ? parameter[FormatFileField] : DefaultResultFormat;
                var isInMemoryFile = parameter.ContainsKey(IsInMemoryField) ? bool.Parse(parameter[IsInMemoryField]) : false;
                if (dynamicResult.resultData != null)
                {
                    var JDataStructure = dynamicResult.result.DataStructure != null ? JObject.Parse(dynamicResult.result.DataStructure.ToString()) : null;
                    var isNamed = JDataStructure != null && JDataStructure.ContainsKey(IsDataNamedField) && bool.Parse(JDataStructure[IsDataNamedField].ToString());
                    var isJsonObject = JDataStructure != null && JDataStructure.ContainsKey(IsJsonObjectField) && bool.Parse(JDataStructure[IsJsonObjectField].ToString());
                    var IsUserInfoObject = JDataStructure != null && JDataStructure.ContainsKey(IsUserInfoIncluded) && bool.Parse(JDataStructure[IsUserInfoIncluded].ToString());
                    if (IsUserInfoObject)
                    {
                        ProccessUserInfo(dynamicResult.resultData, dynamicResult.result);
                    }
                    dynamicResult.result.Data = Convert(dynamicResult.resultData, dynamicRequest.BrandId ?? 0, formatFile, isNamed, isInMemoryFile, isJsonObject);
                }

                dynamicResult.result.DataStructure = null;
                if (dynamicResult.result.IsSuccessfull)
                    CacheHelper.Set<T>(cachKey, dynamicResult.result, configuration.Route.CacheTimeout);
                return dynamicResult.result;

            }
            catch (Exception ex)
            {
                return (T)HandleException(ex, dynamicRequest);
            }
        }

        public static U CallApiWithUri<U, T>(T request, string uri, NameValueCollection headers)
        {
            Logging.Logger.WriteLog(Logging.Model.Enums.eLogLevel.Debug, $"Call {uri}");
            var result = HttpHelper.PostJson<U, T>(uri, request, null, headers);
            if (result.Item2 != null)
                throw result.Item2;
            return result.Item1;
        }

        private static readonly MemoryCache Cache = new MemoryCache(new MemoryCacheOptions());
        public static int? GetOnlineBranchIdForDomain(int brandId, string domain, string connectionString)
        {
            var mappingDic = Cache.Get<ConcurrentDictionary<(int, string), int?>>("OnlineBranchPerDomain");
            if (mappingDic == null)
            {
                mappingDic = new ConcurrentDictionary<(int, string), int?>();
            }
            if (mappingDic.ContainsKey((brandId, domain)))
                return mappingDic[(brandId, domain)];
            else
            {
                int? branchId = null;
                using (var sh = new SqlHandler(connectionString, "idb.spGetOnlineBranchPerDomain"))
                {
                    sh.AddParameter("Domain", SqlDbType.NVarChar, domain);

                    if (sh.Read())
                    {
                        branchId = sh.Get<int?>("BranchId");
                        mappingDic.TryAdd((brandId, domain), branchId);
                        Cache.Set<ConcurrentDictionary<(int, string), int?>>("OnlineBranchPerDomain", mappingDic);
                    }
                   
                    return branchId;
                }
            }        
          
        }
    }
}
