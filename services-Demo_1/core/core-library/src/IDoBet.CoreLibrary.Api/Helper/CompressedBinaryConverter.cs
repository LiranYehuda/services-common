﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace IDoBet.CoreLibrary.Api.Helper
{
    public static class CompressedBinaryConverter
    {

        /// <summary>
        /// Compress any serializble object, type does not metter 
        /// </summary>
        /// <param name="toCompress"></param>
        /// <returns></returns>
        public static byte[] SimpleToByteArray(object toCompress)
        {
            return ToByteArray(toCompress);
        }

        /// <summary>
        /// Converts any object into a byte array and then compresses it
        /// </summary>
        /// <param name="o">The object to convert</param>
        /// <returns>A compressed byte array that was the object</returns>
        public static byte[] ToByteArray(object toCompress)
        {
            if (toCompress == null)
                return new byte[0];

            using (MemoryStream outStream = new MemoryStream())
            {
                using (GZipStream zipStream = new GZipStream(outStream, CompressionMode.Compress))
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        new BinaryFormatter().Serialize(stream, toCompress);
                        stream.Position = 0;
                        stream.CopyTo(zipStream);
                        zipStream.Close();
                        return outStream.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Converts a byte array back into an object and uncompresses it
        /// </summary>
        /// <param name="byteArray">Compressed byte array to convert</param>
        /// <returns>The object that was in the byte array</returns>
        public static object ToObject(byte[] byteArray)
        {
            if (byteArray == null || byteArray.Length == 0)
                return null;

            using (MemoryStream decomStream = new MemoryStream(byteArray), ms = new MemoryStream())
            {
                using (GZipStream hgs = new GZipStream(decomStream, CompressionMode.Decompress))
                {
                    hgs.CopyTo(ms);
                    decomStream.Close();
                    hgs.Close();
                    ms.Position = 0;
                    var item = new BinaryFormatter().Deserialize(ms);
                    return item;
                }
            }
        }

        /// <summary>
        /// Compress collection to byte[][]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="S"></typeparam>
        /// <param name="toCompress"></param>
        /// <returns></returns>
        public static byte[][] ToByteArray<S>(IEnumerable<S> toCompress)
        {
            if (toCompress == null)
                return null;
            return toCompress.AsParallel().WithDegreeOfParallelism(4).Select(e => ToByteArray(e)).ToArray();
        }

        /// <summary>
        /// converts byte[][] to collection of objects of choise
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="S"></typeparam>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        public static IEnumerable<S> ToObject<S>(byte[][] byteArray)
            //where T : IEnumerable<S>, new()
        {
            //T result = new T();

            if (byteArray == null || byteArray.Count() == 0)
                yield break;
            foreach (var item in byteArray.AsParallel().Select(e => (S)ToObject(e)).AsEnumerable())
                yield return item;

            //var s = byteArray.AsParallel().Select(e => (S)ToObject(e));

            //foreach (var item in s.AsEnumerable())
            //{
            //    result.Add(item);
            //}

            //return result;
        }

    }
}
