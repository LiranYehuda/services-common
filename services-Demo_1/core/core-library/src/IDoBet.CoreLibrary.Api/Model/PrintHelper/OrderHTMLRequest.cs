﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace IDoBet.CoreLibrary.Api.Model.PrintHelper
{

    public class OrderHTMLRequest : BaseHTMLRequest
    {
        public OrderHTMLRequest(string token, string guid, bool forPrint, int? width = null, string lang = null, string template = null, string barcode = null, bool isReprint = false) : base(token, lang, template)
        {
            RequestGuid = guid;
            ForPrint = forPrint;
            Width = width;
            Barcode = barcode;
            IsReprint = isReprint;
        }

        public bool IsReprint { get; set; }
        public OrderHTMLRequest() { }
        public string RequestGuid { get; set; }
        public bool ForPrint { get; set; }
        public int? Width { get; set; }
        public string Barcode { get; set; }

        public override string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
