﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Api.Model.PrintHelper
{
    public abstract class BaseHTMLRequest
    {
        public BaseHTMLRequest()
        { }

        public BaseHTMLRequest(string userToken, string Lang, string template, string terminalName = "", string branchName = "", string cashierName = "", string currency = "")
        {
            UserToken = userToken;
            Language = Lang;
            Template = template;
            CashierName = cashierName;
            TerminalName = terminalName;
            BranchName = branchName;
            Currency = currency;

        }

        public string UserToken { get; set; }
        public string Language { get; set; }
        public string Template { get; set; }
        public string TerminalName { get; set; }
        public string CashierName { get; set; }
        public string BranchName { get; set; }
        public string Currency { get; set; }
        public abstract string ToJson();

    }
}
