﻿using IDoBet.CoreLibrary.Api.Helper;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace IDoBet.CoreLibrary.Api.Model.Sport
{
    public static class SportGameHandler
    {
        public static SportGame ReadFromDb(string connectionString, long id)
        {
            SportGame result = null;
            using (var sh = new SqlHandler(connectionString, "system.spGetSportGameById"))
            {
                sh.AddParameter("Id", SqlDbType.BigInt, id);
                if (sh.Read())
                    result = (SportGame)CompressedBinaryConverter.ToObject(sh.Get<byte[]>("BinaryData"));
            }
            return result;
        }

    }
}
