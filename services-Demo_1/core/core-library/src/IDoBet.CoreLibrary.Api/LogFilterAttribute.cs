﻿using System;
using System.Linq;

using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

using IDoBet.CoreLibrary.Logging;
using IDoBet.CoreLibrary.Logging.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Request;
using IDoBet.CoreLibrary.Logic.Model.Response;

namespace IDoBet.CoreLibrary.Api
{
    public class LogFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            try
            {
                if (((Microsoft.AspNetCore.Mvc.ObjectResult)context.Result).Value is BaseResponse res)
                {
                    var descriptor = context.ActionDescriptor;
                    var actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ActionName;
                    var ControllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ControllerName;
                    Logger.WriteLog(eLogLevel.Debug, string.Format("Response (IP : {3}) -> {0} -> {1} -> {2}", ControllerName, actionName, ControllerName == "Event" ? "" : JsonConvert.SerializeObject(res), context.HttpContext.Connection.RemoteIpAddress));
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(eLogLevel.Error, "", ex);
            }
            finally
            {
                base.OnActionExecuted(context);
            }

        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                var req = context.ActionArguments.FirstOrDefault(x => x.Value is BaseRequest).Value;
                if (req != null)
                {
                    var descriptor = context.ActionDescriptor;
                    var actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ActionName;
                    var ControllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ControllerName;
                    Logger.WriteLog(eLogLevel.Debug, string.Format("Request (IP : {3}) -> {0} -> {1} -> {2}", ControllerName, actionName, JsonConvert.SerializeObject(req), context.HttpContext.Connection.RemoteIpAddress));
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(eLogLevel.Error, "", ex);
            }
            finally
            {
                base.OnActionExecuting(context);
            }
        }
    }
}
