﻿using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace IDoBet.CoreLibrary.Api.ActionFilters
{
    public class RequestRateLimitAttribute : ActionFilterAttribute
    {
        public double Milliseconds { get; set; } = 500;
        private static MemoryCache Cache { get; } = new MemoryCache(new MemoryCacheOptions());
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            int? channelId = context.HttpContext.Request.Headers.ContainsKey("channelId") && !string.IsNullOrEmpty(context.HttpContext.Request.Headers["channelId"])
                  ? int.Parse(context.HttpContext.Request.Headers["channelId"].ToString())
                  : (int?)null;

            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["ApiId"].ToString(), out int ApiId);
            bool isBackofficeRequest = (channelId.HasValue && channelId.Value == (int)eChannel.BackOffice) || (ApiId == (int)eApi.BackOfficeApi); 
            if (!isBackofficeRequest)
            {                              
                if (!channelId.HasValue || channelId.Value != (int)eChannel.BackOffice)
                {
                    var descriptor = context.ActionDescriptor;
                    var actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ActionName?.ToLower();
                    var controllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)descriptor).ControllerName?.ToLower();

                    string token = context.HttpContext.Request.Headers.ContainsKey("token") && !string.IsNullOrEmpty(context.HttpContext.Request.Headers["token"])
                        ? context.HttpContext.Request.Headers["token"].ToString().ToLower()
                        : null;

                    int? brandId = context.HttpContext.Request.Headers.ContainsKey("brandId") && !string.IsNullOrEmpty(context.HttpContext.Request.Headers["brandId"])
                        ? int.Parse(context.HttpContext.Request.Headers["brandId"].ToString())
                        : (int?)null;

                    bool validateTooManyRequests = true;
                    if (brandId.HasValue)
                    {
                        var requestRateMillisecondsLimit = Configuration.GetValue(brandId.Value, eModule.Core, "requestRateMillisecondsLimit");
                        if (requestRateMillisecondsLimit != null && requestRateMillisecondsLimit.Value != null)
                            Milliseconds = double.Parse(requestRateMillisecondsLimit.Value);
                        else
                            validateTooManyRequests = false;

                        if (Milliseconds <= 1)
                            validateTooManyRequests = false;
                    }


                    if (token != null && validateTooManyRequests)
                    {
                        if (controllerName == "dynamic")
                            actionName = GetDynamicRequestMethod(context);

                        var memoryCacheKey = $"{token}_{actionName}";
                        if (!Cache.TryGetValue(memoryCacheKey, out bool _))
                        {
                            var cacheEntryOptions = new MemoryCacheEntryOptions()
                                .SetAbsoluteExpiration(TimeSpan.FromMilliseconds(Milliseconds));

                            Cache.Set(memoryCacheKey, true, cacheEntryOptions);
                        }
                        else
                        {
                            context.Result = new ContentResult
                            {
                                Content = $"Too many requests",
                            };

                            context.HttpContext.Response.StatusCode = 429; //HttpStatusCode.TooManyRequests
                        }
                    }
                }                
            }
        }

        private static string GetDynamicRequestMethod(ActionExecutingContext context)
        {
            var request = context.HttpContext.Request;
            var bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(request.Body))
            {
                bodyStr = reader.ReadToEnd();
                request.Body.Position = 0;
            }

            if (string.IsNullOrEmpty(bodyStr) && context.ActionArguments.ContainsKey("parameter"))
                bodyStr = context.ActionArguments["parameter"].ToString();

            using JsonDocument document = JsonDocument.Parse(bodyStr.ToLower(), new JsonDocumentOptions
            {
                AllowTrailingCommas = true,
            });

            return document.RootElement.GetProperty("methodname").ToString();
        }
    }
}
