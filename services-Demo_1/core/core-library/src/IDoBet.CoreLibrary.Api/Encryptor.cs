﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;

namespace IDoBet.CoreLibrary.Api
{
    public class Encryptions
    {
        public const string GS_PAYOUT_KEY = "GS_PAYOUT_KEY";
        public const string GS_DEPOSIT_WITHDRAW_KEY = "GS_DEPOSIT_WITHDRAW_KEY";
        public const string GS_REGISTER_DEPOSIT_PREPAID_USER_KEY = "GS_REGISTER_DEPOSIT_PREPAID_USER_KEY";
        public const string ORDER_KEY = "GS_ORDER_KEY";
        public const string ONLINE_USER_ACTIVATION_KEY = "ONLINE_ACTIVATION_KEY";
        public const string ONLINE_RECOVERY_KEY = "ONLINE_RECOVERY_KEY";
        public const string BEYONIC_DEPOSIT_KEY = "BEYONIC_DEPOSIT_KEY";
        public const string YELLOWBET_DEPOSIT_KEY = "YELLOWBET_DEPOSIT_KEY";
    }

    /// <summary>
    /// Gets adresses and machine data 
    /// </summary>
    /// <author>
    /// Guy Levin
    /// </author>
    public static class Encryptor
    {
        public static string SHAhash(string text)
        {
            if (text == null)
                return null;

            SHA256 sha256 = new SHA256CryptoServiceProvider();

            //compute hash from the bytes of text
            sha256.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = sha256.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }


    /// <summary>
    /// Encrypts plain text to DES3, also support writing cyphers to files, using a key
    /// </summary>
    /// <author>
    /// Guy Levin
    /// </author>
    public static class SymmetricEncryptor
    {
        //the length a key should be
        public const int KEY_LENGTH = 24;

        //sets a key to the proper length
        private static string GetKeyReady(string key)
        {
            key = key.Replace("-", "");
            key = key.Replace("_", "");
            key = key.Replace(".", "");

            while (key.Length < KEY_LENGTH) key = string.Concat(key, key);
            key = key.Substring(0, KEY_LENGTH);
            return key;
        }

        //returns encrypted cypher using a given key and plainText
        public static string Encrypt(string plainText, string key)
        {
            key = GetKeyReady(key);

            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(plainText);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            var str = Convert.ToBase64String(resultArray, 0, resultArray.Length);
            str = str.Replace('+', '-');
            str = str.Replace('/', '_');
            str = str.Replace('=', '!');
            return str;
        }

        //returns plainText using a given key and encrypted cypher
        public static string Decrypt(string cypher, string key)
        {

            key = GetKeyReady(key);

            cypher = cypher.Replace('-', '+');
            cypher = cypher.Replace('_', '/');
            cypher = cypher.Replace('!', '=');




            byte[] inputArray = Convert.FromBase64String(cypher);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        //returns encrypted cypher using a given key and plainText, and writes it to file in given path
        public static string EncryptToFile(string input, string key, string filePath)
        {
            using (FileStream fs = File.Create(filePath, 2048, FileOptions.None))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                string cypher = Encrypt(input, key);
                formatter.Serialize(fs, Encoding.Unicode.GetBytes(cypher));
                return cypher;
            }
        }        


        //returns plainText using a given key and filePath of cypher file
        public static string DecryptFromFile(string key, string filePath, out Boolean fileFound, out Boolean decryptionSuccess)
        {
            fileFound = false;
            decryptionSuccess = false;

            try
            {
                if (File.Exists(filePath))
                {
                    fileFound = true;

                    using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        byte[] fileData = (byte[])formatter.Deserialize(fs);
                        string cypher = Encoding.Unicode.GetString(fileData);
                        string plainText = Decrypt(cypher, key);
                        decryptionSuccess = true;
                        return plainText;
                    }
                }

                return null;
            }
            catch (Exception)
            {
                //TODO: LOG

                return null;
            }

        }

    }

}
