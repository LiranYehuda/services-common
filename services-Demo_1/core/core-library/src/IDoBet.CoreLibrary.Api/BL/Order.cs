﻿using IDoBet.CoreLibrary.Logic;
using IDoBet.CoreLibrary.Logic.Model.Data.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using IDoBet.CoreLibrary.Logic.Model.Request.Bet;
using IDoBet.CoreLibrary.Logic.Model.Response.Bet;
using IDoBet.CoreLibrary.SqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Data;

namespace IDoBet.CoreLibrary.Api.BL
{
    public static class Order
    {
        public static PlaceBetBasicResponse PlaceBetBasic(PlaceBetBasicRequest betRequest)
        {
            PlaceBetBasicResponse response = new PlaceBetBasicResponse();
            betRequest.GroupName = "Sport";
            betRequest.MethodName = "PlaceBetBasic";
            BasicOrder order = new BasicOrder();
            //try
            //{
            var configuration = Configuration.GetAction(betRequest.BrandId ?? Configuration.SystemBrandId, eModule.Game, betRequest.GroupName, betRequest.MethodName);
            if (configuration == null)
                throw new Exception("Not routed.");
            using (var sh = new SqlHandler(configuration.Endpoint.Value, configuration.Route.SpName))
            {
                sh.AddParameter("Token", SqlDbType.UniqueIdentifier, !string.IsNullOrWhiteSpace(betRequest.Token) ? (Guid?)Guid.Parse(betRequest.Token) : null);
                sh.AddParameter("Language", SqlDbType.NVarChar, betRequest.Language);
                sh.AddParameter("BrandId", SqlDbType.Int, betRequest.BrandId);
                sh.AddParameter("ChannelId", SqlDbType.Int, betRequest.ChannelId);
                sh.AddParameter("ExtraData", SqlDbType.NVarChar, betRequest.ExtraData?.ToString());
                sh.AddParameter("Terminal", SqlDbType.NVarChar, betRequest.Terminal);



                sh.AddParameter("Amount", SqlDbType.Float, betRequest.Amount);
                sh.AddParameter("MaxPayout", SqlDbType.Float, betRequest.MaxPayout);
                sh.AddParameter("isExecute", SqlDbType.Bit, betRequest.IsExecute);
                sh.AddParameter("ProdType", SqlDbType.TinyInt, betRequest.ProdType);
                sh.AddParameter("ExternalId", SqlDbType.NVarChar, betRequest.ExternalId);
                sh.AddParameter("ExternalData", SqlDbType.NVarChar, betRequest.ExternalData);
                sh.AddParameter("AuthorizationKey", SqlDbType.UniqueIdentifier, betRequest.AuthorizationKey);
                sh.AddParameter("ForUserId", SqlDbType.Int, betRequest.ForUserId);
                sh.AddParameter("UserInfoRequired", SqlDbType.Bit, betRequest.UserInfoRequired);
                sh.AddParameter("IsBasedOnBookingCode", SqlDbType.Bit, betRequest.IsBasedOnBookingCode);

                sh.AddOutputParameter("OrderId", SqlDbType.BigInt);
                sh.AddOutputParameter("RequestGuid", SqlDbType.UniqueIdentifier);
                sh.AddOutputParameter("BranchName", SqlDbType.NVarChar, 250);
                sh.AddOutputParameter("OrderNumber", SqlDbType.NVarChar, 50);
                sh.AddOutputParameter("CurrencyFactor", SqlDbType.Float);
                sh.AddOutputParameter("CurrencyCode", SqlDbType.NVarChar, 3);
                sh.AddOutputParameter("CurrencyId", SqlDbType.Int);
                sh.AddOutputParameter("UserId", SqlDbType.Int);
                sh.AddOutputParameter("CombinedEffect", SqlDbType.Float);
                sh.AddOutputParameter("BarCode", SqlDbType.NVarChar, 250);

                var dbResult = eErrorCode.DatabaseError;
                if (sh.Read())
                {
                    dbResult = (eErrorCode)sh.Get<int>("codeNumber");
                    if (dbResult == eErrorCode.Success && betRequest.IsExecute == true)
                    {
                        sh.NextResult();
                        if (sh.Read())
                        {

                            order.OrderNumber = sh.Get<string>("OrderNumber");
                            order.ExternalId = sh.Get<string>("ExternalId");
                            order.Barcode = sh.Get<string>("Barcode");
                            order.MaxPayout = sh.Get<double>("Payout");
                            order.CancelUntil = sh.Get<int>("CancelUntil");
                            order.OrderProd = (eProductType)sh.Get<byte>("ProdType");
                            order.TotalCommission = sh.Get<double?>("TotalCommission");
                            order.CommissionDesc = sh.Get<string>("CommissionDesc");
                            order.TotalBonus = sh.Get<double?>("TotalBonus");
                            order.BonusDesc = sh.Get<string>("BonusDesc");
                            order.TotalTax = sh.Get<double?>("TotalTax");
                            order.TaxDesc = sh.Get<string>("TaxDesc");
                            order.MaxPayout = sh.Get<double>("MaxPayout");
                            order.ActualBalance = sh.Get<double?>("ActualBalance");
                            order.BettableBonus = sh.Get<double?>("BettableBonus");
                            order.Bonus = sh.Get<double?>("Bonus");
                            order.RequestGuid = sh.Get<string>("RequestGuid");
                            order.TransactionId = sh.Get<long?>("TransactionId");

                            if (betRequest.UserInfoRequired)
                            {
                                var userInfo = sh.Get<string>("UserInfo");
                                response.UserInfo = !string.IsNullOrWhiteSpace(userInfo) ? JObject.Parse(userInfo) : null;
                            }
                        }
                        else
                        {
                            dbResult = (eErrorCode)eErrorCode.InternalServerError;
                        }
                    }
                }
                if (dbResult != eErrorCode.Success)
                {
                    response.SetErrorResult(dbResult);
                    return response;
                }
                sh.CloseReader();
                order.Id = (long)sh.GetParameterValue("OrderId");
                order.RequestGuid = ((Guid)sh.GetParameterValue("RequestGuid")).ToString();
                // order.RequestTimeout = order.OrderProd == eProductType.Live ? 8000 : 0;
                order.RequestTimeout = 0;
                order.BranchName = (string)sh.GetParameterValue("BranchName");
                order.OrderNumber = (string)sh.GetParameterValue("OrderNumber");
                order.CurrencyCode = (string)sh.GetParameterValue("CurrencyCode");
                order.CurrencyFactor = (double)sh.GetParameterValue("CurrencyFactor");
                order.CurrencyId = (int)sh.GetParameterValue("CurrencyId");
                order.CombinedEffect = (double)sh.GetParameterValue("CombinedEffect");
                order.Barcode = (string)sh.GetParameterValue("BarCode");
                var userId = sh.GetParameterValue("UserId");
                if (userId != null)
                    order.EmployeeId = (int)userId;
                if (order.CurrencyFactor == 0)
                {
                    order.CurrencyFactor = 10000d;
                }
                response.Order = order;
            }


            return response;
        }

        public static ExecuteBetBasicResponse ExecuteBetBasic(ExecuteBetBasicRequest betRequest)
        {
            ExecuteBetBasicResponse response = new ExecuteBetBasicResponse();
            betRequest.GroupName = "Sport";
            betRequest.MethodName = "ExecuteOrderBasic";
            BasicOrder order = new BasicOrder();
            //try
            //{
            var configuration = Configuration.GetAction(betRequest.BrandId ?? Configuration.SystemBrandId, eModule.Game, betRequest.GroupName, betRequest.MethodName);
            if (configuration == null)
                throw new Exception("Not routed.");
            using (var sh = new SqlHandler(configuration.Endpoint.Value, configuration.Route.SpName))
            {
                sh.AddParameter("Token", SqlDbType.UniqueIdentifier, !string.IsNullOrWhiteSpace(betRequest.Token) ? (Guid?)Guid.Parse(betRequest.Token) : null);
                sh.AddParameter("Language", SqlDbType.NVarChar, betRequest.Language);
                sh.AddParameter("BrandId", SqlDbType.Int, betRequest.BrandId);
                sh.AddParameter("ChannelId", SqlDbType.Int, betRequest.ChannelId);
                sh.AddParameter("ExtraData", SqlDbType.NVarChar, betRequest.ExtraData?.ToString());
                sh.AddParameter("Terminal", SqlDbType.NVarChar, betRequest.Terminal);

                sh.AddParameter("RequestGuid", SqlDbType.UniqueIdentifier,betRequest.RequestGuid);
                sh.AddParameter("LottoUserBetId", SqlDbType.NVarChar, betRequest.LottoUserBetId);
                sh.AddParameter("ExternalId", SqlDbType.NVarChar, betRequest.ExternalId);
                sh.AddParameter("ForUserId", SqlDbType.Int, betRequest.ForUserId);
                sh.AddParameter("TransactionExtraData", SqlDbType.NVarChar, betRequest.TransactionExtraData);
                sh.AddParameter("ExternalTransactionId", SqlDbType.NVarChar, betRequest.ExternalTransactionId);
                sh.AddParameter("UserInfoRequired", SqlDbType.Bit, betRequest.UserInfoRequired);
                
                var dbResult = eErrorCode.DatabaseError;
                if (sh.Read())
                {
                    dbResult = (eErrorCode)sh.Get<int>("codeNumber");
                    if (dbResult == eErrorCode.Success)
                    {
                        sh.NextResult();
                        if (sh.Read())
                        {
                            order.OrderNumber = sh.Get<string>("OrderNumber");
                            order.ExternalId = sh.Get<string>("ExternalId");
                            order.Barcode = sh.Get<string>("Barcode");
                            order.ActualPayout = sh.Get<double>("Payout");
                            order.CancelUntil = sh.Get<int>("CancelUntil");
                            order.OrderProd = (eProductType)sh.Get<byte>("ProdType");
                            order.TotalCommission = sh.Get<double?>("TotalCommission");
                            order.CommissionDesc = sh.Get<string>("CommissionDesc");
                            order.TotalBonus = sh.Get<double?>("TotalBonus");
                            order.BonusDesc = sh.Get<string>("BonusDesc");
                            order.TotalTax = sh.Get<double?>("TotalTax");
                            order.TaxDesc = sh.Get<string>("TaxDesc");
                            order.CurrencyFactor = sh.Get<double>("CurrencyFactor");
                            order.CombinedEffect = sh.Get<double>("CombinedEffect");
                            order.MaxPayout = sh.Get<double>("MaxPayout");
                            order.LottoUserBetId = sh.Get<int?>("LottoUserBetId");
                            order.EmployeeId = sh.Get<int?>("EmployeeId");
                            order.Id  = sh.Get<long>("OrderId");
                            order.ActualBalance = sh.Get<double?>("ActualBalance");
                            order.BettableBonus = sh.Get<double?>("BettableBonus");
                            order.Bonus = sh.Get<double?>("Bonus");
                            order.RequestGuid = sh.Get<string>("RequestGuid");
                            order.Payout = sh.Get<double>("Payout");
                            order.TransactionId = sh.Get<long?>("TransactionId");


                            if (betRequest.UserInfoRequired)
                            {
                                var userInfo = sh.Get<string>("UserInfo");
                                response.UserInfo = !string.IsNullOrWhiteSpace(userInfo) ? JObject.Parse(userInfo) : null;
                            }

                            if (order.CurrencyFactor == 0)
                            {
                                order.CurrencyFactor = 10000d;
                            }
                        }
                        else
                        {
                            dbResult = (eErrorCode)eErrorCode.InternalServerError;
                        }
                    }
                }
                if (dbResult != eErrorCode.Success)
                {
                    response.SetErrorResult(dbResult);
                    return response;
                }
                sh.CloseReader();                                
                response.Order = order;
            }


            return response;
        }

        public static CancelOrderResponse CancelOrder(CancelOrderRequest request)
        {

            CancelOrderResponse response = new CancelOrderResponse();
            if (request.OrderId.HasValue)
            {
                response = ValidateAndCancelOrderLotto(request);
                if (!response.IsSuccessfull)
                    return response;
            }
            request.GroupName = "Sport";
            request.MethodName = "CancelOrder";
            var configuration = Configuration.GetAction(request.BrandId ?? Configuration.SystemBrandId, eModule.Game, request.GroupName, request.MethodName);
            if (configuration == null)
                throw new Exception("Not routed.");
            using (var sh = new SqlHandler(configuration.Endpoint.Value, configuration.Route.SpName))
            {
                sh.AddParameter("Token", SqlDbType.UniqueIdentifier, !string.IsNullOrWhiteSpace(request.Token) ? (Guid?)Guid.Parse(request.Token) : null);
                sh.AddParameter("Language", SqlDbType.NVarChar, request.Language);
                sh.AddParameter("BrandId", SqlDbType.Int, request.BrandId);
                sh.AddParameter("ChannelId", SqlDbType.Int, request.ChannelId);
                sh.AddParameter("ExtraData", SqlDbType.NVarChar, request.ExtraData?.ToString());
                sh.AddParameter("Terminal", SqlDbType.NVarChar, request.Terminal);

                sh.AddParameter("OrderId", SqlDbType.BigInt, request.OrderId);
                sh.AddParameter("CanceledBy", SqlDbType.Int, request.CancelBy);
                sh.AddParameter("CanceledByToken", SqlDbType.Bit, request.CanceledByToken);
                sh.AddParameter("CancellationReason", SqlDbType.NVarChar, request.CancellationReason);
                sh.AddParameter("CancelReasonTypeId", SqlDbType.TinyInt, request.CancelReasonTypeId);
                sh.AddParameter("ExpectedProductIds", SqlDbType.NVarChar, request.ExpectedProductIds);
                sh.AddParameter("ExternalId", SqlDbType.NVarChar, request.ExternalId);
                sh.AddParameter("OutPutBalance", SqlDbType.Bit, request.OutPutBalance);

                var dbResult = eErrorCode.DatabaseError;
                if (sh.Read())
                {
                    dbResult = (eErrorCode)sh.Get<int>("codeNumber");
                    if (dbResult == eErrorCode.Success)
                    {
                        if (request.OutPutBalance)
                        {
                            sh.NextResult();
                            if (sh.Read())
                            {
                                response.ActualBalance = sh.Get<double?>("ActualBalance");
                                response.Bonus = sh.Get<double?>("Bonus");
                                response.BarCode = sh.Get<string>("Barcode");
                            }
                            else
                            {
                                dbResult = (eErrorCode)eErrorCode.InternalServerError;
                            }
                        }

                    }
                }
                if (dbResult != eErrorCode.Success)
                {
                    response.SetErrorResult(dbResult);
                    return response;
                }
                sh.CloseReader();
            }

            return response;
        }

        public static void UpdateLottoUserBetId(long orderId, int lottoUserBetId, string connectionString)
        {
            using (var sh = new SqlHandler(connectionString, "betting.spAddLottoUserBetId"))
            {
                sh.AddParameter("OrderId", SqlDbType.BigInt, orderId);
                sh.AddParameter("LottoUserBetId", SqlDbType.Int, lottoUserBetId);
                sh.ExecuteNonQuery();
            }
        }


        #region private methods

        private static GetCancelOrderResponse GetCancelOrder(GetCancelOrderRequest request)
        {
            GetCancelOrderResponse response = new GetCancelOrderResponse();
            request.GroupName = "Sport";
            request.MethodName = "GetOrderForCancel";
            var configuration = Configuration.GetAction(request.BrandId ?? Configuration.SystemBrandId, eModule.Game, request.GroupName, request.MethodName);
            if (configuration == null)
                throw new Exception("Not routed.");
            using (var sh = new SqlHandler(configuration.Endpoint.Value, configuration.Route.SpName))
            {
                sh.AddParameter("Token", SqlDbType.NVarChar, !string.IsNullOrWhiteSpace(request.Token) ? request.Token : null);
                sh.AddParameter("Language", SqlDbType.NVarChar, request.Language);
                sh.AddParameter("BrandId", SqlDbType.Int, request.BrandId);
                sh.AddParameter("ChannelId", SqlDbType.Int, request.ChannelId);
                sh.AddParameter("ExtraData", SqlDbType.NVarChar, request.ExtraData?.ToString());
                sh.AddParameter("Terminal", SqlDbType.NVarChar, request.Terminal);

                sh.AddParameter("OrderId", SqlDbType.BigInt, request.OrderId);
                sh.AddParameter("IsUndoCancel", SqlDbType.Bit, request.IsUndoCancel);

                var dbResult = eErrorCode.DatabaseError;
                if (sh.Read())
                {
                    dbResult = (eErrorCode)sh.Get<int>("codeNumber");
                    if (dbResult == eErrorCode.Success)
                    {
                        sh.NextResult();
                        if (sh.Read())
                        {
                            response.Order = new BasicOrder()
                            {
                                Id = request.OrderId,
                                BrandId = sh.Get<int>("BrandId"),
                                OrderProd = (eProductType)sh.Get<byte>("OrderProdType"),
                                LottoUserBetId = sh.Get<int?>("LottoUserBetId"),
                                EmployeeId = sh.Get<int?>("OrderUserId")
                            };
                        }
                        else
                        {
                            dbResult = (eErrorCode)eErrorCode.InternalServerError;
                        }
                    }
                }
                if (dbResult != eErrorCode.Success)
                {
                    response.SetErrorResult(dbResult);
                    return response;
                }
                sh.CloseReader();
            }

            return response;
        }

        private static CancelOrderLottoResponse CancelOrderLotto(CancelOrderLottoRequest request)
        {
            CancelOrderLottoResponse response = new CancelOrderLottoResponse();
            request.GroupName = "Lotto";
            request.MethodName = "CancelTicket";
            var configuration = Configuration.GetAction(request.BrandId ?? Configuration.SystemBrandId, eModule.Game, request.GroupName, request.MethodName);
            if (configuration == null)
                throw new Exception("Not routed.");
            using (var sh = new SqlHandler(configuration.Endpoint.Value, configuration.Route.SpName))
            {
                sh.AddParameter("Token", SqlDbType.UniqueIdentifier, !string.IsNullOrWhiteSpace(request.Token) ? (Guid?)Guid.Parse(request.Token) : null);
                sh.AddParameter("Language", SqlDbType.NVarChar, request.Language);
                sh.AddParameter("BrandId", SqlDbType.Int, request.BrandId);
                sh.AddParameter("ChannelId", SqlDbType.Int, request.ChannelId);
                sh.AddParameter("ExtraData", SqlDbType.NVarChar, request.ExtraData?.ToString());
                sh.AddParameter("Terminal", SqlDbType.NVarChar, request.Terminal);
                sh.AddParameter("UserId", SqlDbType.BigInt, request.UserId);
                sh.AddParameter("UserBetId", SqlDbType.Int, request.UserBetId);

                var dbResult = eErrorCode.DatabaseError;
                if (sh.Read())
                {
                    dbResult = (eErrorCode)sh.Get<int>("codeNumber");
                }
                if (dbResult != eErrorCode.Success)
                {
                    response.SetErrorResult(dbResult);
                    return response;
                }
                sh.CloseReader();
            }

            return response;
        }


        private static CancelOrderResponse ValidateAndCancelOrderLotto(CancelOrderRequest request)
        {
            CancelOrderResponse response = new CancelOrderResponse();
            var isCancel = request.CancelBy.HasValue || request.CanceledByToken;
            var getCancelOrderResponse = GetCancelOrder(new GetCancelOrderRequest()
            {
                BrandId = request.BrandId,
                ChannelId = request.ChannelId,
                ExtraData = request.ExtraData,
                IP = request.IP,
                Language = request.Language,
                OrderId = request.OrderId.Value,
                Terminal = request.Terminal,
                Token = request.Token,
                IsUndoCancel = !isCancel
            });

            if (!getCancelOrderResponse.IsSuccessfull)
            {
                response.SetErrorResult(getCancelOrderResponse.Result.ErrorCode, getCancelOrderResponse.Result.ErrorDescription, getCancelOrderResponse.Result.AdditionalInfo);
                return response;
            }

            if (getCancelOrderResponse.Order.OrderProd == eProductType.Lotto && (!request.CancelReasonTypeId.HasValue || request.CancelReasonTypeId.Value != 6))
            {
                if (!isCancel)
                {
                    response.SetErrorResult(eErrorCode.BadRequest, "Can't undo cancel lotto bet.");
                    return response;
                }
                if (getCancelOrderResponse.Order.LottoUserBetId == null)
                {
                    response.SetErrorResult(eErrorCode.BadRequest, "Invalid order.");
                    return response;
                }
                var cancelOrderLottoResponse = CancelOrderLotto(new CancelOrderLottoRequest()
                {
                    BrandId = request.BrandId,
                    ChannelId = request.ChannelId,
                    ExtraData = request.ExtraData,
                    IP = request.IP,
                    Language = request.Language,
                    Terminal = request.Terminal,
                    Token = request.Token,
                    UserBetId = getCancelOrderResponse.Order.LottoUserBetId.Value,
                    UserId = getCancelOrderResponse.Order.EmployeeId.Value
                });

                if (!cancelOrderLottoResponse.IsSuccessfull)
                {
                    response.SetErrorResult(getCancelOrderResponse.Result.ErrorCode, getCancelOrderResponse.Result.ErrorDescription, getCancelOrderResponse.Result.AdditionalInfo);
                    return response;
                }
            }

            return response;
        }

        #endregion        

    }
}
