﻿using System.Linq;
using System.Collections.Generic;

using IDoBet.CoreLibrary.Logic.Model.Configuration;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Concurrent;

namespace IDoBet.CoreLibrary.Logic
{
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
    public static class Configuration
    {
       
        public static readonly string ServerId = Guid.NewGuid().ToString();
        public static Action<string> Log { get; set; }
        public static string Instance { get; set; }
        public static readonly int SystemBrandId = 0;
        private static readonly object Lock = new object();

        private static readonly string DefaultScheme = "dbo";
        private static bool IsAutoMapped { get; set; } = false;
        //private static List<ApiEndpointConfiguration> Endpoints { get; set; }
        private static Dictionary<(int brandId, int moduleId, string instance, string key), ApiEndpointConfiguration> Endpoints { get; set; }

        private static List<ApiRouteConfiguration> Routes { get; set; }
        private static Dictionary<(int brandId, int moduleId, string instance, string key, int? channelId, int? branchId), ApiVariableConfiguration> Variables { get; set; }
        private static ConcurrentDictionary<(int brandId, int moduleId, string instance, string groupName, string methodName), ActionConfiguration> Actions =
            new ConcurrentDictionary<(int brandId, int moduleId, string instance, string groupName, string methodName), ActionConfiguration>();
        /*
ConfigVar: SportMetadata[0].brandids->[].[0]="10001"
ConfigEnv: SportMetadata[0].brandids->[].[0]="10001"

[1] - CfgEnv[SportRepository] - [0]->[][0] "RedisAddressFeed"="localhost:6379"

*/
        private static readonly bool EnableLog = false;
        private static void LogVariableRequest(int brandId, eModule module, string key, ApiVariableConfiguration response)
        {
            if (EnableLog) Log($"CfgVar[{module}] - [{brandId}]->[{response?.Instance}][{response?.BrandId}] \"{key}\"=\"{response?.Value}\""); else return;
        }
        private static void LogEndpointRequest(int brandId, eModule module, string key, ApiEndpointConfiguration response)
        {
            if (EnableLog) Log($"CfgEndp[{module}] - [{brandId}]->[{response?.Instance}][{response?.BrandId}] \"{key}\"=\"{response?.Value}\""); else return;
        }

        //Log($"Config request. BrandId: {brandId}, module: {module}, key: {key}, response: Instance: {response?.Instance}, BrandId: {response?.BrandId}, value: {response?.Value}");

        public static ActionConfiguration GetAction(int brandId, eModule module, string groupName, string methodName, string originGroupName = null) //originGroupName was add to support game api v12.6.2
        {
            groupName = groupName?.ToLower();
            methodName = methodName?.ToLower();


            //var result = Actions.ContainsKey((brandId, (int)module, Instance, groupName, methodName))
            //        ? Actions[(brandId, (int)module, Instance, groupName, methodName)]
            //        : Actions.ContainsKey((brandId, (int)module, null, groupName, methodName)) ? Actions[(brandId, (int)module, null, groupName, methodName)] : null;

            if (!Actions.TryGetValue((brandId, (int)module, Instance, groupName, methodName), out ActionConfiguration result))
                if (Actions.TryGetValue((brandId, (int)module, null, groupName, methodName), out result))
                    return result;            
            
            if (!Actions.TryGetValue((SystemBrandId, (int)module, Instance, groupName, methodName), out result))
                if (Actions.TryGetValue((SystemBrandId, (int)module, null, groupName, methodName), out result))
                    return result;
                         
            result = new ActionConfiguration
            {                
                Endpoint = GetEndpoint(brandId, module, groupName)
            };

            if (result.Endpoint == null)
                return null;

            result.Route = Routes.FirstOrDefault(x => x.BrandId == brandId && x.MethodName == methodName) ??
                            Routes.FirstOrDefault(x => x.BrandId == 0 && x.MethodName == methodName);

            if (result.Route == null) 
            {
                if (IsAutoMapped)
                {
                    result.Route = new ApiRouteConfiguration()
                    {
                        BrandId = brandId,
                        MethodName = methodName,
                        SpName = $"{originGroupName ?? DefaultScheme}.sp{methodName}"
                    };

                    Routes.Add(result.Route);
                }
                else
                {
                    result = null; // No routing exists
                }
            }

            if (result != null)
                Actions.TryAdd((brandId, (int)module, result.Endpoint.Instance, groupName, methodName), result);
            return result;
        }


        public static ApiVariableConfiguration GetValue(int brandId, eModule module, string key, int? channelId = null, int? branchId = null)
        {
            ApiVariableConfiguration result;
            var originalKey = key;
            key = key?.ToLower();

            if (Variables.ContainsKey((brandId, (int)module, Instance, key, channelId, branchId)))
                result = Variables[(brandId, (int)module, Instance, key, channelId, branchId)];
            else if (Variables.ContainsKey((brandId, (int)module, Instance, key, channelId, null)))
                result = Variables[(brandId, (int)module, Instance, key, channelId, null)];
            else if (Variables.ContainsKey((brandId, (int)module, null, key, channelId, branchId)))
                result = Variables[(brandId, (int)module, null, key, channelId, branchId)];
            else if (Variables.ContainsKey((brandId, (int)module, null, key, channelId, null)))
                result = Variables[(brandId, (int)module, null, key, channelId, null)];
            else
                result = null;

           
            if (result == null && brandId != SystemBrandId)
                return GetValue(SystemBrandId, module, key);
            LogVariableRequest(brandId, module, originalKey, result);
            return result;
        }

        public static ApiEndpointConfiguration GetEndpoint(int brandId, eModule module, string key)
        {
            var originalKey = key;
            key = key?.ToLower();
            var result = Endpoints.ContainsKey((brandId, (int)module, Instance, key))
                    ? Endpoints[(brandId, (int)module, Instance, key)]
                  : Endpoints.ContainsKey((brandId, (int)module, null, key)) ? Endpoints[(brandId, (int)module, null, key)] : (Endpoints.ContainsKey((brandId, (int)eModule.Core, null, key)) ? Endpoints[(brandId, (int)eModule.Core, null, key)] : null);
            if (result == null && brandId != SystemBrandId)
                return GetEndpoint(SystemBrandId, module, key);
            LogEndpointRequest(brandId, module, originalKey, result);
            return result;
        }

        public static void AddEnpoint(ApiEndpointConfiguration src)
        {
            if (Endpoints == null)
                Endpoints = new Dictionary<(int brandId, int moduleId, string instance, string key), ApiEndpointConfiguration>();
            Endpoints[(src.BrandId, src.ModuleId, src.Instance, src.Key)] = src;
        }

        public static void AddRoute(ApiRouteConfiguration src)
        {
            if (Routes == null)
                Routes = new List<ApiRouteConfiguration>();
            Routes.Add(src);
        }
        public static void AddVariable(ApiVariableConfiguration src)
        {
            if (Variables == null)
                Variables = new Dictionary<(int brandId, int moduleId, string instance, string key, int? channelId, int? branchId), ApiVariableConfiguration>();
            Variables[(src.BrandId, src.ModuleId, src.Instance, src.Key, src.ChannelId, src.BranchId)] = src;
        }
        public static void SetAutoMapped(bool isAutoMapped)
        {
            IsAutoMapped = isAutoMapped;
        }
    }
#endif
}
