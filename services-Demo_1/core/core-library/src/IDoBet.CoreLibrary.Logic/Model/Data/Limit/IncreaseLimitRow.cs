﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class IncreaseLimitRow
    {
        public long EventId { get; set; }
        public long? BetTypeId { get; set; }
        public double? MaxAmount { get; set; }

        public override string ToString()
        {
            if (BetTypeId == null)
            {
                if (MaxAmount == null)
                    return $"Event {EventId} limit increase.";
                else
                    return $"Event {EventId} limit change to {MaxAmount}.";
            }
            else
            {
                if (MaxAmount == null)
                    return $"Event {EventId}, BetType {BetTypeId} limit increase.";
                else
                    return $"Event {EventId}, BetType {BetTypeId} limit change to {MaxAmount}.";
            }
        }
    }
}
