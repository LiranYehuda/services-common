﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class OrderAuthorization
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? AuthorizationDate { get; set; }
        public string AuthorizationKey { get; set; }
        public DateTime? AuthorizationValidTo { get; set; }
        public eOrderAuthorizationStatus Status { get; set; }
        public List<OrderAuthorizationRow> Rows { get; set; }
        public int? AuthorizedBy { get; set; }
        public string AuthorizedByFirstName { get; set; }
        public string AuthorizedByLastName { get; set; }
        public long? SoldOrderId { get; set; }
    }
}
