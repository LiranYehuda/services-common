﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    [Serializable, DataContract]
    public class LimitResultRow
    {
        [DataMember]
        public long EventsDataId { get; set; }
        [DataMember]
        public long BetTypeId { get; set; }
        [DataMember]
        public double OriginalLimitation { get; set; }
        [DataMember]
        public double LimitationAfterBet { get; set; }
        [DataMember]
        public double BetTypeCap { get; set; }
        [DataMember]
        public long OrderRowId { get; set; }
        [DataMember]
        public long OrderBetId { get; set; }
        [DataMember]
        public double? LimitationBeforeBet { get; set; }
    }
}
