﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class LimitNotificationMessage
    {
        public string EventName { get; set; }
        public string BetTypeName { get; set; }
        public double LimitUsed { get; set; }
        public double MaxLimit { get; set; }
    }
}
