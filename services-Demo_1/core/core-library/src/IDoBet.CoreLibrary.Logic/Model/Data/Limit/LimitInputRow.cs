﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class LimitInputRow
    {
        public long EventsDataId { get; set; }
        public long BetTypeId { get; set; }
        public long OrderBetId { get; set; }
        public string OddName { get; set; }
        public string Line { get; set; }
        public double Price { get; set; }
    }
}
