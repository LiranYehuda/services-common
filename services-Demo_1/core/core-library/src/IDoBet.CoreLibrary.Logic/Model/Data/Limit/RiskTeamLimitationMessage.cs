﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class RiskTeamLimitationMessage : LimitResultRow
    {
        public long OrderId { get; set; }
        public string EventName { get; set; }
        public double BetTypeCapAfterBet { get; set; }
        public double Amount { get; set; }
        public string BranchName { get; set; }
        public List<long> Selections { get; set; }
    }
}
