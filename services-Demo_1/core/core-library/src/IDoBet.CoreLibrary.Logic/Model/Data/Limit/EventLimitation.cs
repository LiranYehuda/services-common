﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class EventLimitationToDb : EventLimitation
    {
        public double BetSummary { get; set; }
    }

    public class EventLimitation
    {
        public static Tuple<string, string>[] HashArray(EventLimitation src)
        {
            /*
             EventId.BetTypes
             EventId.Limits
             EventId.BetTypeId             
             */

            var result = new List<Tuple<string, string>>
            {
                //new Tuple<string, string>("Id", src.Id.ToString()),
                new Tuple<string, string>("BrandId", src.BrandId.ToString()),
                new Tuple<string, string>("CurrentLimit", src.Id.ToString()),
                new Tuple<string, string>("MaxLimit", src.Id.ToString()),
//                new Tuple<string, string>("Summaries", string.Join(";", src.Summaries?.Select(x => EventSummary.HashArray(x).Select( y => $"{y.Item1}:{y.Item2}")) )),
            };
            return result.ToArray();
        }

        public EventLimitation()
        {
            Summaries = new List<EventSummary>();
        }
        public EventLimitation ShallowCopy()
        {
            return (EventLimitation)this.MemberwiseClone();
        }
        
        public long Id { get; set; }
        public int BrandId { get; set; }
        public long EventsDataId { get; set; }
        public long BetTypeId { get; set; }
        public long OrderBetId { get; set; }
        // public bool IsLive => BetTypeId % 2 == 1;
        public double CurrentLimit { get; set; }
        public double? MaxLimit { get; set; }
        public int WinningOddCount { get; set; }
        
        public List<EventSummary> Summaries { get; set; }
        public double DiffToOriginal()
        {
            var maxSums = Summaries.OrderBy(x => x.Summary).Take(WinningOddCount).Sum(x => x.Summary);
            var otherSums = Summaries.OrderBy(x => x.Summary).Skip(WinningOddCount).Sum(x => x.Summary);
            return maxSums - otherSums;
        }
    }
}
