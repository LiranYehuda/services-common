﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class OrderAuthorizationRow
    {
        public long Id { get; set; }
        public long OrderBetId { get; set; }
        public double OriginalEventLimitation { get; set; }
        public double EventLimitationPlus { get; set; }
        public double OriginalBetTypeCap { get; set; }
        public double BetTypeCapPlus { get; set; }
        public double? NewPrice { get; set; }
        public double? NewAmount { get; set; }
    }
}
