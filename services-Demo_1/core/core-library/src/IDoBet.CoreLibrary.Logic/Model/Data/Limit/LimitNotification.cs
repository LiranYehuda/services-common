﻿using System;

using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class LimitNotification
    {
        public DateTime Created { get; set; }
        public long EventId { get; set; }
        public long BetTypeId { get; set; }
        public eLimitNotificationType NotificationType { get; set; }
        public LimitNotification ShallowCopy()
        {
            return (LimitNotification)MemberwiseClone();
        }
        public LimitNotificationMessage Message { get; set; }
    }
}
