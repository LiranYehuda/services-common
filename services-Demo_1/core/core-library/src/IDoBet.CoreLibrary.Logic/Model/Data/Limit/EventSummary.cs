﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Limit
{
    public class EventSummary
    {
        public long Id { get; set; }
        public long EventsDataId { get; set; }
        public long BetTypeId { get; set; }
        public string OddKey { get; set; }
        public decimal Price { get; set; }
        public string Line { get; set; }
        public double Summary { get; set; }

        public static Tuple<string, string>[] HashArray(EventSummary src)
        {
            var result = new List<Tuple<string, string>>
            {
                new Tuple<string, string>("OddKey", src.OddKey),
                new Tuple<string, string>("Line", src.Line),
                new Tuple<string, string>("Summary", src.Summary.ToString()),
            };
            return result.ToArray();
        }

        public EventSummary ShallowCopy()
        {
            return (EventSummary)this.MemberwiseClone();
        }
    }
}
