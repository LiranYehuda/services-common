﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class NextBetBonusOrder
    {
        public int Id { get; set; }
        public long OrderId { get; set; }
        public long OrderRowId { get; set; }
        public long? NextOrderId { get; set; }
        public double Bonus { get; set; }
        public DateTime ExperationTime { get; set; }
        public eNextBetBonusOrderStatus Status { get; set; }
        public NextBetBonusConfig NextBetBonus { get; set; }
    }
}
