﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class OrderRow
    {
        public List<OrderBet> Bets { get; set; }

        public eWinStatus WinStatus { get; set; }
        public long OrderId { get; set; }

        public long Id { get; set; }

        public double Amount { get; set; }
        public double? MaxPayout { get; set; }
        public double? Payout { get; set; }
        public double? NakedPayout { get; set; }
        public double? BonusAmount { get; set; }
        public double? RowBonusAmount { get; set; }        
        public NextBetBonusOrder NextBetBonusOrder { get; set; }        
        public int? NextBetBonusOrderId { get; set; }
        public double? CommissionAmount { get; set; }
        public double? CommissionLowAmount { get; set; }
        public double? TaxValue { get; set; }
        public double? TaxAmount { get; set; }
        public decimal? TotalRatio { get; set; }
        public double PendingRatio { get; set; }
        public int PendingSelection { get; set; }
        public double ProbabilityToWin { get; set; }
        public int TotalSelections { get; set; }
        // public Entity<int> UserCampaign { get; set; }

    }
}
