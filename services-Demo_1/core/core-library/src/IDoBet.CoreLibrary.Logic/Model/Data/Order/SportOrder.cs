﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class SportOrder : BasicOrder
    {        
        public List<OrderRow> OrderRows { get; set; }        
    }
}
