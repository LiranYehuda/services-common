﻿using IDoBet.CoreLibrary.Logic.Model.Data.Limit;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class OrderBet
    {
        public long Id { get; set; }
        public OddEntity Odd { get; set; }
        public long BetTypeId { get; set; }

        public SportGame Event { get; set; }
        public int? CouponId { get; set; }
        public int? CurrentHomeScore { get; set; }
        public int? CurrentAwayScore { get; set; }
        public long OrderRowId { get; set; }
        public long OrderId { get; set; }
//        public BasicUser UpdatedManuallyBy { get; set; }
        public string BetInfo { get; set; }
        public string LiveInfo { get; set; }
        public DateTime? MatchDate { get; set; }
        public bool IsBanker { get; set; }
        public decimal OriginalPrice { get; set; }
        public bool? IsPaused { get; set; } = false;
        public OrderAuthorizationRow AuthorizationRow { get; set; }

    }
}
