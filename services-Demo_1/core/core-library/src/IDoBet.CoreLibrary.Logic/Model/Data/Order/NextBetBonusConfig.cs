﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class NextBetBonusConfig
    {
        public int Id { get; set; }
        public int BrandId { get; set; }
        public int? MinSelection { get; set; }
        public double? MaxTotalOdd { get; set; }
        public double? MinStack { get; set; }
        public int? WinStatus { get; set; }
        public double BonusPercentage { get; set; }
        public double? MinBonus { get; set; }
        public double? MaxBonus { get; set; }
        public int? SupportedBranch { get; set; }
        public TimeSpan StartHour { get; set; }
        public TimeSpan EndHour { get; set; }
        public int? Product { get; set; }
        public int AMinSelection { get; set; }
        public double AMaxTotalOdd { get; set; }
        public double AMinStack { get; set; }
        public int? AProduct { get; set; }
        public int ExpirationTimeDays { get; set; }
    }
}
