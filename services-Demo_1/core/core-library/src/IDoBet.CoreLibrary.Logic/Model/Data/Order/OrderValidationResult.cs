﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class OrderValidationResult
    {
        public List<OrderBetValidationResult> OrderBetValidationResults { get; set; }
        public OrderValidationResult() { OrderBetValidationResults = new List<OrderBetValidationResult>(); }
        public void SetError(string message, eErrorCode validationResult = eErrorCode.ApiError, long? eventId = null, Object ExtraData = null)
        {
            OrderBetValidationResult bResult = new OrderBetValidationResult();
            bResult.SetError(message, validationResult,  new EventValidationResult(eventId) , ExtraData);
            OrderBetValidationResults.Add(bResult);
        }
    }
}
