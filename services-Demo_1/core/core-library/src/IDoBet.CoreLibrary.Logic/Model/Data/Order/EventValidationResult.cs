﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class EventValidationResult
    {
        public long? EventId { get; set; }
        public eEventStatus EventStatus { get; set; }
        public long BetTypeId { get; set; }        
        public string OddName { get; set; }
        public string OddLine { get; set; }
        public string BetTypeLine { get; set; }
        public double? OddPrice { get; set; }
        public eOddStatus OddStatus { get; set; }
        public string LiveTime { get; set; }
        public int? ScoreHomeTeam { get; set; }
        public int? ScoreAwayTeam { get; set; }
        public string Score { get; set; }
        public EventValidationResult(long? eventid)
        {
            EventId = eventid;
        }
    }
}
