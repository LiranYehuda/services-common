﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class BasicOrder
    {
        public long Id { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ReprintUntil { get; set; }
        public string OrderNumber { get; set; }
        public int? PoolGameId { get; set; }
        public string PoolGameName { get; set; }
        public int? LottoUserBetId { get; set; }
        public int PrintCount { get; set; }
        public bool IsInUserCampaign { get; set; }
        public string ExternalId { get; set; } 
        public long? TransactionId { get; set; }
        public string Barcode { get; set; }
        public int BrandId { get; set; }
        public bool? Printed { get; set; }
        public double Amount { get; set; }
        public double MaxPayout { get; set; }
        public double NakedPayout { get; set; }
        public double ActualPayout { get; set; }  

        public string BranchName { get; set; }
        public int CurrencyId { get; set; }
        public int? EmployeeId { get; set; }

        public double Payout { get; set; }

        //        public Branch Branch { get; set; }
        //        public SystemUser Employee { get; set; } // 04/04/2018 - Omri Kodzidlo
        public int CancelUntil { get; set; }  //needed
        public DateTime? CancelTime { get; set; }
//        public BasicUser CanceledBy { get; set; }
//        public CancelReasonType CancelReasonType { get; set; }
        public string CancellationReason { get; set; }
        public string CancelUserRole { get; set; }
        public DateTime? CheckoutTime { get; set; }
//        public SystemUser CheckedoutBy { get; set; }// 04/04/2018 - Omri Kodzidlo
//        public Branch CheckedoutBranch { get; set; }
//        public Terminal Terminal { get; set; }
        public string RequestGuid { get; set; }
        public int RequestTimeout { get; set; }
        public eOrderStatus Status { get; set; }
        public DateTime? LockedUntil { get; set; }
//        public int TotlaSelection { get; set; }
//        public int TotlaLines { get; set; }
        public eProductType OrderProd { get; set; }  
//        public eOrderType OrderType { get; set; }
        public eWinStatus WinStatus { get; set; }
        public DateTime? SettledTime { get; set; }
        public DateTime? SoldTime { get; set; }
        public long? ParentId { get; set; }
//        public Terminal CheckedoutTerminal { get; set; }
//        public BasicUser LockedBy { get; set; }
        public string LockedReason { get; set; }
//        public Currency OrderCurrency { get; set; }
        public DateTime ExpiryDate { get; set; }
        public double? TotalCommission { get; set; } 
        public string CommissionDesc { get; set; }  
        public double? TotalBonus { get; set; }  
        public string BonusDesc { get; set; }  
        public double? TotalTax { get; set; } 
        public string TaxDesc { get; set; }  
        public string AuthorizationKey { get; set; }
//        public OrderAuthorization Authorization { get; set; }
        public string CurrencyCode { get; set; }
        public double CurrencyFactor { get; set; }
        public double CombinedEffect { get; set; }
        public Guid? PreviousOrderGuid { get; set; }
        public bool IsBooking { get; set; }

//        public double PendingRatio { get; set; }
//        public int PendingSelection { get; set; }

//        public string ProductName { get; set; }
//        public virtual string ProductType { get; set; }
//        public double ProbabilityToWin { get; set; }
        public bool IsReprintable { get; set; }
//        public int ErrorCode { get; set; }
        public bool BranchAllowToPayout { get; set; }
        public string ExternalData { get; set; }
        public double? ActualBalance { get; set; }
        public double? BettableBonus { get; set; }
        public double? Bonus { get; set; }
        public DateTime? CancelTimePossibile
        {
            get
            {
                DateTime? result = null;
                if (CancelTime == null && !IsPayoutPossibile && CancelUntil > 0)
                    result = CreateTime.AddSeconds(CancelUntil);
                return result;

            }
        }
        public bool IsPayoutPossibile
        {
            get
            {
                return new List<eWinStatus>()
                {
                    eWinStatus.HalfLose,
                    eWinStatus.HalfWin,
                    eWinStatus.MoneyBack,
                    eWinStatus.Winner
                }.Contains(WinStatus) &&
                CheckoutTime == null &&
                CancelTime == null &&
                LockedUntil == null &&
                ExpiryDate > DateTime.Now &&
                BranchAllowToPayout;
            }
        }

    }
}
