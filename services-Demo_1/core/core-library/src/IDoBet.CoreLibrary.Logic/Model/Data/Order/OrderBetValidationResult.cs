﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Generic;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Order
{
    public class OrderBetValidationResult
    {

        public eErrorCode ValidationResult { get; set; }
        public string Message { get; set; }
        public EventValidationResult Event { get; set; }
        public object ExtraData { get; set; }
        public List<long> Selections { get; set; }
        public void SetError(string message, eErrorCode validationResult, EventValidationResult eve = null, Object ExtraData = null)
        {
            Message = message;
            ValidationResult = validationResult;
            Event = eve;
        }
    }

    
}
