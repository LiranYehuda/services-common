﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.User
{
    public class Access
    {        
        public int UserId { get; set; }
        public DateTime LoginTime { get; set; }
        public string LoginIp { get; set; }
        public int? LoginTerminalId { get; set; }
        public int? LoginFrom { get; set; }
        public DateTime? LogoutTime { get; set; }
        public int? LogoutType { get; set; }
        public Guid Token { get; set; }
        public int? LoginBranchId { get; set; }
        public long ID { get; set; }
        public long? ShiftId { get; set; }
        public string UserAgent { get; set; }
        public string UserDeviceType { get; set; }
        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public string ExternalId { get; set; }
        public string ExternalData { get; set; }
        public int BrandId { get; set; }
    }
}
