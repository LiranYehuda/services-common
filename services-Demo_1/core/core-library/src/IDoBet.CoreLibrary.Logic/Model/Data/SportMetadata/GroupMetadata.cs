﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata
{
    [Serializable]
    public class GroupMetadata : BaseMetadata
    {
        public LimitMetadata Limit { get; set; }
        public new GroupMetadata Clone()
        {
            var result = new GroupMetadata
            {
                Order = Order,
                IsActive = IsActive,
                ShortName = ShortName,
                Limit = Limit?.Clone()
            };
            return result;
        }
    }
}
