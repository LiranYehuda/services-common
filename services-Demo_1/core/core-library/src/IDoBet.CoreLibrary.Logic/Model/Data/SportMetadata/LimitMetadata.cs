﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata
{
    [Serializable]
    public class LimitMetadata
    {
        /// <summary>
        /// Set on sport/country/league/event level. Change only from backoffice
        /// </summary>
        public double? PreMatchLimit { get; set; }
        public double? LiveLimit { get; set; }
        /// <summary>
        /// Initially null, after each order it start decreasing from the above number
        /// </summary>
        public double? CurrentLimit { get; set; }
        public double? IncreaseLimitEffect { get; set; }
        public LimitMetadata Clone() =>
            new LimitMetadata
            {
                PreMatchLimit = PreMatchLimit,
                LiveLimit = LiveLimit,
                IncreaseLimitEffect = IncreaseLimitEffect,
                CurrentLimit = CurrentLimit
            };
    }
}
