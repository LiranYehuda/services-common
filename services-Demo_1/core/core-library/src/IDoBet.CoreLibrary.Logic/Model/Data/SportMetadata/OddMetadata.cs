﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata
{
    [Serializable]
    public class OddMetadata : BaseMetadata
    {
        public string Shortcut { get; set; }
        public double? FixPrice { get; set; }
        public double? LimitSummary { get; set; }
        public new OddMetadata Clone()
        {
            var result = new OddMetadata
            {
                Order = Order,
                IsActive = true,
                ShortName = ShortName,
                LimitSummary = LimitSummary,
                FixPrice = FixPrice,
                Shortcut = Shortcut != null ? string.Copy(Shortcut) : null
            };
            return result;
        }
    }
}
