﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata
{
    [Serializable]
    public class BetTypeMetadata : BaseMetadata
    {
        public string Shortcut { get; set; }
        public double? LimitEffect { get; set; }
        public double? LimitCapPreMatch { get; set; }
        public double? LimitCapLive { get; set; }
        public double? LimitLeft { get; set; }
        public double? MaxLimit { get; set; }
        public int? WinningOddCount { get; set; }
        public double? SuspendPriceUnder { get; set; }
        public double? ReducePriceOver { get; set; }
        public new BetTypeMetadata Clone() =>
            new BetTypeMetadata
            {
                Order = Order,
                IsActive = IsActive,
                ShortName = ShortName,
                Shortcut = Shortcut,
                LimitEffect = LimitEffect,
                LimitCapPreMatch = LimitCapPreMatch,
                LimitCapLive = LimitCapLive,
                LimitLeft = LimitLeft,
                SuspendPriceUnder = SuspendPriceUnder,
                ReducePriceOver = ReducePriceOver,                
                WinningOddCount = WinningOddCount
            };
    }
}
