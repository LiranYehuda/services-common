﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata
{
    [Serializable]
    public class BaseMetadata
    {
        public int? Order { get; set; }
        public bool IsActive { get; set; }
        public string ShortName { get; set; }
        public BaseMetadata Clone() =>
            new BaseMetadata
            {
                Order = Order,
                IsActive = IsActive,
                ShortName = ShortName
            };
    }
}
