﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class SportGameScore
    {
        public List<SportGameScoreRow> Scores { get; set; }

        public static Tuple<string, string>[] HashArray(SportGameScore src) =>
            src.Scores.Select(x => new Tuple<string, string>(x.IncidentKey, SportGameScoreRow.SerializeToRedis(x))).ToArray();
        public static SportGameScore Deserialize(Dictionary<string, string> src) =>       
             new SportGameScore { Scores = new List<SportGameScoreRow>(src.Select(x => SportGameScoreRow.Deserialize(x.Key, x.Value))) };

        public SportGameScore Clone() =>
            new SportGameScore { Scores = new List<SportGameScoreRow>(Scores) };

            

    }
}
