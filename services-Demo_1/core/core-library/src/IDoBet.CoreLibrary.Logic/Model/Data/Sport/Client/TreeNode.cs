﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport.Client
{
    public class TreeNode
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public string Counts { get; set; }
   
        public List<TreeNode> Children { get; private set; }

        public TreeNode(string id, string name)
            : this(id, name, new TreeNode[0])
        { }

        public TreeNode(string node, string name, params TreeNode[] children)
        {
            Id = node;
            Name = name;
            Children = new List<TreeNode>(children);
        }
    }
}
