﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public struct IndexEntry
    {
        public long e { get; set; } // event id
        public long? b { get; set; } // bettype id
        public bool? r { get; set; } // is result
    }
}
