﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class PoolOdd
    {
        public long Id { get; set; }
        public string DisplayName { get { return ShortName ?? Name; } }    
        public string ShortName { get; set; }
        public string Name { get; set; }
        public int OrderRow { get; set; }
        public int OrderCol { get; set; }
        public decimal Price { get; set; }
        //public eWinStatus? WinStatus { get; set; }
        public string Shortcut { get; set; }
    }
}
