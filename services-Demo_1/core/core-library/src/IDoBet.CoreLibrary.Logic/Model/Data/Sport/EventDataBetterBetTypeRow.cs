﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class EventDataBetterBetTypeRow
    {
        public long Id { get; set; }
        public long ProviderKey { get; set; }
        public long SportTypeId { get; set; }
        public bool Lineable { get; set; }
        public string BetTypeName { get; set; }
        public string PeriodKey { get; set; }
        public string IncidentKey { get; set; }
    }
}
