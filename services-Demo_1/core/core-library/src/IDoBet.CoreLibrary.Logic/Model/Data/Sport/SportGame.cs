﻿using System;
using System.Linq;
using System.Collections.Generic;
using IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using IDoBet.CoreLibrary.Logic.Model.Data.Virtual;
using System.Globalization;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    // Order fields: source, branded

    [Serializable]
    public class SportGame : BaseEntity
    {
        // public static long ConvertId(long srcId, int providerId, int providerSourceId) => long.Parse($"{providerId}0{providerSourceId}0{srcId}");

        public DateTime Expire => GetExpiry(EventDate);
        public static DateTime GetExpiry(DateTime? eventDate) =>
            eventDate == null || eventDate < DateTime.Now ? DateTime.UtcNow.AddDays(2) : eventDate.Value.ToUniversalTime().AddDays(2);

        public void SetFeedInitial(DateTime date)
        {

        }
        private DateTime? LastUpdateField { get; set; }
        public DateTime? LastUpdate { get { return LastUpdateField ?? BetTypes?.Max(x => x.LastUpdate); } set { LastUpdateField = value; } }

        public DateTime? EventDate { get; set; }
        public long? EventStatusId { get; set; }
        public BaseEntity SportType { get; set; }
        public BaseEntity Country { get; set; }
        public BaseEntity League { get; set; }
        public Team HomeTeam { get; set; }
        public Team AwayTeam { get; set; }
        public bool? IsAllResulted { get; set; }
        public List<BetType> BetTypes { get; set; }
        public bool IsRunning { get; set; }

        public SportGameScore LiveScore { get; set; }

        public Incident LastIncident { get; set; }

        public bool? HasLiveIncidents { get; set; }

        public LiveData Live { get; set; }
        public int? ProviderId { get; set; }
        public int? ProviderSourceId { get; set; } // for example: DelaSport early feed, IDoBet SqlDb, etc.
        public long? LiveCenterId { get; set; }

        public long? StatisticsId { get; set; }

        // public DateTime? Resulted => BetTypes?.SelectMany(x => x.Odds).Max(y => y.Resulted);
        public static Tuple<string, string>[] HashArray(SportGame src)
        {
            var result = new List<Tuple<string, string>>();
            if (src.ProviderId != null)
                result.Add(new Tuple<string, string>("ProviderId", src.ProviderId.ToString()));
            if (src.ProviderSourceId != null)
                result.Add(new Tuple<string, string>("ProviderSourceId", src.ProviderSourceId.ToString()));
            if (src.EventDate != null)
                result.Add(new Tuple<string, string>("EventDate", src.EventDate?.ToString()));
            if (src.EventStatusId != null)
                result.Add(new Tuple<string, string>("EventStatusId", src.EventStatusId.ToString()));
            if (src.IsAllResulted != null)
                result.Add(new Tuple<string, string>("IsAllResulted", src.IsAllResulted.ToString()));
            if (src.SportType != null)
                result.Add(new Tuple<string, string>("SportTypeId", src.SportType.Id.ToString()));
            if (src.Country != null)
                result.Add(new Tuple<string, string>("CountryId", src.Country.Id.ToString()));
            if (src.League != null)
                result.Add(new Tuple<string, string>("LeagueId", src.League.Id.ToString()));
            if (src.HomeTeam != null)
                result.Add(new Tuple<string, string>("HomeTeamId", src.HomeTeam.Id.ToString()));
            if (src.AwayTeam != null)
                result.Add(new Tuple<string, string>("AwayTeamId", src.AwayTeam.Id.ToString()));
            if (src.LastUpdate != null)
                result.Add(new Tuple<string, string>("LastUpdate", src.LastUpdate.Value.ToString()));
            if (src.LiveCenterId != null)
                result.Add(new Tuple<string, string>("LiveCenterId", src.LiveCenterId.ToString()));
            if (src.HasLiveIncidents != null)
                result.Add(new Tuple<string, string>("HasLiveIncidents", src.HasLiveIncidents.ToString()));
            if (src.StatisticsId != null)
                result.Add(new Tuple<string, string>("StatisticsId", src.StatisticsId.ToString()));
            if (src.Live != null)
                result.Add(new Tuple<string, string>("Live", $"{src.Live.Period},{src.Live.Minute},{src.Live.HomeScore},{src.Live.AwayScore},{src.Live.HomeRedCards},{src.Live.AwayRedCards}"));
            result.Add(new Tuple<string, string>("IsRunning", src.IsRunning.ToString()));
            return result.ToArray();
        }
        private static readonly char[] Separator = new char[] { ',' };
        public static SportGame Deserialize(Dictionary<string, string> src)
        {
            SportGame result = CreateSportGame(src);
            if (result == null)
                return null;

            foreach (var key in src.Keys)
                switch (key)
                {
                    case "ProviderId":
                        result.ProviderId = int.Parse(src[key]);
                        break;
                    case "ProviderSourceId":
                        result.ProviderSourceId = int.Parse(src[key]);
                        break;
                    case "EventDate":
                        result.EventDate = DateTime.ParseExact(
                      src[key],
                      "M/d/yyyy h:mm:ss tt",
                      CultureInfo.InvariantCulture);

                       
                        break;
                    case "EventStatusId":
                        result.EventStatusId = long.Parse(src[key]);
                        break;
                    case "SportTypeId":
                        result.SportType = new BaseEntity { Id = long.Parse(src[key]) };
                        break;
                    case "CountryId":
                        result.Country = new BaseEntity { Id = long.Parse(src[key]) };
                        break;
                    case "LeagueId":
                        result.League = new BaseEntity { Id = long.Parse(src[key]) };
                        break;
                    case "HomeTeamId":
                        result.HomeTeam = new Team { Id = long.Parse(src[key]) };
                        break;
                    case "AwayTeamId":
                        result.AwayTeam = new Team { Id = long.Parse(src[key]) };
                        break;
                    case "IsRunning":
                        result.IsRunning = bool.Parse(src[key]);
                        break;
                    case "IsAllResulted":
                        result.IsAllResulted = bool.Parse(src[key]);
                        break;
                    case "LastUpdate":
                        result.LastUpdate =  DateTime.ParseExact(
                        src[key],
                        "M/d/yyyy h:mm:ss tt",
                        CultureInfo.InvariantCulture);                        
                        break;
                    case "LiveCenterId":
                        result.LiveCenterId = long.Parse(src[key]);
                        break;
                    case "HasLiveIncidents":
                        result.HasLiveIncidents = bool.Parse(src[key]);
                        break;                          
                    case "StatisticsId":
                        result.StatisticsId = long.Parse(src[key]);
                        break;
                    case "Live":
                        var srcFields = src[key].Split(Separator);
                        result.Live = new LiveData
                        {
                            Period = srcFields[0],
                            Minute = int.Parse(srcFields[1])
                        };
                        if (srcFields.Length > 2)
                        {
                            result.Live.HomeScore = int.Parse(srcFields[2]);
                            result.Live.AwayScore = int.Parse(srcFields[3]);
                            result.Live.HomeRedCards = int.Parse(srcFields[4]);
                            result.Live.AwayRedCards = int.Parse(srcFields[5]);
                        }
                        break;
                }
            return result;
        }

        public int ClientBetTypeCount { get; set; }

        public new SportGame Clone()
        {
            var result = CreateSportGameClone(this);
            result.Id = Id;
            result.Name = Name;
            result.EventDate = EventDate;
            result.EventStatusId = EventStatusId;
            result.SportType = SportType?.Clone();
            result.Country = Country?.Clone();
            result.League = League?.Clone();
            result.HomeTeam = HomeTeam?.Clone();
            result.AwayTeam = AwayTeam?.Clone();
            result.LiveCenterId = LiveCenterId;
            result.ProviderId = ProviderId;
            result.ProviderSourceId = ProviderSourceId;
            result.IsRunning = IsRunning;
            result.HasLiveIncidents = HasLiveIncidents;
            result.LastUpdateField = LastUpdateField;
            result.StatisticsId = StatisticsId;
            result.Live = Live?.Clone();
            result.LiveScore = LiveScore?.Clone();
            result.LastIncident = LastIncident?.Clone();
            if ((BetTypes?.Count ?? 0) > 0)
                result.ClientBetTypeCount = BetTypes.Count(x => x.ClientStatus != 2);
            return result;
        }

        private static SportGame CreateSportGameClone(SportGame src)
        {
            switch (src.ProviderId)
            {
                case 4:
                    switch (src.SportType?.Id)
                    {
                        case 431:
                            return ((VirtualSoccer)src).Clone();
                        case 432:
                        case 433:
                            return ((VirtualRace)src).Clone();
                        case 434:
                            return ((VirtualFootballLeague)src).Clone();
                    }
                    break;
            }
            return new SportGame();
        }

        public LimitMetadata Limit { get; set; }
        // public SportGameCustomization Customization { get; set; }

        public static bool IsLive(SportGame src) =>
            (src.EventStatusId ?? (int)eEventStatus.Unknown) == (int)eEventStatus.Live;

        public int GameNumber { get; set; }        

        public int CouponId { get; set; }

        private static SportGame CreateSportGame(Dictionary<string, string> src)
        {
            if (src == null)
                return null;
            if (!src.ContainsKey("ProviderId") || !src.ContainsKey("SportTypeId"))
                return null;
            var providerId = src["ProviderId"];
            var sportTypeId = src["SportTypeId"];
            switch (providerId)
            {
                case "6":
                case "5":
                    return new SportGame();
                case "4":
                    switch (sportTypeId)
                    {
                        case "431":
                            return VirtualSoccer.Deserialize(src);
                        case "432":
                        case "433":
                            return VirtualRace.Deserialize(src);
                        case "434":
                            return VirtualFootballLeague.Deserialize(src);
                    }
                    break;
            }
            return null;
        }
    }
}
