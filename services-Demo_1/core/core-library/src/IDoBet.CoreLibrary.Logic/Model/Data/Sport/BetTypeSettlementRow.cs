﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class BetTypeSettlementRow
    {
        public long EventId { get; set; }
        public long BetTypeId { get; set; }
        public string Line { get; set; }
        public int? BrandId { get; set; }
        public bool? IsRemoveManual { get; set; }
    }
}
