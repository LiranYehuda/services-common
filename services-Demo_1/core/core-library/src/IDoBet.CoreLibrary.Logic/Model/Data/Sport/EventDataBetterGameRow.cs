﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class EventDataBetterGameRow
    {
        public int ProviderId { get; set; }
        public int ProviderSourceId { get; set; }
        public BaseEntity SportType { get; set; }
        public BaseEntity Country { get; set; }
        public BaseEntity League { get; set; }
        public BaseEntity Team { get; set; }
        public static EventDataBetterGameRow Create(SportGame src) =>
            new EventDataBetterGameRow
            {
                SportType = src.SportType,
                Country = src.Country,
                League = src.League,
                ProviderId = src.ProviderId ?? 0,
                ProviderSourceId = src.ProviderSourceId ?? 0
            };
    }
}
