﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class PoolGame
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public long BetTypeId { get; set; }
        public string BetTypeName { get; set; }
        public string Line { get; set; }
        public long SportTypeId { get; set; }
        public string SportTypeName { get; set; }
        public double? BetAmountPerLine { get; set; }
        public int CurrencyId { get; set; }
        public string CurrencyCode { get; set; }
        public int? MaxLines { get; set; }
        public bool AllowDownloadLines { get; set; }
        public DateTime? SettleDate { get; set; }
        public int PoolTemplateId { get; set; }
        public string PoolTemplateName { get; set; }
        public int Status { get; set; }
        public bool IsCorrectScore { get { return BetTypeId == 5010120107 || BetTypeId == 3332310  /*correct score*/; } }
        public List<PoolPrize> Prizes { get; set; }
        public List<PoolGameEvent> Events { get; set; }
        public bool HasWinning => Prizes?.Any(x => x.WinLineCount > 0) ?? false;
        public bool DisplayOdds { get; set; }
        public int ReserveCount { get; set; }
        public int EventCount { get; set; }
    }
}
