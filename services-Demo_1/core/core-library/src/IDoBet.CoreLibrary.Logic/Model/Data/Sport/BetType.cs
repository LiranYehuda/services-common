﻿using System;
using System.Linq;
using System.Collections.Generic;
using IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class BetType : BaseEntity
    {    
        public string PeriodKey { get; set; }
        public string IncidentKey { get; set; }
        // public string Line { get; set; }
        public long MarketId { get; set; }
        public DateTime? LastUpdate { get; set; }        
        public List<OddEntity> Odds { get; set; }
        public new BetType Clone() =>
            new BetType
            {
                Id = Id,
                Name = Name,
                PeriodKey = PeriodKey,
                IncidentKey = IncidentKey,
                // Line = Line,
                MarketId = MarketId,
                LastUpdate = LastUpdate,
                Odds = new List<OddEntity>(Odds?.Select(x => x.Clone()))
            };
        public static long GetId(int providerId, int providerSourceId, long periodId, long incidentId, long marketId) =>
            long.Parse($"{providerId}0{providerSourceId}0{periodId}0{incidentId}0{marketId}");

        public new BetTypeMetadata Metadata { get; set; }
        public override string ToString() =>            
            $"{Id} [{Odds.Count}] {Name}";


        public string ClientName =>
            Metadata?.ShortName == null ? Name : Metadata.ShortName;
        public int? ClientOrder =>
            Metadata?.Order;

        /*
    public enum eBetStatus
    {
        Open = 0,
        Suspended = 1,
        Closed = 2,
        Deleted = 3,
        Unknown = -1,
        Canceled = 4
    }         

              */
        // if no odds -> closed
        // if there are at least 1 open odd -> open
        // if there are at least 1 suspended odd -> supsended
        // else closed
        public int ClientStatus =>
            (Odds?.Count ?? 0) == 0
                ? 2
                : Odds.Any(x => x.OddStatusId == (int)eOddStatus.Open)
                        ? 0
                        : Odds.Any(x => x.OddStatusId == (int)eOddStatus.Suspended)
                                ? 1
                                : 2;

        public bool AllOddsResulted =>
            !(Odds?.Any(x => x.WinStatusId == (int)eWinStatus.Pending) ?? false);
        //public override string ToString() =>
        //    Line == null ?
        //    $"{Id} [{Odds.Count}] {Name}"
        //    : $"{Id}/{Line} [{Odds.Count}] {Name}";
    }
}
