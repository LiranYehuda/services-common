﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
    public class SportGameCustomizationInDb
    {
        public double? PreMatchLimit { get; set; }
        public double? LiveLimit { get; set; }
        public SportGameScore Score { get; set; }
        public long? EventStatusId { get; set; }
        public List<SportGameCustomizationRow> Odds { get; set; }
        public static SportGameCustomizationInDb ConvertToDb(SportGameCustomization src) =>
            src == null ? null :
            new SportGameCustomizationInDb
            {
                PreMatchLimit = src.PreMatchLimit,
                LiveLimit = src.LiveLimit,
                Score = src.Score,
                EventStatusId = src.EventStatusId,
                Odds = ConvertRowToDb(src)
            };
        public static SportGameCustomization ConvertFromDb(SportGameCustomizationInDb src) =>
            src == null ? null :
            new SportGameCustomization
            {
                PreMatchLimit = src.PreMatchLimit,
                LiveLimit = src.LiveLimit,
                Score = src.Score,
                EventStatusId = src.EventStatusId,
                Odds = ConvertRowFromDb(src.Odds)
            };
        private static List<SportGameCustomizationRow> ConvertRowToDb(SportGameCustomization src) =>
            src.Odds == null ? null :
            new List<SportGameCustomizationRow>(src.Odds.Select(x => new SportGameCustomizationRow
            {
                BetTypeId = x.Key.betTypeId,
                Line = x.Key.line,
                OddKey = x.Key.oddKey,
                Customization = new OddCustomization
                {
                    OddStatusId = x.Value.OddStatusId,
                    Price = x.Value.Price,
                    Resulted = x.Value.Resulted,
                    Settled = x.Value.Settled,
                    WinStatusId = x.Value.WinStatusId
                }
            }));
        private static Dictionary<(long betTypeId, string oddKey, string line), OddCustomization> ConvertRowFromDb(List<SportGameCustomizationRow> src)
        {
            if (src == null)
                return null;
            var Odds = new Dictionary<(long betTypeId, string oddKey, string line), OddCustomization>();
            foreach (var odd in src)
                Odds.Add((odd.BetTypeId, odd.OddKey, odd.Line), new OddCustomization
                {
                    OddStatusId = odd.Customization.OddStatusId,
                    Price = odd.Customization.Price,
                    Resulted = odd.Customization.Resulted,
                    Settled = odd.Customization.Settled,
                    WinStatusId = odd.Customization.WinStatusId
                });
            return Odds;
        }

    }
#endif
}
