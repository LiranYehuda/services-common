﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class Team : BaseEntity
    {
        public static long ConvertId(long srcId, int providerId, int providerSourceId) => long.Parse($"{providerId}0{providerSourceId}0{srcId}");
        public int Score { get; set; }
        public int RedCards { get; set; }
        public new Team Clone() =>        
            new Team
            {
                Id = Id,
                Name = Name,
                Score = Score,
                RedCards = RedCards
            };
        
    }
}
