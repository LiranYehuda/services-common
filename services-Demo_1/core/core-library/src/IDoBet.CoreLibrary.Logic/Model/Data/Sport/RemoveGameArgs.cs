﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class RemoveGameArgs : EventArgs
    {
        public long Id { get; }
        public RemoveGameArgs(long id)
        {
            Id = id;
        }
    }
}
