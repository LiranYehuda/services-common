﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public struct InitOddRecord
    {
        public string OddName { get; set; }
        public string Line { get; set; }
        public int OddStatusId { get; set; }
    }
}
