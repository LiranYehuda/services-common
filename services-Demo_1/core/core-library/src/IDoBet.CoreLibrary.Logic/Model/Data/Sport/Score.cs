﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class Score
    {
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        public override string ToString()
        {
            return string.Format("{0}-{1}", HomeScore, AwayScore);
        }
    }
}
