﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class SportGameCustomizationRow
    {
        public long BetTypeId { get; set; }
        public string OddKey { get; set; }
        public string Line { get; set; }
        public OddCustomization Customization { get; set; }
    }
}
