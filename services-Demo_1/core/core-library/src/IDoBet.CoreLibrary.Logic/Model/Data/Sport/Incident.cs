﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class Incident
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public string XPosition { get; set; }
        public string YPosition { get; set; }
        public bool IsHomeTeam { get; set; }
        public int TypeId { get; set; }

        public bool ForceSuspend { get; set; }

        public static Tuple<string, string>[] HashArray(Incident src) => new Tuple<string, string>[]
        {
            new Tuple<string, string>("Created", RedisConstant.DateTimeToStr(src.Created)),
            new Tuple<string, string>("XPosition", src.XPosition),
            new Tuple<string, string>("YPosition", src.YPosition),
            new Tuple<string, string>("Name", src.Name),
            new Tuple<string, string>("TypeId", src.TypeId.ToString()),
            new Tuple<string, string>("IsHomeTeam", src.IsHomeTeam.ToString()),
            new Tuple<string, string>("ForceSuspend", src.ForceSuspend.ToString())
        };
        public static Incident Deserialize(Dictionary<string, string> src)
        {
            if (src == null)
                return null;
            var result = new Incident();
            foreach (var key in src.Keys)
            {
                switch (key)
                {
                    case "Created":
                        result.Created = RedisConstant.DateTimeFromStr(src[key]);
                        break;
                    case "XPosition":
                        result.XPosition = src[key];
                        break;
                    case "YPosition":
                        result.YPosition = src[key];
                        break;
                    case "Name":
                        result.Name = src[key];
                        break;
                    case "TypeId":
                        result.TypeId = int.Parse(src[key]);
                        break;
                    case "IsHomeTeam":
                        result.IsHomeTeam = bool.Parse(src[key]);
                        break;
                    case "ForceSuspend":
                        result.ForceSuspend = bool.Parse(src[key]);
                        break;
                }
            }
            return result;
        }

        public Incident Clone() =>
            new Incident
            {
                Id = Id,
                Created = Created,
                IsHomeTeam = IsHomeTeam,
                Name = Name,
                TypeId = TypeId,
                XPosition = XPosition,
                YPosition = YPosition,
                ForceSuspend = ForceSuspend
            };

    }
}
