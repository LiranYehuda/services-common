﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
    public class SportGameCustomization
    {
        public double? PreMatchLimit { get; set; }
        public double? LiveLimit { get; set; }
        public SportGameScore Score { get; set; }
        public long? EventStatusId { get; set; }
        public Dictionary<(long betTypeId, string oddKey, string line), OddCustomization> Odds { get; set; }
    }
#endif
}
