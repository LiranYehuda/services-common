﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class ResultLogRow
    {
        public int PeriodInSeconds { get; set; }
        public int RangeInSeconds { get; set; }
        public DateTime Started { get; set; }
        public DateTime? Finished { get; set; }

        //public List<IndexEntry> Changes { get; set; }

        static string Field(string name, object val, bool quoted) => $"\"{name}\":" + (quoted ? $"\"{val}\"" : $"{val}");
        static T Val<T>(string field) => (T)Convert.ChangeType(field.Split(':')[1], typeof(T));

        public static string ToJson(ResultLogRow src) =>
            $"{{{Field("PeriodInSecond", src.PeriodInSeconds, false)}," +
            $"{Field("RangeInSeconds", src.RangeInSeconds, false)}," +
            $"{Field("Started", src.Started, true)}," +
            $"{Field("Finished", src.Finished, true)}" +
            // $",{Field("Changes", string.Join(",", src.Changes.Select(x => $"{x.e}" + (x.b != null ? $",{x.b.Value}" :""))), false)}" +
            $"}}";

        public static ResultLogRow FromJson(string src)
        {
            if (src == null)
                return null;
            var result = new ResultLogRow();
            var fields = src.Substring(1, src.Length - 2).Split(',');
            foreach (var field in fields)
                switch (field.Split(':')[0].Trim())
                {
                    case "\"PeriodInSecond\"":
                        result.PeriodInSeconds = Val<int>(field);
                        break;
                    case "\"RangeInSeconds\"":
                        result.RangeInSeconds = Val<int>(field);
                        break;
                    case "\"Started\"":
                        result.Started = Val<DateTime>(field);
                        break;
                    case "\"Finished\"":
                        result.Finished = (field.Split(':')[1] == "") ? null :  Val<DateTime?>(field);
                        break;
                }
            return result;
        }
    }
}

