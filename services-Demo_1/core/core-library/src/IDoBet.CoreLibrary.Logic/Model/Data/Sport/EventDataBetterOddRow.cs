﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class EventDataBetterOddRow 
    {
        public int ProviderId { get; set; }
        public int ProviderSourceId { get; set; }
        public long SportTypeProviderKey { get; set; }
        public long BetTypeProviderKey { get; set; }
        public BaseEntity Odd { get; set; }
        public bool IsLineable { get; set; }
        public bool IsLive { get; set; }
        public string Line { get; set; }
        public string WinPeriod { get; set; }
    }
}
