﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class OddCustomization
    {
        public int? OddStatusId { get; set; }
        public int? WinStatusId { get; set; }
        public double? Price { get; set; }
        public DateTime? Resulted { get; set; }
        public DateTime? Settled { get; set; }
    }
}
