﻿
namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class ResultOdd
    {
        // public long OddId { get; set; }
        public int WinStatusId { get; set; }

        // public long BetTypeId { get; set; }
        public string OddKey { get; set; }
        public string Line { get; set; }
    }
}
