﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public static class RedisConstant
    {
        private static List<string> MustHaveTtlKeys = new List<string> { ".Event", ".BetType", ".MarketMap", ".LiveScore", ".Incident" };
        public static bool IsEventKey(string key) =>
            key?.EndsWith(".Event") ?? false;

        public static bool MustHaveTtl(string key) =>
            MustHaveTtlKeys.Any(x => key.EndsWith(x));
        public static readonly int MinimumEventHashLength = 10;

        public static DateTime IncidentExpire = DateTime.Now.AddDays(2);
        public static string EventLockKey(long eventId) => $"{eventId}.EventLock";
        public static string EventKey(long eventId) => $"{eventId}.Event";
        public static long EntryId(string redisKey) => long.Parse(redisKey.Substring(0, redisKey.IndexOf('.')));
        public static string EventSearchPattern(int providerId, int providerSourceId) => $"{providerId}0{providerSourceId}0*.Event";
        public static string BetTypeSearchPattern(int providerId, int providerSourceId) => $"{providerId}0{providerSourceId}0*.BetType";
        public static string BetTypeKey(long eventId) => $"{eventId}.BetType";
        public static string MarketMapKey(long eventId) => $"{eventId}.MarketMap";
        public static string LiveScoreKey(long eventId) => $"{eventId}.LiveScore";
        public static string IncidentKey(long liveCenterId) => $"{liveCenterId}.Incident";
        public static string SportTypeKey(long sportTypeId) => $"{sportTypeId}.SportType";
        public static string CountryKey(long countryId) => $"{countryId}.Country";
        public static string LeagueKey(long leagueId) => $"{leagueId}.League";
        public static string TeamKey(long teamId) => $"{teamId}.Team";

        public static string FeedInitialKey => "FeedInitial";
        public static string FeedInitialField(int providerId, eEventStatus status) => $"{providerId}.{((int)status)}";
        public static string FeedInitialField(int providerId, int providerSourceId, eEventStatus status) => $"{providerId}.{providerSourceId}.{((int)status)}";

        // Processed
        public static string UpdateMessageStreamKey => "UpdateStream";
        public static int UpdateStreamLength => 10_000;

        public static int LogStreamLength => 10_000;
        
        // Diagnositcs
        public static string UpdateMessageListKey => "UpdateMessages";
        public static long UpdateMessageListLength => 10_000;

        // Raw input
        public static string LogMessageStreamKey => "SocketMessages";

        // Last processed stream id
        public static string ProcessServiceIndex => "ProcessServiceIndex";

        public static string DateTimeToStr(DateTime src) => src.ToString("yyyy-MM-dd HH:mm:ss.fff"); // "2019-08-08 14:02:53.979"
        public static DateTime DateTimeFromStr(string src) => DateTime.Parse(src);


        #region TEST
        public static string TestStreamKey => "TestStream";
        public static int TestStreamLength => 10_000;
        public static string TestControlKey = "TestControl";
        #endregion
    }
}
