﻿using IDoBet.CoreLibrary.Logic.Model.Request.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class EventApiCommandArgs : EventArgs
    {
        public EventApiCommandRequest Request { get; }
        public EventApiCommandArgs(EventApiCommandRequest request)
        {
            Request = request;
        }
    }
}
