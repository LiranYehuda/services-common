﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)

using System.Text.Json;
using System.Text.Json.Serialization;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport.Client
{
    public class ClientSportGame
    {

        public static string ToUpdateEvents(IEnumerable<SportGame> src, long timeStamp, int[] activeCouponIds)
        {
            var result = "";
            var filterClosedBetTypes = false;
            // updateMessages.value : array of object
            // key: updateEvents
            // value: array of object
            //  data, bts, ck
            var options = new JsonWriterOptions
            {
                SkipValidation = true
            };
            using (var stream = new MemoryStream())
            {
                using (var writer = new Utf8JsonWriter(stream, options))
                {
                    writer.WriteStartObject();

                    writer.WriteStartArray("updateMessages");
                    writer.WriteStartObject();
                    writer.WriteString("key", "updateEvents");
                    writer.WriteStartArray("value");
                    foreach (var game in src)
                        WriteSportGame(writer, game, filterClosedBetTypes);
                    writer.WriteEndArray();
                    writer.WriteString("time", DateTime.UtcNow);
                    // writer.WriteNumber("messageId")
                    writer.WriteEndObject();
                    writer.WriteEndArray();

                    WriteFooter(writer, timeStamp, activeCouponIds);
                    writer.WriteEndObject();
                }
                result = Encoding.UTF8.GetString(stream.ToArray());
            }
            return result;
        }
        public static string ToGetEvents(IEnumerable<SportGame> src, long timeStamp, long totalEventCount, int[] activeCouponIds)
        {
            var result = "";
            var filterClosedBetTypes = true;
            // events: array of object
            //   // data, bts, ck, gn, isEnable
            var options = new JsonWriterOptions
            {
                SkipValidation = true
            };
            using (var stream = new MemoryStream())
            {
                using (var writer = new Utf8JsonWriter(stream, options))
                {
                    // writer.WriteString()
                    var eventCount = 0;
                    writer.WriteStartObject();
                    writer.WriteStartArray("events");

                    foreach (var game in src)
                    {
                        ++eventCount;
                        WriteSportGame(writer, game, filterClosedBetTypes);
                    }
                    writer.WriteEndArray();

                    #region Response data
                    /*
                        "eventCount": 804,
                        "totalEventCount": 804,
                        "lastTimestamp": 1561445985093,
                        "activeCoupinIds": [
                            819
                        ],
                        "result": null,
                        "isSuccessfull": true,
                        "userInfo": null,
                                         */
                    writer.WriteNumber("eventCount", eventCount);
                    writer.WriteNumber("totalEventCount", totalEventCount);
                    WriteFooter(writer, timeStamp, activeCouponIds);
                    #endregion

                    writer.WriteEndObject();
                }

                result = Encoding.UTF8.GetString(stream.ToArray());
            }
            return result;
        }

        private static void WriteFooter(Utf8JsonWriter writer, long timeStamp, int[] activeCouponIds)
        {
            writer.WriteNumber("lastTimestamp", timeStamp);
            writer.WriteStartArray("activeCoupinIds");
            if (activeCouponIds != null)
                foreach (var couponId in activeCouponIds)
                    writer.WriteNumberValue(couponId);
            writer.WriteEndArray();
            writer.WriteBoolean("isSuccessfull", true);
        }


        private static void WriteSportGame(Utf8JsonWriter writer, SportGame game, bool filterClosedBetTypes)
        {
            writer.WriteStartObject();
            #region Events data
            /*
                            x   "state": 1,
                            x   "status": 0,
                            x   "time": "2019-06-25T08:00:00Z",
                            x   "book": false,
                            x   "topEvent": null,
                            +   "mtd": {
                            x       "btg": "g7,g10",
                            x       "pt": null
                            +   },
            */
            writer.WriteStartObject("data");
            writer.WriteNumber("id", game.Id);
            writer.WriteNumber("btCount", game.ClientBetTypeCount);
            writer.WriteNumber("t", GetEventType(game));
            writer.WriteNumber("state", 1); // Not started, todo
            writer.WriteNumber("status", game.EventStatusId ?? 0);
            writer.WriteNumber("couponId", game.CouponId);
            if ((game.StatisticsId ?? 0) > 0)
                writer.WriteNumber("statId", game.StatisticsId.Value);

            if (game.EventStatusId == (int)eEventStatus.Live
                && game.Live != null)
            {
                if (game.LiveCenterId != null)
                    writer.WriteNumber("liveCenterId", game.LiveCenterId.Value);
                writer.WriteStartObject("live");
                writer.WriteString("time", game.Live.ClientTime);
                writer.WriteStartObject("score");
                writer.WriteNumber("home", game.Live.HomeScore);
                writer.WriteNumber("away", game.Live.AwayScore);
                writer.WriteEndObject();
                writer.WriteNumber("homeRedCards", game.Live.HomeRedCards);
                writer.WriteNumber("awayRedCards", game.Live.AwayRedCards);
                writer.WriteBoolean("clockPaused", false);
                writer.WriteEndObject();
            }

            if (game.SportType != null)
            {
                writer.WriteString("sportName", game.SportType.Name);
                writer.WriteNumber("sportId", game.SportType.Id);
            }
            if (game.Country != null)
            {
                writer.WriteString("countryName", game.Country.Name);
                writer.WriteNumber("countryId", game.Country.Id);
            }
            if (game.League != null)
            {
                writer.WriteString("leagueName", game.League.Name);
                writer.WriteNumber("leagueId", game.League.Id);
            }
            if (game.HomeTeam != null)
                writer.WriteString("home", game.HomeTeam.Name);
            if (game.AwayTeam != null)
                writer.WriteString("away", game.AwayTeam.Name);
            if (game.EventDate.HasValue)
                writer.WriteString("time", game.EventDate.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"));
            writer.WriteBoolean("book", false);
            // writer.WriteString("topEvent", null);
            writer.WriteStartObject("mtd");
            writer.WriteNumber("so", game.SportType?.Metadata?.Order ?? 0);
            writer.WriteNumber("co", game.Country?.Metadata?.Order ?? 0);
            writer.WriteNumber("lo", game.League?.Metadata?.Order ?? 0);
            // BetTypesGroup
            // writer.WriteNumber("btg", game.SportType?.Metadata?.Order ?? 0);
            // PeriodLength
            // writer.WriteNumber("pt", game.SportType?.Metadata?.Order ?? 0);
            writer.WriteEndObject();

            writer.WriteEndObject();
            #endregion
            #region Bettypes
            /*
        "data": {
            "name": "2 Way",
            "status": 0,
            "line": null,
            "id": 320350,
            "order": 10,
            "md": "p1-o-g7"
        },
        "ck": "320350",
        "odds": [
            {
                "ck": "1",
                "name": "1",
                "price": "2.35",
                "status": 0,
                "shortcut": "1",
                "orderRow": null,
                "orderCol": null
            }
        ],
        "betable": true

             */
            writer.WriteStartArray("bts");
            short? ordertmp;
            if ((game.BetTypes?.Count ?? 0) > 0)
                foreach (var betType in game.BetTypes.Where(x => filterClosedBetTypes == false || x.ClientStatus != 2))
                {
                    if ((betType?.Odds?.Count ?? 0) > 0)
                    {
                        foreach (var oddGroup in betType.Odds.GroupBy(x => x.Line))
                        {
                            WriteBetType(writer, betType, oddGroup.Key);
                            writer.WriteStartArray("odds");
                            foreach (var odd in oddGroup)
                            {
                                writer.WriteStartObject();
                                writer.WriteString("ck", odd.OddKey);
                                writer.WriteString("name", odd.ClientOddName);
                                writer.WriteString("price", odd.ClientPrice);
                                writer.WriteNumber("status", odd.OddStatusId);
                                if (odd.ClientShortcut != null)
                                    writer.WriteString("shortcut", odd.ClientShortcut);
                                ordertmp = odd.ClientOrderRow;
                                if (ordertmp != null)
                                    writer.WriteNumber("orderRow", ordertmp.Value);

                                ordertmp = odd.ClientOrderCol;
                                if (ordertmp != null)
                                    writer.WriteNumber("orderCol", ordertmp.Value);

                                writer.WriteEndObject();
                            }
                            writer.WriteEndArray();
                            writer.WriteEndObject();
                        }
                    }
                    else
                    {
                        WriteBetType(writer, betType, null);
                        writer.WriteEndObject();
                    }

                }
            writer.WriteEndArray();
            #endregion
            writer.WriteString("ck", game.Id.ToString());
            writer.WriteString("gn", game.GameNumber.ToString());
            writer.WriteBoolean("isEnable", true);
            /*
                "ck": "318510392",
                "gn": "98",
                "isEnable": true
            */
            writer.WriteEndObject();
        }

        public static string ToSearchEvents(List<SportGame> src)
        {
            var result = "";
            var filterClosedBetTypes = true;
            // events: array of object
            //   // data, bts, ck, gn, isEnable
            var options = new JsonWriterOptions
            {
                SkipValidation = true
            };
            using (var stream = new MemoryStream())
            {
                using (var writer = new Utf8JsonWriter(stream, options))
                {                
                    var eventCount = 0;
                    writer.WriteStartObject();
                    writer.WriteStartArray("events");

                    foreach (var game in src)
                    {
                        ++eventCount;
                        WriteSportGame(writer, game, filterClosedBetTypes);
                    }
                    writer.WriteEndArray();                  

                    writer.WriteEndObject();
                }

                result = Encoding.UTF8.GetString(stream.ToArray());
            }
            return result;
        }

        private static void WriteBetType(Utf8JsonWriter writer, BetType betType, string line)
        {
            writer.WriteStartObject();
            writer.WriteStartObject("data");
            writer.WriteNumber("id", betType.Id);
            writer.WriteString("name", betType.ClientName);
            if (!string.IsNullOrWhiteSpace(line))
                writer.WriteString("line", line);
            if (betType.ClientOrder != null)
                writer.WriteNumber("order", betType.ClientOrder.Value);
            //else
            //    writer.WriteString("order", (string)null);
            // writer.WriteString("md", (string)null);
            writer.WriteNumber("status", betType.ClientStatus);
            if (betType.Metadata?.Order != null)
                writer.WriteNumber("order", betType.Metadata.Order.Value);
            writer.WriteEndObject();
            writer.WriteString("ck", $"{betType.Id}{(string.IsNullOrEmpty(line) ? "" : $"_{line}")}");
            writer.WriteBoolean("betable", betType.ClientStatus != 2);
        }

        private static int GetEventType(SportGame src) =>
            ((src.ProviderId ?? 5) == 4) ? 1 : 0;

       
    }
}
#endif