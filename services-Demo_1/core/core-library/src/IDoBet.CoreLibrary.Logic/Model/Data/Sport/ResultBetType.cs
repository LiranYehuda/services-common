﻿using System.Collections.Generic;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public struct ResultBetType
    {
        public long EventId { get; set; }
        public long BetTypeId { get; set; }
        public List<ResultOdd> Odds { get; set; }        
        public long PeriodId { get; set; }
        public long IncidentId { get; set; }
        public long MarketId { get; set; }
    }
}
