﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class PoolPrize
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double InitialPrize { get; set; }
        public double PercentFromBet { get; set; }
        public double PrizeFromSales { get; set; }
        public double PrizeFromPrevious { get; set; }
        public int MaximumMistakes { get; set; }
        public int WinLineCount { get; set; }
        public double PrizePerLine { get; set; }
        public bool IsDistributed { get; set; }
        public double CurrentPrize => InitialPrize + PrizeFromSales + PrizeFromPrevious;
    }
}
