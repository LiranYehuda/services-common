﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class PoolGameEvent
    {
        public int Id { get; set; }
        public long EventId { get; set; }
        public DateTime EventDate { get; set; }
       // public long CountryId { get; set; }
        public string CountryName { get; set; }
       // public long LeagueId { get; set; }
        public string LeagueName { get; set; }
       // public long HomeTeamId { get; set; }
        public string HomeTeamName { get; set; }
       // public long AwayTeamId { get; set; }
        public string AwayTeamName { get; set; }
        public int EventStatusId { get; set; }
        public int ShowOrder { get; set; }
        public string WinnerOdd { get; set; }
        public List<PoolOdd> Odds { get; set; }
        //public Dictionary<ePeriod, Score> Result { get; set; }
        public Dictionary<ePeriod, Score> Result { get; set; }
        public long? StatisticId { get; set; }
    }
}
