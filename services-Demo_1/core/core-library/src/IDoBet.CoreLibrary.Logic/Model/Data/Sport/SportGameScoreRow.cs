﻿using System;
using System.Collections.Generic;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class SportGameScoreRow
    {
        public long PeriodId { get; set; }
        /// <summary>
        /// 1 = goals, 2 = corners
        /// </summary>
        public long IncidentId { get; set; }
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        /// <summary>
        /// goals, goals_first-to-score_0.00, goals_next_3.00
        /// </summary>
        public string IncidentKey { get; set; }
        /// <summary>
        /// away or home or ?
        /// </summary>
        public string FirstTeam { get; set; }
        /// <summary>
        /// away or home or ?
        /// </summary>
        public string LastTeam { get; set; }

        public static string SerializeToRedis(SportGameScoreRow src) =>
            $"{src.PeriodId},{src.IncidentId},{src.HomeScore},{src.AwayScore}";

        public static SportGameScoreRow Deserialize(string incidentKey, string content)
        {
            var result = new SportGameScoreRow();
            var tmp = content.Split(',');
            result.PeriodId = long.Parse(tmp[0]);
            result.IncidentId = long.Parse(tmp[1]);
            result.HomeScore = int.Parse(tmp[2]);
            result.AwayScore = int.Parse(tmp[3]);
            result.IncidentKey = incidentKey;
            return result;
        }

    }
}