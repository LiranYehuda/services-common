﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class BetTypeSettlementOddRow
    {
        public long EventId { get; set; }
        public long BetTypeId { get; set; }
        public string Line { get; set; }
        public string OddName { get; set; }
        public int WinStatusId { get; set; }
    }
}
