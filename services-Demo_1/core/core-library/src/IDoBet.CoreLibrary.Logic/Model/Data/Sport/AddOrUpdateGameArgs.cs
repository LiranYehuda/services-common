﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class AddOrUpdateGameArgs : EventArgs
    {
        public SportGame Cached { get; }
        public SportGame Updates { get; }
        public AddOrUpdateGameArgs(SportGame cached, SportGame updates)
        {
            Cached = cached;
            Updates = updates;
        }
    }
}
