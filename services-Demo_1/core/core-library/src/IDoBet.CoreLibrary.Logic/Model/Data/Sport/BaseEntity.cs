﻿using IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class BaseEntity
    {
        public static long ConvertId(long srcId, int providerId, int providerSourceId) => long.Parse($"{providerId}0{providerSourceId}0{srcId}");
        public static long ConvertId(string srcId, int providerId, int providerSourceId) => long.Parse($"{providerId}0{providerSourceId}0{srcId}");
        public static long ConvertCompositeId(long parentId, long srcId, int providerId, int providerSourceID) =>
            ConvertId($"{parentId}0{srcId}", providerId, providerSourceID);
        public static bool IdMatchToSource(long id, int providerId, int providerSourceId) => id.ToString().StartsWith($"{providerId}0{providerSourceId}0");
        public static long ProviderKey(long id) => long.Parse(id.ToString().Substring(4));

        public long Id { get; set; }
        public string Name { get; set; }
        public BaseEntity Clone() =>
            new BaseEntity
            {
                Id = Id,
                Name = Name
            };
        public BaseMetadata Metadata { get; set; }
    }
}
