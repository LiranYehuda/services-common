﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class SportGameLog
    {
        public long Id { get; set; }        
        public DateTime CreateTime { get; set; }
        // public BasicUser CreatedBy { get; set; }
        public string LogInfo { get; set; }
        public long EventId { get; set; }
    }
}
