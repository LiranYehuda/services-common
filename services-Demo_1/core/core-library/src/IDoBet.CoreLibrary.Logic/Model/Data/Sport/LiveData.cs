﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class LiveData
    {
        public string Period { get; set; }
        public int Minute { get; set; }
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        public int HomeRedCards { get; set; }
        public int AwayRedCards { get; set; }
        public LiveData Clone() =>
            new LiveData
            {
                Period = Period,
                Minute = Minute,
                HomeRedCards = HomeRedCards,
                AwayRedCards = AwayRedCards, 
                HomeScore = HomeScore,
                AwayScore = AwayScore
            };
        public string ClientTime
        {
            get
            {
                if (Minute == 0)
                    if (Period == "H_TIME")
                        return "HT";
                    else
                        return Period;
                else
                    return $"{Period}{Minute}'";
            }
        }
    }
}
