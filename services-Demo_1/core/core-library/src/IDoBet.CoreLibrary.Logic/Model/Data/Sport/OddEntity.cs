﻿using System;

using IDoBet.CoreLibrary.Logic.Helper;
using IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    [Serializable]
    public class OddEntity
    {
        private static bool CompareLines(string srcLine, string dstLine)
        {
            var tmpSrc = (srcLine ?? "").ToLower().Trim();
            var tmpDst = (dstLine ?? "").ToLower().Trim();
            if (tmpSrc == tmpDst)
                return true;
            double dblSrc;
            if (tmpSrc == "")
                dblSrc = 0;
            else
                if (!double.TryParse(tmpSrc, out dblSrc))
                dblSrc = 0;

            double dblDst;
            if (tmpDst == "")
                dblDst = 0;
            else
                if (!double.TryParse(tmpDst, out dblDst))
                dblDst = 0;

            //if (tmpSrc != "" && tmpDst != "")
            //{
            //    if (double.TryParse(tmpSrc, out double dblSrc)
            //        && double.TryParse(tmpDst, out double dblDst))
            //        if (dblSrc == dblDst)
            //            return true;
            //}
            return dblSrc == dblDst;
        }
        public static bool CompareOdds(string srcKey, string srcLine, string dstKey, string dstLine) =>
            (srcKey ?? "").ToLower().Trim() == (dstKey ?? "").ToLower().Trim()
            && CompareLines(srcLine, dstLine);
            //(srcLine ?? "").ToLower().Trim() == (dstLine ?? "").ToLower().Trim();

        private long SourceId { get; set; }
        public long Id
        {
            get;
            set;
        }
        public string OddKey
        {
            get;
            set;
        }
        public long MarketId { get; set; }

        private string m_Line;
        public string Line
        {
            get => m_Line ?? "";
            set => m_Line = (value ?? "");
        }


        public double? Price { get; set; }
        public int OddStatusId { get; set; }
        public int WinStatusId { get; set; }

        public DateTime? Resulted { get; set; }
        //        public DateTime? Settled { get; set; }

        public bool IsCustomized  { get; set; }

        public OddEntity Clone() =>
            new OddEntity
            {
                Id = Id,
                OddKey = OddKey,
                MarketId = MarketId,
                Line = Line,
                Price = Price,
                OddStatusId = OddStatusId,
                WinStatusId = WinStatusId,
                Resulted = Resulted,
                IsCustomized = IsCustomized
            };

        private static float GetLine(string line) =>
            string.IsNullOrEmpty(line) ? 0 : float.Parse(line);

        public static long GetId(long betTypeId, string oddKey, string line) =>
            Math.Abs(HashGenerator.GetInt64HashCode($"{betTypeId},{oddKey},{GetLine(line)}"));

        public OddMetadata Metadata { get; set; }
        // public OddCustomization Customization { get; set; }

        public string ClientOddName =>
            (Metadata != null && !string.IsNullOrEmpty(Metadata.ShortName)) ? Metadata.ShortName : OddKey;

        public string ClientPrice => $"{Price, 0:0.00}";

        public string ClientShortcut => Metadata?.Shortcut;

        private short DecompressRow(int order) => (short)(order >> 16);
        private short DecompressCol(int order) => (short)(order & 0xffff);

        public short? ClientOrderRow => Metadata?.Order == null
            ? null
            : DecompressRow(Metadata.Order.Value) == -1 
                ? (short?)null 
                : DecompressRow(Metadata.Order.Value);
        public short? ClientOrderCol => Metadata?.Order == null
            ? null
            : DecompressCol(Metadata.Order.Value) == -1
                ? (short?)null
                : DecompressCol(Metadata.Order.Value);

    }
}
