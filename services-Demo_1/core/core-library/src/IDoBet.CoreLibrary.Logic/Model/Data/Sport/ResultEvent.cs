﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Sport
{
    public class ResultEvent
    {
        public long Id { get; set; }
        public DateTime EventDate { get; set; }        
        public SportGameScore Score { get; set; }
        public List<ResultBetType> BetType { get; set; }
    }
}
