﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Virtual
{
    [Serializable]
    public class VirtualSoccer :  SportGame
    {
        public string Stadium { get; set; }
        public int? GameNumber { get; set; }


        public new static Tuple<string, string>[] HashArray(SportGame src)
        {

            var vsGame = src as VirtualSoccer;
            var result = SportGame.HashArray(src).ToList();

            if (vsGame.Stadium != null)
                result.Add(new Tuple<string, string>("Stadium", vsGame.Stadium));

            if (vsGame.GameNumber != null)
                result.Add(new Tuple<string, string>("GameNumber", vsGame.GameNumber.ToString()));

            return result.ToArray();
        }

        public new VirtualSoccer Clone() =>
            new VirtualSoccer
            {
                Stadium = Stadium,
                GameNumber = GameNumber
            };

        public static new VirtualSoccer Deserialize(Dictionary<string, string> src)
        {
            var result = new VirtualSoccer();
            foreach (var key in src.Keys)
                switch (key)
                {
                    case "SeasonId":
                        result.Stadium = src[key];
                        break;
                    case "GameNumber":
                        result.GameNumber = int.Parse(src[key]);
                        break;
                }
            return result;
        }

    }
}
