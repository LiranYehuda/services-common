﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Virtual
{
    [Serializable]
    public class VirtualRace : SportGame
    {
        private static readonly string[] JsonSeparator = new string[] { "},{" };
        // private static readonly string JsonSeparator = "},{";
        public List<Runner> Runners { get; set; }
        public int? PlacePaysOn { get; set; }
        public string Distance { get; set; }
        public int? GameNumber { get; set; }

        public new static Tuple<string, string>[] HashArray(SportGame src)
        {

            var vrGame = src as VirtualRace;
            var result = SportGame.HashArray(src).ToList();

            if (vrGame.PlacePaysOn != null)
                result.Add(new Tuple<string, string>("PlacePaysOn", vrGame.PlacePaysOn.ToString()));

            if (vrGame.Distance != null)
                result.Add(new Tuple<string, string>("Distance", vrGame.Distance.ToString()));

            if (vrGame.GameNumber != null)
                result.Add(new Tuple<string, string>("GameNumber", vrGame.GameNumber.ToString()));

            if (vrGame.Runners != null)
                result.Add(new Tuple<string, string>("Runners", $"[{string.Join(",", vrGame.Runners.Select(x => Runner.ToJson(x)))}]"));
            return result.ToArray();          
        }

        public new VirtualRace Clone() =>
            new VirtualRace
            {
                Runners = new List<Runner>(Runners),
                PlacePaysOn = PlacePaysOn,
                Distance = Distance,
                GameNumber = GameNumber
            };

        public static new VirtualRace Deserialize(Dictionary<string, string> src)
        {
            var result = new VirtualRace();
            foreach (var key in src.Keys)
                switch (key)
                {
                    case "Runners":
                        var strRunners = src[key];
                        if (!string.IsNullOrWhiteSpace(strRunners)
                            && strRunners.Length > 3)
                        {
                            strRunners = strRunners.Substring(2, strRunners.Length - 4);
                            var runners = strRunners.Split(JsonSeparator, StringSplitOptions.RemoveEmptyEntries);
                            result.Runners = new List<Runner>(runners.Select(x => Runner.FromJson($"{{{x}}}")));
                        }
                        break;
                    case "PlacePaysOn":
                        result.PlacePaysOn = int.Parse(src[key]);
                        break;
                    case "Distance":
                        result.Distance = src[key];
                        break;
                    case "GameNumber":
                        result.GameNumber = int.Parse(src[key]);
                        break;
                }
            return result;
        }


    }
}
