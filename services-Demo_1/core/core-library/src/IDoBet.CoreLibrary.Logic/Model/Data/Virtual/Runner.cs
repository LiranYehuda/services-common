﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Virtual
{
    [Serializable]
    public class Runner
    {
        private static readonly char[] CommaSeparator = new char[] { ',' };
        private static readonly char[] ColonSeparator = new char[] { ':' };
        public long ProviderKey { get; set; }    
        public int Draw { get; set; }
        public int FinishPosition { get; set; }
        public string Name { get; set; }

        public static string ToJson(Runner src) =>
            $"{{\"ProviderKey\":{src.ProviderKey},\"Draw\":{src.Draw},\"FinishPosition\":{src.FinishPosition},\"Name\":\"{src.Name}\"}}";
        public static Runner FromJson(string src)
        {
            Runner result = null;
            if (string.IsNullOrWhiteSpace(src)
                || src.Length < 3)
                return result;
            src = src.Substring(1, src.Length - 2);
            var properties = src.Split(CommaSeparator, StringSplitOptions.RemoveEmptyEntries);
            result = new Runner();
            foreach (var property in properties)
            {
                var propertyName = property.Split(ColonSeparator)[0];
                propertyName = propertyName.Substring(1, propertyName.Length - 2);
                var propertyValue = property.Split(ColonSeparator)[1];
                if (propertyValue[0] == '\"')
                    propertyValue = propertyValue.Substring(1, propertyValue.Length - 2);
                if (string.IsNullOrWhiteSpace(propertyValue)
                    || propertyValue == "null")
                    continue;                
                switch (propertyName)
                {
                    case "ProviderKey":
                        result.ProviderKey = long.Parse(propertyValue);
                        break;
                    case "Draw":
                        result.Draw = int.Parse(propertyValue);
                        break;
                    case "FinishPosition":
                        result.FinishPosition = int.Parse(propertyValue);
                        break;
                    case "Name":
                        result.Name = propertyValue;
                        break;
                }
            }
            return result;
        }
    }
}
