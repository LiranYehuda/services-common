﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System.Linq;

namespace IDoBet.CoreLibrary.Logic.Model.Data.Virtual
{
    [Serializable]
    public class VirtualFootballLeague : SportGame
    {
        public int? SeasonId { get; set; }
        public int? WeekId { get; set; }
        public int? GameNumber { get; set; }

        public new static Tuple<string, string>[] HashArray(SportGame src)
        {
            var vfbGame = src as VirtualFootballLeague;
            var result = SportGame.HashArray(src).ToList();
            if (vfbGame.SeasonId != null)
                result.Add(new Tuple<string, string>("SeasonId", vfbGame.SeasonId.ToString()));

            if (vfbGame.WeekId != null)
                result.Add(new Tuple<string, string>("WeekId", vfbGame.WeekId.ToString()));

            if (vfbGame.GameNumber != null)
                result.Add(new Tuple<string, string>("GameNumber", vfbGame.GameNumber.ToString()));


            return result.ToArray();
        }

        public new VirtualFootballLeague Clone() =>
            new VirtualFootballLeague
            {
                SeasonId = SeasonId,
                WeekId = WeekId,
                GameNumber = GameNumber
            };

        public static new VirtualFootballLeague Deserialize(Dictionary<string, string> src)
        {
            var result = new VirtualFootballLeague();
            foreach (var key in src.Keys)
                switch (key)
                {
                    case "SeasonId":
                        result.SeasonId = int.Parse(src[key]);
                        break;
                    case "WeekId":
                        result.WeekId = int.Parse(src[key]);
                        break;
                    case "GameNumber":
                        result.GameNumber = int.Parse(src[key]);
                        break;
                }
            return result;
        }

        public string GeneratedName =>
           HomeTeam == null ?
               $"League season {SeasonId}, week {WeekId}" :
               $"{HomeTeam} {AwayTeam}";
    }
}
