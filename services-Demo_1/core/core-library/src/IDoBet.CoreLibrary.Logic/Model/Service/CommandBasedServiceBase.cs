﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Service;
using IDoBet.CoreLibrary.Logic.Model.Interfaces.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;

namespace IDoBet.CoreLibrary.Logic.Model.Service
{
    public abstract class CommandBasedServiceBase : ICommandBasedService
    {
        protected event EventHandler OnTimer;
        protected event EventHandler<string[]> OnCommand = delegate { };

        protected Thread TimerThread;
        protected Thread MainThread;
        protected bool IsStopping = false;
        protected Dictionary<string, string> Configuration = new Dictionary<string, string>();

        protected event EventHandler<ServiceMessage> OnMessageStore = delegate { };
        public event EventHandler<ServiceMessage> OnMessage
        {
            add { OnMessageStore += value; }
            remove { OnMessageStore -= value; }
        }

        // protected abstract EventHandler<ServiceMessage> OnMessage;
        protected virtual HashSet<string> RequiredConfiguration { get; set; } = new HashSet<string>();
        protected abstract bool EnableTimer { get; }
        protected abstract string ServiceName { get; }
        protected abstract void OnStart();
        protected abstract void OnStop();
        protected abstract bool IsRunning { get; set; }


        protected int ProviderId { get; set; }
        protected int ProviderSourceId { get; set; }

        protected HashSet<int> BrandIds { get; set; }

        public void Command(ServiceCommand command)
        {
            switch (command.Command)
            {
                case eServiceCommand.Start:
                    if (IsRunning)
                        return;
                    var missingConfiguration = RequiredConfiguration.Except(Configuration.Keys.Select(x => x)).ToList();
                    if (missingConfiguration.Count > 0)
                    {
                        Message(eErrorCode.ArgumentError, $"Following keys are not configured: {string.Join(",", missingConfiguration)}");
                        return;
                    }
                    IsStopping = false;
                    MainThread = new Thread(new ThreadStart(OnStart));
                    MainThread.Start();
                    if (EnableTimer)
                    {
                        TimerThread = new Thread(new ThreadStart(TimerMain));
                        TimerThread.Start();
                    }
                    break;
                case eServiceCommand.Stop:
                    if (!IsRunning)
                        return;
                    IsStopping = true;
                    OnStop();
                    break;
                case eServiceCommand.Status:
                    Message($"{ServiceName} is {(IsRunning ? "" : "NOT ")}RUNNING.");
                    if (command.Arguments?.Length > 0)
                        if (command.Arguments[0] == "CONF")
                            Message($"Configuration:{Environment.NewLine}{string.Join(Environment.NewLine, Configuration.Select(x => $"{x.Key}: {x.Value}"))}");
                    break;
                case eServiceCommand.Set:
                    if (command.Arguments?.Length != 2)
                        return;
                    Configuration[command.Arguments[0]] = command.Arguments[1];
                    Message($"{ServiceName} set {command.Arguments[0]} = {command.Arguments[1]}");
                    break;
                case eServiceCommand.Command:
                    if (command.Arguments?.Length < 0)
                        return;
                    OnCommand(this, command.Arguments);
                    break;
                default:
                    break;
            }
        }

        private void TimerMain()
        {
            while (!IsStopping)
            {
                OnTimer?.Invoke(this, EventArgs.Empty);
                Thread.Sleep(1000);
            }
        }


        protected void SendMessage(ServiceMessage msg) =>
            OnMessageStore(this, msg);
        protected void SendCommand(string serviceKey, string command, bool echoOn) =>
            SendMessage(new ServiceMessage
            {
                ErrorCode = eErrorCode.Success,
                Message = $"{ServiceCommand.GetString(eServiceCommand.Command)} {serviceKey} {command}",
                ProviderId = ProviderId,
                ProviderSourceId = ProviderSourceId,
                IsCommand = true,
                EchoOn = echoOn
            });
        protected void Message(string message) => SendMessage(CreateMessage(message));
        protected void Message(eErrorCode errorCode, string message) => SendMessage(CreateMessage(errorCode, message));
        protected ServiceMessage CreateMessage(string message) => CreateMessage(eErrorCode.Success, message);
        protected ServiceMessage CreateMessage(eErrorCode errorCode, string message) =>
            new ServiceMessage { ErrorCode = errorCode, Message = message, ProviderId = ProviderId, ProviderSourceId = ProviderSourceId };
        protected bool LoadIntConfiguration(string key, out int result)
        {
            if (!int.TryParse(Configuration[key], out result))
            {
                Message($"{ServiceName} invalid configuration value {key}");
                return false;
            }
            return true;
        }
        protected bool LoadBoolConfiguration(string key, out bool result)
        {
            if (!bool.TryParse(Configuration[key], out result))
            {
                Message($"{ServiceName} invalid configuration value {key}");
                return false;
            }
            return true;
        }
        public void Initialize(int providerId, int providerSourceId)
        {
            ProviderId = providerId;
            ProviderSourceId = providerSourceId;
        }

        public void Initialize(HashSet<int> brandIds)
        {
            BrandIds = brandIds;
        }
    }
}
