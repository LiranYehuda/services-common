﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Service
{
    public class ServiceCommand
    {
        public eServiceCommand Command { get; set; }
        public string[] Arguments { get; set; }

        public static eServiceCommand GetEnum(string input)
        {
            switch (input)
            {
                case "STATUS":
                    return eServiceCommand.Status;
                case "START":
                    return eServiceCommand.Start;
                case "STOP":
                    return eServiceCommand.Stop;
                case "HELP":
                    return eServiceCommand.Help;
                case "LOAD":
                    return eServiceCommand.Load;
                case "LOADBRANDED":
                    return eServiceCommand.LoadBranded;
                case "QUIT":
                case "EXIT":
                    return eServiceCommand.Quit;
                case "CLS":
                    return eServiceCommand.ClearScreen;
                case "LIST":
                    return eServiceCommand.List;
                case "UNLOAD":
                    return eServiceCommand.UnLoad;
                case "SET":
                    return eServiceCommand.Set;
                case "EXEC":
                    return eServiceCommand.Exec;
                case "CMD":
                    return eServiceCommand.Command;
                default:
                    return eServiceCommand.Unknown;
            }
        }
        public static string GetString(eServiceCommand input)
        {
            switch (input)
            {
                case eServiceCommand.Unknown:
                    return "UNKNOWN";
                case eServiceCommand.Status:
                    return "STATUS";
                case eServiceCommand.Start:
                    return "START";
                case eServiceCommand.Stop:
                    return "STOP";
                case eServiceCommand.Help:
                    return "HELP";
                case eServiceCommand.Load:
                    return "LOAD";
                case eServiceCommand.LoadBranded:
                    return "LOADBRANDED";
                case eServiceCommand.Quit:
                    return "QUIT";
                case eServiceCommand.ClearScreen:
                    return "CLS";
                case eServiceCommand.List:
                    return "LIST";
                case eServiceCommand.UnLoad:
                    return "UNLOAD";
                case eServiceCommand.Set:
                    return "SET";
                case eServiceCommand.Exec:
                    return "EXEC";
                case eServiceCommand.Command:
                    return "CMD";
                default:
                    return "";
            }            
        }
    }
}
