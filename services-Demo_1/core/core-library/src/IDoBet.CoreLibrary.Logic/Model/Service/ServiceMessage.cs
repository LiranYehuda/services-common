﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Service
{
    public class ServiceMessage
    {
        public bool IsCommand { get; set; } = false;
        public int ProviderId { get; set; }
        public int ProviderSourceId { get; set; }
        public eErrorCode ErrorCode { get; set; } 
        public string Message { get; set; }
        public bool EchoOn { get; set; } = true;
    }
}
