﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums
{
    public enum eErrorCode : int
    {
        Success = 0,

        #region General Error

        DatabaseError = 1,

        InternalServerError = 2,

        BadRequest = 3,

        BadOdds = 4,

        EventColosedForBetting = 5,

        EventEnded = 6,

        EventDosntExsist = 7,

        NoPermission = 8,

        DuplicateData = 9,

        EmptyResultData = 10,

        InvalidBrandId = 11,

        ArgumentError = 12,

  
        FileNotFound = 13,
        #endregion

        #region Register + Login Error (100)

        // THIS VALUES ARE USED IN DB, DO NOT CHANGE

        UserTokenInvalid = 100,

        TerminalTokenInvalid = 101,

        TerminalTokenAlreadyInUse = 102,

        WrongCredetials = 103,

        NoOpenShiftForTerminalUser = 104,

        LogoutFailed = 105,

        UserHasBeenLocked = 106,
        //Register
        UserNameAlreadyExists = 120,

        EmailAlreadyExists = 121,

        FailedToChangePassword = 122,        

        OTPCodeNotValid = 123,        

        PrePaidPasswordNotExist = 124,
        
        PrePaidPasswordNotSent = 125,

        PhoneCountryCodeNotSupported = 126,

        PhoneIsNotValid = 127,

        PhoneOperatorNotSupported = 128,

        GeoLocationAccessDenied = 129,

        //User 
        AgentUserLimitHit = 130,

        NoUserFoundForPhone = 131,

        MultipleUsersFoundForPhone = 132,

        IpNotExistAtSupportSystem = 133,

        WrongSecurityAnswer = 134,
        #endregion

        #region order error (200)


        OrderValidationError = 200,


        OrderNotFound = 201,


        OrderCanotPayout = 202,


        //Event 

        EventDataNotFound = 240,

        OddsPriceChanged = 241,

        //Limitation

        LimitationFailed,
        //User 

        MinBetLimit = 250,


        MaxBetLimit = 251,


        OverCrditLimit = 252,


        EventStatusDisabled = 253,


        BetTypeStatusDisabled = 254,


        OddStatusDisabled = 255,

        EventScoreChanged = 256,

        ExecuteTimeIncorrect = 260,

        EventLimitHit = 270,

        BetTypeCapHit = 271,

        AuthorizationInvalid = 272,
        #endregion

        #region Terminal Error (300)

        TerminalNotFound = 300,

        TerminalAlreadyRegister = 301,

        TerminalIsNotActive = 302,
        #endregion

        #region Finance error(400)


        NotEnoughMoney = 400,

        InsertWithdrawFailed = 410,

        CommitWithdrawFailed = 411,

        CurrncyNotMatch = 412,        

        ApplyWithdrawFailed = 413,        

        CreditNotEnough = 414,        

        ExistBrnUsersDiffCurrency = 415,
        
        TransferNotFound = 416,

        PendingCommissionExist = 417,

        BranchFinanceCommissionOverlaps = 418,

        TransactionNotFound = 419,

        ExternalTransactionIdAlreadyExists = 420,

        TransactionIntervalViolation = 421,

        #endregion


        #region Online (500)

        AlreadyActivated = 500,
        UpdateInfoFailed = 501,
        NoneCountry = 502,   
        CouldNotRemoveActivePhone = 503,    
        CouldNotRemoveRegisteredPhone = 504,
        #endregion

        #region Bonus (600)
        CampaignNotFound = 600,
        CampaignNotAllowed = 601,
        NextBetBonusValidation = 602,
        #endregion

        #region Lottto

        drawId_not_declared = 700,
        LotteryTypeId_not_match = 701,
        Lotto_ticket_not_active = 702,
        brandId_not_valid = 703,
        betTypeId_not_valid = 704,
        payment_not_match = 705,
        endDrawId_Not_valid = 706,
        Missing_selected_numbers = 707,
        missing_winning_numbers = 708,
        drawId_already_settled = 709,
        no_rows_to_settle = 711,
        betId_not_found = 712,
        betId_already_activated = 713,
        drawId_not_settled = 714,
        Numbers_Not_Match = 715,
        Must_Sattle_Prev_DrwId = 716,
        INVALID_REQUEST = 717,
        Numbers_Not_Valid = 718,
        draw_time_closed = 719,
        draw_prizes_undefine = 720,
        #endregion

        #region Prepaid errors (800)
        PrepaidCodeNotExists = 800,
        PrepaidIsConverted = 801,
        NoPermmisionForPrepaidUser = 802,
        PrepaidSecured = 803,
        IncorrectPrepaidCode = 804,
        PrepaidIsActivated = 805,
        PrepaidMinAmount = 806,
        #endregion

        #region Expenses (900)
        CategoryContainsRelatedItems = 900,
        CategoryNameAlreadyExists = 901,
        ItemNameAlreadyExists = 902,
        ItemContainsRelatedExpenses = 903,
        ExpensesMonthAlreadyExists = 904,
        AmountExceededMaxAmount = 905,
        MonthNotOpenedForExpenseRequest =906,
        NumOfRequetsExceededPeriodLimit = 907,
        ExpenseRequestAlreadyApproved = 908,
        ExpenseRequestCanceled = 909,
        RegionNotPermmitedForExpenseRequest = 910,
        UserExceededMonthlyCap = 911,
        UserExceededDailyCap = 912,
        UserExceededWeeklyCap = 913,

        #endregion

        #region API ERRORS
        EventsNotReady = 1000,
        ApiError = 1001,
        GerneralApi = 1002,
        PasswordCantBeNull = 1010,
        PasswordMinPasswordLength = 1011,
        PasswordMustContainsSpecialChar = 1012,
        PasswordCantStartSpecialChar = 1013,
        PasswordAtLeatOneDigit = 1014,
        PasswordAtLeastOneLetter = 1015,
        PasswordCombination = 1016,
        PasswordSequence = 1017,
        ExteralTransactionIdAlreadyExists = 1018,
        DuplicateRequest = 1019,
        #endregion



        #region Third Party ERRORS
        BettingThirdPartyProviderNotFound = 1200,
        CasinoSessionIdNotExist = 1201,
        #endregion

    # region Brandsettings
        BrandSettingsIsNotExistsForSelectedChannelAndBranch = 1300
    #endregion

    }
}
