﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums
{
    public enum eFileType : int
    {
        Expenses = 1,
        Banner = 2,
        Html = 3,
        Translations = 4
    }
}
