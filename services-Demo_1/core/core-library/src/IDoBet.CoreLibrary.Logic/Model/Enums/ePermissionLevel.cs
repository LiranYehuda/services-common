﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums
{
    public enum ePermissionLevel : int
    {
        Read = 1,
        Write = 15
    }
}
