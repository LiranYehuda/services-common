﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums
{
    public enum eChannel : int
    {
        Online = 1,
        CashBox = 2,
        Pos = 3,
        Mobile = 4,
        BackOffice = 5,
        Kiosk = 6,
        External = 7,
        Server = 8,
        USSD = 9
    }
}
