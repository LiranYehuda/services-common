﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Order
{
    public enum eNextBetBonusOrderStatus
    {
        Inactive,
        Active,
        Used,
        Expaired
    }
}
