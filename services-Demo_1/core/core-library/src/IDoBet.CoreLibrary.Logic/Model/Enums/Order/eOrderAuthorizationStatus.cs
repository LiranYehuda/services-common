﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Order
{
    public enum eOrderAuthorizationStatus : int
    {
        WaitForAuthorization = 1,
        AuthorizationDeclined = 2,
        WaitForSell = 3,
        Sold = 4,
        Expired = 5,
    }
}
