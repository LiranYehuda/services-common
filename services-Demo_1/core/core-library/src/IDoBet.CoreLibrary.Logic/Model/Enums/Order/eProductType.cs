﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Order
{
    public enum eProductType
    {
        Unknown = 0,
        Prematch = 1,
        Live = 2,
        Virtual = 3,
        Pool = 4,
        Lotto = 5,
        Keno = 6,
        SpinAndWin = 7,
        Colors = 8,
        Casino = 9,
    }
}
