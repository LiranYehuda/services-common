﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Order
{
    public enum eOrderStatus : int
    {
        Unknown = 0,
        Draft = 1,
        Sold = 3,
        Paid = 4,
        Blocked = 5,
        Cancelled = 6,
        Expired = 7
    }
}
