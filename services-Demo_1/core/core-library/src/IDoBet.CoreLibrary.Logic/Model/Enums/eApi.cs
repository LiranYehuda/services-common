﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums
{
    public enum eApi : int 
    {
        All = 0,
        GameApi = 1,
        ExternalApi = 2,
        BackOfficeApi = 3,
        EventApi = 4,
        LottoApi = 5,
        CLApi    = 6,
        EPApi    = 7
    }
}
