﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums.Service
{
    public enum eServiceCommand : int
    {
        Unknown = 0,
        Status = 1,
        Start = 2,
        Stop = 3,
        Help = 4,
        Quit = 5,
        Load = 6,        
        ClearScreen = 7,
        List = 8,
        UnLoad = 9,
        Set = 10,
        Exec = 11,
        Command = 12,
        LoadBranded = 13,
        // ResultEvent = 4,
    }
}
