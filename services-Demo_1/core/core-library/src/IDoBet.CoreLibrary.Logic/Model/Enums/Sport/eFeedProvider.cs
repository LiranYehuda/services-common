﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eFeedProvider : int
    {
        BetRadar = 3,
        Kiron = 4,
        DelaSport = 5,
    }
}
