﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eOddStatus : int
    {
        Unknown = -1,
        Open = 0,
        Suspended = 1,
        Closed = 2,
    }
}
