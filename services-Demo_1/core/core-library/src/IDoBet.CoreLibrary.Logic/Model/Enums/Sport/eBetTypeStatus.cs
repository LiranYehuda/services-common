﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eBetTypeStatus : int
    {
        Unknown = -1,
        Open = 0,
        Suspended = 1,
        Closed = 2,
        Deleted = 3,
        Canceled = 4
    }
}
