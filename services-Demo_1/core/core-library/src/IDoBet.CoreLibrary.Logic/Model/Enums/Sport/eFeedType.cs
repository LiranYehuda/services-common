﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eFeedType : int
    {
        Live = 1,
        Today = 2,
        Early = 3,
        Virtual = 4,
        Unknown = 0,
    }
}
