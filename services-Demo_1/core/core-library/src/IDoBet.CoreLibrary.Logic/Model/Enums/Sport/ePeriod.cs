﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum ePeriod
    {
        //used for not supported time periods (newly added, or just not interesting ones
       
        _GENERAL = 0,

        //Full time result - This is sent for all sports and indicates the result after normal time.
        //This is also referred to as "Regular time"
      
        _FT = 1,

        /* Soccer, Handball, Rugby, Futsal, Bandy and Field Hockey    */
        //Half time result
      
        _HT = 2,
        //Overtime result - Can be sent for most sports. Overtime should not be confused with "Injury time" or "Added time".
       
        _OT = 3,
        //AP = Result after penalties - This is a feature primarily used for Soccer and Ice Hockey but it can also be sent for other sports.
      
        _AP = 4,
        //OW - Walkover = Gives the teamID of the winning team/competitor.
        //It is used when a player cannot play or complete a match. The opponent will then proceed to the next round.
        //If a player retires we will set WO status for the opponent.
      
        _WO = 5,
        //C = Canceled game - Indicates the match did not finish (it was not played or it was interrupted).
       
        _C = 6,

        /* Ice Hockey, Floorball and Beach Soccer */
        //1st period result
       
        _1P = 11,
        //2nd period result
        
        _2P = 12,
        //3rd period result
       
        _3P = 13,

        /* Football, Basketball, Aussie Rules, Water Polo, Speedway and Motor Sport */
        //1st quarter result
      
        _1Q = 21,
        //2nd quarter result
       
        _2Q = 22,
        //3rd quarter result
       
        _3Q = 23,
        //4th quarter result
       
        _4Q = 24,

        /* Tennis, Beach Volleyball, Volleyball, Table Tennis and Badminton */
        //1st set result
      
        _SET1 = 31,
        //2nd set result
        
        _SET2 = 32,
        //3rd set result
      
        _SET3 = 33,
        //4th set result
      
        _SET4 = 34,
        //5th set result
       
        _SET5 = 35
    }
}
