﻿namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eEventStatus : int
    {
        /// <summary>
        /// Not initialized/declared/supported
        /// </summary>
        Unknown = -1,
        /// <summary>
        /// Game open in prematch mode
        /// </summary>
        Prematch = 0,
        /// <summary>
        /// Game open in live mode
        /// </summary>
        Live = 1,
        /// <summary>
        /// Game Finished
        /// </summary>
        Closed = 2,
        /// <summary>
        /// Game Canceled
        /// </summary>
        Canceled = 3,
        /// <summary>
        /// Game Interrupted in live mode  (Rain etc…)
        /// </summary>
        Interrupted = 4,

        /// <summary>
        /// Game void by user
        /// </summary>
        Void = 5,

        /// <summary>
        /// Game was imported from live feed without prematch
        /// information and had not stared yet.
        /// </summary>
        AwaitingLive = 6
    }
}
