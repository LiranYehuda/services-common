﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eEventApiCommand : int
    {
        /// <summary>
        /// Parameter is a HashSet<long> of eventIds.
        /// </summary>
//        ReloadSportMetadata = 2,
        Log = 1,
        ReloadSportGameCustomization = 2,
        ReloadSportMetadata = 3,
        ReloadCoupon = 4,
    }
}
