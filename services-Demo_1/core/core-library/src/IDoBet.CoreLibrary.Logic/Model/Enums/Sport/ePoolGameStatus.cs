﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum ePoolGameStatus : int
    {
        Closed = 1,
        Open = 2,
        WaitingForSettle = 3,
        Settled = 4,
        Calculating = 5    }
}
