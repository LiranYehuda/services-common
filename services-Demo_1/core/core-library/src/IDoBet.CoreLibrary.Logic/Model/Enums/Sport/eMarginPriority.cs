﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eMarginPriority : int
    {
        /// <summary>
        /// Determined by algorithm in SportGameFilter class
        /// </summary>
        Automatic = 0,
        /// <summary>
        /// Specific event filter.
        /// </summary>
        High = 1,
        /// <summary>
        /// Country or league filter.
        /// </summary>
        Normal = 2,
        /// <summary>
        /// SportType, Provider filter
        /// </summary>
        BelowNormal = 3,
        /// <summary>
        /// General/global settings. (for example: BetTypes only filter, eventstatus filter) Least strict filter.
        /// </summary>
        Low = 4,
    }
}
