﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums.Sport
{
    public enum eWinStatus : int
    {
        Unknown = -1,
        Pending = 0,
        Lost = 1,
        Winner = 2,
        MoneyBack = 3,
        HalfWin = 4,
        HalfLose = 5,
    }
}
