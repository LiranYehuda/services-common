﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Enums
{
    public enum eModule : int
    {
        Core = 0,
        Game = 1,
        Lotto = 2,
        /// <summary>
        /// EventsData
        /// </summary>
        SportArchive = 3,
        /// <summary>
        /// Limits (authorization)
        /// </summary>
        SportLimit = 4,
        /// <summary>
        /// BrandedCache, SportType, Country, League, BetType templates (ShortName, Order, ...)
        /// </summary>
        SportMetadata = 5,
        /// <summary>
        /// Margin configuration (sportdb)
        /// </summary>
        SportMargin = 6,
        /// <summary>
        /// Redis
        /// </summary>
        SportRepository = 7,
        ExternalApi = 8,
        ThirdParty = 9,
        /// <summary>
        /// Common cache (shared by all brands)
        /// </summary>
        SportCache = 10,
        /// <summary>
        /// Coupon handling (brand db)
        /// </summary>
        SportCoupon = 11,
        Backoffice = 12


    }
}
