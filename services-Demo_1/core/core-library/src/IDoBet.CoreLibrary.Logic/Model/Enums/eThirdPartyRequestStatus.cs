﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.ThirdPartyNotification.Logic.Model
{

    public enum eThirdPartyRequestStatus
    {
        OPEN = 1,
        SUCCESS,
        ERROR,
        MANUAL_CLOSE
    }

}
