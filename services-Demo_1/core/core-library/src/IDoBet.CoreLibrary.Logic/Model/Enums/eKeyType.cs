﻿
namespace IDoBet.CoreLibrary.Logic.Model.Enums
{
    public enum eKeyType : int
    {
        Unknown = 0,
        Database = 1,
        Api = 2,
        LocalValue = 3
    }
}
