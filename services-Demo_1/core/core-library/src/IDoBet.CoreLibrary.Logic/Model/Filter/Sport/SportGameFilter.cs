﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Filter.Sport
{
    public class SportGameFilter
    {
        private static HashSet<T> GetHashsetFromList<T>(List<T> src) => 
            src == null ? null : new HashSet<T>(src.Distinct());

        private static HashSet<T> IntersectWithSystemFilter<T>(HashSet<T> request, HashSet<T> system) =>
            (request != null && system != null)
                ? GetHashsetFromList(request.Intersect(system).ToList())
                : request;
            
        public static SportGameFilter Intersect(SportGameFilter systemFilter, SportGameFilter requestFilter)
        {
            if (systemFilter == null)
                return requestFilter;
            requestFilter.ProviderIds = IntersectWithSystemFilter(requestFilter.ProviderIds, systemFilter.ProviderIds);
            requestFilter.SportTypeIds = IntersectWithSystemFilter(requestFilter.SportTypeIds, systemFilter.SportTypeIds);
            requestFilter.EventStatusIds = IntersectWithSystemFilter(requestFilter.EventStatusIds, systemFilter.EventStatusIds);
            requestFilter.EventIds = IntersectWithSystemFilter(requestFilter.EventIds, systemFilter.EventIds);
            requestFilter.BetTypeIds = IntersectWithSystemFilter(requestFilter.BetTypeIds, systemFilter.BetTypeIds);
            requestFilter.CountryIds = IntersectWithSystemFilter(requestFilter.CountryIds, systemFilter.CountryIds);
            requestFilter.LeagueIds = IntersectWithSystemFilter(requestFilter.LeagueIds, systemFilter.LeagueIds);
            requestFilter.WinStatusIds = IntersectWithSystemFilter(requestFilter.WinStatusIds, systemFilter.WinStatusIds);
            requestFilter.CouponIds = IntersectWithSystemFilter(requestFilter.CouponIds, systemFilter.CouponIds);

            if (requestFilter.FromDate != null && systemFilter.FromDate != null && requestFilter.FromDate < systemFilter.FromDate)
                requestFilter.FromDate = systemFilter.FromDate;

            if (requestFilter.ToDate != null && systemFilter.ToDate != null && requestFilter.ToDate > systemFilter.ToDate)
                requestFilter.ToDate = systemFilter.ToDate;

            return requestFilter;
        }

        public void SetProductFilter(List<eProductType> products)
        {
            foreach (var product in products)
                switch (product)
                {
                    case eProductType.Prematch:
                    case eProductType.Live:
                        if (EventStatusIds == null)
                            EventStatusIds = new HashSet<long>();
                        if (product == eProductType.Prematch && !EventStatusIds.Contains((int)eEventStatus.Prematch))
                            EventStatusIds.Add((int)eEventStatus.Prematch);
                        if (product == eProductType.Live && !EventStatusIds.Contains((int)eEventStatus.Live))
                            EventStatusIds.Add((int)eEventStatus.Live);
                        break;
                    case eProductType.Virtual:
                        if (ProviderIds == null)
                            ProviderIds = new HashSet<long>();
                        if (!ProviderIds.Contains((int)eFeedProvider.Kiron))
                            ProviderIds.Add((int)eFeedProvider.Kiron);
                        break;
                }
        }
        public string SearchText { get; set; }
        // public TimeSpan? Since { get; set; }
        public DateTime? SinceDate { get; set; }
        // public bool? ActiveCoupon { get; set; }
        // public List<int> CoupinIds { get; set; }
        // public List<int> ProviderIds { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public TimeSpan MaxAge = TimeSpan.FromHours(-24);

        public double? MinPrice { get; set; }

        public HashSet<long> ProviderIds { get; set; }
        public HashSet<long> EventStatusIds { get; set; }
        public HashSet<long> EventIds { get; set; }
        public HashSet<long> BetTypeIds { get; set; }
        public HashSet<long> SportTypeIds { get; set; }
        public HashSet<long> CountryIds { get; set; }
        public HashSet<long> LeagueIds { get; set; }
        public HashSet<int> WinStatusIds { get; set; }
        public HashSet<int> CouponIds { get; set; }
        public bool LoadFromDb => (EventIds?.Count ?? 0) > 0;

        public bool IsForClient { get; set; } = false;

        public HashSet<long> AsianFilteringBetTypes { get; set; }
        public string AsianFilteringLines { get; set; }

        //public List<string> ProdTypeIds { get; set; }
        //public string SortBy { get; set; }
        //public long? LastTimestamp { get; set; }
        public bool ListNullOrContains<T>(ICollection<T> list, T element) =>
            list == null
            || list.Contains(element);
        public bool IsGameMatch(SportGame src)
        {

            if (src == null)
                return false;

            if (src.EventDate == null
                && (FromDate.HasValue || ToDate.HasValue)
                && src.EventStatusId != (int)eEventStatus.Live)
                return false;
            if ((FromDate ?? DateTime.MinValue) > (src.EventDate ?? DateTime.Now)
                || (ToDate ?? DateTime.MaxValue) < (src.EventDate ?? DateTime.Now))
                return false;
            if (!ListNullOrContains(EventStatusIds, (src.EventStatusId ?? (int)eEventStatus.Unknown))
                || !ListNullOrContains(EventIds, src.Id)
                || !ListNullOrContains(SportTypeIds, src.SportType?.Id ?? -1)
                || !ListNullOrContains(CountryIds, src.Country?.Id ?? -1)
                || !ListNullOrContains(LeagueIds, src.League?.Id ?? -1)
                || !ListNullOrContains(ProviderIds, src.ProviderId ?? -1))
                return false;
            if (IsForClient && !IsClient(src))
                return false;

            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                var loweredSearchText = SearchText.ToLower();
                if (!(src.Id.ToString().StartsWith(loweredSearchText) ||
                    (src.League != null && src.League.Name.ToLower().StartsWith(loweredSearchText)) ||
                     (src.HomeTeam != null && src.HomeTeam.Name.ToLower().StartsWith(loweredSearchText)) ||
                    (src.AwayTeam != null && src.AwayTeam.Name.ToLower().StartsWith(loweredSearchText)) ||
                    src.GameNumber.ToString().StartsWith(loweredSearchText)))
                    return false;

            }
            return true;
        }

        private bool IsClient(SportGame src) =>
            !(src.SportType.Id < 0
                    || src.SportType.Name == null
                    || src.HomeTeam?.Name == null
                    || src.AwayTeam?.Name == null);

        public BetType FilterLines(BetType betType)
        {
            if (AsianFilteringBetTypes != null
                && AsianFilteringBetTypes.Contains(betType.Id))
            {
                var result = betType.Clone();
                result.Odds.RemoveAll(x =>
                    x.Line != null
                    && (!x.Line.EndsWith(AsianFilteringLines) && (!x.Line.EndsWith($"{AsianFilteringLines}0")))
                    );
                return result;
            }
            else
                return betType.Clone();
        }

        public IEnumerable<BetType> FilterAndCloneBetTypes(SportGame src, IEnumerable<long> betTypeIds = null)
        {
            IEnumerable<long> resultIds;
            if (src.BetTypes == null)
                yield break;
            if (BetTypeIds != null)
                resultIds = BetTypeIds.Intersect(src.BetTypes.Select(x => x.Id));
            else
                resultIds = src.BetTypes.Select(x => x.Id);
            if (betTypeIds != null)
                resultIds = resultIds.Intersect(betTypeIds);
            if (WinStatusIds != null && resultIds.Any())
                resultIds = resultIds.Intersect(src.BetTypes.Where(x => x.Odds?.Any(y => WinStatusIds.Contains(y.WinStatusId)) ?? false).Select(z => z.Id));
           
            if(MinPrice != null && resultIds.Any())
            {
                resultIds = resultIds.Intersect(src.BetTypes.Where(b => b.Odds?.All(o => o.Price >= MinPrice.Value) ?? false).Select(z => z.Id));
            }

            foreach (var betType in src.BetTypes.Where(x => resultIds.Contains(x.Id)))
                yield return FilterLines(betType);
        }

        public bool IsUpdatedSince(SportGame src) =>
            !(SinceDate != null
                && (
                        !src.LastUpdate.HasValue
                        || src.LastUpdate.Value > SinceDate.Value
                    )
            );

        public static eMarginPriority MarginPriority(SportGameFilter src)
        {
            if (src == null)
                return eMarginPriority.Low;
            if ((src.EventIds?.Count ?? 0) > 0)
                return eMarginPriority.High;
            if (
                    (src.LeagueIds?.Count ?? 0) > 0
                    || (src.CountryIds?.Count ?? 0) > 0
               )
                return eMarginPriority.Normal;
            if ((src.SportTypeIds?.Count ?? 0) > 0)
                return eMarginPriority.BelowNormal;
            return eMarginPriority.Low;
        }
    }
}
