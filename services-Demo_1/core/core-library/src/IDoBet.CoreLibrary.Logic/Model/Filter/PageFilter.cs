﻿
namespace IDoBet.CoreLibrary.Logic.Model.Filter
{
    public class PageFilter
    {
        public int? EntryStart { get; set; }
        public int? EntryCount { get; set; }
        public string SortBy { get; set; }
        public bool SortDesc { get; set; }
    }
}
