﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Legacy.PrintService
{
    public abstract class PrintServiceBaseRequest
    {
        public string UserToken { get; set; }
        public string Language { get; set; }
        public string Template { get; set; }
        public string TerminalName { get; set; }
        public string CashierName { get; set; }
        public string BranchName { get; set; }
        public string Currency { get; set; }
        public int? ChannelId { get; set; }

    }
}
