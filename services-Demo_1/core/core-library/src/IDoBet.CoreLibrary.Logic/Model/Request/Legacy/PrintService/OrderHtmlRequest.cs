﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Legacy.PrintService
{
    public class OrderHtmlRequest : PrintServiceBaseRequest
    {
        public bool IsReprint { get; set; }        
        public string RequestGuid { get; set; }
        public bool ForPrint { get; set; }
        public int? Width { get; set; }
        public string Barcode { get; set; }
        public int? BrandId { get; set; }
        public string ReceiptHeaderText { get; set; }

    }
}
