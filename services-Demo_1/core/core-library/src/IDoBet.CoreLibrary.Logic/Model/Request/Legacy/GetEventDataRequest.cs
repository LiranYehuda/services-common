﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Request.Legacy.Filter;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Legacy
{
    public class GetEventDataRequest : BaseRequest
    {
        public GetEventDataRequest(EventDataCacheFilter filter, DateTime? lastUpdated = null, bool reloadBetTypeThoughNotUpdated = false)
        {
            Filter = filter;
            LastUpdated = lastUpdated;
            ReloadBetTypeThoughNotUpdated = reloadBetTypeThoughNotUpdated;

        }

        public DateTime? LastUpdated { get; set; }

        public EventDataCacheFilter Filter { get; set; }

    
        public bool ReloadBetTypeThoughNotUpdated { get; set; }


    }
}
