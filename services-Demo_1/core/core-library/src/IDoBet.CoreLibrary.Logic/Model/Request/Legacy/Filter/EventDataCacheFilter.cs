﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Legacy.Filter
{
   
    public class EventDataCacheFilter
    {
        #region Ctor

        public EventDataCacheFilter()
        {
            EventDataStructs = new List<EventDataStruct>();
        }

        #endregion

        #region Props  

       
        public List<EventDataStruct> EventDataStructs { get; set; }

        
        #endregion

    }

   
    public class EventDataStruct
    {

       
        public EventFilter EventFilter { get; set; }

        
        public List<long> BetTypeIds { get; set; }

       
        public List<OddIdentityStruct> Odds { get; set; }

        
        public List<string> EventPropertieKeys { get; set; }
    }

   
    public class OddIdentityStruct
    {
       
        public String Name { get; set; }

       
        public String Line { get; set; }
    }
}
