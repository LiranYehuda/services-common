﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Legacy.Filter
{
   
    public class EventFilter : BaseFilter, IEntityIdsFilter, IProviderFilter, ICloneable
    {
        #region Ctor

        public EventFilter()
        {

        }


        #endregion

        #region Props

        
        public DateTime? FromDate { get; set; }

       
        public DateTime? ToDate { get; set; }

       
        public List<long> SportTypeIds { get; set; }

       
        public List<long> CountryIds { get; set; }

       
        public List<long> LeaugeIds { get; set; }

        
        public List<long> TeamIds { get; set; }

        
        public List<long> HomeTeamIds { get; set; }

       
        public List<long> AwayTeamIds { get; set; }

       
        public List<int> CouponIds { get; set; }

       
        public bool? OnlyActiveCoupon { get; set; }

       
        public string FreeTextSearch { get; set; }

        
        public bool? IsSettled { get; set; }

        
        public List<eEventStatus> EventStatuses { get; set; }

       
        public bool? IsAttachedToCoupon { get; set; }

       
        public bool? IsAllResulted { get; set; }

        
        public bool? EventsWithoutOdds { get; set; }

       
        public bool? LoadFromDB { get; set; }
        #endregion

        /// <summary>
        /// Request only the specific game ids 
        /// NOTE: if other filters are filed they will be ignored  
        /// </summary>
        
        public List<long> Ids { get; set; }
      
        public long? ProviderKey { get; set; }
       
        public int? ProviderId { get; set; }
      
        public List<int> ProviderIds { get; set; }
        public virtual object Clone()
        {
            EventFilter cloneObj = (EventFilter)this.MemberwiseClone();

            if (SportTypeIds != null)
                cloneObj.SportTypeIds = this.SportTypeIds.ToList();

            if (CountryIds != null)
                cloneObj.CountryIds = this.CountryIds.ToList();

            if (LeaugeIds != null)
                cloneObj.LeaugeIds = this.LeaugeIds.ToList();

            if (TeamIds != null)
                cloneObj.TeamIds = this.TeamIds.ToList();

            if (HomeTeamIds != null)
                cloneObj.HomeTeamIds = this.HomeTeamIds.ToList();

            if (AwayTeamIds != null)
                cloneObj.AwayTeamIds = this.AwayTeamIds.ToList();

            if (CouponIds != null)
                cloneObj.CouponIds = this.CouponIds.ToList();

            if (EventStatuses != null)
                cloneObj.EventStatuses = this.EventStatuses.ToList();
            if (ProviderIds != null)
                cloneObj.ProviderIds = this.ProviderIds.ToList();
            return cloneObj;
        }

    }
}
