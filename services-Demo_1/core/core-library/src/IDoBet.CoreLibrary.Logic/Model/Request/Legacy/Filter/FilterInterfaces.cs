﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Legacy.Filter
{
    interface IEntityIdsFilter
    {
        List<long> Ids { get; set; }
    }

    interface IParentIdsFilter
    {
        List<long> ParentIds { get; set; }
    }

    interface IProviderFilter
    {
        long? ProviderKey { get; set; }
        int? ProviderId { get; set; }
    }

    interface IIsActiveFliter
    {
        bool? IsActive { get; set; }
    }

    interface ISortedOnlyFliter
    {
        bool SortedOnly { get; set; }
    }
}
