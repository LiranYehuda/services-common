﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Legacy
{
    public abstract class BaseRequest
    {
        
        public string UserToken { get; set; }

       
        public string Language { get; set; }

       
        public int BrandId { get; set; } = 0;
    }
}
