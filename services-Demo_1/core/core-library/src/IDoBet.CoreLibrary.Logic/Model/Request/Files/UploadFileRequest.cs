﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDoBet.CoreLibrary.Logic.Model.Enums;
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
using Microsoft.AspNetCore.Http;
#endif

namespace IDoBet.CoreLibrary.Logic.Model.Request.Files
{
    public class UploadFileRequest : BaseRequest
    {
        public eFileType FileType { get; set; }

        public string FileName { get; set; }

        public string OriginalFileName { get; set; }

#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
        public IFormFile File { get; set; }
#endif
    }
}
