﻿namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty
{
    public class StartGameRequest : BaseRequest
    {
        public string SessionId { get; set; }
        public string Currency { get; set; }
        public string UserId { get; set; }
        public string Device { get; set; }
        public string HomeUrl { get; set; }
        public string ExitUrl { get; set; }
        public string TACUrl { get; set; }

    }
}
