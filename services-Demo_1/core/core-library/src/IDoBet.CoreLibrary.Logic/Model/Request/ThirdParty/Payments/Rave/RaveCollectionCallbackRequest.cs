﻿#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.Payments.Rave
{
   public class RaveCollectionCallbackRequest
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("txRef")]
        public string TxRef { get; set; }
        [JsonPropertyName("flwRef")]
        public string FlwRef { get; set; }
        [JsonPropertyName("orderRef")]
        public string OrderRef { get; set; }
        [JsonPropertyName("paymentPlan")]
        public object PaymentPlan { get; set; }
        [JsonPropertyName("createdAt")]
        public DateTime? CreatedAt { get; set; }
        [JsonPropertyName("amount")]
        public double? Amount { get; set; }
        [JsonPropertyName("charged_amount")]
        public double? ChargedAmount { get; set; }
        [JsonPropertyName("status")]
        public string Status { get; set; }
        [JsonPropertyName("IP")]
        public string IP { get; set; }
        [JsonPropertyName("currency")]
        public string Currency { get; set; }
        [JsonPropertyName("customer")]
        public RaveCustomer Customer { get; set; }
    }

    public class RaveCustomer
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("phone")]
        public string Phone { get; set; }
        [JsonPropertyName("fullName")]
        public string FullName { get; set; }
        [JsonPropertyName("customertoken")]
        public object CustomerToken { get; set; }
        [JsonPropertyName("email")]
        public string Email { get; set; }
        [JsonPropertyName("createdAt")]
        public DateTime? CreatedAt { get; set; }
        [JsonPropertyName("updatedAt")]
        public DateTime? UpdatedAt { get; set; }

        [JsonPropertyName("deletedAt")]
        public DateTime? DeletedAt { get; set; }

        [JsonPropertyName("accountId")]
        public int AccountId { get; set; }
    }
}
#endif
