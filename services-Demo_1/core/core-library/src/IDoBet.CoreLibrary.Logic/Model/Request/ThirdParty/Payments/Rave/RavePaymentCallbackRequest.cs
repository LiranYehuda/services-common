﻿#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.Payments.Rave
{
    public class RaveTransfer
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("account_number")]
        public string AccountNumber { get; set; }
        [JsonPropertyName("bank_code")]
        public string BankCode { get; set; }
        [JsonPropertyName("fullname")]
        public string FullName { get; set; }
        [JsonPropertyName("date_created")]
        public DateTime? DateCreated { get; set; }
        [JsonPropertyName("currency")]
        public string Currency { get; set; }
        [JsonPropertyName("debit_currency")]
        public object DebitCurrency { get; set; }
        [JsonPropertyName("amount")]
        public double? Amount { get; set; }
        [JsonPropertyName("fee")]
        public double? Fee { get; set; }
        [JsonPropertyName("status")]
        public string Status { get; set; }
        [JsonPropertyName("reference")]
        public string Reference { get; set; }
        [JsonPropertyName("meta")]
        public object Meta { get; set; }
        [JsonPropertyName("narration")]
        public string Narration { get; set; }
        [JsonPropertyName("approver")]
        public object Approver { get; set; }
        [JsonPropertyName("complete_message")]
        public string CompleteMessage { get; set; }
        [JsonPropertyName("requires_approval")]
        public int RequiresApproval { get; set; }
        [JsonPropertyName("is_approved")]
        public int IsApproved { get; set; }
        [JsonPropertyName("bank_name")]
        public string BankName { get; set; }

        public bool IsSuccessfull { get { return Status == "success"; } }
    }

    public class RavePaymentCallbackRequest
    {
        [JsonPropertyName("event.type")]
        public string EventType { get; set; }

        [JsonPropertyName("transfer")]
        public RaveTransfer Transfer { get; set; }
    }
}
#endif
