﻿#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.Payments.Yellowbet
{
    public class NotifyDYAReceiveRequest
    {
        public static readonly int SuccessRetCode = 1000;

        [JsonPropertyName("MSISDN")]
        public long Msisdn { get; set; }
        [JsonPropertyName("ServiceID")]
        public long ServiceId { get; set; }
        [JsonPropertyName("TransactionID")]
        public string TransactionId { get; set; }
        [JsonPropertyName("PartnerTransactionID")]
        public string PartnerTransactionId { get; set; }
        [JsonPropertyName("Description")]
        public string Description { get; set; }
        [JsonPropertyName("ResultCode")]
        public int ResultCode { get; set; }
        [JsonPropertyName("Timestamp")]
        public string Timestamp { get; set; }
    }
}
#endif
