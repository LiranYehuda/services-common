﻿#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
using System.Text.Json.Serialization;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.Payments.Rave
{

    public class RaveFetchTransferRequest
    {
        [JsonPropertyName("reference")]
        public string Reference { get; set; }
        [JsonPropertyName("seckey")]
        public string SecretKey { get; set; }
    }
}
#endif
