﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.BettableProvider
{
    public class CheckAccountRequest : BaseRequest
    {                
        public override string EndpointConfigurationKey => $"{this.EndpointConfigurationKeyPrefix}CheckAccount";
    }
}
