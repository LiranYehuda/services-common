﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.BettableProvider
{
    public class SettleBetRequest : BaseRequest
    {        
        public int WinStatus { get; set; }
        public int Payout { get; set; }
        public int Bonus { get; set; }
        public int Tax { get; set; }        

        public override string EndpointConfigurationKey => $"{this.EndpointConfigurationKeyPrefix}SettleBet";
    }
}
