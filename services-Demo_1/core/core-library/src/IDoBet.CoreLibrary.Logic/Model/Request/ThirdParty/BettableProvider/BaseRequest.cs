﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.BettableProvider
{
    public abstract class BaseRequest
    {
        public string SessionId { get; set; }
        public string ApiToken { get; set; }
        public int BrandId { get; set; }
        public long BetId { get; set; }
        public int UserId { get; set; }
        /// <summary>
        // Will used to redirect all request to DB action and not http actions
        /// </summary>
        public bool UseBackgroundProcess { get; set; }
        public object ExtraData { get; set; }
        protected readonly string EndpointConfigurationKeyPrefix = "BettingThirdParty";
        public abstract string EndpointConfigurationKey { get;  }
        public string EndpointAddress { get; set;  }
    }
}
