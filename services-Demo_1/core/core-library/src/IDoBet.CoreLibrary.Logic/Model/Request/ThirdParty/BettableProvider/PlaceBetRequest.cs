﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.BettableProvider
{
    public class PlaceBetRequest : BaseRequest
    {       
        public double Amount { get; set; }
        public double MaxPayout { get; set; }
        public double? MaxBonus { get; set; }
        public double? MaxTax { get; set; }
        public string ProductType { get; set; }
        public string ProductDesc { get; set; }        

        public override string EndpointConfigurationKey => $"{this.EndpointConfigurationKeyPrefix}PlaceBet";
    }
}
