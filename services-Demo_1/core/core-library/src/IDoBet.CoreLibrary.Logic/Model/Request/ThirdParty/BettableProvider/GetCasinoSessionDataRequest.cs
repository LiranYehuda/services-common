﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.BettableProvider
{
    public class GetCasinoSessionDataRequest : BaseRequest
    {
        public bool ValidateSessionOpen { get; set; } = false;
        public override string EndpointConfigurationKey => null;
    }
}
