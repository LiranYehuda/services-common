﻿

using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty
{
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
    public class GenerateCasinoSessionIdRequest : BaseRequest
    {        
        public long GameId { get; set; }
        public string Currency { get; set; }        
        public long UserId { get; set; }
        public string Device { get; set; }
        public string HomeUrl { get; set; }
        public string HistoryUrl { get; set; }
        public bool IsDemo { get; set; }        
    }
#endif
}
