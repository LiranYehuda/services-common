﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.BettableProvider
{
    public class RollbackBetRequest : BaseRequest
    {                                
        public override string EndpointConfigurationKey => $"{this.EndpointConfigurationKeyPrefix}RollbackBet";      
    }
}
