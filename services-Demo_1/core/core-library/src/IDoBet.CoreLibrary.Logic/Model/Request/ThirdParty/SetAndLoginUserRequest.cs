﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty
{
    public class SetAndLoginUserRequest : BaseRequest
    {
        public string SessionId { get; set; }
        public string Currency { get; set; }
        public string UserId { get; set; }
        public string Device { get; set; }
        public string HomeUrl { get; set; }
        public string ExitUrl { get; set; }
        public string TACUrl { get; set; }
        public eProductType? ProductId { get; set; }        
        public new string GroupName => "Sport";
        public new string MethodName => "ThirdPartySetAndLoginUser";
    }
}
