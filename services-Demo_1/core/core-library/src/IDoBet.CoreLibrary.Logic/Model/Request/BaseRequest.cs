﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using IDoBet.CoreLibrary.Logic.Model.Enums;


namespace IDoBet.CoreLibrary.Logic.Model.Request
{
    public abstract class BaseRequest
    {
        public string GroupName { get; set; }
        public string OriginGroupName { get; set; }
        public string MethodName { get; set; }

        public static readonly List<string> DefaultParameters = new List<string> { "Token", "Language", "BrandId", "ChannelId", "ExtraData", "Terminal" };

        public void SetHeaders(Dictionary<string, List<string>> headers)
        {
            Token = headers.ContainsKey("token") ? headers["token"][0] : null;
            Language = headers.ContainsKey("language") ? headers["language"][0] : null;
            if (headers.ContainsKey("brandid"))
                if (int.TryParse(headers["brandid"][0], out int brandId))
                    BrandId = brandId;
            ExtraData = headers.ContainsKey("extradata") ? headers["extradata"][0] : null;

            if (headers.ContainsKey("channelid"))
                if (int.TryParse(headers["channelid"][0], out int channelId))
                    ChannelId = channelId;


            Terminal = headers.ContainsKey("terminal") ? headers["terminal"][0] : null;
            IP = headers.ContainsKey("ip") ? headers["ip"][0] : null;
        }
      


        public NameValueCollection GetHeaders()
        {
            var result = new NameValueCollection();
            if (!string.IsNullOrEmpty(Token))
                result.Add("token", Token);
            if (!string.IsNullOrEmpty(Language))
                result.Add("language", Language);
            if (BrandId != null)
                result.Add("brandid", BrandId.ToString());
            if (!string.IsNullOrEmpty(ExtraData?.ToString()))
                result.Add("extradata", ExtraData?.ToString());
            if (ChannelId != null)
                result.Add("channelid", ChannelId.ToString());
            if (!string.IsNullOrEmpty(Terminal))
                result.Add("terminal", Terminal);
            if (!string.IsNullOrEmpty(IP))
                result.Add("ip", IP);
            return result;
        }


        public string Token { get; set; }
        public string Language { get; set; }
        public int? BrandId { get; set; }
        public object ExtraData { get; set; }
        public int? ChannelId { get; set; }
        public string Terminal { get; set; }
        public string IP { get; set; }      

        public T CopyBase<T>(T to)
            where T : BaseRequest
        {
            to.Token = Token;
            to.Language = Language;
            to.BrandId = BrandId;
            to.ExtraData = ExtraData;
            to.ChannelId = ChannelId;
            to.Terminal = Terminal;
            to.IP = IP;
            return to;
        }
    }
}
