﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Request;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Cms
{
    public class SaveFileRequest : BaseRequest
    {
        public int RequestedChannelId { get; set; }

        public int? RequestedBranchId { get; set; }

        public bool IsDeleted { get; set; }

        public string FileKey { get; set; }

        public eFileType FileType { get; set; }


    }
}
