﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDoBet.CoreLibrary.Logic.Model.Enums;


namespace IDoBet.CoreLibrary.Logic.Model.Request.Cms
{
    public class GetTranslationsRequest : BaseRequest
    {     
        public string RequestedLangCode { get; set; }
        public int RequestedChannelId { get; set; }
        public int? RequestedBranchId { get; set; }
    }
}
