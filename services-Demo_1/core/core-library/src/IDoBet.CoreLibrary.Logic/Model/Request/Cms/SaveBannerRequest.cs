﻿#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
using Microsoft.AspNetCore.Http;
#endif

namespace IDoBet.CoreLibrary.Logic.Model.Response.Cms
{
    public class SaveBannerRequest : SaveFileRequest
    {
        public string Link { get; set; }

        public string PageAddress { get; set; }

        public string Alt { get; set; }

        public int? MaxWeight { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
        public IFormFile File { get; set; }
#endif

    }
}
