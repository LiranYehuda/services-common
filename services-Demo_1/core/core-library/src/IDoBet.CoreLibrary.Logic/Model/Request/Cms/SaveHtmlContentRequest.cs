﻿namespace IDoBet.CoreLibrary.Logic.Model.Response.Cms
{
    public class SaveHtmlContentRequest : SaveFileRequest
    {

        public string HtmlContent { get; set; }

    }
}
