﻿using System;
using System.Collections.Generic;
using System.Text;

using IDoBet.CoreLibrary.Logic.Model.Data.Limit;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class ApplyLimitRequest : BaseRequest
    {
        public List<LimitInputRow> Bets { get; set; }
        public double Amount { get; set; }
        public double CombinedEffect { get; set; }
        public double CurrencyRate { get; set; }
        public bool IsDbUpdate { get; set; }
    }
}
