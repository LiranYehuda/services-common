﻿using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class GetSportGamesCacheRequest : BaseRequest
    {
        public SportGameFilter Filter { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }
        public bool DbQuery { get; set; }
        public bool LoadBlobFromDb { get; set; }
        public string BrandConnectionString { get; set; }
        /// <summary>
        /// True used by Sql archiving to turn off bet type filtering.
        /// </summary>
        public bool IsForArchive { get; set; } = false;
        public bool IsRemoveEmptyGames { get; set; } = false;
        public bool IncludeResults { get; set; } = true;

    }
}
