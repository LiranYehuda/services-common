﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport.Client
{
    public class GetSportsTreeRequest : BaseRequest, ISportGameFilter
    {
        public SportGameFilter Filter
        {
            get
            {
                return new SportGameFilter {
                    SportTypeIds = GetHashsetFromList(SportTypeIds)
                };
            }
        }

        private HashSet<T> GetHashsetFromList<T>(List<T> src) => src == null ? null : new HashSet<T>(src.Distinct());
        private string _level;
        public List<string> ProdTypeIds { get; set; }
        public List<long> SportTypeIds { get; set; }
        public bool TopLevel { get; set; }
        public string Level
        {
            get { return TopLevel ? "sport" : (_level ?? "all"); }
            set { _level = string.IsNullOrEmpty(value) ? null : value.ToLower(); }
        }

        public GetSportGamesRequest ConvertToServerRequest(SportGameFilter systemFilter = null)
        {
            List<eProductType> productTypes = null;            

            var request = new GetSportGamesRequest
            {
                BrandId = BrandId,
                ChannelId = ChannelId,      
                IsCompressed = false,
                Language = Language,
                Filter = SportGameFilter.Intersect(systemFilter, Filter)               
            };

          

            if (ProdTypeIds != null)
            {
                productTypes = new List<eProductType>();

                ProdTypeIds = ProdTypeIds.Select(a => a.ToLower()).ToList();

                if (ProdTypeIds.Contains("live"))
                    productTypes.Add(eProductType.Live);

                if (ProdTypeIds.Contains("prematch"))
                    productTypes.Add(eProductType.Prematch);

                if (ProdTypeIds.Contains("virtual"))
                    productTypes.Add(eProductType.Virtual);
            }

            if (productTypes != null)
                request.Filter.SetProductFilter(productTypes);


            return request;
        }


    }
}
