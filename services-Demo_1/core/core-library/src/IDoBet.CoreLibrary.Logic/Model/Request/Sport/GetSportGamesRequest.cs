﻿using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using System;
using System.Collections.Generic;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class GetSportGamesRequest : BaseRequest
    {
        public SportGameFilter Filter { get; set; }        
        public int? Skip { get; set; }
        public int? Take { get; set; }
        public bool IsCompressed { get; set; } = true;
        public bool LogRequest { get; set; } = false;
        public bool IsDbQuery { get; set; } = false;
        public bool IsRemoveEmptyGames { get; set; } = false;
        public bool IncludeResults { get; set; } = true;
    }
}
