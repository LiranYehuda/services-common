﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport.Client
{
    public class GetClientEventsRequest : BaseRequest, ISportGameFilter
    {
        private HashSet<T> GetHashsetFromList<T>(List<T> src) => src == null ? null : new HashSet<T>(src.Distinct());
        public bool? ActiveCoupon { get; set; }
        public List<int> CouponIds { get; set; }
        public List<long> EventIds { get; set; }
        public List<long> BetTypeIds { get; set; }
        public List<long> SportIds { get; set; }
        public List<long> LeagueIds { get; set; }
        public List<string> ProdTypeIds { get; set; }
        public string SortBy { get; set; }
        public long? LastTimestamp { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }

        public SportGameFilter Filter
        {
            get
            {
                return new SportGameFilter
                {
                    EventStatusIds = new HashSet<long> { (int)eEventStatus.Prematch, (int)eEventStatus.Live },
                    CouponIds = GetHashsetFromList(CouponIds),
                    EventIds = GetHashsetFromList(EventIds),
                    SportTypeIds = GetHashsetFromList(SportIds),
                    LeagueIds = GetHashsetFromList(LeagueIds)
                };
            }
        }

        public GetSportGamesRequest ConvertToServerRequest(SportGameFilter systemFilter = null)
        {
            var result = new GetSportGamesRequest
            {
                BrandId = BrandId,
                ChannelId = ChannelId,
                Skip = Skip,
                Take = Take,
                IsCompressed = false,
                Language = Language,
                Filter = SportGameFilter.Intersect(systemFilter, Filter)               
            };
            if (LastTimestamp != null)
            {
                //result.Take = 5;
                result.Filter.SinceDate = LastTimestamp.Value.FromUnixTimeSatmp();                
            }
            if (BetTypeIds != null)
                if (BetTypeIds[0] != -1)
                    result.Filter.BetTypeIds = GetHashsetFromList(BetTypeIds);
            
            return result;
        }

        
    }
}
