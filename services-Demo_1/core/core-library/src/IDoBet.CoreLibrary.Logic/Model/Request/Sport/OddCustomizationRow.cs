﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class OddCustomizationRow
    {
        public bool Remove { get; set; }
        public long BetTypeId { get; set; }
        public string OddKey { get; set; }
        public string Line { get; set; }
        public OddCustomization Customization { get; set; }
        //         public Dictionary<(long betTypeId, string oddKey, string line), OddCustomization> Odds { get; set; }

    }
}
