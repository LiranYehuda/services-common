﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class UpdateSportGameCustomizationRequest : BaseRequest
    {
        public bool Remove { get; set; }
        public long SportGameId { get; set; }
        public double? PreMatchLimit { get; set; }
        public double? LiveLimit { get; set; }
        public SportGameScore Score { get; set; }
        public long? EventStatusId { get; set; }
        
        public List<OddCustomizationRow> Odds { get; set; }
        //public Dictionary<(long betTypeId, string oddKey, string line), OddCustomization> Odds { get; set; }
        //public SportGameCustomization Customization { get; set; }
    }
}
