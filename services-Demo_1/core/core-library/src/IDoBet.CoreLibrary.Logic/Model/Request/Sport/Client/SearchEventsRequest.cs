﻿using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport.Client
{
    public class SearchEventsRequest : BaseRequest, ISportGameFilter
    {
        public string Search { get; set; }
        public int? Take { get; set; }

        public SportGameFilter Filter
        {
            get
            {
                return new SportGameFilter {
                    SearchText = Search
                };
            }
        }

        public GetSportGamesRequest ConvertToServerRequest(SportGameFilter systemFilter = null)
        {
            var result = new GetSportGamesRequest
            {
                BrandId = BrandId,
                ChannelId = ChannelId,
                Take = Take ?? 20,
                IsCompressed = false,
                Language = Language,
                Filter = SportGameFilter.Intersect(systemFilter, Filter)
               
            };            

            return result;
        }
    }
}
