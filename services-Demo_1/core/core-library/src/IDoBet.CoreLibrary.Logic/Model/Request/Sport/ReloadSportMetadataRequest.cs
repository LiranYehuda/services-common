﻿using IDoBet.CoreLibrary.Logic.Model.Data.SportMetadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class ReloadSportMetadataRequest : BaseRequest
    {
        public bool? IsReloadSport      { get; set; }
        public bool? IsReloadCountry    { get; set; }
        public bool? IsReloadLeague     { get; set; }
        public bool? IsReloadTeam       { get; set; }
        public bool? IsReloadBetType    { get; set; }
        public bool? IsReloadOdd        { get; set; }

        public bool IsReload =>
            (IsReloadSport ?? false)
            || (IsReloadCountry ?? false)
            || (IsReloadLeague ?? false)
            || (IsReloadTeam ?? false)
            || (IsReloadBetType ?? false)
            || (IsReloadOdd ?? false);
    }
}
