﻿using IDoBet.CoreLibrary.Logic.Model.Data.Limit;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class IncreaseLimitRequest : BaseRequest
    {
        public List<IncreaseLimitRow> IncreaseLimit { get; set; }
    }
}
