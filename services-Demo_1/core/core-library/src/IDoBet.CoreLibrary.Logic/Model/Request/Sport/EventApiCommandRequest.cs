﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Sport
{
    public class EventApiCommandRequest : BaseRequest
    {
        public eEventApiCommand Command { get; set; }
        public string SenderId { get; set; }
        public string Parameters { get; set; }
    }
}
