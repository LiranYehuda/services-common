﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Internal
{
    public class HttpProxyRequest : BaseRequest
    {
        public Dictionary<string, string> HttpHeaders { get; set; }
        public string Url { get; set; }        
        public string Json =>            
            $"{{\"url\":\"{Url}\"" + 
            (HttpHeaders == null 
                ? "}" 
                : ", \"httpHeaders\":{" + string.Join(",", HttpHeaders.Select(x => $"\"{x.Key}\":\"{x.Value}\"")) + "}}");

    }
}
