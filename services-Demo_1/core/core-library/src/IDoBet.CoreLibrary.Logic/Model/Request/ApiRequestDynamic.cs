﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Enums;

namespace IDoBet.CoreLibrary.Logic.Model.Request
{
    public class ApiRequestDynamic : BaseRequest
    {
        public dynamic Parameter { get; set; }
    }
}
