﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PlaceBetSelection
    {
        public string Key { get; set; }
        public long EventId { get; set; }
        public long BetTypeId { get; set; }       
        public string OddName { get; set; }
        public string OddLine { get; set; }
        public decimal OddPrice { get; set; }        
        public string Score { get; set; }
    }
}
