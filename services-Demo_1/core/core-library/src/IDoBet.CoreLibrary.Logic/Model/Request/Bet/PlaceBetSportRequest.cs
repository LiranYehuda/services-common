﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PlaceBetSportRequest : BaseRequest
    {
        public List<PlaceBetSelection> Selections { get; set; }
        public List<PlaceBetRow> Rows { get; set; }
        public bool? IsBooking { get; set; }
        public string AuthorizationKey { get; set; }     
        public string ExternalId { get; set; }
        public string PreviousOrderGuid { get; set; }
        public int? ForUserId { get; set; }
    }
}
