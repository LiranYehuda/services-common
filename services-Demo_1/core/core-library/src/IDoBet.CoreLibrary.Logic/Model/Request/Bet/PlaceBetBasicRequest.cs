﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PlaceBetBasicRequest : BaseRequest
    {
        public double Amount { get; set; }
        public double MaxPayout { get; set; }
        public bool? IsExecute { get; set; }
        public byte ProdType { get; set; }
        public string ExternalId { get; set; }
        public string ExternalData { get; set; }
        public Guid? AuthorizationKey { get; set; }
        public int? ForUserId { get; set; }
        public bool UserInfoRequired { get; set; } = true;
        public bool IsBasedOnBookingCode { get; set; }
    }
}
