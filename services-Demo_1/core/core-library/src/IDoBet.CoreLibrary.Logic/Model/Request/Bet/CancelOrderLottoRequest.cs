﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class CancelOrderLottoRequest : BaseRequest
    {
        public int UserId { get; set; }
        public int UserBetId { get; set; }

    }
}
