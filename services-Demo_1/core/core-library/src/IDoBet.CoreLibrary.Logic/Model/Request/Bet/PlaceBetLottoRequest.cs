﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PlaceBetLottoRequest : BaseRequest
    {
        public double Payment { get; set; }
        public int LotteryTypeId { get; set; }
        public int BetTypeId { get; set; }
        public string DrawId { get; set; }
        public string BetNumbers { get; set; }
        public int GroupId { get; set; }
        public int CurrencyId { get; set; }
        public int UserId { get; set; }
        public string TransactionId { get; set; }
        public bool IsSpecialJackpot { get; set; }
        public string MethodName { get; set; } = "UpdateUserBet";
        public string GroupName { get; set; } = "Lotto";
        public bool UserInfoRequired { get; set; } = true;
        public bool IsExecute { get; set; } = true;
        public string ExternalId { get; set; }
        public string BookingCodeOrigin { get; set; }


    }
}
