﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class ExecuteBetLottoRequest : BaseRequest
    {
        public Guid RequestGuid { get; set; }
        public string ExternalId { get; set; }
        public int UserBetId { get; set; }
        public int? ForUserId { get; set; }
        public bool UserInfoRequired { get; set; } = true;
        
        public string TransactionId { get; set; }
        public int UserId { get; set; }
        public string ExternalTransactionId { get; set; }
        public string TransactionExtraData { get; set; }
        public string MethodName { get; set; } = "ConfirmBet";
        public string GroupName { get; set; } = "Lotto";
    }
}
