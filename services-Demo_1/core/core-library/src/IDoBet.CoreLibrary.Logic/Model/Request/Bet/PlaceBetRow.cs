﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PlaceBetRow
    {
        public double Amount { get; set; }
        public double? RowBonusAmount { get; set; }        
        public int? NextBetBonusOrderId { get; set; }
        public List<string> SelectionKeys { get; set; }
        public PlaceBetCombination CombinationData { get; set; }
    }
}
