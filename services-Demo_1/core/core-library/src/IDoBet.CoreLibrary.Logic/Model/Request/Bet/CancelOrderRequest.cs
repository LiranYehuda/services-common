﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class CancelOrderRequest : BaseRequest
    {
        public long? OrderId { get; set; }
        public int? CancelBy { get; set; }
        public bool CanceledByToken { get; set; }
        public string CancellationReason { get; set; }
        public byte? CancelReasonTypeId { get; set; }
        public string ExpectedProductIds { get; set; }                                
        public string ExternalId { get; set; }
        public bool OutPutBalance { get; set; }
    }
}
