﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class ExecuteBetBasicRequest : BaseRequest
    {
        public Guid RequestGuid { get; set; }
        public string ExternalId { get; set; }
        public int? LottoUserBetId { get; set; }
        public int? ForUserId { get; set; }
        public string ExternalTransactionId { get; set; }
        public string TransactionExtraData { get; set; }
        public bool UserInfoRequired { get; set; } = true;
    }
}
