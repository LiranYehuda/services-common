﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class GetPoolGameByIdRequest : BaseRequest
    {
        public int PoolGameId { get; set; }
        public bool IsFromBackoffice { get; set; }
        public new string GroupName => "Sport";
        public new string MethodName => "GetPoolById";

    }
}
