﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PlaceBetCombination
    {
        public int ComboCount { get; set; }
        public bool IsGroup { get; set; }
        //BankerEvents should be kept at this level for future use of groups. Nemmy
        //public List<long> BankerEvents { get; set; }
        public List<string> BankerSelections { get; set; }
        public bool SplitAmount { get; set; }
    }
}
