﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Request;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class GetOrderKeyRequest: BaseRequest
    {
        public string Barcode { get; set; }                
    }
}
