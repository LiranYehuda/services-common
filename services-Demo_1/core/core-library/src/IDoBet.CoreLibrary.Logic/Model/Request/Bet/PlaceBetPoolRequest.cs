﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PlaceBetPoolRequest : BaseRequest
    {      
        public int PoolGameId { get; set; }    
        public List<PoolBet> Bets { get; set; }        
        public string ExternalId { get; set; }      
        public bool IsBooking { get; set; }           
    }
}
