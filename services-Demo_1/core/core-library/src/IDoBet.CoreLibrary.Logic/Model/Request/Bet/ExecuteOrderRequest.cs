﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class ExecuteOrderRequest : BaseRequest
    {
        public string TimeoutGuid { get; set; }
        public int? ForUserId { get; set; }
        public string ExternalTransactionId { get; set; }
        public string TransactionExtraData { get; set; }
    }
}
