﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.Bet
{
    public class PoolBet
    {
        
        public long EventId { get; set; }
       
        public List<string> Odds { get; set; }
    }
}
