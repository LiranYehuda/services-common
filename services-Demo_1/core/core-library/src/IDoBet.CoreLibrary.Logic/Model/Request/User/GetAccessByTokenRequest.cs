﻿namespace IDoBet.CoreLibrary.Logic.Model.Request.User
{
    public class GetAccessByTokenRequest : BaseRequest
    {
        public new string GroupName => "Sport";
        public new string MethodName => "GetAccessByToken";
    }
}
