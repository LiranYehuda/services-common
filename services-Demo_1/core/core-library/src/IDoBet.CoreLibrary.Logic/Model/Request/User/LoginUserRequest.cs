﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.User
{
    public class LoginUserRequest : BaseRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }        
        public string UserAgent { get; set; }
        public string TerminalSecurityCode { get; set; }
        public bool IsPrePaid { get; set; }
        public int RegistrationType { get; set; }
        public string LoginIp { get; set; }
        public bool IpRequestedFromService { get; set; } = false;

        public new string GroupName => "Sport";
        public new string MethodName => "LoginUser";
    }
}
