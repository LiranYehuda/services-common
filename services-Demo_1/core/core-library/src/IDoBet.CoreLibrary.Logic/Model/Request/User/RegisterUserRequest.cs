﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Request.User
{
    public class RegisterUserRequest : BaseRequest
    {
        public int RegistrationType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PrepaidCode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PhoneCountryCode { get; set; }
        public string CampaignCode { get; set; }
        public string RegistrationDetails { get; set; }
        public string ClientIP { get; set; }
        public bool IpRequestedFromService { get; set; } = false;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? CurrencyId { get; set; }
        public int? CountryId { get; set; }
       

        public new string GroupName => "Sport";
        public new string MethodName => "RegisterUser";
    }
}
