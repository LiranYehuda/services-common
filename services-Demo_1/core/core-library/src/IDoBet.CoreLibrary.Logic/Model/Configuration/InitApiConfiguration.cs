﻿using IDoBet.CoreLibrary.Logic.Model.Enums;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration
{
    public class InitApiConfiguration
    {
        public int Id { get; set; }
        public int BrandId { get; set; }
        public eKeyType KeyType { get; set; }
        public string GroupName { get; set; }
        public string MethodName { get; set; }
        public string EndPoint { get; set; }
        public string ActionName { get; set; }
        public string Value { get; set; }
      
    }
}
