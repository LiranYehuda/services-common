﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration
{
    public class ApiEndpointConfiguration
    {
        public long Id { get; set; }
        public int BrandId { get; set; }
        public int ModuleId { get; set; }
        public string Instance { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Authentication { get; set; }
        public string AdditionalData { get; set; }
        public string Description { get; set; }
    }
}
