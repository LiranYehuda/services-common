﻿using System.Collections.Generic;
using System.Reflection;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration
{
    public class ApiRouteConfiguration
    {
        public long? Id { get; set; }
        public int BrandId { get; set; }                
        public string MethodName { get; set; }
        public string SpName { get; set; }
        public long CacheTimeout { get; set; }
        public bool IsReadOnly { get; set; }
        public object Parameters { get; set; }
    }
}
