﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration
{
    public class ActionConfiguration
    {
        public ApiRouteConfiguration Route { get; set; }
        public ApiEndpointConfiguration Endpoint { get; set; }
    }
}
