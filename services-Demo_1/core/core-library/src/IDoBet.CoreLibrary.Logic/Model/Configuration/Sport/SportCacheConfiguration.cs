﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class SportCacheConfiguration
    {
        public int CheckForChangesInMsec { get; set; }
        public int CheckForRemoveInMsec { get; set; } = 600;
        public int TtlHoursFromGameStart { get; set; } = 24;
        public bool IsCompressed { get; set; }
        public SportGameFilter Filter { get; set; }        
    }
}
