﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class SportPluginConfiguration
    {
        public SportRepositoryConfiguration Repository { get; set; }
        public SportCacheConfiguration Cache { get; set; }
        public SportSqlConfiguration Sql { get; set; }
        public SportMetadataConfiguration Metadata { get; set; }
        public SportLimitConfiguration Limit { get; set; }
        public int BrandId { get; set; }
    }
}
