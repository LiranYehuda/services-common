﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class MarginCacheConfiguration
    {
        public ISportCachePlugin Cache { get; set; }
        /// <summary>
        /// Margin configurations per channel. Sorted list key is evaluation order.
        /// For example if Premier League need different margins settings it must preceed other filters which also contains premier league.
        /// Last element of the list will be the default settings if none of preceeding filter match the game.
        /// </summary>
        public ConcurrentDictionary<eChannel, ConcurrentDictionary<eMarginPriority, List<MarginConfiguration>>> Channels { get; set; } = new ConcurrentDictionary<eChannel, ConcurrentDictionary<eMarginPriority, List<MarginConfiguration>>>();

        public void Add(eChannel channel, MarginConfiguration src)
        {
            if (!Channels.ContainsKey(channel))            
                Channels.AddOrUpdate(channel, new ConcurrentDictionary<eMarginPriority, List<MarginConfiguration>>(), (key, oldValue) => { return oldValue; });
            var currentConfig = Channels[channel];
            var priority = src.Priority ?? SportGameFilter.MarginPriority(src.Filter);
            if (!currentConfig.ContainsKey(priority))           
                currentConfig.AddOrUpdate(priority, new List<MarginConfiguration>(), (key, oldValue) => { return oldValue; });
            currentConfig[priority].Add(src);            
        }
    }
}
