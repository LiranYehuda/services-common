﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class SportCouponConfiguration
    {
        public int BrandId { get; set; }
        public string ConnectionString { get; set; }
    }
}
