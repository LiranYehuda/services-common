﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class BrandCacheConfiguration
    {
        public int BrandId { get; set; }
        public SportLimitConfiguration Limit { get; set; }
        public MarginCacheConfiguration Margin { get; set; }
        public SportCouponConfiguration CouponConfiguration { get; set; }
        public ISportCachePlugin CommonCache { get; set; }
        public ISportMetadataPlugin Metadata { get; set; }
        public ISportCouponPlugin Coupon { get; set; }
    }
}
