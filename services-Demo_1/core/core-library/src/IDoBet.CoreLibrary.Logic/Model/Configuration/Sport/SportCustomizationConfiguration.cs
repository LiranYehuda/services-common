﻿using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{

    public class SportCustomizationConfiguration
    {
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
        public ISportCustomizationRepositoryPlugin Repository { get; set; }
#endif
    }
}
