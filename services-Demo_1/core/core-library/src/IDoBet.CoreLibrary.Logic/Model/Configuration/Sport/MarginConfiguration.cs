﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class MarginConfiguration
    {
        public int Id { get; set; }
        public eMarginPriority? Priority { get; set; }
        public HashSet<eChannel> Channels { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Apply margin on games/bettypes match the filter.
        /// </summary>
        public SportGameFilter Filter { get; set; }
        /// <summary>
        /// Increase current margin with percent. (0-100)
        /// </summary>
        public double? MarginPercent { get; set; }
        /// <summary>
        /// In case current margin under this value, it's reset to this value (0-100)
        /// </summary>
        public double? MinimumMargin { get; set; }
        /// <summary>
        /// Set margin exactly this value (0-100)
        /// </summary>
        public double? ExactMargin { get; set; }
        /// <summary>
        /// Upper limit of result margin.
        /// </summary>
        public double? MaximumMargin { get; set; }
        /// <summary>
        /// If odd result price is under this value, odd suspended.
        /// </summary>
        public double? PriceCap { get; set; }
    }
}
