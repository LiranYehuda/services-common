﻿using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class SportLimitConfiguration 
    {
        public string ConnectionString { get; set; }
        public string RedisAddress { get; set; }
        public ISportCachePlugin Cache { get; set; }
        public ISportMetadataPlugin Metadata { get; set; }
        public ISportLimitRepositoryPlugin Repository { get; set; }
    }
}
