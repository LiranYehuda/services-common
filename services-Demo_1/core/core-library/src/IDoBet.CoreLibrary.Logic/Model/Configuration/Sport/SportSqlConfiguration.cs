﻿using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class SportSqlConfiguration
    {
        public bool IsEnabled { get; set; } = true;
        public int CheckForChangesInMsec { get; set; } = 2000;
        public ISportCachePlugin Cache { get; set; }
        public bool IsCompressed { get; set; }
        public SportGameFilter Filter { get; set; }
        public string ConnectionString { get; set; }
        public int? BatchSize { get; set; }
    }
}