﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class SportRepositoryConfiguration
    {
        public string SportGamesConnectionString { get; set; }
        public string RedisAddressFeed { get; set; }
        public string RedisAddressBranded { get; set; }        
    }
}
