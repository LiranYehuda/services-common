﻿using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration.Sport
{
    public class MainCacheConfiguration
    {
        public string InstancePrefix { get; set; }
        public HashSet<int> InitBrandIds { get; set; }
        public SportRepositoryConfiguration Repository { get; set; }
        public SportCacheConfiguration Cache { get; set; }
        public SportSqlConfiguration Archive { get; set; }
        public SportMetadataConfiguration Metadata { get; set; }
        public SportCustomizationConfiguration Customization { get; set; }
    }
}
