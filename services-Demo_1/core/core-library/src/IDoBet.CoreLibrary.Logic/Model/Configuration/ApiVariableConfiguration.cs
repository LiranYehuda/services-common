﻿using System.Collections.Generic;

namespace IDoBet.CoreLibrary.Logic.Model.Configuration
{
    public class ApiVariableConfiguration
    {
        public long Id { get; set; }
        public int BrandId { get; set; }
        public int ModuleId { get; set; }
        public string Instance { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int? ChannelId { get; set; }
        public int? BranchId { get; set; }
    }
}
