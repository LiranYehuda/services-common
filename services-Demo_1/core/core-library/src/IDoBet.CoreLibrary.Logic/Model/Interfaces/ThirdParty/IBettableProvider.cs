﻿using IDoBet.CoreLibrary.Logic.Model.Request.ThirdParty.BettableProvider;
using IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty.BettableProvider;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.ThirdParty
{
    public interface IBettableProvider
    {
        CheckAccountResponse CheckAccount(CheckAccountRequest request);        
        CheckBalanceResponse CheckBalance(CheckBalanceRequest request);        
        PlaceBetResponse PlaceBet(PlaceBetRequest request);        
        SettleBetResponse SettleBet(SettleBetRequest request);        
        RollbackBetResponse RollbackBet(RollbackBetRequest request);        
    }
}
