﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces
{
    public interface IService
    {
        void Start();
        void Stop();
    }
}
