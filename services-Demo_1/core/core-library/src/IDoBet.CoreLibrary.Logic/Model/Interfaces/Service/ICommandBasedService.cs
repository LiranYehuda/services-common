﻿using IDoBet.CoreLibrary.Logic.Model.Enums.Sport;
using IDoBet.CoreLibrary.Logic.Model.Service;
using System;
using System.Collections.Generic;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Service
{
    public interface ICommandBasedService
    {
        event EventHandler<ServiceMessage> OnMessage;
        void Initialize(int providerId, int providerSourceId);
        void Initialize(HashSet<int> brandId);
        void Command(ServiceCommand command);
    }
}
