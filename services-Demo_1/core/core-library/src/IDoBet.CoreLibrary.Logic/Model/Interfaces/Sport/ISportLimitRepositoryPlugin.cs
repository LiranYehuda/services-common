﻿using IDoBet.CoreLibrary.Logic.Model.Data.Limit;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportLimitRepositoryPlugin
    {
        EventLimitation Get(int brandId, long eventId, long betTypeId);
        HashSet<long> GetBetTypeIds(int brandId, long eventId);
        void AddOrUpdate(EventLimitation src, int brandId);
    }
}
