﻿using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportGameFilter
    {
        SportGameFilter Filter { get; }
    }
}
