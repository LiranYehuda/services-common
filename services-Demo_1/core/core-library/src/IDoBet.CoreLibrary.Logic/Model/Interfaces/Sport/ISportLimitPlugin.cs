﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Request.Sport;
using IDoBet.CoreLibrary.Logic.Model.Response;
using IDoBet.CoreLibrary.Logic.Model.Response.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportLimitPlugin : ISportPluginBase
    {
        ApplyLimitResponse ApplyLimit(ApplyLimitRequest requets);
        IEnumerable<SportGame> Merge(IEnumerable<SportGame> src);
        IncreaseLimitResponse IncreaseMaxLimit(IncreaseLimitRequest src);
    }
}
