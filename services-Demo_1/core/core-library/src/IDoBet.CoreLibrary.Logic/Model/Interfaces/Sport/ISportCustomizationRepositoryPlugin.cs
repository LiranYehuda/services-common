﻿using IDoBet.CoreLibrary.Logic.Model.Configuration.Sport;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)

    public interface ISportCustomizationRepositoryPlugin
    {

        void Reload(int? brandId, HashSet<long> eventIds);
        void Reset(HashSet<long> eventIds);
        void Initialize(SportRepositoryConfiguration configuration);
        void RemoveFromCache(long sportGameId);
        SportGameCustomization this[int brandId, long eventId]
        {
            get;
            set;
        }

        SportGameCustomization this[int brandId, long eventId, bool searchArchive]
        {
            get;
        }

        //OddCustomization this[int brandId, long eventId, long betTypeId, string oddName, string line]
        //{
        //    get;
        //    set;
        //}
    }
#endif
}
