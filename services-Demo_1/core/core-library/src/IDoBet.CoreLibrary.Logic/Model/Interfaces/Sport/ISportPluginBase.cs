﻿using IDoBet.CoreLibrary.Logic.Model.Configuration.Sport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportPluginBase
    {
        // IEnumerable<SportGame> Process(ISportRepository sportGameRepository, 
        /*
                            Input               Output              Dependency
        Initialize          None                None                TargetDep
        Cache               GameFilter          GameList            RedisRep
        Alter               GameList/GameEntity 
                            Updates?            GameList            None
        Validate(Limit)     GameList
                            ValidationData      ValidationResult    None | SqlRep
        BO custom           Updates             None                TargetRep 
        Margin              GameList/Entity     GameList            None
                            Config
        
        
        
        
        
        
         */
        //Action<ISportRepository> Initialize { get; }
        //Func<SportGameFilter, ISportRepository, IEnumerable<SportGame>> Cache { get; }
        //Func<IEnumerable<SportGame>, object, IEnumerable<SportGame>> Alter { get; }
        //Func<IEnumerable<SportGame>, object, object, ISportRepository> Validate { get;  }
        //Func<object, ISportRepository> Update { get; }
        //Func<IEnumerable<SportGame>, object, IEnumerable<SportGame>> Margin { get; }

        // void Initialize(ISportRepositoryPlugin repository, SportPluginConfiguration configuration);
        bool Loaded { get; }
        void ShutDown();
    }
}
