﻿using IDoBet.CoreLibrary.Logic.Model.Configuration.Sport;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Request.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportCustomizationPlugin : ISportPluginBase
    {
        void Initialize(SportCustomizationConfiguration configuration, HashSet<long> eventIds);
        IEnumerable<SportGame> Alter(int brandId, IEnumerable<SportGame> src, bool searchArchive);
        void UpdateSportCustomization(UpdateSportGameCustomizationRequest request);
        void ReloadSportCustomization(int? brandId, HashSet<long> eventIds);

        void RemoveFromCache(long sportGameId);

        // void SaveOddCustomization(OddCustomization src);
    }
}
