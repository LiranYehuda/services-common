﻿using IDoBet.CoreLibrary.Logic.Model.Configuration.Sport;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportMarginPlugin : ISportPluginBase
    {
        void Initialize(MarginCacheConfiguration configuration);
        IEnumerable<SportGame> Apply(int brandId, int channelId, IEnumerable<SportGame> src);
    }
}
