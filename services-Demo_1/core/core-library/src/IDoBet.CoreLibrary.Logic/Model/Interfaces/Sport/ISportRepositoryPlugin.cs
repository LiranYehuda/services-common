﻿using System;
using System.Collections.Generic;
using IDoBet.CoreLibrary.Logic.Model.Configuration.Sport;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using IDoBet.CoreLibrary.Logic.Model.Request.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportRepositoryPlugin : ISportPluginBase
    {
        DateTime ServerTime { get; }
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
        (List<IndexEntry> indexEntries, List<string> messageKeys)  GetUpdateIndexes(DateTime since);
        (byte[][] games, List<string> messageKeys) GetCompressedSportGames(SportGameFilter filter);
        (IEnumerable<SportGame> games, List<string> messageKeys) GetLocalSportGames(SportGameFilter filter);
        void ReReadStream();
        List<(string, long, long, long)> CheckSportGame(SportGame src);
#endif
        void Initialize(SportRepositoryConfiguration configuration);

        event EventHandler<EventApiCommandArgs> OnCommand;
        void BroadcastCommand(EventApiCommandArgs request);

        IEnumerable<SportGame> GetSportGamesFromDb(GetSportGamesCacheRequest request);

        List<string> UpdateMessages();

        List<IndexEntry> GetTestUpdateIndexes(DateTime from);
    }
}
