﻿using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDoBet.CoreLibrary.Logic.Model.Configuration.Sport;
using IDoBet.CoreLibrary.Logic.Model.Response.Sport;
using IDoBet.CoreLibrary.Logic.Model.Request.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportCachePlugin : ISportPluginBase
    {
        void Initialize(ISportRepositoryPlugin repository, SportCacheConfiguration configuration, bool waitUntilLoaded);
        GetSportGamesCacheResponse GetSportGames(GetSportGamesCacheRequest request);
        HashSet<long> EventsInCache { get; }
        /// <summary>
        /// Occurs when adding new game to cache, or updating one.
        /// </summary>
        event EventHandler<AddOrUpdateGameArgs> AddOrUpdateGame;
        /// <summary>
        /// Occurs when removing item from cache.
        /// </summary>
        event EventHandler<RemoveGameArgs> RemoveGame;
        /// <summary>
        /// 
        /// </summary>
        event EventHandler<List<string>> OnUpdateCache;

        IEnumerable<SportGame> CacheContent { get; }

    }
}
