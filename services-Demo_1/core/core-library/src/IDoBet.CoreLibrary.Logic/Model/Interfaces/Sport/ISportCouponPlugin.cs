﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Filter.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportCouponPlugin : ISportPluginBase
    {
        IEnumerable<SportGame> Merge(IEnumerable<SportGame> src);
        void Reload();
        void ApplyCouponFilter(SportGameFilter src);
        int[] ActiveCouponIds { get; }
    }
}
