﻿using IDoBet.CoreLibrary.Logic.Model.Configuration.Sport;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Request.Sport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Interfaces.Sport
{
    public interface ISportMetadataPlugin : ISportPluginBase
    {
        void Initialize(SportMetadataConfiguration configuration);
        IEnumerable<SportGame> Merge(IEnumerable<SportGame> src, int? brandId);
        void Reload(ReloadSportMetadataRequest request);
        void AddBrands(HashSet<int> brandIds);
        void RemoveBrand(int brandId);
    }
}
