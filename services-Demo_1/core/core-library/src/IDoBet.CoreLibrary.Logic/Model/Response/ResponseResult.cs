﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using System.Runtime.Serialization;

namespace IDoBet.CoreLibrary.Logic.Model.Response
{
    //[DataContract]
    public class ResponseResult
    {
        //[DataMember]
        public string ErrorDescription { get; set; }
        //[DataMember]
        public object AdditionalInfo { get; set; }
        //[DataMember]
        public eErrorCode ErrorCode { get; set; }
        //[DataMember]
        public eResultCode ResultCode { get; set; }
        //[DataMember]
        public string ErrorCodeDescription { get; set; }
    }
}
