﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.User
{
    public class LoginUserResponse : BaseResponse
    {
        public Guid? Token { get; set; }
    }
}
