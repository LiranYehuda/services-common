﻿using IDoBet.CoreLibrary.Logic.Model.Data.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.User
{
    public class GetAccessByTokenResponse: BaseResponse
    {
        public Access Access { get; set; }
    }
}
