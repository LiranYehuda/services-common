﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty.BettableProvider
{
    public class CheckAccountResponse : BaseResponse
    {
        public int Status { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CurrencyId { get; set; }
    }
}
