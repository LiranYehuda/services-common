﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty.BettableProvider
{
    public class CheckBalanceResponse : BaseResponse
    {
        public string UserId { get; set; }
    }
}
