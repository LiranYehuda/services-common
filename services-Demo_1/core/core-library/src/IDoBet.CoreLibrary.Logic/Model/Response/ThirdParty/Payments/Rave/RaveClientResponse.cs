﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty.Payments.Rave
{
    public class RaveClientResponse
    {
        public bool IsSuccessfull { get { return Status == "success"; } }
        public string Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public object AdditionalData { get; set; }
    }
}
