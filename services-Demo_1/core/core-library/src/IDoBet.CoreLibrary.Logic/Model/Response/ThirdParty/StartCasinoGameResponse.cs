﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty
{
    public class StartCasinoGameResponse : BaseResponse
    {
        public string SourceUrl { get; set; }
    }
}
