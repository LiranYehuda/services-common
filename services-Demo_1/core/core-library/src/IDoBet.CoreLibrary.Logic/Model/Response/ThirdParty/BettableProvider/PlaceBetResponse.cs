﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty.BettableProvider
{
    public class PlaceBetResponse : BaseResponse
    {
        public string TransactionId { get; set; }
    }
}
