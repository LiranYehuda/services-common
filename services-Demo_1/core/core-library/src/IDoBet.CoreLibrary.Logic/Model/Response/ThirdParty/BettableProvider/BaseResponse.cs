﻿using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty.BettableProvider
{
    public class BaseResponse
    {
        public int ResultCode { get; set; }
        public string Error { get; set; }
        public object AdditionalInfo { get; set; }

        public double Balance { get; set; }
        public double BonusBalance { get; set; }

        public object Data { get; set; }

        public void SetErrorResult(eErrorCode errorCode = eErrorCode.ApiError, string errorDescription = null, object additionalInfo = null)
        {
            ResultCode = (int)errorCode;
            Error = errorDescription;            
            AdditionalInfo = additionalInfo;
        }
        public bool IsSuccessfull
        {
            get
            {
                return ResultCode == (int)eErrorCode.Success;
            }
        }
    }
}
