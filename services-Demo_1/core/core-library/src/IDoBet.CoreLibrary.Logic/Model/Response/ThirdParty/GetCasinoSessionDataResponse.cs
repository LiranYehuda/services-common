﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty
{
    public class GetCasinoSessionDataResponse : BaseResponse
    {
        public string Token { get; set; }        
        public string Language { get; set; }
        public int BrandId { get; set; }                
        public long UserId { get; set; }
        public string CurrencyCode { get; set; }
        public long GameId { get; set; }
    }
}
