﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ThirdParty
{
    public class SetAndLoginUserResponse : BaseResponse
    {
        public string Token { get; set; }
    }
}
