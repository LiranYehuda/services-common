﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Request;
using System;
using System.Collections.Generic;
using System.Text;

//#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
//using System.Text.Json;
//#endif
namespace IDoBet.CoreLibrary.Logic.Model.Response.Sport
{
    public class GetSportGamesResponse : BaseResponse
    {
        //public int[] ActiveCouponIds { get; set; }
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
        [System.Text.Json.Serialization.JsonPropertyName("Data")]
#endif
        public new List<SportGame> Data { get; set; }
        public DateTime? LastServerTime { get; set; }
        public byte[][] CompressedData { get; set; }
        public int[] ActiveCouponIds { get; set; }
        /*
        public static T Error<T>(T request, eErrorCode error)


        public static U Error<U, T>(T request, eErrorCode error)
            where U : BaseResponse, new()
            where T : BaseRequest
        {
            switch (error)
            {
                case eErrorCode.EventsNotReady:
                    return NotLoaded<T>();
                case eErrorCode.InvalidBrandId:
                    return InvalidBrandId<T>();
                default:
                    return new T
                    {
                        Result = new ResponseResult
                        {
                            ErrorCode = error,
                            ResultCode = eResultCode.Failure
                        }
                    };
            }
        }
        */
        public static T BrandNotLoaded<T>() where T : BaseResponse, new() =>
            new T
            {                
                Result = new ResponseResult
                {
                    ErrorCode = eErrorCode.EventsNotReady,
                    ResultCode = eResultCode.Failure
                }
            };
        public static T InvalidBrandId<T>() where T : BaseResponse, new() =>
            new T
            {
                Result = new ResponseResult
                {
                    ErrorCode = eErrorCode.InvalidBrandId,
                    ResultCode = eResultCode.Failure
                }
            };
    }
}
