﻿using System;
using System.Collections.Generic;
using System.Text;
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
using System.Text.Json;
using System.Text.Json.Serialization;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport.Client;
#endif

namespace IDoBet.CoreLibrary.Logic.Model.Response.Sport.Client
{
    public class GetEventsResponse : BaseResponse
    {
#if !(NET46 || NET461 || NET462 || NET47 || NET471 || NET472)
        private JsonSerializerOptions JsonSerializerOptions = new JsonSerializerOptions
        {
            IgnoreNullValues = true,            
        };
        public string Test { get; set; }
        // public List<ClientSportGame> Events { get; set; }

        // public int EventCount => Events?.Count ?? 0;
        public int TotalEventCount { get; set; }
        public long LastTimestamp { get; set; }
        [JsonIgnore]
        public List<int> ActiveCouponIds { get; set; }
        // Should fix on client side from ActiveCoupinIds to ActiveCouponIds
        public List<int> ActiveCoupinIds => ActiveCoupinIds;

        public string Serialize()
        {
            return JsonSerializer.ToString(this);
            // return "";
        }
#endif

    }
}
