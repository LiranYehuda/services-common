﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport.Client;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Sport.Client
{
    public class GetSportsTreeResponse : BaseResponse
    {
        public TreeNode SportsTree { get; set; }

    }
}
