﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Sport
{
    public class IncreaseLimitResponse : BaseResponse
    {
        public List<SportGameLog> Logs { get; set; }
    }
}
