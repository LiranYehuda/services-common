﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Sport
{    
    public class GetSportGamesCacheResponse
    {
        public DateTime? LastServerTime { get; set; }
        public IEnumerable<SportGame> Games { get; set; }
        public static GetSportGamesCacheResponse NotLoaded =>
            new GetSportGamesCacheResponse
            {
                LastServerTime = null,
                Games = null
            };
    }
}
