﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Enums;

namespace IDoBet.CoreLibrary.Logic.Model.Response
{
    //[DataContract]
    public class BaseResponse
    {
        public BaseResponse() { }

        //[DataMember]
        public ResponseResult Result { get; set; } = new ResponseResult();

        public void SetResult(DataTable from)
        {
            Result = new ResponseResult { ResultCode = eResultCode.Success, ErrorCode = eErrorCode.Success };
            if (from.Rows.Count != 1)
                return;
            Result.ErrorCode = (eErrorCode)(int.Parse(from.Rows[0]["codeNumber"]?.ToString()));
            Result.ErrorCodeDescription = from.Rows[0]["codeDescription"]?.ToString();
            if (Result.ErrorCode != eErrorCode.Success)
                Result.ResultCode = eResultCode.Failure;

            if (from.Columns.Contains("DataStructure"))            
                this.DataStructure = from.Rows[0]["DataStructure"]?.ToString();
        }
        public void SetErrorResult(eErrorCode errorCode = eErrorCode.ApiError, string errorDescription = null, object additionalInfo = null)
        {            
            Result.ErrorCode = errorCode;
            Result.ErrorDescription = errorDescription;
            Result.ResultCode = eResultCode.Failure;
            Result.AdditionalInfo = additionalInfo;
        }
        public bool IsSuccessfull
        {
            get
            {
                return Result.ErrorCode == eErrorCode.Success;
            }
        }
        // public object Result { get; set; }
        //[DataMember]
        public object Data { get; set; }
        //[DataMember]
        public object DataStructure { get; set; }
        //[DataMember]
        public object AdditionalData { get; set; }
        //[DataMember]
        public object UserInfo { get; set; }

        public static T Ok<T>() where T : BaseResponse, new() =>
            new T { Result = new ResponseResult { ResultCode = eResultCode.Success, ErrorCode = eErrorCode.Success} };

        public static T EmptyResponse<T>() where T : BaseResponse, new() =>
            new T { Result = new ResponseResult { ResultCode = eResultCode.Failure, ErrorCode = eErrorCode.EmptyResultData } };

        public static T SqlErrorResponse<T>(string message) where T : BaseResponse, new() =>
            new T { Result = new ResponseResult { ResultCode = eResultCode.Failure, ErrorCode = eErrorCode.DatabaseError, ErrorCodeDescription = message } };

        public static T ArgumentErrorResponse<T>(string message) where T : BaseResponse, new() => 
            new T { Result = new ResponseResult { ResultCode = eResultCode.Failure, ErrorCode = eErrorCode.ArgumentError, ErrorCodeDescription = message } };

        public static BaseResponse Ok() =>
            Ok<BaseResponse>();
        public static BaseResponse EmptyResponse() =>
            EmptyResponse<BaseResponse>();
        public static BaseResponse SqlErrorResponse(string message) =>
            SqlErrorResponse<BaseResponse>(message);
        public static BaseResponse ArgumentErrorResponse(string message) =>
            ArgumentErrorResponse<BaseResponse>(message);
    }
}
