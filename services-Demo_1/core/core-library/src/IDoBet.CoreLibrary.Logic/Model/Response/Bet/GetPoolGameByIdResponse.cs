﻿using System;
using System.Collections.Generic;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Data.Sport;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class GetPoolGameByIdResponse : BaseResponse
    {
        public PoolGame Game { get; set; }
    }
}
