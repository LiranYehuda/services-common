﻿using IDoBet.CoreLibrary.Logic.Model.Data.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
   
    public class PlaceBetSportResponse : BaseResponse
    {       
        public string TimeoutGuid { get; set; }       
        public int Timeout { get; set; }      
        public string Number { get; set; }       
        public string AuthorizationKey { get; set; }      
        public int AuthorizationTimeout { get; set; }      
        public List<OrderValidationResult> ValidationResult { get; set; }
        public static PlaceBetSportResponse Error(eErrorCode error, string message, IEnumerable<OrderBetValidationResult> validationResult = null, string authorizationKey = null, int? authorizationTimeout = null)
        {
            var result = new PlaceBetSportResponse();
            result.SetErrorResult(error, message);
            if (validationResult != null)
            {
                result.ValidationResult = new List<OrderValidationResult>();
                result.ValidationResult.Add(new OrderValidationResult() { OrderBetValidationResults = new List<OrderBetValidationResult>(validationResult) });
                result.ValidationResult[0].OrderBetValidationResults.Where(x => x.ValidationResult != eErrorCode.EventScoreChanged).ToList().ForEach(x => x.ExtraData = null);
            }
            result.AuthorizationKey = authorizationKey;
            if (authorizationTimeout != null)
                result.AuthorizationTimeout = authorizationTimeout.Value;
            return result;
        }

    }
}
