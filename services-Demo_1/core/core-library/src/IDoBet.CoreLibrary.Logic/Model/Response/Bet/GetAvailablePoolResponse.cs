﻿using IDoBet.CoreLibrary.Logic.Model.Data.Sport;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class GetAvailablePoolResponse : BaseResponse
    {
        public List<PoolGame> Games { get; set; }
    }
}
