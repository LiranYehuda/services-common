﻿using IDoBet.CoreLibrary.Logic.Model.Data.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class PlaceBetLottoResponse : BaseResponse
    {
        public string Number { get; set; }
        public string LottoUserBetId { get; set; }
        public string Barcode { get; set; }
        public string UrlKey { get; set; }
        public string RequestGuid { get; set; }
        public int? EmployeeId { get; set; }

        public double Payout { get; set; }
        public int CancelUntil { get; set; }
        public eProductType OrderProd { get; set; }
        public double? TotalCommission { get; set; }
        public string CommissionDesc { get; set; }
        public double? TotalBonus { get; set; }
        public string BonusDesc { get; set; }
        public double? TotalTax { get; set; }
        public string TaxDesc { get; set; }
        public double MaxPayout { get; set; }
        public long? TransactionId { get; set; }
    }
}
