﻿using IDoBet.CoreLibrary.Logic.Model.Data.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class PlaceBetBasicResponse : BaseResponse
    {
        public BasicOrder Order { get; set; }
    }
}
