﻿using IDoBet.CoreLibrary.Logic.Model.Data.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Enums.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class ExecuteOrderResponse : BaseResponse
    {
        public string OrderNumber { get; set; }
        public string ExternalId { get; set; }
        public string Barcode { get; set; }
        public double Payout { get; set; }
        public int CancelUntil { get; set; }
        public eProductType OrderProd { get; set; }
        public double? TotalCommission { get; set; }
        public string CommissionDesc { get; set; }
        public double? TotalBonus { get; set; }
        public string BonusDesc { get; set; }
        public double? TotalTax { get; set; }
        public string TaxDesc { get; set; }
        public double MaxPayout { get; set; }
        public long TransactionId { get; set; }
        public List<OrderValidationResult> ValidationResult { get; set; }

        public static ExecuteOrderResponse Error(eErrorCode error, string message, IEnumerable<OrderBetValidationResult> validationResult = null, string authorizationKey = null, int? authorizationTimeout = null)
        {
            var result = new ExecuteOrderResponse();
            result.SetErrorResult(error, message);
            if (validationResult != null)
            {
                result.ValidationResult = new List<OrderValidationResult>();
                result.ValidationResult.Add(new OrderValidationResult() { OrderBetValidationResults = new List<OrderBetValidationResult>(validationResult) });
                result.ValidationResult[0].OrderBetValidationResults.Where(x => x.ValidationResult != eErrorCode.EventScoreChanged).ToList().ForEach(x => x.ExtraData = null);
            }
            return result;
        }
    }
}
