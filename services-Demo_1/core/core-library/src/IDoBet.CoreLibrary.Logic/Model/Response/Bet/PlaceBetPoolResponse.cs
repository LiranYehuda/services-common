﻿using IDoBet.CoreLibrary.Logic.Model.Data.Order;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class PlaceBetPoolResponse : BaseResponse
    {
        public long Id { get; set; }
        public string Number { get; set; }
        public string Barcode { get; set; }
        public double MaxPayout { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string UrlKey { get; set; }
    }
}
