﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Bet
{
    public class CancelOrderResponse : BaseResponse
    {
        public double? ActualBalance { get; set; }
        public double? Bonus { get; set; }
        public string BarCode { get; set; }
    }
}
