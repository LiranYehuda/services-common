﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Files
{
    public class GetFileResponse : BaseResponse
    {
        public MemoryStream FileStream { get; set; }

        public string ContentType { get; set; }

        public string FileName { get; set; }

    }
}
