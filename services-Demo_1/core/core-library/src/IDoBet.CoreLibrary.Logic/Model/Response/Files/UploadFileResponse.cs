﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Files
{
    public class UploadFileResponse : BaseResponse
    {
        public decimal FileRecordId { get; set; }
    }
}
