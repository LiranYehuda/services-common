﻿using IDoBet.CoreLibrary.Logic.Model.Data.Limit;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Legacy
{
    [DataContract]
    public class ApplyLimitResponse : BaseResponse
    {
        [DataMember]
        public List<LimitResultRow> Data;
    }
}
