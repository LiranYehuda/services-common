﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Legacy
{
    [DataContract]
    public class GetEventDataResponse : BaseResponse
    {
      
        [DataMember]
        public byte[][] DataItems { get; set; }


        [DataMember]
        public DateTime? ServerTime { get; set; }


        [DataMember]
        public int[] ActiveCouponIds { get; set; }
    }
}
