﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Enums;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Legacy
{
    [DataContract]
    public abstract class BaseResponse
    {
        private Result mResult;

        [DataMember]
        public Result Result
        {
            get
            {
                if (mResult == null) mResult = new Result();
                return mResult;
            }
            set { mResult = value; }
        }

        public virtual void SetErrorResult(eErrorCode errorCode, string errorDescription, object additionalInfo = null)
        {
            if (mResult == null) mResult = new Result();
            mResult.ErrorCode = errorCode;
            mResult.ErrorDescription = errorDescription;
            mResult.ResultCode = eResultCode.Failure;
            mResult.AdditionalInfo = additionalInfo;
        }

        public virtual void SetErrorResult(eErrorCode errorCode)
        {
            if (mResult == null) mResult = new Result();
            mResult.ErrorCode = errorCode;
            mResult.ErrorDescription = Enum.GetName(typeof(eErrorCode), errorCode);
            mResult.ResultCode = eResultCode.Failure;
        }

        public virtual bool IsSuccessfull()
        {
            return Result.ResultCode == eResultCode.Success ? true : false;
        }
    }
}
