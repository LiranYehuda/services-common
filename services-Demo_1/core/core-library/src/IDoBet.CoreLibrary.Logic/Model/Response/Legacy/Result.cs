﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using IDoBet.CoreLibrary.Logic.Model.Enums;

namespace IDoBet.CoreLibrary.Logic.Model.Response.Legacy
{
    [DataContract]
    public class Result
    {
      
        [DataMember]
        public string ErrorDescription { get; set; }

        [DataMember]
        public object AdditionalInfo { get; set; }


        [DataMember]
        public eErrorCode ErrorCode { get; set; }


        [DataMember]
        public eResultCode ResultCode { get; set; }
    }
}
