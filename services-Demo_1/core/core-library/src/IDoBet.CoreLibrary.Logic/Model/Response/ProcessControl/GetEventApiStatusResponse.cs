﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logic.Model.Response.ProcessControl
{
    public class GetEventApiStatusResponse : BaseResponse
    {
        public long LostMessageCount { get; set; }
        public long AwaitingMessageCount { get; set; }
        public long CheckCount { get; set; }
        public long ProducerHistoryLength { get; set; }
    }
}
