EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetSupportedLanguages',
		@SpName = N'[idb].[spGetSupportedLanguages]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;


EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUsersAutoComplete',
		@SpName = N'[idb].[spGetUsersAutoComplete]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'ConnectUsersToCampaign',
		@SpName = N'[idb].[spConnectUsersToCampaign]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetActiveCampaigns',
		@SpName = N'[idb].[spGetActiveCampaigns]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetItems',
		@SpName = N'[idb].[spGetItems]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetSettingsJson',
		@SpName = N'[idb].[spGetSettingsJson]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SaveSettingsJson',
		@SpName = N'[idb].[spSaveSettingsJson]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetBanners',
		@SpName = N'[idb].[spGetBanners]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetHtmls',
		@SpName = N'[idb].[spGetHtmls]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3;