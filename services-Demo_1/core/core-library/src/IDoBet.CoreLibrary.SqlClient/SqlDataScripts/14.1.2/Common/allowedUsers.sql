update usr
set usr.RoleLevel = ISNULL(cr.Level,100000),
	usr.CanReadRoleLevel = IIF(cr.CanReadOtherUser = 0, NULL, CR.CanReadRoleLevel)
from [user].Users usr
inner join [user].UserRoles ur on usr.ID = ur.UserId
inner join [user].[v_Roles] cr on cr.ID = ur.RoleId

