IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='USER_EXCEEDED_MONTHLY_CAP' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(911,'USER_EXCEEDED_MONTHLY_CAP', 'User exceeded monthly expenses cap',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='USER_EXCEEDED_DAILY_CAP' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(912,'USER_EXCEEDED_DAILY_CAP', 'User exceeded daily expenses cap',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='USER_EXCEEDED_WEEKLY_CAP' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(913,'USER_EXCEEDED_WEEKLY_CAP', 'User exceeded weekly expenses cap',0)
END


IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='CASINO_SESSION_NOT_EXISTS' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(1201,'CASINO_SESSION_NOT_EXISTS', 'Casino session id not exist',0)
END