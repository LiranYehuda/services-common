IF NOT EXISTS (SELECT TOP 1 ID FROM [user].[ActivityType] WHERE ID = 11000)
	INSERT INTO [user].[ActivityType] (ID, [Name], [CodeAttribute]) VALUES (11000, 'API', 'API')
GO

IF NOT EXISTS(SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] IN ('REVERSE_PAYMENT'))
BEGIN
	INSERT INTO [user].[Activity]
	           ([Name]
	           ,[ActivityTypeId]
	           ,[CodeAttribute])
	     VALUES
	           ('Reverse Payment'
	           ,11000
	           ,'REVERSE_PAYMENT')				
END

