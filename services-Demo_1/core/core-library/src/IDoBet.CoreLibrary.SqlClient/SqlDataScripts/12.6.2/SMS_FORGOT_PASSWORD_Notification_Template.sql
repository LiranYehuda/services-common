DECLARE @FROM NVARCHAR(250) = dbo.fnGetConfigurationValueByCodeAttribute('ACTIVATION_CODE_EMAIL_SEND_FROM')

IF NOT EXISTS(SELECT * FROM [dbo].[NotificationTemplate] WHERE CodeAttribute IN ('SMS_FORGOT_PASSWORD'))
BEGIN
	INSERT INTO [dbo].[NotificationTemplate]
	           ([Name]
	           ,[Desc]
	           ,[Title]
	           ,[Body]
	           ,[IsHtml]
	           ,[NotificationMethod]
	           ,[PlaceHolders]
	           ,[Subject]
	           ,[CodeAttribute]
	           ,[From])
	     VALUES
	           ('SMS Forgot Password'
	           ,'SMS Forgot Password template'
	           ,'SMS Forgot Password'
	           ,'Your Authentication Code is {0}'
	           ,0
	           ,2
	           ,NULL
	           ,'SMS Forgot Password'
	           ,'SMS_FORGOT_PASSWORD'
	           , @FROM)
END
GO


