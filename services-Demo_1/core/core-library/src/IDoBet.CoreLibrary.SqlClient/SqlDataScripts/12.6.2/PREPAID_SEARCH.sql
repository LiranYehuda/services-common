IF NOT EXISTS ( SELECT TOP 1 ID FROM [user].[Activity] WHERE ActivityTypeId = 10000 AND CodeAttribute= 'PREPAID_SEARCH')
	INSERT INTO [user].[Activity] ([Name], ActivityTypeId, CodeAttribute)
	VALUES ('PrePaid Search (Support POS)',10000,'PREPAID_SEARCH');