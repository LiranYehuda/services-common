IF NOT EXISTS(SELECT 1 FROM [finance].[TransferRequestTypes]  WHERE [CodeAttribute] = 'WITHDRAW_BY_PREPAID_USER')
BEGIN
	INSERT INTO [finance].[TransferRequestTypes] (ID, [Name]
	      ,[CodeAttribute]
	      ,[IsIncome]
	      ,[ApproveTransactionReasonFromId]
	      ,[ApproveTransactionReasonToId]
	      ,[CancelTransactionReasonFromId]
	      ,[CancelTransactionReasonToId]
	      ,[ConnectApprovedTransactions]
	      ,[IsDepositBonus]
	      ,[AdditionalTransactionReasonId])
	VALUES
	(
	    19,
	    'Prepaid Withdraw',
	    'WITHDRAW_BY_PREPAID_USER',
	    0,
	    30,
	    30,
	    NULL,
	    NULL,
	    1,
	    0,
	    27
	);
END