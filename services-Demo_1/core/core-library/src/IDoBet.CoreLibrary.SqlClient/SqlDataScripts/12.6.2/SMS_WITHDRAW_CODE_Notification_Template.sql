DECLARE @FROM NVARCHAR(250) = dbo.fnGetConfigurationValueByCodeAttribute('ACTIVATION_CODE_EMAIL_SEND_FROM')

IF NOT EXISTS(SELECT 1 FROM [dbo].[NotificationTemplate] WHERE [CodeAttribute] = 'SMS_WITHDRAW_CODE')
BEGIN
INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
     VALUES
           ('SMS Withdraw Code'
           ,'SMS Withdraw Code template'
           ,'SMS Withdraw Code'
           ,'Your withdrawal Code is {0}'
           ,0
           ,2
           ,NULL
           ,'SMS Withdraw Code'
           ,'SMS_WITHDRAW_CODE'
           ,@FROM)
END
GO


