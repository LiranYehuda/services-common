DECLARE @FROM NVARCHAR(250) = dbo.fnGetConfigurationValueByCodeAttribute('ACTIVATION_CODE_EMAIL_SEND_FROM')

IF NOT EXISTS(SELECT 1 FROM NotificationTemplate WHERE CodeAttribute = 'WITHDRAW_CODE')
BEGIN
	INSERT INTO [dbo].[NotificationTemplate]
	           ([Name]
	           ,[Desc]
	           ,[Title]
	           ,[Body]
	           ,[IsHtml]
	           ,[NotificationMethod]
	           ,[PlaceHolders]
	           ,[Subject]
	           ,[CodeAttribute]
	           ,[From])
	     VALUES
	           ('Withdraw Code'
	           ,'Withdraw Code'
	           ,'Withdraw Code'
	           ,'Hello {0} {1},<br><br> Your withdrawal code is {2}'
	           ,1
	           ,1
	           ,NULL
	           ,'Withdraw Code'
	           ,'WITHDRAW_CODE'
	           ,@FROM)
END