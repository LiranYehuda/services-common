IF NOT EXISTS(SELECT 1 FROM  [user].[Activity] WHERE [CodeAttribute] = 'LIVE_CHAT')
BEGIN
	INSERT INTO [user].[Activity]
	           ([Name]
	           ,[ActivityTypeId]
	           ,[CodeAttribute])
	     VALUES
	           (N'Live Chat'
	           ,10000
	           ,N'LIVE_CHAT')
END
GO


