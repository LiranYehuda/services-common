

--/*Get Files Name */
--create table #files  (ID int IDENTITY, FileName varchar(100))
--insert into #files execute xp_cmdshell 'dir c:\temp\*.xml /b'
--GO
--/*Get XML Content*/
--create table #xmlContent  (BulkColumn XML,[FileName] NVARCHAR(200))
--declare @dir nvarchar(max)= 'c:\temp\'
--declare @col nvarchar(max)
--select @col = COALESCE(@col + '', '') +'
--INSERT #xmlContent
--SELECT CONVERT(XML, BulkColumn) AS BulkColumn,'''+[FileName]+''' FROM OPENROWSET(BULK '''+@dir +[FileName]+''', SINGLE_BLOB) AS x'  from #files
--where FileName is not null and FileName like '%.xml'

--EXECUTE sp_executesql @col

DECLARE @Venue TABLE( ID int IDENTITY,
					  [Timestamp] [varchar](100), 
					  Name        [varchar](100), 
					  Code        [varchar](100), 
					  Date        [varchar](100),
					  Racevalid   [varchar](100), 
					  Starts      [varchar](MAX))
	
						   
DECLARE @RACES table (ID INT IDENTITY
					 ,[Date]       [varchar](100) 
					 ,Code         [varchar](100) 
					 ,Number       [varchar](100) 
					 ,RaceTime     [varchar](100) 
					 ,MeetingId    [varchar](100) 
					 ,Raceid	     [varchar](100) 
					 ,[Timestamp]  [varchar](100) 
					 ,Field        [varchar](MAX)
					 ,Finishes     [varchar](MAX)
					 ,Dividends    [varchar](MAX)
					 ,Favourite    [varchar](100) 
					 ,Status	   [varchar](100) )          										 

DECLARE @hDoc AS INT,@BulkColumn XML,@FileName NVARCHAR(200);

DECLARE MY_CURSOR CURSOR 
  LOCAL STATIC READ_ONLY FORWARD_ONLY
FOR 
SELECT  BulkColumn,[FileName]
FROM #xmlContent

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @BulkColumn,@FileName
WHILE @@FETCH_STATUS = 0
BEGIN 		
	IF (@FileName LIKE '%VEN.xml%')
	BEGIN
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @BulkColumn

		INSERT @Venue
		SELECT 
			*,
			(
				SELECT Code,Race			  			  
				FROM OPENXML(@hDoc, 'venue/starts/start')
				WITH 
				(						
					Code        [varchar](100) 'code',			
					Race   [varchar](100) 'race'
				)
				FOR JSON PATH, ROOT('Starts')
			) Starts
		FROM OPENXML(@hDoc, 'venue')
		WITH 
		(
		[Timestamp] [varchar](100) 'timestamp',
		Name        [varchar](100) 'name',
		Code        [varchar](100) 'code',
		Date        [varchar](100) 'date',		
		Racevalid   [varchar](100) 'racevalid'				
		)	

		EXEC sp_xml_removedocument @hDoc  

	END
	ELSE
	BEGIN
		IF (@FileName LIKE '%.R%.xml%') -- RACE
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @BulkColumn
			
			INSERT @RACES
			SELECT 
			[Date]       
			,Code         
			,Number       
			,RaceTime     
			,MeetingId    
			,Raceid	     
			,[Timestamp]  
			,(
				SELECT  
						(
						SELECT number,    
							   name,     
							   [current] 'prices.current',
							   [prev]    'prices.prev',
							   [prev2]'prices.prev2'
						FROM OPENXML(@hDoc, 'race/field/runner')
						WITH 
						(						
							number    [varchar](100) 'number',			
							name      [varchar](100) 'name',
							[current] [varchar](100) 'prices/current',
							[prev]  [varchar](100) 'prices/prev',
							[prev2] [varchar](100) 'prices/prev2'
						)
						FOR JSON PATH
					) Runner,
					(
						SELECT *
						FROM OPENXML(@hDoc, 'race/field/scratched')
						WITH 
						(						
							number        [varchar](100) 'number',			
							name   [varchar](100) 'name',
							time   [varchar](100) 'time',
							deduction    [varchar](100) 'deduction'
						)
						FOR JSON PATH
					) Scratched		  			  
				FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
			) Field,
			(
				SELECT *
				FROM OPENXML(@hDoc, 'race/finishes/finish')
				WITH 
				(						
					position    [varchar](100) 'position',			
					number    [varchar](100) 'number',			
					odds    [varchar](100) 'odds'
				)
				FOR JSON PATH
			) Finishes,
			(
				SELECT *
				FROM OPENXML(@hDoc, 'race/dividends/dividend')
				WITH 
				(						
					bettype [varchar](100)'bettype',
					number  [varchar](100)'number', 
					payout  [varchar](100)'payout'
				)
				FOR JSON PATH
			) Finishes,
			Favourite,    
			[Status]
			FROM OPENXML(@hDoc, 'race')
			WITH 
			(
			[Date]       [varchar](100) 'date',
			Code         [varchar](100) 'code',
			Number       [varchar](100) 'number',
			RaceTime     [varchar](100) 'racetime',			
			MeetingId    [varchar](100) 'meetingid',		
			Raceid	     [varchar](100) 'raceid',		
			[Timestamp]  [varchar](100) 'timestamp',																											
			Favourite    [varchar](100) 'favourite',
			Status       [varchar](100) 'status'
			)

			EXEC sp_xml_removedocument @hDoc  
		END
		ELSE
		BEGIN
			IF (@FileName LIKE '%.C%.xml%') -- RACE CARD
			BEGIN
				PRINT @FileName + '- Not Implamanted'
			END
		END
	END

    FETCH NEXT FROM MY_CURSOR INTO @BulkColumn,@FileName
END
CLOSE MY_CURSOR
DEALLOCATE MY_CURSOR

select * from @Venue
SELECT * FROM @RACES
GO