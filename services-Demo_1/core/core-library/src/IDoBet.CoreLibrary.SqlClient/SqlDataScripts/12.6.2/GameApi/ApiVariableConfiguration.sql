declare @BrandId INT = ?

EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'ImportFilePrefix',
		@Value = N'',
		@Description = 'Import file prefix const'

EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'ImportFileLocalPrefix',
		@Value = ?N'C:\GSports\Sites\ImportFiles',
		@Description = 'Import file local path prefix'

EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'ImportFileServerPrefix',
		@Value = ?N'http://uganda.idobet.com/ImportFiles',
		@Description = 'Import file server path prefix'


  EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = NULL,
		@Instance = NULL,
		@Key = N'IsWriteSpToDb',
		@Value = ?'true',
		@Description = N'Enable dyanmic sql db calls trace'
