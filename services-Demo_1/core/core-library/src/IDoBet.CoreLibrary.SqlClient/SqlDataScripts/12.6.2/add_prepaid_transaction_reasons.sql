﻿IF NOT EXISTS(SELECT 1 FROM [finance].[TransactionReasons] WHERE CodeAttribute IN ('PREPAID_DEPOSIT','PREPAID_WITHDRAW'))
BEGIN
	INSERT INTO [finance].[TransactionReasons] VALUES
	(29, 'Prepaid Deposit', 'PREPAID_DEPOSIT', 'CREDIT', 0),
	(30, 'Prepaid Withdraw', 'PREPAID_WITHDRAW', 'DEBIT', 0);
END

GO