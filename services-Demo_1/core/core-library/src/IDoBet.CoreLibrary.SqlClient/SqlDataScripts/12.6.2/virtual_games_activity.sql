﻿IF NOT EXISTS(SELECT 1 FROM [user].[ActivityType] WHERE ID = 1200)
BEGIN
	INSERT INTO [user].[ActivityType] VALUES (1200,	'Virtual Game',	'BACKOFFICE_VIRTUAL_GAME');

	DECLARE @ActivityId INT; 
	DECLARE @RoleId INT = 10; -- SuperAdmin
	DECLARE @Permission INT = 15;

	INSERT INTO [user].[Activity] VALUES ('Spin And Win Results',	1200,	'SPIN_AND_WIN_REPORT');
	SELECT @ActivityId = SCOPE_IDENTITY();
	INSERT [user].[Permissions] (RoleId, ActivityId, Permission) VALUES (@RoleId, @ActivityId, @Permission);

	INSERT INTO [user].[Activity] VALUES ('Keno Results',	1200,	'KENO_REPORT');
	SELECT @ActivityId = SCOPE_IDENTITY();
	INSERT [user].[Permissions] (RoleId, ActivityId, Permission) VALUES (@RoleId, @ActivityId, @Permission);


	INSERT INTO [user].[Activity] VALUES ('Colors Results',	1200,	'COLORS_REPORT');
	SELECT @ActivityId = SCOPE_IDENTITY();
	INSERT [user].[Permissions] (RoleId, ActivityId, Permission) VALUES (@RoleId, @ActivityId, @Permission);

END;