IF NOT EXISTS( SELECT 1 FROM [const].[ProductType] WHERE [CodeAttribute] ='KENO')
BEGIN
	INSERT INTO [const].[ProductType]
	           ([Name]
	           ,[CancelTime]
	           ,[CodeAttribute]
	           ,[ReprintTime])
	     VALUES ('Keno',	0,	'KENO',	600)
END
GO
IF NOT EXISTS( SELECT 1 FROM [const].[ProductType] WHERE [CodeAttribute] ='SPINANDWIN')
BEGIN
	INSERT INTO [const].[ProductType]
	           ([Name]
	           ,[CancelTime]
	           ,[CodeAttribute]
	           ,[ReprintTime])
	     VALUES ('SpinAndWin',	0,	'SPINANDWIN',	600)
END
GO
IF NOT EXISTS( SELECT 1 FROM [const].[ProductType] WHERE [CodeAttribute] ='COLORS')
BEGIN
	INSERT INTO [const].[ProductType]
	           ([Name]
	           ,[CancelTime]
	           ,[CodeAttribute]
	           ,[ReprintTime])
	     VALUES ('Colors',	0,	'COLORS',	600)
END
GO
		  


