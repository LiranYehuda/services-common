
--FIX WRONG CODE
update system.configuration 
set codeattribute = 'ACTIVATION_CODE_EMAIL_BODY'
where id  = 6007

--Remove old template
IF EXISTS(SELECT 1 FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'SMS_REGISTERATION')
BEGIN
	DELETE  [dbo].[NotificationTemplate] WHERE CodeAttribute = 'SMS_REGISTERATION'
END


DECLARE @FROM NVARCHAR(250) = dbo.fnGetConfigurationValueByCodeAttribute('ACTIVATION_CODE_EMAIL_SEND_FROM')

UPDATE [dbo].[NotificationTemplate]
SET [FROM] = @FROM


IF EXISTS(SELECT 1 FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'SMS_RESET_PASSWORD')
BEGIN
	UPDATE [dbo].[NotificationTemplate] 
	SET [Subject] = 'SMS Recovery Password',Title = 'SMS Recovery Password',[Desc] = 'SMS Reset password template',[Name] = 'SMS Reset Password'
	WHERE CodeAttribute = 'SMS_RESET_PASSWORD'
END
ELSE
BEGIN
	INSERT INTO [dbo].[NotificationTemplate]
	           ([Name]
	           ,[Desc]
	           ,[Title]
	           ,[Body]
	           ,[IsHtml]
	           ,[NotificationMethod]
	           ,[PlaceHolders]
	           ,[Subject]
	           ,[CodeAttribute]
			   ,[FROM])
	     VALUES
	           ('SMS Reset Password'
	           ,'SMS Reset password template'
	           ,'SMS Recovery Password'
	           ,'Your verification code is {0}'
	           ,0
	           ,2
	           ,NULL
	           ,'SMS Recovery Password'
	           ,'SMS_RESET_PASSWORD'
			   ,@FROM)
END


IF NOT EXISTS(SELECT * FROM [dbo].[NotificationTemplate] WHERE CodeAttribute IN ('ACTIVATION_CODE_EMAIL_BODY','ACTIVATION_CODE_SMS_BODY'))
BEGIN
	INSERT INTO [dbo].[NotificationTemplate]
	           ([Name]
	           ,[Desc]
	           ,[Title]
	           ,[Body]
	           ,[IsHtml]
	           ,[NotificationMethod]
	           ,[PlaceHolders]
	           ,[Subject]
	           ,[CodeAttribute]
			   ,[FROM])
	     VALUES
	           ('Activation Code Email Body'
	           ,'email body for activation link after online user is registred'
	           ,'Activation Code Email Body'
	           ,'Hello,<br><br>You recently registered to our online site <a href={0}>Please click here to activate your account</a><br>See you soon'
	           ,1
	           ,1
	           ,NULL
	           ,'Activation Code Email Body'
	           ,'ACTIVATION_CODE_EMAIL_BODY'
			   ,@FROM)
			   
	INSERT INTO [dbo].[NotificationTemplate]
	           ([Name]
	           ,[Desc]
	           ,[Title]
	           ,[Body]
	           ,[IsHtml]
	           ,[NotificationMethod]
	           ,[PlaceHolders]
	           ,[Subject]
	           ,[CodeAttribute]
			   ,[FROM])	
	VALUES ('Activation Code SMS Body'
	           ,'SMS body for activation code after online user is registred'
	           ,'Activation Code SMS Body'
	           ,'Hello,You recently registered to our online site ,Your Activation Code is {0}'
	           ,0
	           ,2
	           ,NULL
	           ,'Activation Code SMS Body'
	           ,'ACTIVATION_CODE_SMS_BODY'
			   ,@FROM)
END


DECLARE @Title NVARCHAR(250) = dbo.fnGetConfigurationValueByCodeAttribute('ACTIVATION_CODE_EMAIL_TITLE')

UPDATE [dbo].[NotificationTemplate]
SET Title = @Title
WHERE CodeAttribute = 'ACTIVATION_CODE_EMAIL_BODY'

----DELETE from system.configuration where id in (6001,6007,6008)