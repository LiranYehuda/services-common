IF NOT EXISTS(SELECT 1 FROM [const].[CommissionType] WHERE ID = 1004)
	INSERT INTO [const].[CommissionType]
           ([Id]
           ,[Name]
           ,[Desc]
           ,[CodeAttribute]
           ,[ProductId]
           ,[IsDeposit])
     VALUES
           (1004
           ,'Prepaid Deposit'
           ,'Create Prepaid card Commission'
           ,'PREPAIDCARD_DEPOSIT'
           ,NULL
           ,1)