UPDATE [user].[ActivityType]
SET IsBackoffice = 1
WHERE CodeAttribute LIKE '%BACKOFFICE_%' OR CodeAttribute IN 
(
'EXPENSES',
'CAMPAIGNS',
'CMS'
)

DELETE FROM [user].ActivityType
WHERE id IN ( 27,35) /*PUSH_EVENTS + Shifts*/
