DECLARE @BrandId int = ? 

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 1,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'ReplaceCurrencyCodeConfigString',
		@Value = --N'{"UGX":"XOF"}', 
		@Description = N'Replace currency code config string',
		@ChannelId = NULL,
		@BranchId = NULL