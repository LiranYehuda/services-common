UPDATE US
SET US.UserPhones = 
	(	SELECT	Phone phone,
		PhoneCountryCode AS phoneCountryCode,
		CAST(1 AS bit) AS isActive,
		IIF(ISNULL(RegistrationType,1/*EMAIL*/) = 1,CAST(0 AS bit),CAST(1 AS bit)) AS isVerified,
		IIF(ISNULL(RegistrationType,1/*EMAIL*/) = 1,CAST(0 AS bit),CAST(1 AS bit)) AS isRegistered,
		NULL AS deletedDateTime,
		GETDATE() AS createdDateTime
FROM [user].[Users]
WHERE Id = US.Id
FOR JSON PATH, INCLUDE_NULL_VALUES)
FROM [user].[Users] US
WHERE US.Phone IS NOT NULL 



UPDATE [system].Contents 
SET Value = '{"header":"The amount will be deducted from your balance and will transfer directly to your mobile money account (%PHONE_NUMBER%) %HREF_CHANGE_PHONE_NUMBER%<br>Enter your account password.","footer":"<h1 class=\"center\">Supported networks:<\/h1>\r\n           <div class=\"mobile-supported-networks\">\r\n             <ul class=\"center\">\r\n               <li>\r\n                 <img style=\"\" src=\".\/assets\/common\/images\/mobileBeyonic\/mtn.png\"  alt=\"mtn\">\r\n               <\/li><li>\r\n                 <img style=\"\" src=\".\/assets\/common\/images\/mobileBeyonic\/africell.png\"  alt=\"africell\">\r\n               <\/li><li>\r\n                 <img style=\"\" src=\".\/assets\/common\/images\/mobileBeyonic\/airtel.png\"  alt=\"airtel\">\r\n               <\/li>\r\n             <\/ul>\r\n           <\/div>"}'
WHERE Id = 20 /*WEBPAY_WITHDRAW_CONTENT*/