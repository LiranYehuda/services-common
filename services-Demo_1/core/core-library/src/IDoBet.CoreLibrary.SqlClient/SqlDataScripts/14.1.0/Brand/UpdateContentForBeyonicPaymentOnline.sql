-- 0 rows affected in QA

UPDATE CT
SET [value] = REPLACE([value],'(%PHONE_NUMBER%)','(%PHONE_NUMBER%) %HREF_CHANGE_PHONE_NUMBER%<\/BR>')
FROM system.Contents CT
INNER JOIN finance.PaymentMethodConfiguration PMC ON CT.Id = PMC.ContentId
WHERE PMC.ChannelId = 1 and PMC.PaymentMethodId = 4 AND CT.[Value] NOT LIKE '%HREF_CHANGE_PHONE_NUMBER%'