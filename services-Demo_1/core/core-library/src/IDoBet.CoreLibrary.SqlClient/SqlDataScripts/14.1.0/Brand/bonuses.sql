DECLARE @BrandDBId INT = ?? 


IF NOT EXISTS (SELECT 1 FROM [GSportsConfiguration].[system].[JobToDatabase] WHERE [JobId] = 11 and [DatabaseId] =  @BrandDBId)
BEGIN


	INSERT INTO [GSportsConfiguration].[system].[JobToDatabase]
			   ([JobId]
			   ,[DatabaseId]
			   ,[Enabled])
	VALUES		(
			    11 /*BonusesAutoCancel*/
			   ,@BrandDBId
			   ,1)	

END

