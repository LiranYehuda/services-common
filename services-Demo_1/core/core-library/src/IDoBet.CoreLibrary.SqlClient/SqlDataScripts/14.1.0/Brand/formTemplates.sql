

  update [system].[FormTemplateFields]
  set Text = 'Allow Claim'
  where Id = 225 

  update [system].[FormTemplateFieldValues]
  set Text = 'Allow Claim'
  where FormTemplateFieldId = 225 

  
 UPDATE [system].[FormTemplateFields]
 SET DefaultValue = '0'
 WHERE FieldType = 'Boolean'


 update fv
 set fv.DefaultValue = '0'
 from [system].[FormTemplateFieldValues] fv
 inner join [system].[FormTemplateFields] ft on fv.FormTemplateFieldId = ft.Id
 WHERE ft.FieldType = 'Boolean'

