IF NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[NotificationTemplate] WHERE [CodeAttribute] = 'USER_PHONE_VERIFICATION_CODE')
	INSERT INTO [dbo].[NotificationTemplate]
			   ([Name]
			   ,[Desc]
			   ,[Title]
			   ,[Body]
			   ,[IsHtml]
			   ,[NotificationMethod]
			   ,[PlaceHolders]
			   ,[Subject]
			   ,[CodeAttribute]
			   ,[From])
		 VALUES
			   ('User Phone Verification Code'
			   ,'User phone verification Code'
			   ,'User Phone Verification Code'
			   ,'Your verification code is {0}'
			   ,0
			   ,2
			   ,null
			   ,'Verification code'
			   ,'USER_PHONE_VERIFICATION_CODE'
			   ,'IDoBet');
