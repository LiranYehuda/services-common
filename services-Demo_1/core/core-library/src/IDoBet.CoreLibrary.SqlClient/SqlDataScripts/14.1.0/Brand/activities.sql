IF NOT EXISTS (SELECT TOP 1 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'CLIENT_ORDER_RISK')
	INSERT INTO [user].[Activity]
			   ([Name]
			   ,[ActivityTypeId]
			   ,[CodeAttribute])
		 VALUES
			   ('Client Order Risk'
			   ,300
			   ,'CLIENT_ORDER_RISK');


