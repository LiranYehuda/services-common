
  IF NOT EXISTS(SELECT 1 FROM [const].[CustomEnums] WHERE EnumType = 'eBranch' AND VALUE = -2)
	INSERT [const].[CustomEnums]([EnumType]
      ,[Name]
      ,[Value])
	SELECT 'eBranch','All Branch Except Online',-2
  ELSE
    UPDATE [const].[CustomEnums]
	SET [Name] = 'All Branch Except Online'
	WHERE EnumType = 'eBranch' AND VALUE = -2 AND [Name] <> 'All Branch Except Online'

IF NOT EXISTS(SELECT 1 FROM [const].[CustomEnums] WHERE EnumType = 'eBranch' AND VALUE = -1)
	INSERT [const].[CustomEnums]([EnumType]
      ,[Name]
      ,[Value])
	SELECT 'eBranch','ALL',-1
  ELSE
    UPDATE [const].[CustomEnums]
	SET [Name] = 'ALL'
	WHERE EnumType = 'eBranch' AND VALUE = -1 AND [Name] <> 'ALL'

IF NOT EXISTS(SELECT 1 FROM [const].[CustomEnums] WHERE EnumType = 'eWinStatus' AND VALUE = -1)
	INSERT [const].[CustomEnums]([EnumType]
      ,[Name]
      ,[Value])
	SELECT 'eWinStatus','ALL',-1
  ELSE
    UPDATE [const].[CustomEnums]
	SET [Name] = 'ALL'
	WHERE EnumType = 'eWinStatus' AND VALUE = -1 AND [Name] <> 'ALL'

IF NOT EXISTS(SELECT 1 FROM [const].[CustomEnums] WHERE EnumType = 'eProductTypeImmediateBonus' AND VALUE = -2)
	INSERT [const].[CustomEnums]([EnumType]
      ,[Name]
      ,[Value]
	  ,[RowNumber])
	SELECT 'eProductTypeImmediateBonus','Sport Betting (live & prematch)',-2,-1

IF NOT EXISTS(SELECT 1 FROM [const].[CustomEnums] WHERE EnumType = 'eRegion' AND VALUE = 0)
	INSERT [const].[CustomEnums]([EnumType]
      ,[Name]
      ,[Value]
	  ,[RowNumber])
	SELECT 'eRegion','ALL',0,-100

IF NOT EXISTS(SELECT 1 FROM [const].[CustomEnums] WHERE EnumType = 'eProductType' AND VALUE = 0)
	INSERT [const].[CustomEnums]([EnumType]
      ,[Name]
      ,[Value]
	  ,[RowNumber])
	SELECT 'eProductType','ALL',0,-100

IF NOT EXISTS(SELECT 1 FROM [const].[CustomEnums] WHERE EnumType = 'eProductTypeImmediateBonus' AND VALUE = 0)
	INSERT [const].[CustomEnums]([EnumType]
      ,[Name]
      ,[Value]
	  ,[RowNumber])
	SELECT 'eProductTypeImmediateBonus','ALL',0,-100

UPDATE  [const].[CustomEnums]
SET [RowNumber] = -100,NAME ='ALL'
WHERE NAME ='ALL'