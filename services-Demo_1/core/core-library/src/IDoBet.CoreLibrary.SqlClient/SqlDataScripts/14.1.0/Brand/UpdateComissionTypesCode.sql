UPDATE [finance].[TransactionReasons]
  SET [CommissionTypesCode] = [TransactionTypesCode]

GO

 UPDATE [finance].[TransactionReasons]
  SET  [CommissionTypesCode] = 'CREDIT'
  WHERE CodeAttribute IN ( 'WITHDRAW','PAYOUT_TICKET')

   UPDATE [finance].[TransactionReasons]
  SET  [CommissionTypesCode] = 'DEBIT'
  WHERE CodeAttribute = 'REVERSE_PAYOUT'