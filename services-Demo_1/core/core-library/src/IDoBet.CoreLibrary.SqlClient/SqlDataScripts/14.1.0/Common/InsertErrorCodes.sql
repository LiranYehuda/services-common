IF NOT EXISTS(SELECT 1 FROM common.const.ErrorCodes WHERE CodeAttribute = 'NEXT_BET_BONUS_VALIDATION')
BEGIN
	INSERT common.const.ErrorCodes(CodeNumber	,CodeAttribute	,[Name],	BrandId)
	VALUES(602,'NEXT_BET_BONUS_VALIDATION','Next bet bonus validation',0)
END

IF NOT EXISTS(SELECT 1 FROM common.const.ErrorCodes WHERE CodeAttribute = 'CouldNotRemoveActivePhone')
BEGIN
	INSERT common.const.ErrorCodes(CodeNumber	,CodeAttribute	,[Name],	BrandId)
	VALUES(503,'CouldNotRemoveActivePhone','Could Not Remove Active Phone',0)
END

IF NOT EXISTS(SELECT 1 FROM common.const.ErrorCodes WHERE CodeAttribute = 'CouldNotRemoveRegisteredPhone')
BEGIN
	INSERT common.const.ErrorCodes(CodeNumber	,CodeAttribute	,[Name],	BrandId)
	VALUES(504,'CouldNotRemoveRegisteredPhone','Could Not Remove Registered Phone',0)
END

END

IF NOT EXISTS(SELECT 1 FROM common.const.ErrorCodes WHERE CodeAttribute = 'BRANCH_FINANCE_COMMISSION_OVERLAPS')
BEGIN
	INSERT common.const.ErrorCodes(CodeNumber	,CodeAttribute	,[Name],	BrandId)
	VALUES(418,'BRANCH_FINANCE_COMMISSION_OVERLAPS','Branch finance commission overlaps with a diffrent commission',0)
END