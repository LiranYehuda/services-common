EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserPhones',
		@SpName = N'[idb].[spGetUserPhones]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 1

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserPhones',
		@SpName = N'[idb].[spGetUserPhones]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SaveUserPhone',
		@SpName = N'[idb].[spSaveUserPhone]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 1

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'VerifyUserPhone',
		@SpName = N'[idb].[spVerifyUserPhone]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 1

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SendUserPhoneVerificationOTP',
		@SpName = N'[idb].[spSendUserPhoneVerificationOTP]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 1

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'PayCommission',
		@SpName = N'[idb].[spB2BPayCommission]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2