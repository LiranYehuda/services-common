USE [Common]
GO

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'ValidateAndResetPassword',
		@SpName = N'[user].[spValidateAndResetPassword]',
		@BrandId = ?,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0
