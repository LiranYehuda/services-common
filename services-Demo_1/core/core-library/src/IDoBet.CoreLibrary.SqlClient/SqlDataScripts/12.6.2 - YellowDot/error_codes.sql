-- Obsolete: from 14.1.1 all error code arrives from [const].[ErrorCodesProessed] (from common)

/*
INSERT INTO [const].[ErrorCodes]([CodeNumber] ,[CodeAttribute] ,[Description])
SELECT *
FROM
(
SELECT 0 AS CodeNumber, 'Success' AS CodeAttribute, 'Success' AS Description 
UNION
SELECT 1 AS CodeNumber, 'DatabaseError' AS CodeAttribute, 'Database Error' AS Description 
UNION
SELECT 2 AS CodeNumber, 'InternalServerError' AS CodeAttribute, 'Internal Server Error' AS Description 
UNION
SELECT 3 AS CodeNumber, 'BadRequest' AS CodeAttribute, 'Bad Request' AS Description 
UNION
SELECT 7 AS CodeNumber, 'EventDosntExsist' AS CodeAttribute, 'Event Dosnt Exsist' AS Description 
UNION
SELECT 8 AS CodeNumber, 'NoPermission' AS CodeAttribute, 'No Permission' AS Description 
UNION
SELECT 9 AS CodeNumber, 'DuplicateData' AS CodeAttribute, 'Duplicate Data' AS Description 
UNION
SELECT 10 AS CodeNumber, 'CacheNotReady' AS CodeAttribute, 'Cache Not Ready' AS Description 
UNION
SELECT 11 AS CodeNumber, 'ConvertorError' AS CodeAttribute, 'Convertor Error' AS Description 
UNION
SELECT 12 AS CodeNumber, 'SystemUserNotSet' AS CodeAttribute, 'System User Not Set' AS Description 
UNION
SELECT 100 AS CodeNumber, 'UserTokenInvalid' AS CodeAttribute, 'User Token Invalid' AS Description 
UNION
SELECT 101 AS CodeNumber, 'TerminalTokenInvalid' AS CodeAttribute, 'Terminal Token Invalid' AS Description 
UNION
SELECT 102 AS CodeNumber, 'TerminalTokenAlreadyInUse' AS CodeAttribute, 'Terminal Token Already In Use' AS Description 
UNION
SELECT 103 AS CodeNumber, 'WrongCredetials' AS CodeAttribute, 'Wrong Credetials' AS Description 
UNION
SELECT 104 AS CodeNumber, 'NoOpenShiftForTerminalUser' AS CodeAttribute, 'No Open Shift For Terminal User' AS Description 
UNION
SELECT 105 AS CodeNumber, 'LogoutFailed' AS CodeAttribute, 'Logout Failed' AS Description 
UNION
SELECT 106 AS CodeNumber, 'UserHasBeenLocked' AS CodeAttribute, 'User Has Been Locked' AS Description 
UNION
SELECT 110 AS CodeNumber, 'CantFindShift' AS CodeAttribute, 'Cant Find Shift' AS Description 
UNION
SELECT 111 AS CodeNumber, 'ShiftAlreadyClosed' AS CodeAttribute, 'Shift Already Closed' AS Description 
UNION
SELECT 112 AS CodeNumber, 'CantFindUserInShift' AS CodeAttribute, 'Cant Find User In Shift' AS Description 
UNION
SELECT 113 AS CodeNumber, 'ThereAreUsersInCurrentShift' AS CodeAttribute, 'There Are Users In Current Shift' AS Description 
UNION
SELECT 114 AS CodeNumber, 'CantStopManagerShift' AS CodeAttribute, 'Cant Stop Manager Shift' AS Description 
UNION
SELECT 120 AS CodeNumber, 'UserNameAlreadyExists' AS CodeAttribute, 'User Name Already Exists' AS Description 
UNION
SELECT 121 AS CodeNumber, 'EmailAlreadyExists' AS CodeAttribute, 'Email Already Exists' AS Description 
UNION
SELECT 122 AS CodeNumber, 'FailedToChangePassword' AS CodeAttribute, 'Failed To Change Password' AS Description 
UNION
SELECT 123 AS CodeNumber, 'OTPCodeNotValid' AS CodeAttribute, 'OTP Code Not Valid' AS Description 
UNION
SELECT 124 AS CodeNumber, 'PrePaidPasswordNotExist' AS CodeAttribute, 'Pre Paid Password Not Exist' AS Description 
UNION
SELECT 125 AS CodeNumber, 'PrePaidPasswordNotSent' AS CodeAttribute, 'Pre Paid Password Not Sent' AS Description 
UNION
SELECT 130 AS CodeNumber, 'AgentUserLimitHit' AS CodeAttribute, 'Agent User Limit Hit' AS Description 
UNION
SELECT 200 AS CodeNumber, 'OrderValidationError' AS CodeAttribute, 'Order Validation Error' AS Description 
UNION
SELECT 201 AS CodeNumber, 'OrderNotFound' AS CodeAttribute, 'Order Not Found' AS Description 
UNION
SELECT 202 AS CodeNumber, 'OrderCanotPayout' AS CodeAttribute, 'Order Canot Payout' AS Description 
UNION
SELECT 203 AS CodeNumber, 'OrderReprint' AS CodeAttribute, 'Order Reprint' AS Description 
UNION
SELECT 230 AS CodeNumber, 'Error' AS CodeAttribute, 'Error' AS Description 
UNION
SELECT 231 AS CodeNumber, 'ServerError' AS CodeAttribute, 'Server Error' AS Description 
UNION
SELECT 232 AS CodeNumber, 'PriceCapHit' AS CodeAttribute, 'Price Cap Hit' AS Description 
UNION
SELECT 240 AS CodeNumber, 'EventDataNotFound' AS CodeAttribute, 'Event Data Not Found' AS Description 
UNION
SELECT 241 AS CodeNumber, 'OddsPriceChanged' AS CodeAttribute, 'Odds Price Changed' AS Description 
UNION
SELECT 250 AS CodeNumber, 'MinBetLimit' AS CodeAttribute, 'Min Bet Limit' AS Description 
UNION
SELECT 251 AS CodeNumber, 'MaxBetLimit' AS CodeAttribute, 'Max Bet Limit' AS Description 
UNION
SELECT 252 AS CodeNumber, 'OverCrditLimit' AS CodeAttribute, 'Over Crdit Limit' AS Description 
UNION
SELECT 253 AS CodeNumber, 'EventStatusDisabled' AS CodeAttribute, 'Event Status Disabled' AS Description 
UNION
SELECT 254 AS CodeNumber, 'BetTypeStatusDisabled' AS CodeAttribute, 'Bet Type Status Disabled' AS Description 
UNION
SELECT 255 AS CodeNumber, 'OddStatusDisabled' AS CodeAttribute, 'Odd Status Disabled' AS Description 
UNION
SELECT 256 AS CodeNumber, 'EventScoreChanged' AS CodeAttribute, 'Event Score Changed' AS Description 
UNION
SELECT 260 AS CodeNumber, 'ExecuteTimeIncorrect' AS CodeAttribute, 'Execute Time Incorrect' AS Description 
UNION
SELECT 270 AS CodeNumber, 'EventLimitHit' AS CodeAttribute, 'Event Limit Hit' AS Description 
UNION
SELECT 271 AS CodeNumber, 'BetTypeCapHit' AS CodeAttribute, 'Bet Type Cap Hit' AS Description 
UNION
SELECT 272 AS CodeNumber, 'AuthorizationInvalid' AS CodeAttribute, 'Authorization Invalid' AS Description 
UNION
SELECT 280 AS CodeNumber, 'PoolNotOpen' AS CodeAttribute, 'Pool Not Open' AS Description 
UNION
SELECT 281 AS CodeNumber, 'PoolLineLimitHit' AS CodeAttribute, 'Pool Line Limit Hit' AS Description 
UNION
SELECT 300 AS CodeNumber, 'TerminalNotFound' AS CodeAttribute, 'Terminal Not Found' AS Description 
UNION
SELECT 301 AS CodeNumber, 'TerminalAlreadyRegister' AS CodeAttribute, 'Terminal Already Register' AS Description 
UNION
SELECT 302 AS CodeNumber, 'TerminalIsNotActive' AS CodeAttribute, 'Terminal Is Not Active' AS Description 
UNION
SELECT 400 AS CodeNumber, 'NotEnoughMoney' AS CodeAttribute, 'Not Enough Money' AS Description 
UNION
SELECT 410 AS CodeNumber, 'InsertWithdrawFailed' AS CodeAttribute, 'Insert Withdraw Failed' AS Description 
UNION
SELECT 411 AS CodeNumber, 'CommitWithdrawFailed' AS CodeAttribute, 'Commit Withdraw Failed' AS Description 
UNION
SELECT 412 AS CodeNumber, 'CurrncyNotMatch' AS CodeAttribute, 'Currncy Not Match' AS Description 
UNION
SELECT 413 AS CodeNumber, 'ApplyWithdrawFailed' AS CodeAttribute, 'Apply Withdraw Failed' AS Description 
UNION
SELECT 414 AS CodeNumber, 'CreditNotEnough' AS CodeAttribute, 'Credit Not Enough' AS Description 
UNION
SELECT 415 AS CodeNumber, 'ExistBrnUsersDiffCurrency' AS CodeAttribute, 'Exist Brn Users Diff Currency' AS Description 
UNION
SELECT 500 AS CodeNumber, 'AlreadyActivated' AS CodeAttribute, 'Already Activated' AS Description 
UNION
SELECT 501 AS CodeNumber, 'UserNotFound' AS CodeAttribute, 'User Not Found' AS Description 
UNION
SELECT 600 AS CodeNumber, 'CampaignNotFound' AS CodeAttribute, 'Campaign Not Found' AS Description 
UNION
SELECT 601 AS CodeNumber, 'CampaignNotAllowed' AS CodeAttribute, 'Campaign Not Allowed' AS Description 
UNION
SELECT 1000 AS CodeNumber, 'EventsNotReady' AS CodeAttribute, 'Events Not Ready' AS Description 
UNION
SELECT 1001 AS CodeNumber, 'ApiError' AS CodeAttribute, 'Api Error' AS Description 
UNION
SELECT 1010 AS CodeNumber, 'PasswordCantBeNull' AS CodeAttribute, 'Password Cant Be Null' AS Description 
UNION
SELECT 1011 AS CodeNumber, 'PasswordMinPasswordLength' AS CodeAttribute, 'Password Min Password Length' AS Description 
UNION
SELECT 1012 AS CodeNumber, 'PasswordMustContainsSpecialChar' AS CodeAttribute, 'Password Must Contains Special Char' AS Description 
UNION
SELECT 1013 AS CodeNumber, 'PasswordCantStartSpecialChar' AS CodeAttribute, 'Password Cant Start Special Char' AS Description 
UNION
SELECT 1014 AS CodeNumber, 'PasswordAtLeatOneDigit' AS CodeAttribute, 'Password At Leat One Digit' AS Description 
UNION
SELECT 1015 AS CodeNumber, 'PasswordAtLeastOneLetter' AS CodeAttribute, 'Password At Least One Letter' AS Description 
UNION
SELECT 1016 AS CodeNumber, 'PasswordCombination' AS CodeAttribute, 'Password Combination' AS Description 
UNION
SELECT 1017 AS CodeNumber, 'PasswordSequence' AS CodeAttribute, 'Password Sequence' AS Description 
)a WHERE a.CodeNumber NOT IN (SELECT CodeNumber FROM [const].[ErrorCodes])
*/