﻿USE [Common]
GO

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'Sport',
		@Value = ?,
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Sql connection string of GameApi database (BrandDb).'

