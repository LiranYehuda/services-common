
UPDATE [finance].[PaymentMethods]
SET ActionType = 'REDIRECT'
where ActionType is null and CodeAttribute like 'REDIRECT%'
GO
UPDATE [finance].[PaymentMethods]
SET ActionType = 'IFRAME'
where ActionType is null and CodeAttribute like 'IFRAME%'
GO
UPDATE [finance].[PaymentMethods]
SET ActionType = 'EMBEDDED'
where ActionType is null
GO

UPDATE [finance].[PaymentMethods]
SET ActionValue = LTRIM(RTRIM(RIGHT(CodeAttribute,LEN(CodeAttribute) -LEN('REDIRECT_'))))
where ActionType = 'REDIRECT'
GO
UPDATE [finance].[PaymentMethods]
SET ActionValue = LTRIM(RTRIM(RIGHT(CodeAttribute,LEN(CodeAttribute) -LEN('IFRAME_'))))
where ActionType = 'IFRAME'
GO

UPDATE [finance].[PaymentMethods]
SET CodeAttribute = REPLACE(NAME,' ','_')
WHERE ActionType IN ('IFRAME','REDIRECT')
GO

UPDATE [finance].[PaymentMethods]
SET [Name] = 'MTN MOMO',CodeAttribute = 'DYA_YELLOWDOT',ActionValue = 'AMOUNT', icon = 'icon-Mobile-Money'
WHERE ID = 6
GO

UPDATE [finance].[PaymentMethods]
SET ActionValue = 'CODE',icon = 'icon-Shop'
WHERE CodeAttribute = 'SHOP_CASH'

INSERT [GSportsConfiguration].[Content].[ContentTypes](CodeAttribute,Name)
SELECT 'GENERAL_CONTENT','General Content'
GO

update finance.PaymentMethods
set icon = 'icon-USSD'
where Id IN (8,9)
GO

update finance.PaymentMethods
set icon = 'icon-Betslip-Alert'
where icon is null
GO


UPDATE [finance].[PaymentMethodConfiguration]
SET ButtonContentId = 2,ContentId = 5,SuccessContentId = 6
WHERE IsDeposit = 1 AND PaymentMethodId = 1
GO
UPDATE [finance].[PaymentMethodConfiguration]
SET ButtonContentId = 4,ContentId = 9,SuccessContentId = 11
WHERE IsDeposit = 0 AND PaymentMethodId = 1
GO
UPDATE [finance].[PaymentMethodConfiguration]
SET ButtonContentId = 1,ContentId = 7,SuccessContentId = 8
WHERE IsDeposit = 1 AND PaymentMethodId = 6
GO
UPDATE [finance].[PaymentMethodConfiguration]
SET ButtonContentId = 3,ContentId = 10,SuccessContentId = 12
WHERE IsDeposit = 0 AND PaymentMethodId = 6
GO


SET IDENTITY_INSERT [finance].[PaymentMethods] ON 
GO
INSERT [finance].[PaymentMethods] ([Id], [Name], [CodeAttribute], [ActionType], [ActionValue], [Icon], [ContentName], [EnablePhoneChange]) VALUES (13, N'USSD', N'USSD_CODE', N'EMBEDDED', N'CODE', N'icon-USSD', N'USSD_CODE', 0)
GO
INSERT [finance].[PaymentMethods] ([Id], [Name], [CodeAttribute], [ActionType], [ActionValue], [Icon], [ContentName], [EnablePhoneChange]) VALUES (14, N'USSD', N'USSD_AMOUNT', N'EMBEDDED', N'AMOUNT', N'icon-USSD', N'USSD_AMOUNT', 0)
GO
SET IDENTITY_INSERT [finance].[PaymentMethods] OFF
GO
