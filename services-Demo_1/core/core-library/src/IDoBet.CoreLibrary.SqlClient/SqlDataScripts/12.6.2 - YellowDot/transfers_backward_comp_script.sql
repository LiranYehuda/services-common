
update t
set t.ToAccountId = ac.ID
from [finance].[Transfers] t
inner join [user].[Access] a on t.ResponseAccessId = a.ID
inner join [finance].[Accounts] ac on ac.[UserId] = a.UserId
where t.ToAccountId = 2


update t
set t.FromAccountId = ac.ID
from [finance].[Transfers] t
inner join [user].[Access] a on t.RequestAccessId = a.ID
inner join [finance].[Accounts] ac on ac.[UserId] = a.UserId
where t.FromAccountId = 1

  UPDATE [finance].[TransferRequestTypes]
  SET [ApproveTransactionReasonFromId] = NULL,
		CancelTransactionReasonToId = NULL
  WHERE ID = 5 /*Deposit Shop/ USSD*/

  UPDATE [finance].[TransferRequestTypes]
  SET [ApproveTransactionReasonToId] = 6
  WHERE ID = 6 /*Withdrawal Shop/ USSD*/


 -- 12.6.2 yellow -  update customers to be from to account
  update [finance].[Transfers]
 set [FromAccountId] = [ToAccountId],
      [ToAccountId] = [FromAccountId]    
 where id IN
    (
        SELECT [ID]
          FROM [finance].[Transfers]
          where [RequestTypeId] IN (
          select ID FROM  [finance].[TransferRequestTypes]
          where [CodeAttribute] like '%WITHDRAW%')
          AND [FromAccountId] IN
          (
              SELECT A.ID FROM [user].[Users] U INNER JOIN [finance].[Accounts] A  ON  U.Id = A.UserId
              WHERE userType =2
              and U.id <> 2
          )
      )




