SET IDENTITY_INSERT [system].[Contents] ON 
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (1, 0, N'BUTTON_AMOUNT_DEPOSIT_CONTENT', N'Start deposit process')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (2, 0, N'BUTTON_CODE_DEPOSIT_CONTENT', N'Complete Deposit')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (3, 0, N'BUTTON_AMOUNT_WITHDRAW_CONTENT', N'Start Withdraw process')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (4, 0, N'BUTTON_CODE_WITHDRAW_CONTENT', N'Start Withdraw process')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (5, 0, N'SHOP_CASH_DEPOSIT_CONTENT', N'{"header":"Please visit any of our shops and give your CUSTOMER ID %USER_ID% <\/br>\r\nYou will be given a receipt with a TRANSFER CODE which you need to enter here to complete the deposit","footer":""}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (6, 0, N'SHOP_CASH_DEPOSIT_SUCCESS_CONTENT', N'{"header":"Deposit","footer":"Deposit successful! The money has been added to your account balance."}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (7, 0, N'YELLOWBET_DEPOSIT_CONTENT', N'{"header":"Enter the deposit amount<\/BR> Your MOMO phone number:%PHONE_NUMBER%","footer":"<h1 class=\"center\">Supported networks:<\/h1>\r\n           <div class=\"mobile-supported-networks\">\r\n             <ul class=\"center\">\r\n               <li>\r\n                 <img style=\"width: 150px\" src=\".\/assets\/common\/contentPages\/mtn_momo.svg\"  alt=\"mtn_momo\">\r\n               <\/li>\r\n             <\/ul>\r\n           <\/div>"}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (8, 0, N'YELLOWBET_DEPOSIT_SUCCESS_CONTENT', N'{"header":"Deposit Request Sent","footer":"The deposit process has been started. You''ll get a USSD popup to your phone, enter your PIN in order to finish the transaction. After completion of the transaction, please check your balance. If you did not get your funds, please call to the support team or dial *400# - (9) - My approvals"}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (9, 0, N'SHOP_CASH_WITHDRAW_CONTENT', N'{"header":"Shop Withdraw </br> The amount will be deducted from your balance and can be collected from any valid Transfer Code which will be given to you confirm Withdraw.","footer":""}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (10, 0, N'YELLOWBET_WITHDRAW_CONTENT', N'{"header":"MOMO MTN Withdraw<br>The amount will be deducted from your balance and will transfer directly to your mobile money account.<br>Your MOMO phone number : %PHONE_NUMBER%.<br>Enter your account password.","footer":"<h1 class=\"center\">Supported networks:<\/h1>\r\n           <div class=\"mobile-supported-networks\">\r\n             <ul class=\"center\">\r\n               <li>\r\n                 <img style=\"width: 150px\" src=\".\/assets\/common\/contentPages\/mtn_momo.svg\"  alt=\"mtn_momo\">\r\n               <\/li>\r\n             <\/ul>\r\n           <\/div>"}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (11, 0, N'SHOP_CASH_WITHDRAW_SUCCESS_CONTENT', N'{"header":"Withdraw","footer":"Request successful! Go to your nearest store with the code that was sent to your phone to complete the process!"}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (12, 0, N'YELLOWBET_WITHDRAW_SUCCESS_CONTENT', N'{"header":"Withdraw Request Sent","footer":"The withraw process has been started. After completion of the transaction, we will send you SMS. If you did not get your funds, please call to the support team"}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (13, 0, N'YELLOWBET_USSD_DEPOSIT_CONTENT', N'{"header":"","footer":"Dial *365#<br>(2) - On line deposit<br>Enter amount and choose *OK*<br>Please enter your MOMO PIN CODE into the POP-UP message and confirm the transaction. if the POP-UP wasn''t shown, please dial *400# and choose (8) *My approvals*"}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (14, 0, N'YELLOWBET_USSD_WITHDRAW_CONTENT', N'{"header":"","footer":"Dial *365#<BR>(3) - On line withdrawal<BR>Enter the amount and choose *OK*<BR>Please enter your MOMO PIN CODE into the POP-UP message and confirm the transaction"}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (15, 8, N'USSD_DEPOSIT_CONTENT', N'{"header":"","footer":"1. Dial *404# <BR>2. Select the option « 2 » : Online deposit.<BR>3. Insert your online winner.bet user ID.<BR>4. Insert Your IPIN(The IPIN is a code you will find in the menu where you edit your profile). <BR>5. Insert the deposit amount.<BR>6. Confirm the deposit amount by pressing *1*.<BR>7. Please Wait until you see a pop up appears for you to insert your Mpesa or Orange money PIN.<BR>8. You will get a SMS to notify that money has been deducted from your mobile money wallet showing that your deposit was successful."}')
GO
INSERT [system].[Contents] ([Id], [BrandId], [CodeAttribute], [Value]) VALUES (16, 8, N'USSD_WITHDRAW_CONTENT', N'{
  "header": "Ussed Withdraw </br> The amount will be deducted from your balance and can be collected from any valid Transfer Code which will be given to you confirm Withdraw.",
  "footer": "1.Dial *404# <BR>2.Select the option « 3 » : Online withdrawal. <BR>3.Insert your online winner.bet user ID(%USER_ID%). <BR>4.Insert the withdrawal code that was sent to your email adress.<BR>5.Insert the withdrawal amount. <BR>6.Confirm the withdrawal by pressing *1*<BR>7.You will then get a SMS that your account has been credited which confirms that your withdrawal was successful."}')
GO
SET IDENTITY_INSERT [system].[Contents] OFF
