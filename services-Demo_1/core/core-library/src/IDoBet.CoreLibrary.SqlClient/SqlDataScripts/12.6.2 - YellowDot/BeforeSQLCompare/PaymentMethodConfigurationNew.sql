EXEC sp_rename 'finance.PaymentMethodConfiguration', 'PaymentMethodConfigurationOld'
GO
CREATE TABLE [finance].[PaymentMethodConfiguration](
	[ChannelId] [int] NOT NULL,
	[PaymentMethodId] [int] NOT NULL,
	[IsDeposit] [bit] NOT NULL,
	[Min] [float] NOT NULL,
	[Max] [float] NULL,
	[ShowOrder] [int] NULL,
	[ContentId] [int] NULL,
	[SuccessContentId] [int] NULL,
	[ButtonContentId] [int] NULL
) ON [PRIMARY]
GO

ALTER TABLE [finance].[PaymentMethodConfiguration] ADD  CONSTRAINT [DF_PaymentMethodConfigurationNew_Min]  DEFAULT ((0)) FOR [Min]
GO

INSERT INTO [finance].[PaymentMethodConfiguration] 
(
    ChannelId,
    PaymentMethodId,
    IsDeposit,
    [Min],
    [Max],
    ShowOrder
)
SELECT
    PCD.ChannelId,
    PCD.PaymentMethodId,
    1,
    PCD.MinCredit,
    PCD.MaxCredit,
    PCD.ShowOrder
FROM
    [finance].[PaymentMethodConfigurationOld] PCD
WHERE
    ISNULL(PCD.MaxCredit, 1) <> 0
UNION
SELECT
    PCW.ChannelId,
    PCW.PaymentMethodId,
    0,
    PCW.MinDebit,
    PCW.MaxDebit,
    PCW.ShowOrder
FROM
    [finance].[PaymentMethodConfigurationOld] PCW
WHERE
    ISNULL(PCW.MaxDebit, 1) <> 0
GO 



