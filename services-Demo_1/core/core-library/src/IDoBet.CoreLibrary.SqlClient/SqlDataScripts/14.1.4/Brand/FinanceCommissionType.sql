IF NOT EXISTS(SELECT 1 FROM [const].[CommissionType] WHERE ID = 1003)
	INSERT INTO [const].[CommissionType]
           ([Id]
           ,[Name]
           ,[Desc]
           ,[CodeAttribute]
           ,[ProductId]
           ,[IsDeposit])
     VALUES
           (1003
           ,'WinLotto'
           ,'Winning commission for lotto bet'
           ,'WIN_LOTTO'
           ,NULL
           ,0)