--Tanzania
update branch.Terminals
set IsDeleted = 0
where ID in (1725, 1726)

--Uganda
update branch.Terminals
set IsDeleted = 0
where ID in (898, 899)

--Moors
update branch.Terminals
set IsDeleted = 0
where ID in (1352, 1353)

--Bwinners
update branch.Terminals
set IsDeleted = 0
where ID in ( 1226, 1227, 1228, 1229)
