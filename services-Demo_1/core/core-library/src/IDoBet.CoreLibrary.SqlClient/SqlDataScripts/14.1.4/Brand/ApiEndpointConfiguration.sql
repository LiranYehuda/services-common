DECLARE @BrandId int = ?

EXECUTE [system].[spSetApiEndpointConfiguration] 
   @BrandId = @BrandId
  ,@ModuleId = 2
  ,@Instance = NULL
  ,@Key = 'LottoApi'
  ,@Value = ?'https://dev.idobet.com/services/lottoapi'
  ,@Authentication = ''
  ,@AdditionalData = ''
  ,@Description = 'lotto api url'

 