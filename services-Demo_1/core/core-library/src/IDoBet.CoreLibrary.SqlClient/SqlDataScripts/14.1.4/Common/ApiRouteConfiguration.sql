delete common.SYSTEM.ApiRouteConfiguration  where MethodName in (
'UpdateUserBet'
,'ConfirmBet'
,'SettleBets'
,'BOGetDrawsToUpdate'
,'GetLotteryTypes'
,'GetLotteryLinePrice'
,'GetNextDraws'
,'GetLotteryPrizes'
,'CancelTicket'
,'GetLatestResults'
,'GetUserWinningLinesResult'
,'UpdateDrawResults'
,'UpdateDrawJackpot'
,'GetDrawWinningsResults'
)

--BOGetDrawsSummaryReport

--Commissions

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLottoCommissionsGroup',
		@SpName = N'[idb].[spGetLottoCommissionsGroup]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'PayLottoCommissionsGroup',
		@SpName = N'[idb].[spPayLottoCommissionsGroup]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3
		
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'LottoGetTotals',
		@SpName = N'[idb].[spBoLottoGetTotals]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3
		
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetDrawWinningsResults',
		@SpName = N'[dbo].[spGetDrawWinningsResults]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetDrawProgresiveJackpot',
		@SpName = N'[idb].[spGetDrawProgresiveJackpot]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SetDrawProgresiveJackpot',
		@SpName = N'[idb].[spSetDrawProgresiveJackpot]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateDrawJackpot',
		@SpName = N'[idb].[spUpdateDrawJackpot]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

 --Get draw to show in white backoffice
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetDrawsToUpdate',
		@SpName = N'[idb].[spGetDrawsToUpdate]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5	

 --Get logs to show in white backoffice
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLogBOChanges',
		@SpName = N'[idb].[spGetLogBOChanges]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5	

--Set results 
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateDrawResults',
		@SpName = N'[idb].[spUpdateDrawResults]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5


--Set Prizes 
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateDrawPrizes',
		@SpName = N'[idb].[spUpdateDrawPrizes]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

--Settle bets
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SettleBets',
		@SpName = N'[idb].[spSettleBets]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

		EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetDrawsSummaryReport',
		@SpName = N'[dbo].[spGetDrawsSummaryReport]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5



--Bet
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'ConfirmBet',
		@SpName = N'[idb].[spConfirmBet]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateUserBet',
		@SpName = N'[idb].[spUpdateUserBet]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5



	
--online/cashbox/pos part of bet show 
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserOrder',
		@SpName = N'[idb].[spGetUserOrder]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5	

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLotteryPrizes',
		@SpName = N'[idb].[spGetLotteryPrizes]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5	


EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserWinningLinesResult',
		@SpName = N'[dbo].[spGetUserWinningLinesResult]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5	
		

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetDrawResults',
		@SpName = N'[idb].[spGetDrawResults]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLotteryLinePrice',
		@SpName = N'[dbo].[spGetLotteryLinePrice]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLotteryTypes',
		@SpName = N'[dbo].[spGetLotteryTypes]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetNextDraws',
		@SpName = N'[dbo].[spGetNextDraws]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'BOGetDrawsToUpdate',
		@SpName = N'[dbo].[spBOGetDrawsToUpdate]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 5

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'CancelTicket',
		@SpName = N'dbo.spCancelTicket',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0


EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetDrawSelectedNumbers',
		@SpName = N'[dbo].spGetDrawSelectedNumbers',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

		