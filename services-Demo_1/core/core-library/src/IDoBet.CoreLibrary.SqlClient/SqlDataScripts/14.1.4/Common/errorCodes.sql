IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='draw_prizes_undefine' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(720,'draw_prizes_undefine', 'draw prizes is undefine',0)
END
