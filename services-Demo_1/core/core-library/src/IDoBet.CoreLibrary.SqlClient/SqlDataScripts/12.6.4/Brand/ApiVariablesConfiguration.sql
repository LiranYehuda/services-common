
EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = ?,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'NotifyWinnerBetting',
		@Value = ?N'1',
		@Description = N'enable sending notification to winners'

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = ?,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'BrandOnlineSite',
		@Value = ?N'http://devidobet.com/#/sports/sportsBetting',
		@Description = N'Brand online site'


EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = ?,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'DefaultAuthenticationMethod',
		@Value = ?--1,
		@Description = N'Default authentication method'


DECLARE @OperatorId int = 550
DECLARE @IsTest nvarchar(10) = ? -- true
DECLARE @country NVARCHAR(10) = ? --  ISO 3166 country
DECLARE @Value NVARCHAR(MAX) = 
	'https://routerstg.groovegaming.com/game/?nogsgameid={0}&nogsoperatorid='+cast(@OperatorId as nvarchar(10))+'&sessionid={1}&nogscurrency={2}&nogslang={3}&accountid={4}&homeurl={5}&nogsmode={6}&device_type={7}&country='+@country+'&is_test_account='+@IsTest+'&license=Curacao'
EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = ?,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'StartGameCasinoUrlFormat',
		@Value = @Value,
		@Description = N'Start game casino url format'


