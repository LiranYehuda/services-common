
UPDATE [??].[system].[Channels]
SET [CodeAttribute] = 'BackOffice'
WHERE [CodeAttribute] = 'Back Office' 

INSERT INTO [??].[system].[Channels]
           ([Id]
           ,[CodeAttribute])
SELECT [Id]     
      ,[CodeAttribute]
FROM [Common].[system].[Channels]
WHERE [CodeAttribute] NOT IN (SELECT [CodeAttribute] FROM [??].[system].[Channels])


