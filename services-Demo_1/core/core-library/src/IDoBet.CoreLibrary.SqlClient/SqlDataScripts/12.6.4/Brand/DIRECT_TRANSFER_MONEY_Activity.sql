IF NOT EXISTS (SELECT TOP 1 ID FROM [user].[ActivityType] WHERE ID = 11000)
	INSERT INTO [user].[ActivityType] (ID, [Name], [CodeAttribute]) VALUES (11000, 'API', 'API')
GO

IF NOT EXISTS(SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] IN ('DIRECT_TRANSFER_MONEY'))
BEGIN
	INSERT INTO [user].[Activity]
	           ([Name]
	           ,[ActivityTypeId]
	           ,[CodeAttribute])
	     VALUES
	           ('Direct Transfer Money'
	           ,11000
	           ,'DIRECT_TRANSFER_MONEY')				
END

