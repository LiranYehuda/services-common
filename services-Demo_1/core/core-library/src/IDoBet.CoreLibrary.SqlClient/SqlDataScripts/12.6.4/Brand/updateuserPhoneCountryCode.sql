UPDATE USR
SET PhoneCountryCode =  USP.PhoneCode
from [user].[users] USR WITH(NOLOCK)
INNER JOIN [IDB].UserCountriesProcessed USP WITH(NOLOCK) ON USP.PhoneCode = LEFT(REPLACE(USR.Phone,'+',''),LEN(USP.PhoneCode))
WHERE USR.DELETED = 0 AND USR.Phone is not null and USR.PHONE NOT LIKE '%*%' AND ISNUMERIC(REPLACE(USR.Phone,'+','')) = 1 AND USP.PhoneCode > 10 AND USR.PhoneCountryCode IS NULL