
EXEC [Common].[idb].[spSyncUserCountries] @BrandIdFilter = 12
GO

update ucc
set IsActive = 1
from idb.UserCountriesConfiguration ucc 
inner join system.userCountries syu on syu.Id = ucc.Id
where syu.ForDisplay = 1
GO

-- run one time and comment 
--update  idb.UserCountriesConfiguration 
--set DisplayName = null,Operators = '[{"code":"750","min":9,"max":9},{"code":"751","min":9,"max":9},{"code":"752","min":9,"max":9},{"code":"753","min":9,"max":9},{"code":"754","min":9,"max":9},{"code":"755","min":9,"max":9},{"code":"756","min":9,"max":9},{"code":"757","min":9,"max":9},{"code":"758","min":9,"max":9},{"code":"759","min":9,"max":9},{"code":"770","min":9,"max":9},{"code":"771","min":9,"max":9},{"code":"772","min":9,"max":9},{"code":"773","min":9,"max":9},{"code":"774","min":9,"max":9},{"code":"775","min":9,"max":9},{"code":"776","min":9,"max":9},{"code":"777","min":9,"max":9},{"code":"778","min":9,"max":9},{"code":"779","min":9,"max":9},{"code":"780","min":9,"max":9},{"code":"781","min":9,"max":9},{"code":"782","min":9,"max":9},{"code":"783","min":9,"max":9},{"code":"784","min":9,"max":9},{"code":"785","min":9,"max":9},{"code":"786","min":9,"max":9},{"code":"787","min":9,"max":9},{"code":"788","min":9,"max":9},{"code":"789","min":9,"max":9},{"code":"710","min":9,"max":9},{"code":"711","min":9,"max":9},{"code":"712","min":9,"max":9},{"code":"713","min":9,"max":9},{"code":"714","min":9,"max":9},{"code":"715","min":9,"max":9},{"code":"716","min":9,"max":9},{"code":"717","min":9,"max":9},{"code":"718","min":9,"max":9},{"code":"719","min":9,"max":9},{"code":"971","min":9,"max":9}]'
--where brandid = 12 AND ID = 222
-----

EXEC [Common].[idb].[spSyncUserCountries] @BrandIdFilter = 12