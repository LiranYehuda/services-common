


IF NOT EXISTS (SELECT * FROM [user].[Activity] WHERE [CodeAttribute] = 'PREPAID_USERS')
BEGIN
		INSERT INTO [user].[Activity]
				   ([Name]
				   ,[ActivityTypeId]
				   ,[CodeAttribute])
		 VALUES
				   ('Prepaid Users'
				   ,800 /*Online*/
				   ,'PREPAID_USERS')
END


IF NOT EXISTS (SELECT * FROM [user].[Activity] WHERE [CodeAttribute] = 'CASHBOX_PREPAID_WITHDRAW')
BEGIN
	INSERT INTO [user].[Activity]
			   ([Name]
			   ,[ActivityTypeId]
			   ,[CodeAttribute])
	 VALUES
			   ('Cashbox Prepaid Withdraw'
			   ,10000
			   ,'CASHBOX_PREPAID_WITHDRAW')
END



	

IF NOT EXISTS (SELECT * FROM [user].[Activity] WHERE [CodeAttribute] = 'CANCEL_PREPAID')
BEGIN
		INSERT INTO [user].[Activity]
				   ([Name]
				   ,[ActivityTypeId]
				   ,[CodeAttribute])
		 VALUES
				   ('Cancel Prepaid'
				   ,800 /*Online*/
				   ,'CANCEL_PREPAID')
END

IF NOT EXISTS (SELECT TOP 1 1 FROM [finance].[TransactionReasons] WHERE CodeAttribute = 'PREPAID_CANCEL')
	INSERT INTO [finance].[TransactionReasons]
			   ([ID]
			   ,[Name]
			   ,[CodeAttribute]
			   ,[TransactionTypesCode]
			   ,[IsComission])
		 VALUES
			   (33
			   ,'Prepaid Cancel'
			   ,'PREPAID_CANCEL'
			   ,'DEBIT'
			   ,0)
