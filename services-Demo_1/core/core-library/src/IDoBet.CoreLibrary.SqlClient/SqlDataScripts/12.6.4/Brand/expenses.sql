IF NOT EXISTS (SELECT 1 FROM [user].[ActivityType] WHERE [ID] = 1300)
BEGIN
	INSERT INTO [user].[ActivityType]
           ([ID]
           ,[Name]
           ,[CodeAttribute])
     VALUES
           (1300
           ,'Expenses'
           ,'EXPENSES')
END

IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'EXPENSES_CATEGORIES')
BEGIN
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Expenses Categories'
           ,1300
           ,'EXPENSES_CATEGORIES')
END

IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'EXPENSES_ITEMS')
BEGIN
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Expenses Items'
           ,1300
           ,'EXPENSES_ITEMS')
END

IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'EXPENSES_MONTHS')
BEGIN
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Expenses Months'
           ,1300
           ,'EXPENSES_MONTHS')
END

IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'EXPENSES')
BEGIN
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Expenses'
           ,1300
           ,'EXPENSES')
END

IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'APPROVE_EXPENSES')
BEGIN
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Approve Expenses'
           ,1300
           ,'APPROVE_EXPENSES')
END

IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'CANCEL_EXPENSES')
BEGIN
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Cancel Expenses'
           ,1300
           ,'CANCEL_EXPENSES')
END



INSERT INTO [const].[CustomEnums]
           ([EnumType]
           ,[Name]
           ,[Value])
SELECT R.[EnumType], R.[Name], R.[Value]
FROM
(
	SELECT 'eExpensesPeriodType' [EnumType] , 'Daily' [Name], 1 [Value]
	UNION
	SELECT 'eExpensesPeriodType' [EnumType] , 'Monthly' [Name], 2 [Value]
	UNION
	SELECT 'eExpensesPeriodType' [EnumType] , 'Yearly' [Name], 3 [Value]
	UNION
	SELECT 'eExpensesType' [EnumType] , 'Opex' [Name], 1 [Value]
	UNION
	SELECT 'eExpensesType' [EnumType] , 'Capex' [Name], 2 [Value]
	UNION
	SELECT 'eExpensesCostType' [EnumType] , 'Fixed' [Name], 1 [Value]
	UNION
	SELECT 'eExpensesCostType' [EnumType] , 'Varies' [Name], 2 [Value]
	UNION
	SELECT 'eExpensesStatus' [EnumType] , 'Approved' [Name], 1 [Value]
	UNION
	SELECT 'eExpensesStatus' [EnumType] , 'Pending' [Name], 2 [Value]
	UNION
	SELECT 'eExpensesStatus' [EnumType] , 'Rejected' [Name], 3 [Value]
	UNION
	SELECT 'eExpensesStatus' [EnumType] , 'Canceled' [Name], 4 [Value]
)R
LEFT JOIN [const].[CustomEnums] C ON R.[EnumType] = C.EnumType AND R.[Name] = C.[Name]
WHERE C.Id IS NULL


IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'EXPENSES' AND ActivityTypeId = 10000)
BEGIN
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Expenses'
           ,10000
           ,'EXPENSES')
END

IF NOT EXISTS (SELECT TOP 1 1 FROM [finance].[TransactionReasons] WHERE [CodeAttribute] = 'EXPENSE')
INSERT INTO [finance].[TransactionReasons]
           ([ID]
           ,[Name]
           ,[CodeAttribute]
           ,[TransactionTypesCode]
           ,[IsComission])
     VALUES
           (31
           ,'Expense'
           ,'EXPENSE'
           ,'DEBIT'
           ,0)


IF NOT EXISTS (SELECT TOP 1 1 FROM [finance].[TransactionReasons] WHERE [CodeAttribute] = 'REVERSE_EXPENSE')
INSERT INTO [finance].[TransactionReasons]
           ([ID]
           ,[Name]
           ,[CodeAttribute]
           ,[TransactionTypesCode]
           ,[IsComission])
     VALUES
           (32
           ,'ReverseExpense'
           ,'REVERSE_EXPENSE'
           ,'CREDIT'
           ,0)

IF NOT EXISTS (SELECT TOP 1 1 FROM [system].[AlertClassificationType] WHERE CodeAttribute = 'EXPENSE_REQUEST_CREATED')
 INSERT INTO [system].[AlertClassificationType]
           ([ID]
           ,[Name]
           ,[Severity]
           ,[CodeAttribute]
           ,[Desc])
     VALUES
           (900
           ,'Expense Request Created'
           ,0
           ,'EXPENSE_REQUEST_CREATED'
           ,'Expense request created')

		   
IF NOT EXISTS (SELECT TOP 1 1 FROM [system].[AlertClassificationType] WHERE CodeAttribute = 'EXPENSE_REQUEST_PENDING_APPROVAL')
 INSERT INTO [system].[AlertClassificationType]
           ([ID]
           ,[Name]
           ,[Severity]
           ,[CodeAttribute]
           ,[Desc])
     VALUES
           (901
           ,'Expense Request Pending Approval'
           ,0
           ,'EXPENSE_REQUEST_PENDING_APPROVAL'
           ,'Expense request pending approval')

IF NOT EXISTS (SELECT TOP 1 1 FROM [system].[AlertClassificationType] WHERE CodeAttribute = 'EXPENSE_REQUEST_EXCEEDED_MAX_AMOUNT')
 INSERT INTO [system].[AlertClassificationType]
           ([ID]
           ,[Name]
           ,[Severity]
           ,[CodeAttribute]
           ,[Desc])
     VALUES
           (902
           ,'Expense Request Exceeded Max Amount'
           ,0
           ,'EXPENSE_REQUEST_EXCEEDED_MAX_AMOUNT'
           ,'Expense request exceeded max amount')

 IF NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'EXPENSE_REQUEST_CREATED')
 INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
     VALUES
           ('Expense Request Created'
           ,'Expense Request Created'
           ,'$ExpenseId$ - $Item$ - $Amount$ $Currency$ - new expense request has been submited' 
           ,'Dear $FirstName$,<br/>A new expense request has been submited:<br/>Expense item: $Item$<br/>Description: $Description$<br/>Amount: $Amount$ $Currency$' 
           ,1
           ,1
           ,NULL
           ,'$ExpenseId$ - $Item$ - $Amount$ $Currency$ - new expense request has been submited' 
           ,'EXPENSE_REQUEST_CREATED'
           ,'YellowBet')

	
IF NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'EXPENSE_REQUEST_EXCEEDED_MAX_AMOUNT')
 INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
     VALUES
           ('Expense Request Exceeded Max Amount'
           ,'Expense Request Exceeded Max Amount'
           ,'$ExpenseId$ - $Item$ - $Amount$ $Currency$ - expense request for your review' 
           ,'Dear $FirstName$,<br/>The below request for your review:<br/>Expense item: $Item$<br/>Description: $Description$<br/>Amount: $Amount$ $Currency$' 
           ,1
           ,1
           ,NULL
           ,'$ExpenseId$ - $Item$ - $Amount$ $Currency$ - expense request for your review' 
           ,'EXPENSE_REQUEST_EXCEEDED_MAX_AMOUNT'
           ,'YellowBet')

IF NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'EXPENSE_REQUEST_PENDING_APPROVAL')
  INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
     VALUES
           ('Expense Request Pending Approval'
           ,'Expense Request Pending Approval'
           ,'$ExpenseId$ - $Item$ - $Amount$ $Currency$ - expense request for your approval' 
           ,'Dear $FirstName$,<br/>A new expense request is pending for your approval:<br/>Expense item: $Item$<br/>Description: $Description$<br/>Amount: $Amount$ $Currency$' 
           ,1
           ,1
           ,NULL
           ,'$ExpenseId$ - $Item$ - $Amount$ $Currency$ - expense request for your approval' 
           ,'EXPENSE_REQUEST_PENDING_APPROVAL'
           ,'YellowBet')

IF NOT EXISTS (SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'SELF_APPROVE_EXPENSE')
	INSERT INTO [user].[Activity]
           ([Name]
           ,[ActivityTypeId]
           ,[CodeAttribute])
     VALUES
           ('Self approve expense'
           ,1300
           ,'SELF_APPROVE_EXPENSE')
