

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PrepaidCodeNotExists' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(800,'PrepaidCodeNotExists', 'Prepaid code not exists',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PrepaidIsConverted' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(801,'PrepaidIsConverted', 'Prepaid is converted',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='NoPermmisionForPrepaidUser' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(802,'NoPermmisionForPrepaidUser', 'No permission - prepaid user not converted to fully profiled user',0)
END
IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PendingCommissionExist' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(417,'PendingCommissionExist', 'Pending commission exist',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PrepaidSecured' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(803,'PrepaidSecured', 'Prepaid secured',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='IncorrectPrepaidCode' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(804,'IncorrectPrepaidCode', 'Incorrect code',0)
END


IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PhoneCountryCodeNotSupported' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(126,'PhoneCountryCodeNotSupported', 'Phone country code not supported',0)
END


IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PHONE_IS_NOT_VALID' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(127,'PHONE_IS_NOT_VALID', 'Phone is not valid',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PHONE_OPERATOR_NOT_SUPPORTED' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(128,'PHONE_OPERATOR_NOT_SUPPORTED', 'Phone operator not supported',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='CategoryContainsRelatedItems' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(900,'CategoryContainsRelatedItems', 'Category should not be deleted since some items related to the category',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='CategoryNameAlreadyExists' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(901,'CategoryNameAlreadyExists', 'Category name already exists',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='ItemNameAlreadyExists' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(902,'ItemNameAlreadyExists', 'Item name already exists',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='ItemContainsRelatedExpenses' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(903,'ItemContainsRelatedExpenses', 'Item should not be deleted since some expenses related to the item',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='ExpensesMonthAlreadyExists' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(904,'ExpensesMonthAlreadyExists', 'Expenses month already exists',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='AmountExceededMaxAmount' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(905,'AmountExceededMaxAmount', 'Amount exceeded the max amount',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='MonthNotOpenedForExpenseRequest' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(906,'MonthNotOpenedForExpenseRequest', 'Month not opened for expense request',0)
END


IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='NumOfRequetsExceededPeriodLimit' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(907,'NumOfRequetsExceededPeriodLimit', 'Num of requets exceeded period limit',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='ExpenseRequestAlreadyApproved' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(908,'ExpenseRequestAlreadyApproved', 'Expense request already approved',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='ExpenseRequestCanceled' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(909,'ExpenseRequestCanceled', 'Expense request canceled',0)
END
IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='FileNotFound' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(13,'FileNotFound', 'File not found',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='RegionNotPermmitedForExpenseRequest' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(910,'RegionNotPermmitedForExpenseRequest', 'Region not permmited for expense request',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='PrepaidIsActivated' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(805,'PrepaidIsActivated', 'Prepaid is activated',0)
END

