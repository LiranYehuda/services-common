

IF NOT EXISTS(SELECT  1 FROM dbo.NotificationTemplate WHERE CodeAttribute = 'WinnerBettingNotification')
BEGIN
	INSERT dbo.NotificationTemplate([Name],	[Desc]	,Title,	Body	,IsHtml	,NotificationMethod	,PlaceHolders	,[Subject]	,CodeAttribute,	[From])
	VALUES('Winner Betting Notification','Winner Betting Notification','Winner Betting Notification','Félicitations, votre mise de [orderid] rapporte un gain de [amount] CFA, [domain]',0,2,NULL,'Winner Betting Notification','WinnerBettingNotification','IDoBet')
END



