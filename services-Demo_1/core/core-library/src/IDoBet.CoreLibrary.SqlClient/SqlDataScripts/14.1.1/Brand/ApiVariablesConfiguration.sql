DECLARE @BrandId int = ? 

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'ValidateGeolocationAccess',
		@Value = ?N'1', -- bit
		@Description = N'enable geolocation valiation access'

EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = 'GameApiURL',
		@Value = ??,
		@Description = 'Game API url'

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'OnlineMonthlyTax',
		@Value = ?N'15',
		@Description = N'Online Monthly Tax (Used for online monthly reports)'

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'GameCasinoOperatorId',
		@Value = ?N'550',
		@Description = N'Our casino operator id on Groove system'


DECLARE @IsTest nvarchar(10) = ? -- true
DECLARE @country NVARCHAR(10) = ? --  ISO 3166 country
DECLARE @Value NVARCHAR(MAX) = 
	'https://routerstg.groovegaming.com/game/?nogsgameid={0}&nogsoperatorid={1}&sessionid={2}&nogscurrency={3}&nogslang={4}&accountid={5}&homeurl={6}&nogsmode={7}&device_type={8}&country='+@country+'&is_test_account='+@IsTest+'&license=Curacao'
EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'StartGameCasinoUrlFormat',
		@Value = @Value,
		@Description = N'Start game casino url format'


EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = 'LastBetTypeSettlementRowId',
		@Value = NULL,
		@Description = 'Lat bet type settelement row id'

EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = 'BetTypeSettlementRowsCount',
		@Value = 1000,
		@Description = 'Bet type settlement rows count'
		
		
EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 12,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = 'WelcomeSMSContent',
		@Value = 'WELCOME! You have successfully signed up to Gal Sport Betting. Enjoy Your UGX 2000 free bet and 100% bonus on your first deposit up to UGX 1 million!',
		@Description = 'Welcome SMS message content'

/**************brand settings - start**********************/
--Uganda
EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 12,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{ "registrationForm": { "showFirstName": true, "showLastName": true, "showCurrency": false, "showPhone": true, "showEmail": false, "showCountry": false, "registrationType": 2 }, "showAddPhoneButton": true }',
		@Description = 'Online settings',
		@ChannelId = 1


EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 12,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{ "registrationForm": { "showFirstName": true, "showLastName": true, "showCurrency": false, "showPhone": false, "showEmail": false, "showCountry": false, "registrationType": 2 }, "showAddPhoneButton": true }',
		@Description = 'Mobile settings',
		@ChannelId = 4

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 12,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{ "registrationForm": { "showFirstName": true, "showLastName": true, "showCurrency": false, "showPhone": false, "showEmail": false, "showCountry": false, "registrationType": 2 }, "showAddPhoneButton": true, "receiptHeaderText":"a better place ltd" }',
		@Description = 'Kiosk settings',
		@ChannelId = 6

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 12,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{"receiptHeaderText":"a better place ltd"}',
		@Description = 'Cashbox settings',
		@ChannelId = 2

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 12,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{"receiptHeaderText":"a better place ltd"}',
		@Description = 'Pos settings',
		@ChannelId = 3

--Moorse
EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 7,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value ='{ "registrationForm": { "showFirstName": true, "showLastName": true, "showCurrency": false, "showPhone": false, "showEmail": false, "showCountry": false, "registrationType": 1 }, "showAddPhoneButton": false}',
		@Description = 'Online settings',
		@ChannelId = 1


EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 7,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{ "registrationForm": { "showFirstName": true, "showLastName": true, "showCurrency": false, "showPhone": false, "showEmail": false, "showCountry": false, "registrationType": 1 }, "showAddPhoneButton": false}',
		@Description = 'Mobile settings',
		@ChannelId = 4

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 7,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{ "registrationForm": { "showFirstName": true, "showLastName": true, "showCurrency": false, "showPhone": false, "showEmail": false, "showCountry": false, "registrationType": 1 }, "showAddPhoneButton": false, "receiptHeaderText":"" }',
		@Description = 'Kiosk settings',
		@ChannelId = 6

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 7,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{"receiptHeaderText":""}',
		@Description = 'Cashbox settings',
		@ChannelId = 2

EXEC	[Common].[system].[spSetApiVariableConfiguration]
		@BrandId = 7,
		@ModuleId = 0,
		@Instance = NULL,		
		@Key = 'Settings',
		@Value = '{"receiptHeaderText":""}',
		@Description = 'Pos settings',
		@ChannelId = 3
		
/**************brand settings - end************************/



/**************Rave*************************************/
EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = 'RavePaymentSecretKey',
		@Value = ?,
		@Description = 'Rave payment secret key'
/**************Rave*************************************/