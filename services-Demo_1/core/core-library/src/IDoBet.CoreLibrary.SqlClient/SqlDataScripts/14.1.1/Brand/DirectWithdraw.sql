IF NOT EXISTS(SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] IN ('DIRECT_WITHDRAW'))
BEGIN
	INSERT INTO [user].[Activity]
	           ([Name]
	           ,[ActivityTypeId]
	           ,[CodeAttribute])
	     VALUES
	           ('Direct withdraw'
	           ,11000
	           ,'DIRECT_WITHDRAW')				
END

