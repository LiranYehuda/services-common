if not exists(select 1 from [user].[Activity] where CodeAttribute = 'BRANCH_COMMISSION')
	INSERT [user].[Activity]([Name]
	      ,[ActivityTypeId]
	      ,[CodeAttribute])
	SELECT 'Allow user to view/edit branch commission',100,'BRANCH_COMMISSION'
