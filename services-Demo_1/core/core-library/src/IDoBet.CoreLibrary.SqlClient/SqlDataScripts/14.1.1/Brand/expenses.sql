
IF(NOT EXISTS (SELECT 1 FROM [system].[AlertClassificationType] WHERE ID = 903))
BEGIN
	INSERT INTO [system].[AlertClassificationType]
           ([ID]
           ,[Name]
           ,[Severity]
           ,[CodeAttribute]
           ,[Desc]
           ,[ForDisplay])
     VALUES
           (903
           ,'Expense Month Reopened'
           ,0
           ,'EXPENSE_MONTH_REOPENED'
           ,'Expense Month Reopened'
           ,1)
END

IF(NOT EXISTS (SELECT 1 FROM [system].[AlertClassificationType] WHERE ID = 904))
BEGIN
	INSERT INTO [system].[AlertClassificationType]
           ([ID]
           ,[Name]
           ,[Severity]
           ,[CodeAttribute]
           ,[Desc]
           ,[ForDisplay])
     VALUES
           (904
           ,'Expense Request Was Opened Retroactively'
           ,0
           ,'EXPENSE_REQUEST_WAS_OPENED_RETROACTIVELY'
           ,'Expense Request Was Opened Retroactively'
           ,1)
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'EXPENSE_REQUEST_WAS_OPENED_RETROACTIVELY')
BEGIN
	INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
	VALUES
			('Expense request was opened retroactively'
			,'Expense request was opened retroactively'
			,'Expense request was opened retroactively'
			,'Hi, please pay attention that expense request #{0} was reopened retroactively.'
			,0
			,1
			,NULL
			,'Expense request was opened retroactively'
			,'EXPENSE_REQUEST_WAS_OPENED_RETROACTIVELY'
			,'IDoBet')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'EXPENSE_MONTH_REOPENED')
BEGIN
	INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
	VALUES
			('Expense month reopened'
			,'Expense month reopened'
			,'Expense month reopened'
			,'Hi, please pay attention that expense month reopened ({0}).'
			,0
			,1
			,NULL
			,'Expense month reopened'
			,'EXPENSE_MONTH_REOPENED'
			,'IDoBet')
END



