
EXEC [maintenance].[spMergeDataScript]
	@TargetDb		 = '[Common]',
	@SourceDb		 = '[Common].',
	@IsProduction	 = 1,
	@IncConstErrorCodes	= 1;


EXEC [maintenance].[spMergeDataScript]
	@TargetDb		 = '[Common]',
	@SourceDb		 = '[Common].',
	@IsProduction	 = 1,
	@IncSysContents						= 0,
	@IncUserActivityType				= 0,
	@IncUserActivity					= 0,
	@IncFinTransactionReasons			= 0,
	@IncFinTransferRequestType			= 0,
	@IncSysChannels						= 0,
	@IncFinPaymentMethods				= 0,
	@IncConstCustomEnums				= 0,
	@IncConstErrorCodes					= 0,
	@IncSysAlertClassificationType		= 0,
	@IncDboNotificationTemplate			= 0,
	@IncSystemConfiguration				= 0,
	@IncConstProductType				= 0,
	@IncConstCommissionType				= 0,
	@IncFinPaymentMethodConfiguration	= 0;
