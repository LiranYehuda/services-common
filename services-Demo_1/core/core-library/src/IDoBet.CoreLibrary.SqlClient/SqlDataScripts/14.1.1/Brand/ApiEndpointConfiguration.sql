DECLARE @BrandId INT = ? 
EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = @BrandId,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'YellowbetPaymentDepositCallback',
		@Value = ?-- {B2B}/api/payments/Rave/Yellowbet/NotifyDYAReceive
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Yellowbet payment deposit(used for logging)'





