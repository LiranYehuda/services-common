IF NOT EXISTS(SELECT 1 FROM system.CancelReasonTypes WHERE ID = 6)
BEGIN
	SET IDENTITY_INSERT system.CancelReasonTypes ON
	
	insert system.CancelReasonTypes(ID,Name,[Desc])
	select 6,'Third Party Error',null
	
	SET IDENTITY_INSERT system.CancelReasonTypes OFF
END