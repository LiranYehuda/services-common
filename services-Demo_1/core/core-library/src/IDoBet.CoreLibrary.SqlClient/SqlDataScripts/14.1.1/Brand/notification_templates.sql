
IF NOT EXISTS (SELECT 1 FROM  [dbo].[NotificationTemplate]  WHERE CodeAttribute =  'EMAIL_FORGOT_PASSWORD')
BEGIN
INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
     VALUES
		('Email Forgot Password',
		 'Email Forgot Password template',
		 'Forgot Password',
		 'Your Authentication Code is {0}',
		 0,
		 1,
		 null,
		 'Forgot Password',
		 'EMAIL_FORGOT_PASSWORD',
		 'IDoBet')
END


IF NOT EXISTS (SELECT 1 FROM  [dbo].[NotificationTemplate]  WHERE CodeAttribute =  'USER_EMAIL_VERIFICATION_CODE')
BEGIN
INSERT INTO [dbo].[NotificationTemplate]
           ([Name]
           ,[Desc]
           ,[Title]
           ,[Body]
           ,[IsHtml]
           ,[NotificationMethod]
           ,[PlaceHolders]
           ,[Subject]
           ,[CodeAttribute]
           ,[From])
     VALUES
		('Email Verification Code',
		 'Email Verification Code Template',
		 'Email Verification Code',
		 'Your verification code is {0}',
		 0,
		 1,
		 null,
		 'Email Verification Code',
		 'USER_EMAIL_VERIFICATION_CODE',
		 'IDoBet')
END

