IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='GEOLOCATION_ACCESS_DENIED' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(129,'GEOLOCATION_ACCESS_DENIED', 'Geolocation access denied',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='IP_NOT_EXIST_AT_SUPPORT_SYSTEM' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(133,'IP_NOT_EXIST_AT_SUPPORT_SYSTEM', 'Ip not exist at support system',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='WRONG_SECURITY_ANSWER' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(134,'WRONG_SECURITY_ANSWER', 'Wrong security answer',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='TRANSACTION_NOT_FOUND' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(419,'TRANSACTION_NOT_FOUND', 'Transaction not found',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='EXTERNAL_TRANSACTION_ID_ALREADY_EXISTS' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(420,'EXTERNAL_TRANSACTION_ID_ALREADY_EXISTS', 'External transaction id already exists',0)
END

IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='EXTERAL_TRANSACTIONID_ALREADY_EXISTS' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(1018,'EXTERAL_TRANSACTIONID_ALREADY_EXISTS', 'Exteral transactionId already exists',0)
END


IF NOT EXISTS(SELECT 1 FROM common.const.ErrorCodes WHERE CodeAttribute = 'NO_USER_FOUND_FOR_PHONE')
BEGIN
	INSERT common.const.ErrorCodes(CodeNumber	,CodeAttribute	,[Name],	BrandId)
	VALUES(131,'NO_USER_FOUND_FOR_PHONE','No user found for phone',0)
END

IF NOT EXISTS(SELECT 1 FROM common.const.ErrorCodes WHERE CodeAttribute = 'MULTIPLE_USERS_FOUND_FOR_PHONE')
BEGIN
	INSERT common.const.ErrorCodes(CodeNumber	,CodeAttribute	,[Name],	BrandId)
	VALUES(132,'MULTIPLE_USERS_FOUND_FOR_PHONE','Multiple users found for phone',0)
END


IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='BRANCH_SHIFT_ALREADY_OPEN' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(115,'BRANCH_SHIFT_ALREADY_OPEN', 'Branch Shift Already Open',0)
END

