
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'LoginUser',
		@SpName = N'[idb].[spB2BLoginUser]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetOrders',
		@SpName = N'[idb].[spB2BGetOrders]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'ValidateUserPin',
		@SpName = N'[idb].[spB2BValidateUserPin]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetTransfer',
		@SpName = N'[idb].[spB2BGetTransfer]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserByPhone',
		@SpName = N'[idb].[spB2BGetUserByPhone]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUnpaidOrders',
		@SpName = N'[idb].[spB2BGetUnpaidOrders]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'LockOrder',
		@SpName = N'[idb].[spB2BLockOrder]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2


EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetKeepAlive',
		@SpName = N'[idb].[spGetKeepAlive]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SaveIDoPayTransaction',
		@SpName = N'[idb].[spB2BSaveIDoPayTransaction]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

		
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetIDoPayTransactions',
		@SpName = N'[idb].[spB2BGetIDoPayTransactions]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'DirectDeposit',
		@SpName = N'[idb].[spB2BDirectDeposit]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'Withdraw',
		@SpName = N'[idb].[spB2BWithdraw]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

		
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'DoPayout',
		@SpName = N'[idb].[spB2BDoPayout]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

/****** GS-10223*****/
DELETE FROM  [Common].system.ApiRouteConfiguration
where MethodName  in ('ForgotUserLoginDetails', 'ValidateAndResetPassword')

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'ForgotUserLoginDetails',
		@SpName = N'[idb].[spForgotUserLoginDetails]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'ValidateAndResetPassword',
		@SpName = N'[idb].[spValidateAndResetPassword]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0
/****** GS-10223*****/

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SendUserVerificationOTP',
		@SpName = N'[idb].[spSendUserVerificationOTP]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 1

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateTransaction',
		@SpName = N'[idb].[spB2BUpdateTransaction]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2
	


EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GrooveGetBetResponse',
		@SpName = N'[idb].[spGrooveGetBetResponse]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetPoolById',
		@SpName = N'[betting].[spGetPoolById]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 1

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'CreateUser',
		@SpName = N'[idb].[spB2BCreateUser]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserBonuses',
		@SpName = N'[idb].[spGetUserCampaigns]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 1

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'InsertThirdPartyPaymentMethodResult',
		@SpName = N'[idb].[spInsertThirdPartyPaymentMethodResult]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2


EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateThirdPartyTransfer',
		@SpName = N'[idb].[spUpdateThirdPartyTransfer]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2


/**** Lotto ***************************/
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLatestResults',
		@SpName = N'[dbo].[spGetLatestResults]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLotteryTypes',
		@SpName = N'[dbo].[spGetLotteryTypes]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLotteryLinePrice',
		@SpName = N'[dbo].[spGetLotteryLinePrice]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetNextDraws',
		@SpName = N'[dbo].[spGetNextDraws]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateUserBet',
		@SpName = N'[dbo].[spUpdateUserBet]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'ConfirmBet',
		@SpName = N'[dbo].[spConfirmBet]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserWinningLinesResult',
		@SpName = N'[dbo].[spGetUserWinningLinesResult]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'BOGetDrawsToUpdate',
		@SpName = N'[dbo].[spBOGetDrawsToUpdate]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLotteryTypes',
		@SpName = N'[dbo].[spGetLotteryTypes]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserTickets',
		@SpName = N'[dbo].[spGetUserTickets]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLotteryPrizes',
		@SpName = N'[dbo].[spGetLotteryPrizes]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateDrawResults',
		@SpName = N'[dbo].[spUpdateDrawResults]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'UpdateDrawJackpot',
		@SpName = N'[dbo].[spUpdateDrawJackpot]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetUserOrder',
		@SpName = N'[dbo].[spGetUserOrder]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetDrawWinningsResults',
		@SpName = N'[dbo].[spGetDrawWinningsResults]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'SettleBets',
		@SpName = N'[dbo].[spSettleBets]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetLatestResults',
		@SpName = N'[dbo].[spGetLatestResults]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0;

/******************** Lotto *****************************/

/**** hotfix yellowbet ****/
EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'DirectWithdraw',
		@SpName = N'[idb].[spB2BDirectWithdraw]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

EXEC	[Common].[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetTrasacions',
		@SpName = N'[idb].[spB2BGetTrasacions]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 2

/**** hotfix yellowbet ****/