EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'IpLocationUriService',
		@Value = 'https://api.ipstack.com/{0}?access_key=19fda1d0e3f4a1a947f01e6ee95feec9',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Ip location service'


EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'YellowbetPaymentDeposit',
		@Value = 'https://api.ydplatform.com/api/',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Yellowbet payment deposit(used for logging)'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'YellowbetPaymentWitdraw',
		@Value = 'https://api.ydplatform.com/api/',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Yellowbet payment witdraw(used for logging)'



EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'RavePaymentDeposit',
		@Value = 'https://api.ravepay.co/flwv3-pug/getpaidx/api/charge',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Rave payment deposit'


EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'RavePaymentWitdraw',
		@Value = 'https://api.ravepay.co/v2/gpx/transfers/create',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Rave payment witdraw'


EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'RavePaymentWitdraw',
		@Value = 'https://api.ravepay.co/v2/gpx/transfers/create',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Rave payment witdraw'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'RavePaymentVerifyTransaction',
		@Value = 'https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Rave payment verify transaction'


EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = 0,
		@ModuleId = 0,
		@Instance = NULL,
		@Key = N'RavePaymentFetchTransfer',
		@Value = 'https://api.ravepay.co/v2/gpx/transfers',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Rave payment fetch transfer'
		