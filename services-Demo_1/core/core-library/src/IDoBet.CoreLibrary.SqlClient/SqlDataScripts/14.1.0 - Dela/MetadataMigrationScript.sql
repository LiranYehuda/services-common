use [GSports_DevNew2];

declare @brandId as int = 10001
declare @providerSourceId as int = 1






INSERT INTO  [SportGames].[dbo].[Language] ([Code], [Name])
SELECT [Code], [Name]
FROM [dbo].[Language]
WHERE CODE NOT IN (SELECT [Code] FROM [SportGames].[dbo].[Language])


INSERT INTO [SportGames].[translation].[Countries]
           ([ID]
           ,[Name]
           ,[LanguageCode]
           ,[BrandId])
SELECT [ID]
      ,[Name]
      ,[LanguageCode]
      ,@brandId
FROM [translation].[Countries] CO
WHERE NOT EXISTS (SELECT [ID] FROM [SportGames].[translation].[Countries] WHERE [ID] = CO.ID AND [LanguageCode] = CO.LanguageCode AND BrandId = @brandId)



INSERT INTO [SportGames].[translation].[BetType]
           ([ID]
           ,[Name]
           ,[LanguageCode]
           ,[BrandId])
SELECT [ID]
      ,[Name]
      ,[LanguageCode]
      ,@brandId
FROM [translation].[BetType] BT
WHERE NOT EXISTS (SELECT [ID] FROM [SportGames].[translation].[BetType] WHERE [ID] = BT.ID AND [LanguageCode] = BT.LanguageCode AND BrandId = @brandId)


INSERT INTO [SportGames].[translation].[BetTypeOdds]
           ([Key]
           ,[Name]
           ,[LanguageCode]
           ,[BrandId])
SELECT [Key]
      ,[Name]
      ,[LanguageCode]
      ,@brandId
FROM [translation].[BetTypeOdds] BT
WHERE NOT EXISTS (SELECT [Key] FROM [SportGames].[translation].[BetTypeOdds] WHERE [Key] = BT.[Key] AND [LanguageCode] = BT.LanguageCode AND BrandId = @brandId)


INSERT INTO [SportGames].[translation].[SportType]
           ([ID]
           ,[Name]
           ,[LanguageCode]
		   ,[BrandId])
SELECT [ID]
      ,[Name]
      ,[LanguageCode]
	  ,@brandId
FROM [translation].[SportType] ST
WHERE NOT EXISTS (SELECT [ID] FROM [SportGames].[translation].[SportType] WHERE [ID] = ST.[ID] AND [LanguageCode] = ST.LanguageCode AND BrandId = @brandId)



INSERT INTO [SportGames].[translation].[Leagues]
           ([ID]
           ,[Name]
           ,[LanguageCode]
		   ,[BrandId])
SELECT [ID]
      ,[Name]
      ,[LanguageCode]
	  ,@brandId
FROM [translation].[Leagues] LT	
WHERE NOT EXISTS (SELECT [ID] FROM [SportGames].[translation].[Leagues] WHERE [ID] = LT.[ID] AND [LanguageCode] = LT.LanguageCode AND BrandId = @brandId)



--[SportType]
INSERT INTO [SportGames].[system].[SportType]
           ([Id]
           ,[ProviderId]
           ,[ProviderSourceId]
           ,[ProviderKey]
           ,[Name]
           ,[Order]
           ,[IsActive]       
           ,[PreMatchLimit]
           ,[LiveLimit]
           ,[IncreaseLimitEffect]) 
SELECT [ID]
      ,[ProviderId]
	  ,@providerSourceId
	  ,[ProviderKey]
      ,[Name]
	  ,[Order]
      ,[IsActive]      
      ,[PreMatchLimit]
      ,[LiveLimit]
      ,[IncreaseLimitEffect]
FROM [system].[SportType]
WHERE Id not in (Select Id from [SportGames].[system].[SportType])



MERGE [SportGames].[custom].[SportType] AS TARGET
USING [system].[SportType] AS SOURCE 
ON (TARGET.[SportTypeId] = SOURCE.[ID] AND TARGET.[BrandId] = @brandId) 
WHEN MATCHED THEN
UPDATE set TARGET.[Order] = SOURCE.[Order],
		   TARGET.[IsActive] = SOURCE.[IsActive],
		   TARGET.[PreMatchLimit] = SOURCE.[PreMatchLimit],
		   TARGET.[LiveLimit] = SOURCE.[LiveLimit],
		   TARGET.[IncreaseLimitEffect] = SOURCE.[IncreaseLimitEffect]	
WHEN NOT MATCHED BY TARGET AND (SOURCE.[Order] is not null OR SOURCE.[PreMatchLimit] is not null OR SOURCE.[LiveLimit] is not null OR SOURCE.[IncreaseLimitEffect] is not null)  THEN
INSERT     ([SportTypeId]
           ,[BrandId]
           ,[Order]
           ,[IsActive]          
           ,[PreMatchLimit]
           ,[LiveLimit]
           ,[IncreaseLimitEffect])
VALUES (SOURCE.[ID], @brandId, SOURCE.[Order], SOURCE.[IsActive], SOURCE.[PreMatchLimit], SOURCE.[LiveLimit], SOURCE.[IncreaseLimitEffect]);




--[BetType]
INSERT INTO [SportGames].[system].[BetType]
           ([Id]
           ,[ProviderId]
           ,[ProviderSourceId]
           ,[ProviderKey]         
           ,[Name]
           ,[SportTypeId]
           ,[WinningOddCount]
           ,[Lineable]
           ,[IsForPool]
           ,[IsLive]
           ,[Order]
           ,[IsActive]
           ,[ShortName]
           ,[LimitEffect]
           ,[LimitCapPreMatch]
           ,[LimitCapLive])
SELECT
[ID],
[ProviderId],
@providerSourceId,
[ProviderKey],
[Name],
[SportTypeId],
[WinningOddCount],
[Lineable],
[IsForPool],
[IsLive],
[Order],
[IsActive],
[ShortName],
[LimitEffect],
[LimitCapPreMatch],
[LimitCapLive]
FROM [system].[BetType]
WHERE Id not in (Select Id from [SportGames].[system].[BetType])


MERGE [SportGames].[custom].[BetType] AS TARGET
USING [system].[BetType] AS SOURCE 
ON (TARGET.BetTypeId = SOURCE.[ID] AND TARGET.[BrandId] = @brandId) 
WHEN MATCHED THEN
UPDATE set TARGET.[Order] = SOURCE.[Order],
		   TARGET.[IsActive] = SOURCE.[IsActive],
		   TARGET.LimitEffect = SOURCE.LimitEffect,
		   TARGET.LimitCapPreMatch = SOURCE.LimitCapPreMatch,
		   TARGET.LimitCapLive = SOURCE.LimitCapLive,
		   TARGET.[ShortName] = SOURCE.[ShortName]
WHEN NOT MATCHED BY TARGET AND (SOURCE.[Order] is not null OR SOURCE.LimitEffect is not null OR SOURCE.LimitCapPreMatch is not null OR SOURCE.LimitCapLive is not null)  THEN
INSERT 
           ([BetTypeId]
           ,[BrandId]
           ,[Order]
           ,[IsActive]         
		   ,[ShortName]
           ,[LimitEffect]		  
           ,[LimitCapPreMatch]
           ,[LimitCapLive])          
VALUES (SOURCE.[ID], @brandId, SOURCE.[Order], SOURCE.[IsActive], SOURCE.[ShortName], SOURCE.[LimitEffect], SOURCE.[LimitCapPreMatch], SOURCE.[LimitCapLive]);


--[BetTypeOdds]
INSERT INTO [SportGames].[system].[BetTypeOdds]
           ([ProviderSourceId]
           ,[ProviderKey]
           ,[Name]
           ,[BetTypeId]
           ,[Shortcut]
           ,[WinCondition]
           ,[RefundCondition]
           ,[Order]
           ,[FixPrice])
SELECT	@providerSourceId,
[ProviderKey],
[Name],
[BetTypeId],
[Shortcut],
[WinCondition],
[RefundCondition],
[Order],
[FixPrice]
FROM [system].[BetTypeOdds]
WHERE (CAST( BetTypeId as nvarchar(50)) + '_' + Name) not in (Select (CAST( BetTypeId as nvarchar(50)) + '_' + Name) from [SportGames].[system].[BetTypeOdds])


MERGE [SportGames].[custom].[BetTypeOdds] AS TARGET
USING [system].[BetTypeOdds] AS SOURCE 
ON (TARGET.BetTypeOddId = SOURCE.[ID] AND TARGET.[BrandId] = @brandId) 
WHEN MATCHED THEN
UPDATE set TARGET.[Order] = SOURCE.[Order],
		   TARGET.Shortcut = SOURCE.Shortcut,
		   TARGET.ShortName = SOURCE.ShortName,
		   TARGET.FixPrice = SOURCE.FixPrice
WHEN NOT MATCHED BY TARGET AND (SOURCE.[Order] is not null OR SOURCE.Shortcut is not null OR SOURCE.ShortName is not null OR SOURCE.FixPrice is not null)  THEN
INSERT
           ([BetTypeOddId]
           ,[BrandId]
           ,[BetTypeId]
           ,[Shortcut]
           ,[Order]
           ,[ShortName]
           ,[FixPrice])      
VALUES (SOURCE.[ID], @brandId, SOURCE.BetTypeId, SOURCE.Shortcut, SOURCE.[Order], SOURCE.ShortName, SOURCE.FixPrice);


--[Countries]
INSERT INTO [SportGames].[system].[Countries]
           ([Id]
           ,[ProviderId]
           ,[ProviderSourceId]
           ,[ProviderKey]
           ,[SportTypeId]
           ,[Name]
           ,[Order]
           ,[IsActive]
           ,[ShortName]
           ,[PreMatchLimit]
           ,[LiveLimit]
           ,[IncreaseLimitEffect])
SELECT	
[ID],
[ProviderId],
@providerSourceId,
[ProviderKey],
[SportTypeId],
[Name],
[Order],
[IsActive],
[ShortName],
[PreMatchLimit],
[LiveLimit],
[IncreaseLimitEffect]
FROM [system].[Countries]
WHERE Id not in (Select Id from [SportGames].[system].[Countries])

MERGE [SportGames].[custom].[Countries] AS TARGET
USING [system].[Countries] AS SOURCE 
ON (TARGET.[CountryId] = SOURCE.[ID] AND TARGET.[BrandId] = @brandId) 
WHEN MATCHED THEN
UPDATE set TARGET.[Order] = SOURCE.[Order],
		   TARGET.IsActive = SOURCE.IsActive,
		   TARGET.ShortName = SOURCE.ShortName,
		   TARGET.PreMatchLimit = SOURCE.PreMatchLimit,
		   TARGET.LiveLimit = SOURCE.LiveLimit,
		   TARGET.[IncreaseLimitEffect] = SOURCE.[IncreaseLimitEffect]		  
WHEN NOT MATCHED BY TARGET AND (SOURCE.[Order] is not null OR SOURCE.PreMatchLimit is not null OR SOURCE.ShortName is not null OR SOURCE.LiveLimit is not null  OR SOURCE.[IncreaseLimitEffect] is not null)  THEN
INSERT     ([CountryId]
           ,[BrandId]
           ,[SportTypeId]
           ,[Order]
           ,[IsActive]
           ,[ShortName]
           ,[PreMatchLimit]
           ,[LiveLimit]
           ,[IncreaseLimitEffect])           
VALUES (SOURCE.[ID], @brandId, SOURCE.SportTypeId, SOURCE.[Order], SOURCE.IsActive, SOURCE.ShortName, SOURCE.PreMatchLimit, SOURCE.LiveLimit, SOURCE.[IncreaseLimitEffect]);


--[Leagues]
INSERT INTO [SportGames].[system].[Leagues]
           ([Id]
           ,[ProviderId]
           ,[ProviderSourceId]
           ,[ProviderKey]
           ,[CountryId]         
           ,[Name]
           ,[Order]
           ,[IsActive]
           ,[ShortName]
           ,[PreMatchLimit]
           ,[LiveLimit]
           ,[IncreaseLimitEffect])
SELECT	
[ID],
[ProviderId],
@providerSourceId,
[ProviderKey],
[CountryId],
[Name],
[Order],
[IsActive],
[ShortName],
[PreMatchLimit],
[LiveLimit],
[IncreaseLimitEffect]
FROM [system].[Leagues]
WHERE Id not in (Select Id from [SportGames].[system].[Leagues])


MERGE [SportGames].[custom].Leagues AS TARGET
USING [system].Leagues AS SOURCE 
ON (TARGET.LeagueId = SOURCE.[ID] AND TARGET.[BrandId] = @brandId) 
WHEN MATCHED THEN
UPDATE set TARGET.[Order] = SOURCE.[Order],
		   TARGET.IsActive = SOURCE.IsActive,
		   TARGET.ShortName = SOURCE.ShortName,
		   TARGET.PreMatchLimit = SOURCE.PreMatchLimit,
		   TARGET.LiveLimit = SOURCE.LiveLimit,
		   TARGET.[IncreaseLimitEffect] = SOURCE.[IncreaseLimitEffect]		  
WHEN NOT MATCHED BY TARGET AND (SOURCE.[Order] is not null OR SOURCE.PreMatchLimit is not null OR SOURCE.ShortName is not null OR SOURCE.LiveLimit is not null  OR SOURCE.[IncreaseLimitEffect] is not null)  THEN
INSERT     ([LeagueId]
           ,[BrandId]
           ,[CountryId]
           ,[Order]
           ,[IsActive]
           ,[ShortName]
           ,[PreMatchLimit]
           ,[LiveLimit]
           ,[IncreaseLimitEffect])       
VALUES (SOURCE.[ID], @brandId, SOURCE.[CountryId], SOURCE.[Order], SOURCE.IsActive, SOURCE.ShortName, SOURCE.PreMatchLimit, SOURCE.LiveLimit, SOURCE.[IncreaseLimitEffect]);

--[Teams]
INSERT INTO [SportGames].[system].[Teams]
           ([Id]
           ,[ProviderId]
           ,[ProviderSourceId]
           ,[ProviderKey]
           ,[Name]
           ,[ShortName]
           ,[CreateTime])
SELECT [Id]
      ,[ProviderId]
      ,@providerSourceId
      ,[ProviderKey]
      ,[Name]
      ,[ShortName]
      ,[CreateTime]
FROM [system].[Teams]
WHERE Id not in (Select Id from [SportGames].[system].[Teams])


MERGE [SportGames].[custom].[Teams] AS TARGET
USING [system].[Teams] AS SOURCE 
ON (TARGET.TeamId = SOURCE.[ID] AND TARGET.[BrandId] = @brandId) 
WHEN MATCHED THEN
UPDATE set TARGET.ShortName = SOURCE.ShortName		 	  
WHEN NOT MATCHED BY TARGET AND (SOURCE.ShortName is not null)  THEN
INSERT      ([TeamId]
           ,[BrandId]
           ,[ShortName])      
VALUES (SOURCE.[ID], @brandId, SOURCE.ShortName);

INSERT INTO [SportGames].[system].[LeagueTeams]
           ([LeagueId]
           ,[TeamId])
SELECT [LeagueId], [TeamId] 
FROM [system].[LeagueTeams] LT
WHERE NOT EXISTS (SELECT [LeagueId] FROM [SportGames].[system].[LeagueTeams] WHERE [LeagueId] = LT.[LeagueId] AND [TeamId] = LT.[TeamId])

