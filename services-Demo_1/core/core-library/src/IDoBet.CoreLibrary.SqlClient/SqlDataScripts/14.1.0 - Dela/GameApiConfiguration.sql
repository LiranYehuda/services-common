﻿

-- INCOMPLETE

DECLARE @RC int
DECLARE @BrandId int = 10001
DECLARE @ModuleId int = 1
DECLARE @Instance nvarchar(255) = NULL;
DECLARE @Key nvarchar(255) = 'EventApiMode'
DECLARE @Value nvarchar(max) = '1'
DECLARE @Description nvarchar(max) = 'If EventApiMode=1 GameApi calls eventapi else it calls eventservice.';


EXECUTE @RC = [system].[spSetApiVariableConfiguration] 
   @BrandId
  ,@ModuleId
  ,@Instance
  ,@Key
  ,@Value
  ,@Description
GO
