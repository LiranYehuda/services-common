﻿-- INCOMPLETE

--SELECT * FROM [system].[ApiVariablesConfiguration]

DECLARE @ApiPrefix NVARCHAR(50) = 'EventApi'
DECLARE @InstancePrefix NVARCHAR(50) =  'DevLocal';

EXEC [system].[spSetApiConfiguration]
	@ApiPrefix		= @ApiPrefix,
	@InstancePrefix	= @InstancePrefix,
	@Key			= 'RedisAddressFeed',
	@Value			= 'localhost:6379',
	@BrandId		= 0;

EXEC [system].[spSetApiConfiguration]
	@ApiPrefix		= @ApiPrefix,
	@InstancePrefix	= @InstancePrefix,
	@Key			= 'RedisAddressBranded',
	@Value			= 'localhost:7379',
	@BrandId		= 0;

EXEC [system].[spSetApiConfiguration]
	@ApiPrefix		= @ApiPrefix,
	@InstancePrefix	= @InstancePrefix,
	@Key			= 'ConnectionString',
	@Value			= 'Data Source=mainsql;Initial Catalog=GSports_DevNew2;Persist Security Info=True;User ID=GSUSR;Password=123qwe!@#',
	@BrandId		= 0;
		
EXEC [system].[spSetApiConfiguration]
	@ApiPrefix		= @ApiPrefix,
	@InstancePrefix	= @InstancePrefix,
	@Key			= 'BrandIds',
	@Value			= '1,2',
	@BrandId		= 0;


