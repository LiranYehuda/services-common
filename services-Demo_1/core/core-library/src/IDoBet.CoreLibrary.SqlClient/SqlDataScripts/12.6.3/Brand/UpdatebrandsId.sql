declare @BrandId int = ? 

update system.Regions 
set BrandId = @BrandId

update branch.Branches
set BrandId = @BrandId