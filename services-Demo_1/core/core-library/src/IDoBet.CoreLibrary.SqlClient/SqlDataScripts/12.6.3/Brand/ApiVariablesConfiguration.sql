USE [Common]
GO




EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = ?,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'PrintServiceGetOrderURL',
		@Value = ?N'https://qa.idobet.com/Services/PrintService/Orders/GetOrder?q={orderKey}',
		@Description = 'Url for getting order from print service'

/**********GS-8236 sql script for setting up casino configurations********************/
EXEC	[system].[spSetApiVariableConfiguration]
		@BrandId = ?,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'CasinoImagePath',
		@Value = ?N'https://qa.idobet.com/Services/PrintService/Orders/GetOrder?q={orderKey}',
		@Description = 'Image path for casino categories and games'
/**********GS-8236 sql script for setting up casino configurations********************/


/******************************ThirdpartyConfigurations***********************************************/

EXEC	[system].[spSetApiVariableConfiguraiton]
		@BrandId = @BrandId,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = ?N'BettingThirdPartyProvider',
		@Value = ?N'1',
		@Description = N'Betting ThirdParty Provider is active'

EXEC	[system].[spSetApiVariableConfiguraiton]
		@BrandId = @BrandId,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = ?N'ThirdPartyOnlineUrl',
		@Value = -?-N'http://qa.idobet.com/online1/#/sports/sportsBetting',
		@Description = N'Betting ThirdParty Provider is active'
/******************************ThirdpartyConfigurations***********************************************/



