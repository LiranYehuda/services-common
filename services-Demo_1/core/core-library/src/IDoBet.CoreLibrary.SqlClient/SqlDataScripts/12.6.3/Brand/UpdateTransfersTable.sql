UPDATE T
SET	T.FromUserId	=	A.UserId,
	T.FromBranchId	=	U.BranchId
FROM [finance].[Transfers]		T
INNER JOIN [finance].[Accounts] A	ON	T.FromAccountId		=	A.ID
INNER JOIN [user].[Users]		U	ON	A.UserId			=	U.ID

UPDATE T
SET	T.ToUserId		=	A.UserId,
	T.ToBranchId	=	U.BranchId
FROM [finance].[Transfers]		T
INNER JOIN [finance].[Accounts] A	ON	T.ToAccountId		=	A.ID
INNER JOIN [user].[Users]		U	ON	A.UserId			=	U.ID

UPDATE T
SET	T.RequestUserId		=	A.UserId
FROM [finance].[Transfers]		T
INNER JOIN [user].[Access]		A	ON	T.RequestAccessId	=	A.ID

UPDATE T
SET	T.ResponseUserId	=	A.UserId
FROM [finance].[Transfers]		T
INNER JOIN [user].[Access]		A	ON	T.ResponseAccessId	=	A.ID


