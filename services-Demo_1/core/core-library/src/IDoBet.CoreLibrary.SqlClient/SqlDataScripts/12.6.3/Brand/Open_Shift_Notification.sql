DECLARE @SendFrom NVARCHAR(100)

SELECT TOP 1
	@SendFrom =  [From] 
FROM [dbo].[NotificationTemplate]

IF NOT EXISTS(SELECT * FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'OPEN_SHIFT_NOTIFICATION')
BEGIN
	INSERT [dbo].[NotificationTemplate] ( 
		[Name],
		[Desc], 
		[Title], 
		[Body], 
		[IsHtml], 
		[NotificationMethod], 
		[PlaceHolders], 
		[Subject], 
		[CodeAttribute], 
		[From]) 
	VALUES (
		N'Shifts still open', 
		N'Shifts still open.', 
		N'Shifts still open.', 
		'<table cellpadding="0" cellspacing="0" width="470" align="center"><tbody>'+
		'<tr><th align="center">Name</th><th align="center">UserIdBranch</th><th align="center">Manager</th><th align="center">OpenShiftTime</th></tr>'+
		'<tr class="dataRow"><td align="center">{Name}</td><td align="center">{UserIdBranch}</td><td align="center">{Manager}</td><td align="center">{OpenShiftTime}</td></tr>'+
		'<tr><td align="left" valign="top" width="600px" height="1" style=" background-color: #000000; border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; mso-line-height-rule: exactly; line-height: 1px;" colspan="4"></td></tr title="endrow">'+
		'</tbody></table>', 
		1,
		1, 
		NULL, 
		N'Shifts still open.', 
		N'OPEN_SHIFT_NOTIFICATION', 
		@SendFrom)
END

