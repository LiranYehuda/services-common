update [user].[users] 
set LimitEffect = 100
where LimitEffect is null or LimitEffect < 1

update	branch.branches  
set LimitEffect = 100
where LimitEffect is null or LimitEffect < 1

update system.BetType 
set LimitEffect = 100
where LimitEffect is null or LimitEffect < 1