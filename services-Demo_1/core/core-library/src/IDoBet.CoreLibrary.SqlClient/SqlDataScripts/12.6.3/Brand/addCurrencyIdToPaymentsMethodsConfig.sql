--Multiple currency support will need to handle manually!!!
DECLARE @DefaultCurrencyId	INT
SELECT  @DefaultCurrencyId = ID 
FROM [system].[Currencies]
WHERE IsDefault = 1

UPDATE [finance].[PaymentMethodConfiguration]
SET CurrencyId = @DefaultCurrencyId

INSERT INTO [finance].[PaymentMethodConfiguration]
           ([ChannelId]
           ,[PaymentMethodId]
           ,[IsDeposit]
           ,[Min]
           ,[Max]
           ,[ShowOrder]
           ,[ContentId]
           ,[SuccessContentId]
           ,[ButtonContentId]
           ,[CurrencyId])
SELECT [ChannelId]
      ,[PaymentMethodId]
      ,[IsDeposit]
      ,0 [Min]
      ,NULL [Max]
      ,[ShowOrder]
      ,[ContentId]
      ,[SuccessContentId]
      ,[ButtonContentId]
      ,CUR.ID [CurrencyId]
FROM [finance].[PaymentMethodConfiguration] PMC
CROSS JOIN [system].[Currencies] CUR 
WHERE CUR.IsDefault = 0



