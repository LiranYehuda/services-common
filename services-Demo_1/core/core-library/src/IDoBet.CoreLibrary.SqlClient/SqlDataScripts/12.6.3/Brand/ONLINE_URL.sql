IF NOT EXISTS(SELECT 1 FROM system.configuration WHERE CodeAttribute = 'ONLINE_URL')
BEGIN
	INSERT system.configuration(ID,	[Name],	[Value],	[Desc]	,CodeAttribute)
	VALUES(3,'OnlineUrl',?,'Online url address','ONLINE_URL')
END