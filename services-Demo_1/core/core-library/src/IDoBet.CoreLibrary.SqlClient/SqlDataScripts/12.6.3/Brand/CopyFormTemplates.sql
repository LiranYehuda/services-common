-- * all or FormTemplateValueId:ForDisplay (list)
DECLARE @FormTemplateValuesIdsToCopy NVARCHAR(MAX) = '*'	
DECLARE @DeleteFormTemplateFields BIT = 1
DECLARE @SourceDBName NVARCHAR(255) = '[GSports]'
DECLARE @DestinationDBName NVARCHAR(255) = '[10.84.84.120].?(DBNAME)'
DECLARE @query AS NVARCHAR(MAX) 

IF(@DeleteFormTemplateFields = 1)
		BEGIN			
			SET @query = 
			'DELETE '+@DestinationDBName+'.[system].[FormTemplateFields]
			INSERT INTO '+@DestinationDBName+'.[system].[FormTemplateFields]
				   ([Id]
				   ,[FieldType]
				   ,[FieldName]
				   ,[Text]
				   ,[DefaultValue]
				   ,[IsRequired]
				   ,[IsVisible]
				   ,[IsNullable]
				   ,[ShowOrder]
				   ,[ValidationRegex]
				   ,[ValidationError]
				   ,[FilterType]
				   ,[TableName])

			SELECT		[Id]
					   ,[FieldType]
					   ,[FieldName]
					   ,[Text]
					   ,[DefaultValue]
					   ,[IsRequired]
					   ,[IsVisible]
					   ,[IsNullable]
					   ,[ShowOrder]
					   ,[ValidationRegex]
					   ,[ValidationError]
					   ,[FilterType]
					   ,[TableName]
			FROM	'+@SourceDBName+'.[system].[FormTemplateFields]';	
			EXECUTE sp_executesql @query
		END		

SET @query = ''
DECLARE @FormTemplateValueSetting NVARCHAR(100) 
DECLARE @FormTemplateValueId NVARCHAR(10)
DECLARE @ForDisplay NVARCHAR(1)

DECLARE  @FormTemplateValues TABLE
(
	[Value] VARCHAR(50) 
)

IF (@FormTemplateValuesIdsToCopy = '*')
	BEGIN
		DECLARE @SQL NVARCHAR(MAX) = 'SELECT CAST(Id AS VARCHAR(10)) + '':1'' FROM '+@SourceDBName+'.[system].[FormTemplateValues]';
		INSERT INTO @FormTemplateValues ([Value])
		EXECUTE sp_executesql  @SQL
	END
ELSE
	BEGIN
		INSERT INTO @FormTemplateValues ([Value])
		SELECT TRY_CAST(value as nvarchar(100)) FROM STRING_SPLIT (@FormTemplateValuesIdsToCopy, ',')	
	END


DECLARE fv_cursor CURSOR FOR 
SELECT * FROM @FormTemplateValues
OPEN fv_cursor
FETCH NEXT FROM fv_cursor  
INTO @FormTemplateValueSetting
WHILE @@FETCH_STATUS = 0  
BEGIN		
		PRINT @FormTemplateValueSetting
		 SELECT @FormTemplateValueId = SUBSTRING(@FormTemplateValueSetting,1,CHARINDEX(':',@FormTemplateValueSetting)-1) 
         SELECT @ForDisplay = SUBSTRING(@FormTemplateValueSetting,CHARINDEX(':',@FormTemplateValueSetting)+1,LEN(@FormTemplateValueSetting))  

	
		 SET @query =	'IF EXISTS (SELECT ID FROM '+@DestinationDBName+'.[system].[FormTemplateValues] WHERE [Id] = '+@FormTemplateValueId+')
						BEGIN 
								DELETE FROM '+@DestinationDBName+'.[system].[FormTemplateValues] WHERE [Id] = '+@FormTemplateValueId +'
								DELETE FROM '+@DestinationDBName+'.[system].[FormTemplateFieldValues] WHERE [FormTemplateValueId] = '+@FormTemplateValueId+'
						END 	

						IF NOT EXISTS (SELECT  [Id] FROM '+@DestinationDBName+'.[system].[FormTemplates] WHERE Id = (SELECT [FormTemplateId] FROM '+@SourceDBName+'.[system].[FormTemplateValues] WHERE [Id] = '+@FormTemplateValueId+'))
						BEGIN

							INSERT INTO '+@DestinationDBName+'.[system].[FormTemplates]
									   ([Id]
									   ,[Title]
									   ,[Description])
							SELECT [Id]
								  ,[Title]
								  ,[Description]
							FROM '+@SourceDBName+'.[system].[FormTemplates]
							WHERE Id = (SELECT [FormTemplateId] FROM '+@SourceDBName+'.[system].[FormTemplateValues] WHERE [Id] = '+@FormTemplateValueId+')
	 
						END

						INSERT INTO '+@DestinationDBName+'.[system].[FormTemplateValues]
								   ([Id]
								   ,[FormTemplateId]
								   ,[Title]
								   ,[Description]
								   ,[AllowUpdate]
								   ,[AllowCreate]
								   ,[TableName]
								   ,[Paging]
								   ,[ShowOrder]
								   ,[ForDisplay]
								   ,[AllowDelete]
								   ,[DefaultWhere]
								   ,[DefaultOrderBy])
						SELECT 
									[Id]
								   ,[FormTemplateId]
								   ,[Title]
								   ,[Description]
								   ,[AllowUpdate]
								   ,[AllowCreate]
								   ,[TableName]
								   ,[Paging]
								   ,[ShowOrder]
								   ,'+@ForDisplay+'
								   ,[AllowDelete]
								   ,[DefaultWhere]
								   ,[DefaultOrderBy]
						FROM '+@SourceDBName+'.[system].[FormTemplateValues]
						WHERE ID = '+@FormTemplateValueId+'


						INSERT INTO '+@DestinationDBName+'.[system].[FormTemplateFieldValues]
								   ([FormTemplateValueId]
								   ,[FormTemplateFieldId]
								   ,[Text]
								   ,[DefaultValue]
								   ,[IsRequired]
								   ,[IsVisible]
								   ,[IsReadonly]
								   ,[ValidationRegex]
								   ,[ValidationError]
								   ,[ShowOrder])

						 SELECT
									[FormTemplateValueId]
								   ,[FormTemplateFieldId]
								   ,[Text]
								   ,[DefaultValue]
								   ,[IsRequired]
								   ,[IsVisible]
								   ,[IsReadonly]
								   ,[ValidationRegex]
								   ,[ValidationError]
								   ,[ShowOrder]
						FROM '+@SourceDBName+'.[system].[FormTemplateFieldValues]
						WHERE [FormTemplateValueId] = ' + @FormTemplateValueId;		
				EXECUTE sp_executesql @query

	FETCH NEXT FROM fv_cursor
	INTO @FormTemplateValueSetting
END  
CLOSE fv_cursor;  
DEALLOCATE fv_cursor;


