EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'WakaGetTransactionRecordsByIds',
		@SpName = N'custom.spWakaGetTransactionRecordsByIds',
		@BrandId = 10,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'WakaGetTransactionRecordsChangedStates',
		@SpName = N'custom.spWakaGetTransactionRecordsChangedStates',
		@BrandId = 10,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'WakaGetBetTransactionRecords',
		@SpName = N'custom.spWakaGetBetTransactionRecords',
		@BrandId = 10,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'WakaSetBetTransactionRecords',
		@SpName = N'custom.spWakaSetBetTransactionRecords',
		@BrandId = 10,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0
 
EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'ThirdPartySetAndLoginUser',
		@SpName = N'[IDB].[spThirdPartySetAndLoginUser]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0