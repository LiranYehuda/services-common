INSERT INTO [system].[Channels] ([Id], [CodeAttribute])
SELECT [Id], [CodeAttribute]
FROM
(
	SELECT 1 AS [Id], 'Online'		AS [CodeAttribute]
	UNION
	SELECT 2 AS [Id], 'CashBox'		AS [CodeAttribute]
	UNION
	SELECT 3 AS [Id], 'Pos'			AS [CodeAttribute]
	UNION
	SELECT 4 AS [Id], 'Mobile'		AS [CodeAttribute]
	UNION
	SELECT 5 AS [Id], 'Backoffice'	AS [CodeAttribute]
)A
WHERE A.Id NOT IN (SELECT Id FROM [system].[Channels])
 