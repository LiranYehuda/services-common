IF NOT EXISTS(SELECT 1 FROM [system].[AlertClassificationType]  WHERE CodeAttribute = 'OPEN_SHIFT_NOTIFICATION')
BEGIN
	INSERT INTO [system].[AlertClassificationType]
			   ([ID]
			   ,[Name]
			   ,[Severity]
			   ,[CodeAttribute]
			   ,[Desc])
		 VALUES
			   (801
			   ,'Shifts still open'
			   ,0
			   ,'OPEN_SHIFT_NOTIFICATION'
			   ,'Shifts still open')
END



