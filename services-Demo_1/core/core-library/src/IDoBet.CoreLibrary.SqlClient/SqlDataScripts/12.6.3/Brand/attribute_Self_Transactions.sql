IF NOT EXISTS(SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'SELF_APPROVE_COLLECT_MONEY')
BEGIN
	INSERT INTO [user].[Activity]
	([Name]
	,[ActivityTypeId]
	,[CodeAttribute])
	VALUES
	('Self approve collect money'
	,700
	,'SELF_APPROVE_COLLECT_MONEY')
END

IF NOT EXISTS(SELECT 1 FROM [user].[Activity] WHERE [CodeAttribute] = 'SELF_APPROVE_SEND_MONEY')
BEGIN
	INSERT INTO [user].[Activity]
	([Name]
	,[ActivityTypeId]
	,[CodeAttribute])
	VALUES
	('Self approve send money'
	,700
	,'SELF_APPROVE_SEND_MONEY')
END