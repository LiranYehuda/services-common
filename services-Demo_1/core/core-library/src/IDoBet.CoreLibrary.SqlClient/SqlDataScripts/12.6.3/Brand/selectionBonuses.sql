

ALTER VIEW [const].[v_AllEnums]
AS
/* ALL*/ SELECT CE.EnumType[EnumType], CE.[Name] [Name], CE.[Value] [Value], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [const].[CustomEnums] CE
UNION
SELECT        'eWinStatus' [EnumType], WS.[Name] [Name], WS.[ID] [Value], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [const].[WinStatus] WS
UNION
SELECT        'eBranch' [EnumType], BR.[Name] + ' (' + CAST(BR.[ID] AS varchar(max)) + ')' [Name], BR.[Id] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [branch].[Branches] BR
WHERE        BR.IsDeleted = 0
UNION
SELECT        'eProductType' [EnumType], PT.[Name] [Name], PT.[Id] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [const].[ProductType] PT
UNION
SELECT        'eCurrency' [EnumType], CR.[CurrencyCode] [Name], CR.ID[Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [system].[Currencies] CR
WHERE        [IsValid] = 1
UNION
SELECT        'eRegion' [EnumType], RG.[Name] [Name], RG.ID[Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [system].Regions RG
UNION
SELECT        'eRoles' [EnumType], ROL.[Name] [Name], ROL.ID[Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [const].Roles ROL
UNION
SELECT        'ePoolBetType' [EnumType], ISNULL(PBT.[ShortName], PBT.[Name]) + ' (' + CAST(PBT.[ID] AS varchar(max)) + ')', PBT.[ID] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [system].[BetType] PBT
WHERE        [IsForPool] = 1 AND Lineable = 0
UNION
SELECT        'ePoolBetType' [EnumType], ISNULL(PBT.[ShortName], PBT.[Name]) + ' (' + CAST(PBT.[ID] AS varchar(max)) + '):Line', PBT.[ID] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [system].[BetType] PBT
WHERE        [IsForPool] = 1 AND Lineable = 1
UNION
SELECT        'ePoolTemplate' [EnumType], PT.[Name], PT.[ID] [Id], ROW_NUMBER() OVER (ORDER BY ID DESC) ROW_NUM, NULL AS Channel
FROM            [betting].[PoolTemplates] PT
WHERE        PT.[IsActive] = 1
UNION
SELECT        'eCommissionType' [EnumType], CT.[Name], CT.[ID] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [const].[CommissionType] CT
UNION
SELECT        'eOrderStatus' [EnumType], OS.[Name], OS.[ID] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [const].[OrderStatus] OS
UNION
SELECT        'eEventStatus' [EnumType], ES.[Name], ES.[ID] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, NULL AS Channel
FROM            [const].[EventStatus] ES
UNION
SELECT        'eTransactionReason' [EnumType], TR.[Name], TR.[ID] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, 1 AS Channel
FROM            [finance].[TransactionReasons] TR
WHERE        TR.CodeAttribute IN ('ONLINE_SALE_TICKET', 'ONLINE_PAYOUT_TICKET', 'DEPOSIT', 'WITHDRAW')
UNION
SELECT        'eWinStatus' [EnumType], WS.[Name] [Name], WS.[ID] [Value], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, 1 AS Channel
FROM            [const].[WinStatus] WS
WHERE        WS.CodeAttribute IN ('PENDING', 'LOSER', 'WINNER')
UNION
SELECT        'eProductType' [EnumType], PT.[Name] [Name], PT.[Id] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, 1 AS Channel
FROM            [const].[ProductType] PT
union
select			 'eProductTypeSelectionBonus' [EnumType], PT.[Name] [Name], PT.[Id] [Id], ROW_NUMBER() OVER (ORDER BY ID ASC) ROW_NUM, null AS Channel
FROM            [const].[ProductType] PT
				where PT.ID in (1,2,3)
GO







declare @newId as int 
declare @order as int = 309
declare @registartionFormTemplateValueId as int = 22
declare @depositFormTemplateValueId as int = 17
declare @selectionFormTemplateValueId as int = 15
declare @happyHourFormTemplateValueId as int = 16
declare @shortBonusDescFieldId as int = 104
  insert into [GSports_Dev].[system].FormTemplateFields (FieldType, FieldName, Text, DefaultValue, IsRequired, IsVisible, IsNullable, ShowOrder, ValidationRegex, ValidationError,FilterType,  TableName)
  values ('Enum:eProductTypeSelectionBonus','ProductType', 'Product Type', '', 1,1,1,@order,null,null,'Column', '[system].[Campaigns]')
  
  set @newId = @@IDENTITY
  
  insert into [GSports_Dev].[system].FormTemplateFieldValues (FormTemplateValueId, FormTemplateFieldId,Text,  DefaultValue, IsRequired, IsVisible, IsReadonly, ValidationRegex, ValidationError, ShowOrder)
  values (@selectionFormTemplateValueId, @newId, 'Product Type', '', 1, 1, 0, null, null,@order)



  --ADD ALSO RECORDS FOR DEPOSIT & REGISTRATION WITH ISVISIBLE = 0

   insert into [GSports_Dev].[system].FormTemplateFieldValues (FormTemplateValueId, FormTemplateFieldId,Text,  DefaultValue, IsRequired, IsVisible, IsReadonly, ValidationRegex, ValidationError, ShowOrder)
  values (@registartionFormTemplateValueId, @newId, 'Product Type', '', 0, 0, 0, null, null,@order)

   insert into [GSports_Dev].[system].FormTemplateFieldValues (FormTemplateValueId, FormTemplateFieldId,Text,  DefaultValue, IsRequired, IsVisible, IsReadonly, ValidationRegex, ValidationError, ShowOrder)
  values (@depositFormTemplateValueId, @newId, 'Product Type', '', 0, 0, 0, null, null,@order)

    GO

INSERT INTO [system].[Campaigns]
           ([FormTemplateValueId]
           ,[RegionId]
           ,[BranchId]
           ,[Name]
           ,[Code]
           ,[ClaimAvailable]
           ,[ValidFrom]
           ,[ValidTo]
           ,[RecurringDays]
           ,[ActiveHour]
           ,[MaxDaysToWin]
           ,[InitialWinStatus]
           ,[IsBettable]
           ,[WithdrawAllowed]
           ,[IsImmediate]
           ,[IsRefund]
           ,[ExclusiveGroup]
           ,[Precedence]
           ,[InitialBonus]
           ,[MaxBonus]
           ,[BonusPercent]
           ,[RolloverCount]
           ,[BetPercentToTurnover]
           ,[MinCombo]
           ,[MinOddPerBet]
           ,[MinOddPerRow]
           ,[MinStake]
           ,[DepositAmount]
           ,[DepositType]
           ,[PersistenceDays]
           ,[PersistenceBetAmount]
           ,[Description]
           ,[IsDeleted]
           ,[ShortDescription]
           ,[IsClaim]
           ,[CurrencyId]
           ,[ActiveHourFrom]
           ,[ActiveHourTo]
           ,[ProductType])
select 
           FormTemplateValueId,
           RegionId,
           BranchId,
           Name,
           Code,
           ClaimAvailable,
           ValidFrom, 
           ValidTo,
           RecurringDays,
           ActiveHour, 
           MaxDaysToWin,
           InitialWinStatus,
           IsBettable,
           WithdrawAllowed, 
           IsImmediate,
           IsRefund,
           ExclusiveGroup,
           Precedence,
           InitialBonus,
           MaxBonus, 
           BonusPercent, 
           RolloverCount,
           BetPercentToTurnover, 
           MinCombo,
           MinOddPerBet, 
           MinOddPerRow,
           MinStake,
           DepositAmount, 
           DepositType,
           PersistenceDays, 
           PersistenceBetAmount,
           Description,
           IsDeleted,
           ShortDescription, 
           IsClaim,
           CurrencyId,
           ActiveHourFrom, 
           ActiveHourTo,
           2/*live*/ 
from  [system].[Campaigns]

update  [system].[Campaigns]
set ProductType = 1/*Prematch*/
where ProductType is null 


update [system].[FormTemplateFieldValues]
set IsVisible = 1,
	Text = 'Bonus Description',
	DefaultValue = '%',
	IsRequired = 0
where FormTemplateValueId in (@selectionFormTemplateValueId,@happyHourFormTemplateValueId) and FormTemplateFieldId = @shortBonusDescFieldId

