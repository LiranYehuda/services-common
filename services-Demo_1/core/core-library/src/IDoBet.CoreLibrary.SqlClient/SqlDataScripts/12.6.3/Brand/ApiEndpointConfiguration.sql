﻿USE [Common]
GO

DECLARE @GameApiPath = ? --example  https://dev.idobet.com/services/GameAPI/api

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'GrooveDataProviderUrl',
		@Value = @GameApiPath+'/dynamic/execute',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Groove data provider endpoint'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 1,
		@Instance = NULL,
		@Key = N'GrooveBetProviderUrl',
		@Value = @GameApiPath+'/Bet/',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Groove bet provider endpoint'

/******************************ThirdpartyConfigurations***********************************************/

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'BettingThirdPartyCheckAccount',
		@Value =? N'https://dev2v.groovegaming.com/groove/provider/IDoBet/CheckAccount',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Third Party Provider EndPoint Integration CheckAccount endpoint'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'BettingThirdPartyCheckBalance',
		@Value = ?N'https://dev2v.groovegaming.com/groove/provider/IDoBet/CheckBalance',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Third Party Provider EndPoint Integration CheckBalance endpoint'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = NULL,
		@Instance = NULL,
		@Key = N'BettingThirdPartyPlaceBet',
		@Value = ?N'https://dev2v.groovegaming.com/groove/provider/IDoBet/PlaceBet',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Third Party Provider EndPoint Integration PlaceBet endpoint'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'BettingThirdPartySettleBet',
		@Value = ?N'https://dev2v.groovegaming.com/groove/provider/IDoBet/SettleBet',,
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Third Party Provider EndPoint Integration SettleBet endpoint'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'BettingThirdPartyRollbackBet',
		@Value = ?N'https://dev2v.groovegaming.com/groove/provider/IDoBet/RollbackBet',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Third Party Provider EndPoint Integration RollbackBet endpoint'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'BettingThirdPartyReversePayment',
		@Value = ?N'https://dev2v.groovegaming.com/groove/provider/IDoBet/ReversePayment',
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Third Party Provider EndPoint Integration RollbackBet endpoint'

EXEC	[system].[spSetApiEndpointConfiguration]
		@BrandId = ?,
		@ModuleId = 9,
		@Instance = NULL,
		@Key = N'GameApi',
		@Value = ?--N'https://qa.idobet.com/services/gameapi',		
		@Authentication = NULL,
		@AdditionalData = NULL,
		@Description = N'Game api end point used by External Api'

/******************************ThirdpartyConfigurations***********************************************/
