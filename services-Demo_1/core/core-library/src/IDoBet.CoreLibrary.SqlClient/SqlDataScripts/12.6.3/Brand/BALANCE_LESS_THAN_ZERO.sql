DECLARE @SendFrom NVARCHAR(100)

SELECT TOP 1
	@SendFrom =  [From] 
FROM [dbo].[NotificationTemplate]

IF NOT EXISTS(SELECT * FROM [dbo].[NotificationTemplate] WHERE CodeAttribute = 'BALANCE_IS_NEGATIVE')
BEGIN
	INSERT [dbo].[NotificationTemplate] ( 
		[Name],
		[Desc], 
		[Title], 
		[Body], 
		[IsHtml], 
		[NotificationMethod], 
		[PlaceHolders], 
		[Subject], 
		[CodeAttribute], 
		[From]) 
	VALUES (
		N'Balance is Negative', 
		N'Balance is Negative.', 
		N'User Balance is Negative.', 
		'<table cellpadding="0" cellspacing="0" width="320" align="center"><tbody>'+
		'<tr><th align="center">User Name</th><th align="center">User Id</th><th align="center">Balance</th></tr>'+
		'<tr class="dataRow"><td align="center">{userName}</td><td align="center">{userId}</td><td align="center">{balance}</td></tr>'+
		'</tbody></table>', 
		1,
		1, 
		NULL, 
		N'Balance is Negative.', 
		N'BALANCE_IS_NEGATIVE', 
		@SendFrom)
END


UPDATE [system].[AlertClassificationType]
SET CodeAttribute = 'BALANCE_IS_NEGATIVE',[Name] = 'Balance Is Negative',[Desc] = 'Balance Is Negative'
WHERE CodeAttribute = 'BALANCE_LESS_THAN_ZERO'