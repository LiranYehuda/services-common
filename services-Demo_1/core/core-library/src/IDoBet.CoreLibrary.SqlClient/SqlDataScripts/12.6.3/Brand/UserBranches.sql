;WITH UserBranches AS
(
	SELECT UserId, COUNT(UserId) AS BranchCount, MAX(BranchId) as BranchId
	FROM [user].[BranchUsers]
	GROUP BY UserId
)

UPDATE USR
SET USR.BranchId = (CASE WHEN BRN.UserId IS NULL OR BRN.BranchCount > 1 THEN NULL ELSE BRN.BranchId END)
FROM [user].[Users] USR
LEFT JOIN UserBranches BRN ON USR.ID = BRN.UserId
GO

/*
INSERT INTO [user].[AllowBranches]
           ([CreateTime]
           ,[UserId]
           ,[BranchId])
SELECT GETDATE(),
	   [UserId],
	   [BranchId]
FROM [user].[BranchUsers]
*/
INSERT INTO [user].[AllowBranches]
           ([CreateTime]
           ,[UserId]
           ,[BranchId])
SELECT GETDATE(),
	   BU.[UserId],
	   BU.[BranchId]
FROM [user].[BranchUsers] BU
	LEFT OUTER JOIN [user].[AllowBranches] AW ON 
	BU.UserId = AW.UserId
	AND BU.BranchId= AW.BranchId
WHERE AW.BranchId IS NULL
