EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'PlaceBetVirtual',
		@SpName = N'[betting].[spPlaceBetVirtual]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'SettleBasicOrder',
		@SpName = N'[betting].[spSettleBasicOrder]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'LoginUser',
		@SpName = N'[idb].[spLoginUser]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetOrderForCancel',
		@SpName = N'[betting].[spGetOrderForCancel]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0


EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'GetOrderForCancel',
		@SpName = N'[idb].[spGetOrderForCancel]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'CancelTicket',
		@SpName = N'[dbo].[spCancelTicket]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	[system].[spSetApiRouteConfiguration]
		@MethodName = N'CancelOrder',
		@SpName = N'[idb].[spCancelOrder]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'ValidateOTP',
		@SpName = N'[user].[spValidateOTP]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'GetOTP',
		@SpName = N'[user].[spGetOTP]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'SaveCasinoGame',
		@SpName = N'[idb].[spSaveCasinoGame]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3


/**********GS-8236 sql script for setting up casino configurations********************/

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'GetCasinoMenu',
		@SpName = N'[idb].[spGetCasinoMenu]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0


EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'GetCasinoGames',
		@SpName = N'[idb].[spGetCasinoGames]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 0

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'SaveCasinoGame',
		@SpName = N'[idb].[spSaveCasinoGame]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'GetCasinoGategories',
		@SpName = N'[idb].[spGetCasinoGategories]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3

EXEC	@return_value = [system].[spSetApiRouteConfiguration]
		@MethodName = N'SaveCasinoCategory',
		@SpName = N'[idb].[spSaveCasinoCategory]',
		@BrandId = 0,
		@IsActive = 1,
		@CacheTimeout = 0,
		@ApiId = 3
/**********GS-8236 sql script for setting up casino configurations********************/
