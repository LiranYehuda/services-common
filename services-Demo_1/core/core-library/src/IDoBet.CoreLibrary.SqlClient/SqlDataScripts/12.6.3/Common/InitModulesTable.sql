﻿SET NOCOUNT ON;
IF NOT EXISTS (SELECT TOP 1 Id FROM [system].[Modules])
	INSERT INTO [system].[Modules] (Id, [Name]) VALUES
	(0, 'Core'),
	(1, 'Game'),
	(2, 'Lotto'),
	(3, 'SportArchive'),
	(4, 'SportLimit'),
	(5, 'SportMetadata'),
	(6, 'SportMargin'),
	(7, 'SportRepository'),
	(8, 'ExternalApi'),
	(9, 'ThirdParty');
