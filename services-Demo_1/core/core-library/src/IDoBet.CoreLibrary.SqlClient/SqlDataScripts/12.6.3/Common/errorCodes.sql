IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='POOL_ALREADY_CONTAIN_BET' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(282,'POOL_ALREADY_CONTAIN_BET', 'Events pool can not change - pool already contain bets',0)
END


/********************AddTransferNotFoundError*****************/
IF NOT EXISTS(SELECT 1 FROM [Common].[const].[ErrorCodes] WHERE [CodeAttribute] ='TransferNotFound' AND [BrandId] = 0)
BEGIN
	INSERT INTO [Common].[const].[ErrorCodes]
	           ([CodeNumber]
	           ,[CodeAttribute]
	           ,[Name]
	           ,[BrandId])
	VALUES	(416,'TransferNotFound', 'Transfer Not Found',0)
END

/********************AddTransferNotFoundError*****************/