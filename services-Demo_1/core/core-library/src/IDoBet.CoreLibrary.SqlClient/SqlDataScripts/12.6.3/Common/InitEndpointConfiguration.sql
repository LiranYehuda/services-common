﻿SET NOCOUNT ON;

IF NOT EXISTS (SELECT TOP 1 Id FROM [system].[ApiEndpointConfiguration])
BEGIN
	DECLARE @CoreModule INT = 0;

	INSERT INTO 
		[system].[ApiEndpointConfiguration]
		(
			BrandId,
			ModuleId,
			[Key],
			[Value], 
			[Description]
		)
		SELECT
			BR.ID,
			@CoreModule,
			'Lotto',
			BR.LottoDBConnection,
			'Lotto db'
		FROM
			[dbo].[Brands] BR
		WHERE
			BR.LottoDBConnection IS NOT NULL
		UNION
		SELECT
			BR.ID,
			@CoreModule,
			'Sport',
			BR.SportDBConnection,
			'Sport db'
		FROM
			[dbo].[Brands] BR
		WHERE
			BR.SportDBConnection IS NOT NULL

	UPDATE [system].[ApiVariablesConfiguration] 
	SET ModuleId = @CoreModule;

END;
