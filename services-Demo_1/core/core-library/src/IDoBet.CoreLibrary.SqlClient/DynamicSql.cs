﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using IDoBet.CoreLibrary.Logic.Model.Configuration;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Request;
using IDoBet.CoreLibrary.Logic.Model.Response;

namespace IDoBet.CoreLibrary.SqlClient
{
    public static class DynamicSql
    {
        public static readonly string DefaultSchemaName = "dbo";

        private struct ParameterInfo
        {
            public string Name { get; set; }
            public SqlDbType DataType { get; set; }
        }
        public static (T result, DataSet resultData) Execute<T>(ActionConfiguration configuration, Dictionary<string, string> values, BaseRequest request = null) where T : BaseResponse, new()
        {
            if (configuration == null)
                throw new Exception("Not routed.");
            //if (configuration?.KeyType != eKeyType.Database)
            //    throw new Exception("Invalid route configuration.");

            var commandParts = configuration.Route.SpName.Split(new char[] { '.' });
            var schemaName = commandParts[commandParts.Length - 2].Replace("[", "").Replace("]", "");
            if (string.IsNullOrWhiteSpace(schemaName))
                schemaName = DefaultSchemaName;                     
            var procedureName = commandParts[commandParts.Length - 1].Replace("[", "").Replace("]", "");
            //if (Regex.Match(schemaName, "[a-zA-Z0-9_]{1,116}").Value != schemaName)
            //    throw new ArgumentException(schemaName);
            //if (Regex.Match(procedureName, "[a-zA-Z0-9_]{1,116}").Value != procedureName)
            //    throw new ArgumentException(procedureName);
            List<ParameterInfo> parameters = (List<ParameterInfo>)configuration.Route.Parameters;
            if (configuration.Route.Parameters == null)
            {
                parameters = new List<ParameterInfo>();
                using (var sh = new SqlHandler($"{configuration.Endpoint.Value};ApplicationIntent=ReadOnly", "system.spGetStoredProcedure"))
                {
                    sh.AddParameter("Schema", SqlDbType.NVarChar, schemaName);
                    sh.AddParameter("ProcedureName", SqlDbType.NVarChar, procedureName);
                    while (sh.Read())
                        parameters.Add(new ParameterInfo
                        {
                            Name = sh.Get<string>("PARAMETER_NAME").Substring(1),
                            DataType = GetSqlType(sh.Get<string>("DATA_TYPE"))
                        });

                    if (configuration.Route.Parameters == null)
                    {
                        configuration.Route.Parameters = parameters;
                    }
                }
            }            

            values = new Dictionary<string, string>(values, StringComparer.OrdinalIgnoreCase);
            using (var sh = new SqlHandler($"{configuration.Endpoint.Value}{(configuration.Route.IsReadOnly? ";ApplicationIntent=ReadOnly" :"")}", configuration.Route.SpName))
            {
                var requestValid = true;
                var parseError = false;
                var errors = new StringBuilder();
                var parseErrorParameters = new List<string>();
                foreach (var p in parameters)
                {
                    if (!values.ContainsKey(p.Name))
                    {
                        continue;
                    }
                    else if (values[p.Name] == null)
                        sh.AddParameter<string>(p.Name, p.DataType, null);
                    else
                    {
                        var parameterCount = sh.ParameterCount;
                        switch (p.DataType)
                        {
                            case SqlDbType.Char:
                            case SqlDbType.NText:
                            case SqlDbType.NChar:
                            case SqlDbType.NVarChar:
                            case SqlDbType.VarChar:
                            case SqlDbType.Xml:
                                sh.AddParameter(p.Name, p.DataType, values[p.Name]);
                                break;
                            case SqlDbType.Bit:
                                if (bool.TryParse(values[p.Name], out bool boolValue))
                                    sh.AddParameter(p.Name, p.DataType, boolValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.BigInt:
                                if (long.TryParse(values[p.Name], out long longValue))
                                    sh.AddParameter(p.Name, p.DataType, longValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.DateTime:
                                if (DateTime.TryParse(values[p.Name], out DateTime dateTimeValue))
                                    sh.AddParameter(p.Name, p.DataType, dateTimeValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.Float:
                                if (double.TryParse(values[p.Name], out double doubleValue))
                                    sh.AddParameter(p.Name, p.DataType, doubleValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.Int:
                                if (int.TryParse(values[p.Name], out int intValue))
                                    sh.AddParameter(p.Name, p.DataType, intValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.SmallInt:
                                if (short.TryParse(values[p.Name], out short shortValue))
                                    sh.AddParameter(p.Name, p.DataType, shortValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.TinyInt:
                                if (byte.TryParse(values[p.Name], out byte byteValue))
                                    sh.AddParameter(p.Name, p.DataType, byteValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.UniqueIdentifier:
                                if (Guid.TryParse(values[p.Name], out Guid guidValue))
                                    sh.AddParameter(p.Name, p.DataType, guidValue);
                                else
                                    parseError = true;
                                break;
                            case SqlDbType.Decimal:
                                if (decimal.TryParse(values[p.Name], out decimal decimalValue))
                                    sh.AddParameter(p.Name, p.DataType, decimalValue);
                                else
                                    parseError = true;
                                break;
                        }
                        if (parameterCount == sh.ParameterCount)
                        {
                            if (parseError)
                                errors.AppendLine($"Can't parse '{p.Name}' to '{p.DataType}'.");
                            parseErrorParameters.Add(p.Name);
                            sh.AddParameter(p.Name, SqlDbType.NVarChar, values[p.Name], ParameterDirection.Input, true, true);
                            requestValid = false;
                        }
                    }
                }
                var ignoreParameters = new List<string> { "methodGroup", "methodName", "GroupName", "MethodName", "Api" };
                foreach (var requestParameter in values.Keys)
                    if (!sh.ContainsParameter(requestParameter) && !ignoreParameters.Contains(requestParameter) && !parseErrorParameters.Contains(requestParameter))
                    {
                        sh.AddParameter(requestParameter.Replace("'", "''"), SqlDbType.NVarChar, values[requestParameter], ParameterDirection.Input, true, true);
                        errors.AppendLine($"Invalid parameter '{requestParameter}'.");
                    }
                return sh.ReadResponse<T>(requestValid, errors.ToString());
            }
        }

        private static SqlDbType GetSqlType(string typeName)
        {
            switch (typeName)
            {
                case "bigint":
                    return SqlDbType.BigInt;
                case "bit":
                    return SqlDbType.Bit;
                case "char":
                    return SqlDbType.Char;
                case "datetime":
                    return SqlDbType.DateTime;
                case "float":
                    return SqlDbType.Float;
                case "int":
                    return SqlDbType.Int;
                case "nchar":
                    return SqlDbType.NChar;
                case "ntext":
                    return SqlDbType.NText;
                case "nvarchar":
                    return SqlDbType.NVarChar;
                case "smallint":
                    return SqlDbType.SmallInt;
                case "tinyint":
                    return SqlDbType.TinyInt;
                case "uniqueidentifier":
                    return SqlDbType.UniqueIdentifier;
                case "varchar":
                    return SqlDbType.VarChar;
                case "xml":
                    return SqlDbType.Xml;
                case "decimal":
                    return SqlDbType.Decimal;
            }
            throw new ArgumentException(typeName);
        }
    }
}
