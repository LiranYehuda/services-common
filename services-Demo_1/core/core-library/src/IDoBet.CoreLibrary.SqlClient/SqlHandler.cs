﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using IDoBet.CoreLibrary.Logic.Model.Response;
using IDoBet.CoreLibrary.Logic.Model.Enums;
using IDoBet.CoreLibrary.Logic.Model.Filter;
using IDoBet.CoreLibrary.Logic.Model.Request;
using IDoBet.CoreLibrary.Logic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace IDoBet.CoreLibrary.SqlClient
{
    public class SqlHandler : IDisposable
    {
        private static bool IsWriteSpToDb => bool.Parse(Configuration.GetValue(Configuration.SystemBrandId, eModule.Core, "IsWriteSpToDb")?.Value ?? "false");        

        private SqlConnection cn;
        private SqlCommand cm;
        private SqlCommand cmLog;
        private SqlDataReader dr;
        private Dictionary<string, DataTable> tables;
        private Dictionary<string, DataRow> newRows;
        private Dictionary<string, string> tableSqlTypes;
        private string activeTable = null;

        internal static readonly HashSet<SqlDbType> SqlDbTypeText = new HashSet<SqlDbType> { SqlDbType.Char, SqlDbType.NChar, SqlDbType.NVarChar, SqlDbType.NText, SqlDbType.Text, SqlDbType.UniqueIdentifier, SqlDbType.VarChar, SqlDbType.Xml };

        public int ParameterCount => cm.Parameters.Count;
        public bool ContainsParameter(string name) => cm.Parameters.Contains(name);

        static SqlHandler()
        {
        }

        public SqlHandler(string connectionString, string commandText)
        {
            cn = new SqlConnection(connectionString);
            cm = new SqlCommand(commandText, cn) { CommandType = CommandType.StoredProcedure };
            cmLog = new SqlCommand(commandText, cn) { CommandType = CommandType.StoredProcedure };
        }

        public void AddOutputParameter(string name, SqlDbType type, int? size = null)
        {
            var param = new SqlParameter(name, type) { Direction = ParameterDirection.Output };
            if (size != null)
                param.Size = size.Value;
            cm.Parameters.Add(param);
        }
        public object GetParameterValue(string name) =>
            cm.Parameters[name].Value == DBNull.Value ? null : cm.Parameters[name].Value;

        public void AddToken(string token)
        {
            AddParameter("Token", SqlDbType.NVarChar, token);
        }
        public void AddDefaultParameters(BaseRequest request, bool withToken = true)
        {
            if (withToken)
                if (!string.IsNullOrWhiteSpace(request.Token))
                    AddParameter("Token", SqlDbType.NVarChar, request.Token);
            AddParameter("Language", SqlDbType.NVarChar, request.Language);
            AddParameter("BrandId", SqlDbType.Int, request.BrandId);
            AddParameter("ChannelId", SqlDbType.Int, request.ChannelId);
            AddParameter("ExtraData", SqlDbType.NVarChar, request.ExtraData?.ToString());
            AddParameter("Terminal", SqlDbType.NVarChar, request.Terminal);
        }

        public void SelectTable(string name) => activeTable = name;

        public void AddParameter<T>(string name, SqlDbType type, T value, ParameterDirection direction = ParameterDirection.Input, bool prepareText = true, bool logOnly = false)
        {
            var param = new SqlParameter(name, type) { Direction = direction };
            var paramLog = new SqlParameter(name, type) { Direction = direction };
            if (value == null)
            {
                param.Value = DBNull.Value;
                paramLog.Value = DBNull.Value;
            }
            else
            {
                //if (prepareText && SqlDbTypeText.Contains(type))
                //    param.Value = value.ToString().Replace("'", "''");
                //else
                param.Value = value;
                paramLog.Value = value;
            }
            if (!logOnly)
                cm.Parameters.Add(param);
            cmLog.Parameters.Add(paramLog);
        }

        public void AddPageFilter(PageFilter pageFilter)
        {
            AddParameter("Skip", SqlDbType.Int, pageFilter?.EntryStart);
            AddParameter("Take", SqlDbType.Int, pageFilter?.EntryCount);
            AddParameter("OrderByParam", SqlDbType.NVarChar, pageFilter?.SortBy);
            AddParameter("OrderByDesc", SqlDbType.Bit, pageFilter?.SortDesc);
        }

        public void AddTable(string name, string sqlType, List<Tuple<string, Type>> columns)
        {
            if (tables == null)
            {
                tables = new Dictionary<string, DataTable>();
                tableSqlTypes = new Dictionary<string, string>();
            }
            var table = new DataTable();
            foreach (var column in columns)
                table.Columns.Add(column.Item1, column.Item2);
            tables[name] = table;
            activeTable = name;
            tableSqlTypes[name] = sqlType;
        }

        public void AddRowValue<T>(string name, T value)
        {
            DataRow newRow;
            if (newRows == null)
                newRows = new Dictionary<string, DataRow>();
            if (!newRows.ContainsKey(activeTable))
            {
                newRow = tables[activeTable].NewRow();
                newRows[activeTable] = newRow;
            }
            else
                newRow = newRows[activeTable];
            if (value == null)
                newRow[name] = DBNull.Value;
            else
                newRow[name] = value;
        }

        public void NextRow()
        {
            tables[activeTable].Rows.Add(newRows[activeTable]);
            newRows.Remove(activeTable);
        }

        private void AddTables()
        {
            if (tables != null)
                foreach (var table in tables)
                    cm.Parameters.Add(new SqlParameter(table.Key, SqlDbType.Structured)
                    {
                        TypeName = tableSqlTypes[table.Key],
                        Value = table.Value
                    });
        }

        public void ExecuteNonQuery()
        {
            cn.Open();
            AddTables();
            cm.ExecuteNonQuery();
        }

        public T ExecuteNonQueryWithReturnValue<T>()
        {
            var param = new SqlParameter("SqlResult", SqlDbType.Int)
            {
                Direction = ParameterDirection.ReturnValue
            };
            cm.Parameters.Add(param);
            ExecuteNonQuery();
            return (T)param.Value;
        }

        public bool Read()
        {
            if (dr == null)
            {
                cn.Open();
                AddTables();
                dr = cm.ExecuteReader();
            }
            return dr.Read();
        }

        public void CloseReader() => dr.Close();

        public DataTable ReadDataTable()
        {
            DataTable result = new DataTable();
            cn.Open();
            AddTables();
            using (var da = new SqlDataAdapter(cm))
            {
                da.Fill(result);
            }
            return result;
        }

        public (BaseResponse result, DataSet resultData) ReadResponse(bool isValid = true, string error = null) => ReadResponse<BaseResponse>(isValid, error);
        public (T result, DataSet resultData) ReadResponse<T>(bool isValid = true, string error = null) where T : BaseResponse, new()
        {
            WriteSPToDB(cmLog, cn.ConnectionString, isValid);
            var result = BaseResponse.EmptyResponse<T>();
            if (!isValid)
                if (error == null)
                    return (result, null);
                else
                    return (BaseResponse.SqlErrorResponse<T>(error), null);

            var data = GetDataSet(true, "[dbo].[spSettleBets]");
            if (data.Item2 != null)
                return (BaseResponse.SqlErrorResponse<T>(data.Item2.Message), null);
            var dataSet = data.Item1;
            if (dataSet.Tables.Count == 0)
                return (result, null);

            result.SetResult(dataSet.Tables[0]);
            return (result, dataSet);
            //                 result.Data = Convert(dataSet);

        }


        //public (T result, DataSet resultData) ReadResponse<T>(bool isValid = true, string error = null) where T : BaseResponse
        //{
        //    WriteSPToDB(cmLog, cn.ConnectionString, isValid);
        //    var result = BaseResponse.EmptyResponse<T>();
        //    if (!isValid)
        //        if (error == null)
        //            return (result, null);
        //        else
        //            return (BaseResponse.SqlErrorResponse<T>(error), null);
        //    cn.Open();
        //    AddTables();
        //    if (cm.CommandText == "[dbo].[spSettleBets]")
        //    {
        //        cm.CommandTimeout = 0;
        //    }
        //    var dataSet = new DataSet();
        //    using (var da = new SqlDataAdapter(cm))
        //    {
        //        try
        //        {
        //            da.Fill(dataSet);
        //        }
        //        catch (SqlException ex)
        //        {
        //            return (BaseResponse.SqlErrorResponse<T>(ex.Message), null);
        //        }
        //        if (dataSet.Tables.Count == 0)
        //            return (result, null);
        //        result.SetResult(dataSet.Tables[0]);
        //    }
        //    return (result, dataSet);
        //    //                 result.Data = Convert(dataSet);

        //}

        public (DataSet,Exception) GetDataSet(bool? setTimeoutZero = false,string timeoutZeroProc = null)
        {
            cn.Open();
            AddTables();
            if (setTimeoutZero.HasValue && setTimeoutZero.Value && cm.CommandText == timeoutZeroProc)            
            {
                cm.CommandTimeout = 0;
            }
            var dataSet = new DataSet();
            using (var da = new SqlDataAdapter(cm))
            {
                try
                {
                    da.Fill(dataSet);
                }
                catch (SqlException ex)
                {                    
                    return (null, ex);
                }
            }

            return (dataSet, null);
        } 

        public void NextResult()
        {
            dr.NextResult();
        }

        public T Get<T>(string name)
        {
            if (dr[name] == DBNull.Value)
                return default;
            else
                return (T)dr[name];
        }
        public void Dispose()
        {
            if (tables != null)
                foreach (var table in tables)
                    table.Value.Dispose();
            cm.Dispose();
            cn.Dispose();
        }

        public static string ListToString<T>(List<T> src) =>
            src == null ? null : string.Join(",", src.ToArray());

        //Trace All Income requests
        private static void WriteSPToDB(SqlCommand cmd, string connectionString, bool isExecute = true)
        {
            if (IsWriteSpToDb)
            {
                using (var sh = new SqlHandler(connectionString, "dbo.WriteSPCommands"))
                {
                    sh.AddParameter("spName", SqlDbType.VarChar, cmd.CommandText);
                    sh.AddParameter("searchKey", SqlDbType.VarChar, "");
                    sh.AddParameter("command", SqlDbType.VarChar, LogCommand(cmd), ParameterDirection.Input, false);
                    sh.AddParameter("isExecute", SqlDbType.Bit, isExecute);
                    sh.ExecuteNonQuery();
                }
            }
        }

        private static string LogCommand(SqlCommand cm)
        {
            IEnumerable<string> GetParameter(SqlParameterCollection c)
            {
                foreach (SqlParameter p in c)
                    if (p.Value == DBNull.Value)
                        yield return $"@{p.ParameterName}=NULL";
                    else if (SqlDbTypeText.Contains(p.SqlDbType))
                        yield return $"@{p.ParameterName}='{p.Value.ToString().Replace("'", "''")}'";
                    else
                        yield return $"@{p.ParameterName}={p.Value}";
            }
            if (cm.CommandType != CommandType.StoredProcedure)
                return "Logging only stored procedures.";
            var commandText = cm.CommandText;
            if (!commandText.Contains("["))
                commandText = $"[{commandText.Replace(".", "].[")}]";
            return $"EXEC {commandText} {string.Join(",", GetParameter(cm.Parameters))}";
        }

    }
}
