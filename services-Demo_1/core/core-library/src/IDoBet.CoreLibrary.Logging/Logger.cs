﻿using System;
using IDoBet.CoreLibrary.Logging.Model.Enums;

namespace IDoBet.CoreLibrary.Logging
{
    public static class Logger
    {
        private static readonly log4net.ILog Log;
        static Logger()
        {
            var repositoryAssembly = System.Reflection.Assembly.GetEntryAssembly();
            var logRepository = log4net.LogManager.GetRepository(repositoryAssembly);
            log4net.Config.XmlConfigurator.Configure(logRepository, new System.IO.FileInfo("log4net.config"));
            Log = log4net.LogManager.GetLogger(repositoryAssembly, "Logger");
        }
        public static void TestLog(eLogLevel logLevel, string message)
        {
            Log.Debug(message);
        }
        //public static void WriteLog(eLogLevel logLevel, string serviceName, string className, string methodName, string logMessage, Exception ex)
        //    => throw new NotImplementedException();
        //public static void WriteLog(eLogLevel logLevel, string serviceName, string className, string methodName, string logMessage)
        //    => throw new NotImplementedException();
        //public static void WriteLog(eLogLevel logLevel, string logMessage, int frameIndex = 1)
        //    => throw new NotImplementedException();
        //public static void WriteLog(eLogLevel logLevel, string logMessage, Exception ex, int frameIndex = 1)
        //    => throw new NotImplementedException();
        //public static void WriteLog(Exception ex, int frameIndex = 1)
        //    => throw new NotImplementedException();

        //public static void WriteLog(eLogLevel logLevel, string serviceName, string className, string methodName, string logMessage, Exception ex,
        //int? brandId = null, BaseLogRequest request = null, BaseLogResponse response = null)

        private static void WriteLog(eLogLevel level, string message, Exception ex = null)
        {
            switch (level)
            {
                case eLogLevel.Error:
                    Log.Error(message, ex);
                    break;
                case eLogLevel.Fatal:
                    Log.Fatal(message, ex);
                    break;
                case eLogLevel.Info:
                    Log.Info(message, ex);
                    break;
                case eLogLevel.Warn:
                    Log.Warn(message, ex);
                    break;
                case eLogLevel.Debug:
                    Log.Debug(message, ex);
                    break;
                default:
                    throw new NotImplementedException("eLogLevel");
            }
        }

        public static void WriteLog(eLogLevel logLevel, string serviceName, string className, string methodName, string logMessage, Exception ex,
        int? brandId = null) // , object request = null, object response = null
        {
            WriteLog(logLevel, $"{serviceName}/{className}/{methodName}[{brandId}]: {logMessage}", ex);
            return;            
        }

        public static void WriteLog(eLogLevel logLevel, string logMessage, Exception ex = null, int? brandId = null, object request = null, object response = null, int frameIndex = 1)
        {
            WriteLog(logLevel, logMessage, ex);
            return;
        }
        /*
        public static void WriteLog(eLogLevel logLevel, string logMessage, Exception ex = null, int? brandId = null, BaseLogRequest request = null, BaseLogResponse response = null, int frameIndex = 1)
        {
            string methodName = string.Empty;
            string className = string.Empty;
            string namespaceName = string.Empty;
            string assemblyName = string.Empty;
            try
            {
                MethodBase method = (new StackTrace()).GetFrame(frameIndex).GetMethod();
                methodName = method.Name;
                className = method.ReflectedType.Name;
                namespaceName = method.DeclaringType.FullName;
                assemblyName = method.DeclaringType.Assembly.GetName().Name;
            }
            catch { }
            WriteLog(logLevel, assemblyName, namespaceName, methodName, logMessage, ex, brandId, request, response);
        }
         */
    }
}
