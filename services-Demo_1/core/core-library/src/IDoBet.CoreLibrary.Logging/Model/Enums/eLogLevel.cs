﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDoBet.CoreLibrary.Logging.Model.Enums
{
    public enum eLogLevel
    {
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }
}
